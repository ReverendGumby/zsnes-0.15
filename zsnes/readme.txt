If you need help to compile zsnes, read install.txt.
If you need help to browse the sources, read srcinfo.txt.

For more information about zsnes development, check http://zsnes.sourceforge.net
The official website is at http://www.zsnes.com