;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM DosExit,UpdateDevices,InitSPC,Makemode7Table,MusicRelVol,MusicVol
EXTSYM makesprprtable,romloadskip,start65816,startdebugger,SfxR0
EXTSYM MovieProcessing
EXTSYM MovieFileHand,filefound,inittable,SA1inittable
EXTSYM MessageOn,Msgptr,MsgCount,sndrot,GenerateBank0Table,SnowTimer
EXTSYM inittableb,inittablec,FPUCopy,newgfx16b,EndMessage
EXTSYM Get_Key,CNetType
EXTSYM InitDSP
EXTSYM SPCDisable,osm2dis,CurRecv
EXTSYM SnowData,SnowVelDist
EXTSYM cvidmode, newengen, GUI16VID
EXTSYM NewEngEnForce
EXTSYM PrintChar
EXTSYM mode7tab
EXTSYM per2exec
EXTSYM MovieCounter
EXTSYM chaton
EXTSYM JoyRead,JoyReadB,JoyReadControl,joy4218,joy4219,joy4218j,joy4219j
EXTSYM joy421A,joy421B,joy421Aj,joy421Bj,pressed
EXTSYM pl3Ak,pl3Bk,pl3Lk,pl3Rk,pl3Xk,pl1p209,pl2p209,pl3p209,pl4p209
EXTSYM pl3Yk,pl3contrl,pl3downk,pl3leftk,pl3rightk,pl3selk,pl3startk
EXTSYM pl3upk,pl4Ak,pl4Bk,pl4Lk,pl4Rk,pl4Xk,pl4Yk,pl4contrl,pl4downk
EXTSYM pl4leftk,pl4rightk,pl4selk,pl4startk,pl4upk,mousebuttons,mousexdir
EXTSYM pl5Ak,pl5Bk,pl5Lk,pl5Rk,pl5Xk,pl5Yk,pl5contrl,pl5downk
EXTSYM pl5leftk,pl5rightk,pl5selk,pl5startk,pl5upk
EXTSYM mouseydir,mousexpos,mouseypos,snesmouse,processmouse,ssautosw
EXTSYM GUIDelayB,pl12s34
EXTSYM Turbo30hz,RepeatFrame,nojoystickpoll
EXTSYM NumComboLocl,ComboBlHeader,ComboHeader,CombinDataLocl
EXTSYM CombinDataGlob,NumCombo,GUIComboGameSpec
EXTSYM mousexloc,mouseyloc
EXTSYM extlatch
EXTSYM BackState
EXTSYM FIRTAPVal0,FIRTAPVal1,FIRTAPVal2,FIRTAPVal3,FIRTAPVal4
EXTSYM FIRTAPVal5,FIRTAPVal6,FIRTAPVal7,JoyAPos,JoyBPos
EXTSYM NMIEnab,SPCROM,VIRQLoc,coladdb,coladdg,coladdr,doirqnext
EXTSYM echobuf,forceblnk,nmiprevaddrh,nmiprevaddrl,nmiprevline
EXTSYM nmirept,nmistatus,opexec268,opexec268b,opexec268cph
EXTSYM opexec358,opexec358b,opexec358cph,spcextraram
EXTSYM prevoamptr,reg1read,reg2read,reg3read
EXTSYM reg4read,resolutn,romdata,scrndis,spcBuffera,spcP,spcRam
EXTSYM tableD,timeron,vidbright,DSPMem,OldGfxMode2
EXTSYM SPC700read,SPC700write,GUIDoReset,spc700read
EXTSYM InitC4,SA1Reset,SetAddressingModesSA1,SetAddressingModes,SPC7110init
EXTSYM RTCinit
EXTSYM memaccessspc7110r8,memaccessspc7110r16,memaccessspc7110w8
EXTSYM memaccessspc7110w16
EXTSYM ram7f,snesmap2,snesmmap,sram,MultiTap
EXTSYM memaccessbankr848mb,memaccessbankr1648mb
EXTSYM cpuover,execloop
EXTSYM execloopn,execloopns,execloops
;EXTSYM PHsizeofexecloop,PHsizeofexecloopn,PHsizeofexecloopns
;EXTSYM PHsizeofexecloops
EXTSYM curexecstate
EXTSYM debugdisble,vidbuffer
EXTSYM Sup16mbit,Sup48mbit,debugbufa,pal16b,pal16bcl,pal16bclha
EXTSYM pal16bxcl,ram7fa,regptra,regptwa,srama,vidmemch2,vidmemch4
EXTSYM vidmemch8,vcache2b,vcache4b,vcache8b,vram,wramdata
EXTSYM wramdataa
EXTSYM fname,fnames,GetCurDir
EXTSYM GUIcurrentdir,extractzip,PrintStr
EXTSYM GUIsmcfind,GUIsfcfind,GUIswcfind,GUIfigfind,GUIfind058,GUIfind078,GUIfindBIN
EXTSYM GUIfindUSA,GUIfindJAP,GUIfindZIP,GUIfind1,DTALoc,GUIfindall,ZipError
EXTSYM spc7110romptr,allocspc7110
EXTSYM SRAMDir,SRAMDrive,fnamest,statefileloc
EXTSYM ForcePal,InitDir,InitDrive,enterpress,frameskip
EXTSYM gotoroot,headdata,printnum,romispal
EXTSYM InitFxTables,SFXSRAM,SfxR1,SfxR2,SfxSCMR,SfxSFR,finterleave
EXTSYM initregr,initregw,memtabler16,DSP1Read16b3F,memaccessbankr16
EXTSYM memtabler8,DSP1Read8b3F,memaccessbankr8,memtablew16,DSP1Write16b
EXTSYM memaccessbankw16,memtablew8,DSP1Write8b,memaccessbankw8,DSP1Write16b3F
EXTSYM regaccessbankr16,regaccessbankr8,regaccessbankw16,regaccessbankw8
EXTSYM sfxaccessbankr16,sfxaccessbankr16b,sfxaccessbankr16c,DSP1Write8b3F
EXTSYM sfxaccessbankr16d,sfxaccessbankr8,sfxaccessbankr8b,sfxaccessbankr8c
EXTSYM sfxaccessbankr8d,sfxaccessbankw16,sfxaccessbankw16b
EXTSYM sfxaccessbankw16c,sfxaccessbankw16d,sfxaccessbankw8
EXTSYM sfxaccessbankw8b,sfxaccessbankw8c,sfxaccessbankw8d,sfxramdata
EXTSYM sramaccessbankr16,sramaccessbankr16s,sramaccessbankr8
EXTSYM sramaccessbankr8s,sramaccessbankw16,sramaccessbankw16s
EXTSYM sramaccessbankw8,sramaccessbankw8s,GenerateBank0TableSA1
EXTERN sz_execloop







; Initiation

NEWSYM init                                                             ;0000174B
    mov eax,ds
    mov es,eax

    call clearmem
    ; SPC Init
    call copyexecloop
    call changeexecloop
    ; SNES Init
    call loadfile
    call showinfo

    call UpdateDevices
    call InitSPC
    call init65816
    call initsomething
    call initregr
    call initregw
    call inittable
    call initsnes

    cmp byte[debugger],0
    je near start65816
    jmp startdebugger

EndMessageB

; global variables



; Controls
NEWSYM numjoy,    db 0   ; number of joysticks (1 = 1, 2 = 2)           ;000017A2
; 0 = Disable, 1 = Keyboard, 2 = Joystick, 3 = Gamepad
NEWSYM pl1contrl, db 1   ; player 1 device                              ;000017A3
NEWSYM pl1keya,   db 0                                                  ;000017A4
NEWSYM pl1keyb,   db 0
NEWSYM pl1selk,   db 54  ; 1SELECT = SHIFT
NEWSYM pl1startk, db 28  ; 1START = ENTER
NEWSYM pl1upk,    db 72  ; 1UP = up 
NEWSYM pl1downk,  db 80  ; 1DOWN = down 
NEWSYM pl1leftk,  db 75  ; 1LEFT = left 
NEWSYM pl1rightk, db 77  ; 1RIGHT = right 
NEWSYM pl1Xk,     db 82  ; 1X = INS
NEWSYM pl1Ak,     db 71  ; 1A = HOME
NEWSYM pl1Lk,     db 73  ; 1L = PAGE UP
NEWSYM pl1Yk,     db 83  ; 1Y = DELETE
NEWSYM pl1Bk,     db 79  ; 1B = END
NEWSYM pl1Rk,     db 81  ; 1R = PAGE DOWN
NEWSYM pl2contrl, db 0   ; player 2 device                              ;000017B2
NEWSYM pl2keya,   db 0                                                  ;000017B3
NEWSYM pl2keyb,   db 0
NEWSYM pl2selk,   db 56   ; 2SELECT = alt
NEWSYM pl2startk, db 29   ; 2START = ctrl
NEWSYM pl2upk,    db 37   ; 2UP = k
NEWSYM pl2downk,  db 50   ; 2DOWN = m
NEWSYM pl2leftk,  db 49   ; 2LEFT = n
NEWSYM pl2rightk, db 51   ; 2RIGHT = ,
NEWSYM pl2Xk,     db 31   ; 2X = S
NEWSYM pl2Ak,     db 32   ; 2A = D
NEWSYM pl2Lk,     db 33   ; 2L = F
NEWSYM pl2Yk,     db 44   ; 2Y = Z
NEWSYM pl2Bk,     db 45   ; 2B = X
NEWSYM pl2Rk,     db 46   ; 2R = C                                      ;000017C0

NEWSYM ramsize, dw 0    ; RAM size in bytes                             ;000017C1
NEWSYM ramsizeand, dw 0    ; RAM size in bytes (used to and)            ;000017C3
NEWSYM romtype, db 0    ; ROM type in bytes                             ;000017C5
NEWSYM resetv,  dw 0    ; reset vector                                  ;000017C6
NEWSYM abortv,  dw 0    ; abort vector                                  ;000017C8
NEWSYM nmiv,    dw 0    ; nmi vector                                    ;000017CA
NEWSYM irqv,    dw 0    ; irq vector                                    ;000017CC
NEWSYM brkv,    dw 0    ; brk vector                                    ;000017CE
NEWSYM copv,    dw 0    ; cop vector                                    ;000017D0
NEWSYM abortv8, dw 0    ; abort vector emulation mode                   ;000017D2
NEWSYM nmiv8,   dw 0    ; nmi vector emulation mode                     ;000017D4
NEWSYM irqv8,   dw 0    ; irq vector emulation mode                     ;000017D6
NEWSYM brkv8,   dw 0    ; brk vector emulation mode                     ;000017D8
NEWSYM copv8,   dw 0    ; cop vector emulation mode                     ;000017DA
NEWSYM cycpb268, db 147                                                 ;000017DC
NEWSYM cycpb358, db 170
NEWSYM cycpbl,  db 147  ; percentage left of CPU/SPC to run  (3.58 = 175);000017DE
NEWSYM cycpblt, db 147  ; percentage of CPU/SPC to run
NEWSYM writeon, db 0    ; Write enable/disable on snes rom memory
NEWSYM totlines, dw 261 ; total # of lines                              ;000017E1
NEWSYM soundon, db 0    ; Current sound enabled (1=enabled)
NEWSYM zsmesg,  db 'ZSNES Save State File V0.5',26                      ;000017E4
NEWSYM versn,   db 50   ; version #/100                                 ;000017FF
NEWSYM curcyc,  db 0    ; cycles left in scanline
NEWSYM curypos, dw 0    ; current y position                            ;00001801
NEWSYM cacheud, db 2    ; update cache every ? frames
NEWSYM ccud,    db 0    ; current cache increment
NEWSYM intrset, db 0    ; interrupt set                                 ;00001805
NEWSYM cycpl,   db 0    ; cycles per scanline
NEWSYM cycphb,  db 0    ; cycles per hblank
NEWSYM spcon,   db 0    ; SPC Enable (1=enabled)                        ;00001808
NEWSYM stackand, dw 01FFh ; value to and stack to keep it from going to the wrong area
NEWSYM stackor,  dw 0100h ; value to or stack to keep it from going to the wrong area

; 65816 registers

NEWSYM xa,       dw 0                                                   ;0000180D
NEWSYM xdb,      db 0                                                   ;0000180F
NEWSYM xpb,      db 0                                                   ;00001810
NEWSYM xs,       dw 0                                                   ;00001811
NEWSYM xd,       dw 0                                                   ;00001813
NEWSYM xx,       dw 0                                                   ;00001815
NEWSYM xy,       dw 0                                                   ;00001817
NEWSYM xp,       db 0                                                   ;00001819
NEWSYM xe,       db 0                                                   ;0000181A
NEWSYM xpc,      dw 0                                                   ;0000181B
NEWSYM xirqb,    db 0           ; which bank the irqs start at          ;0000181D
NEWSYM debugger, db 0              ; Start with debugger (1=yes,0=no)   ;0000181E
NEWSYM Curtableaddr,  dd 0                 ; Current table address      ;0000181F
NEWSYM curnmi,   db 0           ; if in NMI(1) or not(0)                ;00001823


; SNES memory map ROM locations

NEWSYM cpuoverptr, dd 0                 ; pointer to cpuover            ;00001824
snesmmap times 256 dd 0         ; addresses 8000-FFFF                   ;00001828
snesmap2 times 256 dd 0         ; addresses 0000-7FFF                   ;00001C28
NEWSYM exeloopa, times 128 dw 0 ; execloop should be stored here        ;00002028
sizeofexeloopa equ $-exeloopa
NEWSYM exeloopb, times 128 dw 0 ; execloopns should be stored here      ;00002128
sizeofexeloopb equ $-exeloopb
NEWSYM exeloopc, times 64 dw 8000h ; execloops should be stored here    ;00002228
                 times 64 dw 0
NEWSYM exeloopd, times 64 dw 8000h ; execloopn should be stored here    ;00002328
                 times 64 dw 0


;*******************************************************
; Read Input Device            Reads from Keyboard, etc.
;*******************************************************

%macro PlayerDeviceHelp 3
    mov al,[%1]
    cmp byte[pressed+eax],0
    je %%yes
    or byte[%2],%3
%%yes
%endmacro

NEWSYM ReadInputDevice
    xor eax,eax                                                         ;00002428
    ; Process Data
    mov byte[pl1keya],0
    mov byte[pl1keyb],0
    ; Get Player1 input device
    PlayerDeviceHelp pl1Bk    ,pl1keya,80h
    PlayerDeviceHelp pl1Yk    ,pl1keya,40h
    PlayerDeviceHelp pl1selk  ,pl1keya,20h
    PlayerDeviceHelp pl1startk,pl1keya,10h
    PlayerDeviceHelp pl1upk   ,pl1keya,08h
    PlayerDeviceHelp pl1downk ,pl1keya,04h
    PlayerDeviceHelp pl1leftk ,pl1keya,02h
    PlayerDeviceHelp pl1rightk,pl1keya,01h
    PlayerDeviceHelp pl1Ak    ,pl1keyb,80h
    PlayerDeviceHelp pl1Xk    ,pl1keyb,40h
    PlayerDeviceHelp pl1Lk    ,pl1keyb,20h
    PlayerDeviceHelp pl1Rk    ,pl1keyb,10h

    cmp byte[pl1contrl],3       ;gamepad
    jz .pl1joy
    cmp byte[pl1contrl],2       ;joystick
    jz .pl1joy
    jmp .pl1done
.pl1joy
    call JoyRead
    cmp byte[numjoy],1
    jnz short .pl1gp
    cmp byte[pl1contrl],3
    jnz short .pl1gp
    mov al,[joy4218]
    or [pl1keyb],al
    mov al,[joy4219]
    or [pl1keya],al
    jmp .pl1done
.pl1gp
    mov al,[joy4218j]
    or [pl1keyb],al
    mov al,[joy4219j]
    or [pl1keya],al
.pl1done

    cmp byte[pl2contrl],0
    jz .pl2done

    xor eax,eax
    ; Process Data
    mov byte[pl2keya],0
    mov byte[pl2keyb],0
    ; Get Player2 input device
    PlayerDeviceHelp pl2Bk    ,pl2keya,80h
    PlayerDeviceHelp pl2Yk    ,pl2keya,40h
    PlayerDeviceHelp pl2selk  ,pl2keya,20h
    PlayerDeviceHelp pl2startk,pl2keya,10h
    PlayerDeviceHelp pl2upk   ,pl2keya,08h
    PlayerDeviceHelp pl2downk ,pl2keya,04h
    PlayerDeviceHelp pl2leftk ,pl2keya,02h
    PlayerDeviceHelp pl2rightk,pl2keya,01h
    PlayerDeviceHelp pl2Ak    ,pl2keyb,80h
    PlayerDeviceHelp pl2Xk    ,pl2keyb,40h
    PlayerDeviceHelp pl2Lk    ,pl2keyb,20h
    PlayerDeviceHelp pl2Rk    ,pl2keyb,10h

    cmp byte[numjoy],2
    jnz .not2
    call JoyReadControl
    mov al,[joy421Aj]
    or [pl2keyb],al
    mov al,[joy421Bj]
    or [pl2keya],al
    jmp .pl2done

.not2
    cmp byte[numjoy],1
    jz .pl2done

    call JoyReadB
    cmp byte[numjoy],1
    jnz short .pl2gp
    cmp byte[pl2contrl],3
    jnz short .pl2gp
    mov al,[joy421A]
    or [pl2keyb],al
    mov al,[joy421B]
    or [pl2keya],al
    jmp .pl2done
.pl2gp
    mov al,[joy421Aj]
    or [pl2keyb],al
    mov al,[joy421Bj]
    or [pl2keya],al
.pl2done

    ret

;*******************************************************
; Init 65816                   Initializes the Registers
;*******************************************************

NEWSYM init65816                                                        ;00002727
    mov word[xa],0
    mov byte[xdb],0
    mov byte[xpb],0
    mov byte[xirqb],0
    mov word[xs],01FFh
    mov word[xd],0
    mov word[xx],0
    mov word[xy],0
    mov byte[xp],00110100b  ; NVMXDIZC

    mov byte[xe],1          ; E
    mov ax,[resetv]
    mov word[xpc],ax
    mov byte[intrset],0
    cmp byte[romtype],1
    je .nohirom
    mov byte[xpb],00h
    mov byte[xirqb],00h
.nohirom
    mov al,[opexec268]
    mov byte[cycpl],al      ; 2.68 Mhz  / 3.58 Mhz = 228
    mov byte[curcyc],al
    mov al,[opexec268cph]
    mov byte[cycphb],al     ; 2.68 Mhz  / 3.58 Mhz = 56
    mov byte[cycpbl],131        ; 3.58Mhz = 175
    mov byte[cycpblt],131
    mov word[curypos],0
    mov eax,tableD
    mov [Curtableaddr],eax
    mov byte[scrndis],00h
    mov word[stackand],01FFh
    mov word[stackor],0100h
    ret

;*******************************************************
; Init something
;*******************************************************

NEWSYM initsomething                                                    ;000027F5
  xor eax,eax
.loop1
  xor bl,bl
  test al,0FFh
  jnz .notFF
  or bl,2
.notFF
  test al,80h
  jz .pos
  or bl,80h
.pos
  mov [thing1+eax],bl
  inc al
  jnz .loop1
.loop3
  xor bl,bl
  test ax,0FFFFh
  jnz .notFFFF
  or bl,2
.notFFFF
  test ax,8000h
  jz .not15
  or bl,80h
.not15
  mov [thing2+eax],bl
  inc ax
  jnz .loop3
  ret

NEWSYM thing1, times 256 db 0                                           ;00002830
NEWSYM thing2, times 65536 db 0                                         ;00002930

;*******************************************************
; Init SNES                      Sets the pointers, etc.
;*******************************************************
; Set banks according to :
;   Banks 00-3F,80-BF : WRAM (0000h-7FFFh), ROM Data (8000h-FFFFh)
;   Banks 40-7F,C0-FF : ROM Data (0000h-FFFFh)
;   Bank  70-77       : SRAM (0000h-7FFFh)
;   Bank  7E          : WRAM (0000h-FFFFh)
;   Bank  7F          : ExtendRAM (0000h-FFFFh)

NEWSYM initsnes                                                         ;00012930
    cmp byte[romtype],1
    jne near .hirom

    ; set addresses 8000-FFFF
    ; set banks 00-3F (40h x 32KB ROM banks @ 8000h)
    mov edi,snesmmap
    mov eax,[romdata]
    sub eax,8000h
    mov ecx,40h
.loopa
    stosd
    add eax,8000h
    loop .loopa
    ; set banks 40-5F (20h x 64KB ROM banks @ 0000h)
    mov cx,20h
.loopb
    stosd
    add eax,10000h
    loop .loopb
    ; set banks 60-7F (20h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    mov cx,20h
.loopc
    stosd
    add eax,10000h
    loop .loopc
    ; set banks 80-BF (40h x 32KB ROM banks @ 8000h)
    mov eax,[romdata]
    sub eax,8000h
    mov cx,40h
.loopd
    stosd
    add eax,8000h
    loop .loopd
    ; set banks C0-DF (20h x 64KB ROM banks @ 0000h)
    mov cx,20h
.loope
    stosd
    add eax,10000h
    loop .loope
    ; set banks E0-FF (20h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    mov cx,20h
.loopf
    stosd
    add eax,10000h
    loop .loopf
    ; set addresses 0000-7FFF (01h x 32KB WRAM @ 0000h)
    ; set banks 00-3F
    mov edi,snesmap2
    mov eax,[wramdata]
    mov ecx,40h
.loopa2
    stosd
    loop .loopa2
    ; set banks 40-6F (30h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    add eax,200000h
    mov cx,20h
.loopb2
    stosd
    add eax,10000h
    loop .loopb2
    ; set banks 
    mov eax,[romdata]
    mov cx,20h
.loopb3
    stosd
    add eax,10000h
    loop .loopb3
    ; set banks 80-BF (01h x 32KB WRAM @ 0000h)
    mov eax,[wramdata]
    mov cx,40h
.loopc2
    stosd
    loop .loopc2
    ; set banks C0-FF (40h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    add eax,200000h
    mov cx,20h
.loopd2
    stosd
    add eax,10000h
    loop .loopd2
    ; set banks
    mov eax,[romdata]
    mov cx,20h
.loopd3
    stosd
    add eax,10000h
    loop .loopd3
    ; set bank 70
    mov eax,[sram]
    mov [snesmap2+70h*4],eax
    mov [snesmmap+70h*4],eax
    ; set bank 7E
    mov eax,[wramdata]
    mov [snesmmap+7Eh*4],eax
    mov [snesmap2+7Eh*4],eax
    ; set bank 7F
    mov eax,[ram7f]
    mov [snesmmap+7Fh*4],eax
    mov [snesmap2+7Fh*4],eax
    ret

.hirom
    ; set addresses 8000-FFFF
    ; set banks 00-3F (40h x 32KB ROM banks @ 8000h)
    mov edi,snesmmap
    mov eax,[romdata]
    mov ecx,40h
.loopab
    stosd
    add eax,10000h
    loop .loopab
    ; set banks 40-6F (30h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    mov ecx,40h
.loopbb
    stosd
    add eax,10000h
    loop .loopbb 
    ; set banks 80-BF (40h x 32KB ROM banks @ 8000h)
    mov eax,[romdata]
    mov ecx,40h
.loopcb
    stosd
    add eax,10000h
    loop .loopcb
    ; set banks C0-FF (40h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    mov ecx,40h
.loopdb
    stosd
    add eax,10000h
    loop .loopdb
    ; set addresses 0000-7FFF (01h x 32KB WRAM @ 0000h)
    ; set banks 00-3F
    mov edi,snesmap2
    mov eax,[wramdata]
    mov ecx,40h
.loopa2b
    stosd
    loop .loopa2b
    ; set banks 40-6F (30h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    mov cx,40h
.loopb2b
    stosd
    add eax,10000h
    loop .loopb2b
    ; set banks 80-BF (01h x 32KB WRAM @ 0000h)
    mov eax,[wramdata]
    mov cx,40h
.loopc2b
    stosd
    loop .loopc2b
    ; set banks C0-FF (40h x 64KB ROM banks @ 0000h)
    mov eax,[romdata]
    mov cx,40h
.loopd2b
    stosd
    add eax,10000h
    loop .loopd2b
    ; set bank 70
    mov eax,[sram]
    mov [snesmap2+70h*4],eax
    mov [snesmmap+70h*4],eax
    ; set bank 7E
    mov eax,[wramdata]
    mov [snesmmap+7Eh*4],eax
    mov [snesmap2+7Eh*4],eax
    ; set bank 7F
    mov eax,[ram7f]
    mov [snesmmap+7Fh*4],eax
    mov [snesmap2+7Fh*4],eax
    ret

;*******************************************************
; Copy execloop
;*******************************************************

NEWSYM copyexecloop                                                     ;00012AFE
    mov eax,016CC4h
    mov [cpuoverptr],eax

    mov ecx,sz_execloop
    mov esi,execloop
    mov edi,exeloopa
    cmp ecx,sizeofexeloopa
    ja near DosExit
.loopa
    mov al,[esi]
    mov [edi],al
    inc esi
    inc edi
    dec ecx
    jnz .loopa

    mov ecx,14h
    mov esi,execloopns
    mov edi,exeloopb
    cmp ecx,sizeofexeloopb
    ja near DosExit
.loopb
    mov al,[esi]
    mov [edi],al
    inc esi
    inc edi
    dec ecx
    jnz .loopb
    ret

;*******************************************************
; Change execloop
;*******************************************************
NEWSYM changeexecloop                                                   ;00012B51
    cmp byte[spcon],0
    jne .noprocloop
    mov ecx,14h
    mov esi,exeloopb
    mov edi,execloop
.loopb
    mov al,[esi]
    mov [edi],al
    inc esi
    inc edi
    dec ecx
    jnz .loopb
    ret

.noprocloop
    mov ecx,sz_execloop
    mov esi,exeloopa
    mov edi,execloop
.loopa
    mov al,[esi]
    mov [edi],al
    inc esi
    inc edi
    dec ecx
    jnz .loopa
    ret

;*******************************************************
; Clear Memory
;*******************************************************
;vidbuffer  resb 244736
;romdata    resb 4194304+32768
;wramdata   resb 65536
;ram7f      resb 65536
;vram       resb 65536
;sram       resb 32768
;debugbuf   resb 80000
;regptr     resb 49152
;regptw     resb 49152
;vcache2b   resb 262144
;vcache4b   resb 131072
;vcache8b   resb 65536

NEWSYM clearmem                                                         ;00012B8C
    mov ecx,5338240             ; sum of the above resb's
    mov edi,[vidbuffer]
    xor eax,eax
    rep stosb
    mov edi,[sram]
    mov eax,0FFFFFFFFh
    mov ecx,8192
    rep stosd
    ret

;*******************************************************
; Print Hexadecimal (16-bit/8-bit)
;*******************************************************
NEWSYM printhex                                                         ;00012BAE
    mov ecx,4
    xor ebx,ebx
.loopa
    mov bx,ax
    and bx,0F000h
    shr bx,12
    mov dl,[.hexdat+ebx]
    push ax
    mov ah,02h
    int 21h
    pop ax
    shl ax,4
    loop .loopa
    ret

.hexdat db '0123456789ABCDEF'

NEWSYM printhex8                                                        ;00012BE6
    mov cx,2
    xor ebx,ebx
.loopb
    mov bx,ax
    and bx,0F0h
    shr bx,4
    mov dl,[.hexdat+ebx]
    push ax
    mov ah,02h
    int 21h
    pop ax
    shl ax,4
    loop .loopb
    ret

.hexdat db '0123456789ABCDEF'

;*******************************************************
; Load File
;*******************************************************

NEWSYM loadfile                                                         ;00012C1D
    ; determine header size
    mov dword[.curfileofs],0
    mov byte[.first],1
    mov byte[.multfound],0
    ; open file
    mov ax,3D00h
    mov edx,fname+1
    int 21h
    jc near .failed
.nextfile
    cmp byte[.first],1
    je .nomul
    cmp byte[.multfound],0
    jne .nomul
    push eax
    push edx
    mov byte[.multfound],1
    mov edx,.mult
    mov ah,9         
    int 21h
    pop edx
    pop eax
.nomul
    mov bx,ax
    mov ah,3Fh
    mov ecx,4194304+32768
    sub ecx,[.curfileofs]
    jnc .nooverflow
    xor ecx,ecx
.nooverflow
    mov edx,[headdata]
    add edx,[.curfileofs]
    int 21h
    jc near .failed
    mov ecx,eax
    shr eax,15
    shl eax,15
    mov esi,[headdata]
    add esi,[.curfileofs]
    mov edi,[headdata]
    add edi,[.curfileofs]
    add [.curfileofs],eax
    sub ecx,eax
    add edi,ecx
    push esi
    mov esi,[headdata]
    add esi,7FDCh
    add esi,200h
    mov ax,[esi]
    xor ax,[esi+2]
    cmp ax,0FFFFh
    jz .nonoheader
    add esi,8000h
    mov ax,[esi]
    xor ax,[esi+2]
    cmp ax,0FFFFh
    jz .nonoheader
    pop esi
    jmp .next

.nonoheader
    pop esi
    mov edi,esi
    add edi,200h

.next
    mov cl,[edi]
    mov [esi],cl
    inc esi
    inc edi
    dec eax
    jnz .next
    mov ah,3Eh
    int 21h
    jc near .failed
    ; check for 2nd+ part of file
    mov edi,fname+1
    mov byte[.cchar],'\'
    ; get position of . or \ 
.nextsearch
    cmp byte[edi],0
    je .nomore
    cmp byte[edi],'.'
    jne .notdot
    mov byte[.cchar],'.'
    mov [.dotpos],edi
.notdot
    cmp byte[edi],'\'
    jne .notslash
    mov byte[.cchar],'\'
.notslash
    inc edi
    jmp .nextsearch
.nomore
    cmp byte[.cchar],'\'
    jne .noslashb
    mov [.dotpos],edi
.noslashb
    mov edi,[.dotpos]
    ; search for .1, .2, etc.
    cmp byte[edi],'.'
    jne .nonumer
    cmp byte[edi+1],'1'
    jb .nonumer
    cmp byte[edi+1],'8'
    ja .nonumer
    cmp byte[edi+2],0
    jne .nonumer
    inc byte[edi+1]
    xor ecx,ecx
    mov byte[.first],2
    mov ax,3D00h
    mov edx,fname+1
    int 21h
    jnc near .nextfile
    dec byte[edi+1]
.nonumer
    ; search for A,B,C, etc.
    cmp byte[.first],0
    je .yesgd
    cmp byte[edi-1],'A'
    je .yesgd
    cmp byte[edi-1],'a'
    je .yesgd
    jmp .nogdformat
.yesgd
    mov byte[.first],0
    inc byte[edi-1]
    mov ax,3D00h
    mov edx,fname+1
    int 21h
    jnc near .nextfile
    dec byte[edi-1]
.nogdformat
    ; open .srm file
    mov ax,3D00h
    mov edx,fnames+1
    int 21h
    jc .notexist
    mov bx,ax
    mov ah,3Fh
    mov ecx,32768
    mov edx,[sram]
    int 21h
    jc .failed
    mov ah,3Eh
    int 21h
    jc .failed
.notexist
    mov edx,.opened
    mov ah,9
    int 21h
    ret

.failed
    mov edx,.failop
    mov ah,9
    int 21h
    jmp DosExit

.multfound db 0                                                         ;00012E0B
.first db 0                                                             ;00012E0C
.cchar db 0                                                             ;00012E0D
.dotpos dd 0                                                            ;00012E0E
.curfileofs dd 0                                                        ;00012E12
.filehand dw 0                                                          ;00012E16
.failop   db 'Error opening file!',13,10,'$'                            ;00012E18
.opened db 'File opened successfully!',13,10,'$'                        ;00012E2E
.mult   db 'Multiple file format detected.',13,10,13,10,'$'             ;00012E4A

;*******************************************************
; Show Information
;*******************************************************
; 
; Maker Code = FFB0-FFB1
; Game Code = FFB2-FFB5
; Expansion RAM Size = FFBD (0=none, 1=16kbit, 3=64kbit, 5=256kbit,etc.
; Map Mode = FFD5 2.68-20h=map20h,21h=map21h,22h=reserved,23h=SA-1,25h=map25h
;                 3.58-30h=map20h,31h=map21h,35h=map25h,highspeed
; Rom Mask Version = FFDB
; FFD6 (ROM Type) : 0*=DSP,1*=SFX,2*=OBC1,3*=SA-1,E*-F*=other
;                   *3=ROM,*4=ROM+RAM,*5=ROM+RAM+BATTERY,*6=ROM+BATTERY
;                   F3=C4

; Convert to interleaved - If LoROM and offset 7FD5 contains 21h, then
;                          uninterleave

NEWSYM showinfo                                                         ;00012E6D
    mov edx,.filename
    mov ah,9
    int 21h
    xor ecx,ecx
    mov cl,[fname]
    mov esi,fname+1
    mov ah,2
.loopa
    lodsb
    mov dl,al
    int 21h
    loop .loopa
    mov edx,.ret
    mov ah,9         
    int 21h
    ; frameskip = ?
    mov edx,.frameskip
    mov ah,9         
    int 21h
    mov dl,[frameskip]
    test dl,0FFh
    jnz .yesfs
    mov edx,.auto
    mov ah,9         
    int 21h          
    jmp .skip
.yesfs
    mov ah,2
    add dl,47
    int 21h
    mov edx,.ret
    mov ah,9         
    int 21h          
.skip
    xor ax,ax
    ; Decrease standard % of execution by 15% to replace branch and 16bit
    ;   cycle deductions
    mov al,[opexec268]
    mov bl,[per2exec]
    mov bl,85
    mul bl
    mov bl,100
    div bl
    mov [opexec268],al
    xor ax,ax
    mov al,[opexec358]
    mov bl,[per2exec]
    mov bl,80
    mul bl
    mov bl,100
    div bl
    mov [opexec358],al
    xor ax,ax
    mov al,[opexec268cph]
    mov bl,[per2exec]
    mov bl,85
    mul bl
    mov bl,100
    div bl
    mov [opexec268cph],al
    xor ax,ax
    mov al,[opexec358cph]
    mov bl,[per2exec]
    mov bl,80
    mul bl
    mov bl,100
    div bl
    mov [opexec358cph],al

    ; debugger on/off
    mov edx,.debugon
    mov ah,9         
    int 21h
    mov al,[debugger]
    test al,0FFh
    jnz .debugron
    mov edx,.off
    mov ah,9
    int 21h
    jmp .skip2
.debugron
    mov edx,.on
    mov ah,9         
    int 21h          
.skip2
    mov edx,.ret
    mov ah,9         
    int 21h
    ; memory free
    mov edx,.memryfr
    int 21h
    mov ax,0500h
    mov edi,.memfree
    int 31h
    mov eax,[.memfree]
    call printnum
    mov edx,.ret
    mov ah,9
    int 21h
    int 21h

    ; ROM Information
    mov edx,.smcname
    mov ah,9
    int 21h

    ; determine whether hirom or lorom is used
    cmp byte[romtype],0
    jnz near .donecheck
    mov esi,[romdata]
    add esi,7FDCh
    lodsw
    mov bx,ax
    lodsw
    xor bx,ax
    cmp bx,0FFFFh
    jne .checkhirom
    mov byte[romtype],1
    jmp .donecheck
.checkhirom
    mov esi,[romdata]
    add esi,0FFDCh
    lodsw
    mov bx,ax
    lodsw
    xor bx,ax
    cmp bx,0FFFFh
    jne .cantcheck
    mov byte[romtype],2
    jmp .donecheck
.cantcheck
    ; check for a header with mostly letters or spaces
    mov esi,[romdata]
    add esi,32704
    mov ecx,21
    mov al,0
.nextletter
    cmp byte[esi],32
    je .yesletter
    cmp byte[esi],'0'
    jb .noletter
    cmp byte[esi],'9'
    jbe .yesletter
    cmp byte[esi],'A'
    jb .noletter
    cmp byte[esi],'Z'
    jbe .yesletter
    cmp byte[esi],'a'
    jb .noletter
    cmp byte[esi],'z'
    ja .noletter
.yesletter
    inc al
.noletter
    inc esi
    loop .nextletter
    cmp al,5
    jna .checkhiromletter
    mov byte[romtype],1
    jmp .donecheck    
.checkhiromletter
    mov esi,[romdata]
    add esi,65472
    mov ecx,21
    mov al,0
.nextletterb
    cmp byte[esi],32
    je .yesletterb
    cmp byte[esi],'0'
    jb .noletterb
    cmp byte[esi],'9'
    jbe .yesletterb
    cmp byte[esi],'A'
    jb .noletterb
    cmp byte[esi],'Z'
    jbe .yesletterb
    cmp byte[esi],'a'
    jb .noletterb
    cmp byte[esi],'z'
    ja .noletterb
.yesletterb
    inc al
.noletterb
    inc esi
    loop .nextletterb
    cmp al,5
    jna .notfound
    mov byte[romtype],2
    jmp .donecheck

.notfound
    mov ah,09h
    mov edx,.doh
    int 21h
    jmp DosExit

.donecheck

;        COP     Software        00FFF4,5    00FFE4,5    N/A
;        ABORT   Hardware        00FFF8,9    00FFE8,9     2
;        NMI     Hardware        00FFFA,B    00FFEA,B     3
;        RES     Hardware        00FFFC.D    00FFFC,D     1
;        BRK     Software        00FFFE,F    00FFE6,7    N/A
;        IRQ     Hardware        00FFFE,F    00FFEE,F     4

    ; Get Vectors (NMI & Reset)
    mov esi,[romdata]
    add esi,32704+21
    cmp byte[romtype],2
    jne .nohirom9
    add esi,8000h
.nohirom9
    mov al,[esi]
    test al,0F0h
    jnz .yesfastrom
    mov al,[opexec268]
    mov [opexec358],al
    mov al,[opexec268cph]
    mov [opexec358cph],al
    mov al,[cycpb268]
    mov [cycpb358],al
.yesfastrom
    mov esi,[romdata]
    add esi,7FE4h
    cmp byte[romtype],2
    jne .nohirom
    add esi,8000h
.nohirom
    lodsw
    mov [copv],ax
    lodsw
    mov [brkv],ax
    lodsw
    mov [abortv],ax
    lodsw
    mov [nmiv],ax
    add esi,2
    lodsw
    mov [irqv],ax
    add esi,4
    ; 8-bit and reset
    lodsw
    mov [copv8],ax
    inc esi
    inc esi
    lodsw
    mov [abortv8],ax
    lodsw
    mov [nmiv8],ax
    lodsw
    mov [resetv],ax
    lodsw
    mov [brkv8],ax
    mov [irqv8],ax

    ; Output Name
    mov esi,[romdata]
    add esi,7FC0h
    cmp byte[romtype],2
    jne .nohirom2
    add esi,8000h
.nohirom2
    mov ecx,21
.loopb
    lodsb
    mov dl,al
    mov ah,2
    int 21h
    loop .loopb
    inc esi
    mov edx,.ret

    ; ROM Type
    mov ah,9
    int 21h
    mov edx,.romtyp
    int 21h
    mov edx,.hirom
    cmp byte[romtype],1
    jne .nolorom
    mov edx,.lorom
.nolorom
    int 21h
    mov edx,.romtype
    xor ebx,ebx
    mov bl,[esi]
    inc esi

;    xor eax,eax
;    mov al,bl
;    call printnum
;    jmp DosExit

    cmp bl,6
    jna .nounknown
    mov bl,7
.nounknown
    shl bl,4
    add edx,ebx
    int 21h

    ; ROM Size
    mov edx,.romsize
    mov ah,9
    int 21h
    mov cl,[esi]
    inc esi
    xor eax,eax
    sub cl,7
    mov al,1
    shl al,cl
    call printnum
    mov edx,.megabit
    mov ah,9
    int 21h

    ; RAM Size
    mov edx,.sramsize
    mov ah,9
    int 21h
    mov cl,[esi]
    inc esi
    xor eax,eax
    mov al,1
    shl al,cl
    cmp al,1
    jne .yessram
    mov al,0
.yessram
    call printnum
    shl ax,10
    mov [ramsize],ax
    dec ax
    mov [ramsizeand],ax
    mov edx,.kilobit
    mov ah,9
    int 21h
    mov al,[esi]
    mov byte[romispal],0
    mov word[totlines],261
    mov dword[MsgCount],120
    cmp al,1
    jbe .nopal
    mov byte[romispal],1
    mov word[totlines],313
    mov dword[MsgCount],100
    mov edx,.romtypep
    mov ah,9
    int 21h
    jmp .yespal
.nopal
    mov edx,.romtypen
    mov ah,9
    int 21h
.yespal
    ; Display NMI & Reset
    mov edx,.nmidisp
    mov ah,9
    int 21h
    xor eax,eax
    mov ax,[nmiv]
    call printhex
    mov edx,.ret
    mov ah,9
    int 21h
    mov edx,.resetdisp
    mov ah,9
    int 21h
    mov ax,[resetv]
    call printhex
    mov edx,.ret
    mov ah,9
    int 21h
    mov edx,.ret
    mov ah,9
    int 21h
    mov edx,.waitkey
    mov ah,9         
    int 21h
    ; wait for key
    cmp byte[enterpress],0
    jne .noesc
    mov ah,07h
    int 21h
    cmp al,27
    jne .noesc
    jmp DosExit
.noesc
    mov edx,.ret
    mov ah,9
    int 21h
    ret

.memfree times 30 db 0                                                  ;000132D7
.filename  db 'Filename    : ','$'
.frameskip db 'Frame Skip  : ','$'
.percexec  db '% to Exec   : ','$'
.debugon   db 'Debugger    : ','$'
.memryfr   db 'Memory Free : ','$'
.auto      db 'AUTO',13,10,'$'
.on        db 'ON',13,10,'$'
.off       db 'OFF',13,10,'$'
.ret       db 13,10,'$'
.waitkey   db 'Press Any Key to Continue.','$'
.smcname   db 'Cartridge name : ','$'
.romtyp    db 'ROM type       : ','$'
.hirom     db 'HIROM/','$'
.lorom     db 'LOROM/','$'
.romtype   db 'ROM          ',13,10,'$'
           db 'ROM/RAM      ',13,10,'$'
           db 'ROM/SRAM     ',13,10,'$'
           db 'ROM/DSP1     ',13,10,'$'
           db 'RAM/DSP1/RAM ',13,10,'$'
           db 'ROM/DSP1/SRAM',13,10,'$'
           db 'SUPER FX     ',13,10,'$'
.unknowns  db 'UNKNOWN      ',13,10,'$'
.romsize   db 'ROM size       : ','$'
.sramsize  db 'RAM size       : ','$'
.romtypep  db 'ROM Type       : PAL',13,10,'$'
.romtypen  db 'ROM Type       : NTSC',13,10,'$'
.megabit   db ' Megabits',13,10,'$'
.kilobit   db ' Kilobytes',13,10,'$'
.nmidisp   db 'NMI Vector Location   : ','$'
.resetdisp db 'Reset Vector Location : ','$'
.doh       db 'Cannot detect whether cartridge is HiROM or LoROM.',13,10,'Please use -h/-l',13,10,'$'

