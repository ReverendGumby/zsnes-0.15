;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM romdata,pressed,vidbuffer,oamram
EXTSYM debstop3
EXTSYM wramdata,regptr,snesmmap,debstop
EXTSYM snesmap2
EXTSYM regptw,writeon
EXTSYM xd
EXTSYM ramsize,ramsizeand,sram
EXTSYM ram7f
EXTSYM DosExit,invalid,invopcd,previdmode,printhex8

;*******************************************************
; Register & Memory Access Banks (0 - 3F) / (80 - BF)
;*******************************************************
; enter : BL = bank number, CX = address location
; leave : AL = value read

NEWSYM regaccessbankr8                                                  ;0002CA63
    test cx,8000h
    jz .regacc
    mov ebx,[snesmmap+ebx*4]
    mov al,[ebx+ecx]
    xor ebx,ebx
    ret
.regacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov al,[ebx+ecx]
    xor ebx,ebx
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptr]
    call [ebx]
    xor ebx,ebx
    ret
.invaccess
    cmp cx,6000h
    jae .hiromsram
    xor al,al
    ret
.hiromsram
    and ebx,7Fh
    cmp bl,30h
    jae .hiromsramok
    mov al,0FFh
    xor ebx,ebx
    ret
.hiromsramok
    push ecx
    sub cx,6000h
    and ecx,1fffh
    sub bl,30h
    shl ebx,13
    add ecx,ebx
    and ecx,07FFFh
    call sramaccessbankr8
    pop ecx
    ret

NEWSYM regaccessbankr16                                                 ;0002CADC
    test cx,8000h
    jz .regacc
    mov ebx,[snesmmap+ebx*4]
    mov ax,[ebx+ecx]
    xor ebx,ebx
    ret
.regacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov ax,[ebx+ecx]
    xor ebx,ebx
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptr]
    call [ebx]
    mov ah,al
    inc ecx
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptr]
    call [ebx]
    mov bl,al
    dec ecx
    mov al,ah
    mov ah,bl
    xor ebx,ebx
    ret
.invaccess
;    jmp regexiter
    cmp cx,6000h
    jae .hiromsram
    xor ax,ax
    ret
.hiromsram
    and ebx,7Fh
    cmp bl,30h
    jae .hiromsramok
    mov ax,0FFFFh
    xor ebx,ebx
    ret
.hiromsramok
    push ecx
    sub cx,6000h
    and ecx,1fffh
    sub bl,30h
    shl ebx,13
    add ecx,ebx
    and ecx,07FFFh
    call sramaccessbankr16
    pop ecx
    ret

NEWSYM regaccessbankw8                                                  ;0002CB71
    test cx,8000h
    jnz .romacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov [ebx+ecx],al
    xor ebx,ebx
    ret
.romacc
    cmp byte[writeon],0
    jne .modrom
    ret
.modrom
    mov ebx,[snesmmap+ebx*4]
    mov [ebx+ecx],al
    xor ebx,ebx
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptw]
    call dword near [ebx]
    xor ebx,ebx
    ret
.invaccess
;    jmp regexiter
    cmp cx,6000h
    jae .hiromsram
    ret
.hiromsram
    and ebx,7Fh
    cmp bl,30h
    jae .hiromsramok
    xor al,al
    xor ebx,ebx
    ret
.hiromsramok
    push ecx
    sub cx,6000h
    and ecx,1fffh
    sub bl,30h
    shl ebx,13
    add ecx,ebx
    and ecx,07FFFh
    call sramaccessbankw8
    pop ecx
    ret

NEWSYM regaccessbankw16                                                 ;0002CBF2
    test cx,8000h
    jnz .romacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov [ebx+ecx],ax
    xor ebx,ebx
    ret
.romacc
    cmp byte[writeon],0
    jne .modrom
    ret
.modrom
    mov ebx,[snesmmap+ebx*4]
    mov [ebx+ecx],ax
    xor ebx,ebx
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptw]
    call dword near [ebx]
    mov al,ah
    inc ecx
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptw]
    call dword near [ebx]
    dec ecx
    xor ebx,ebx
    ret
.invaccess
;    jmp regexiter
    cmp cx,6000h
    jae .hiromsram
    ret
.hiromsram
    and ebx,7Fh
    cmp bl,30h
    jae .hiromsramok
    xor al,al
    xor ebx,ebx
    ret
.hiromsramok
    push ecx
    sub cx,6000h
    and ecx,1fffh
    sub bl,30h
    shl ebx,13
    add ecx,ebx
    and ecx,07FFFh
    call sramaccessbankw16
    pop ecx
    ret

;*******************************************************
; Register & Memory Bank (Bank 0)
;*******************************************************
; enter : BL = bank number, CX = address location
; leave : AL = value read

NEWSYM membank0r8                                                       ;0002CC86
    test cx,8000h
    jz .regacc
    mov ebx,[snesmmap]
    mov al,[ebx+ecx]
    xor ebx,ebx
    ret
.regacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov al,[ebx+ecx]
    xor ebx,ebx
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptr]
    call [ebx]
    xor ebx,ebx
    ret
.invaccess
    xor al,al
    ret

NEWSYM membank0r16                                                      ;0002CCC6
    test cx,8000h
    jz .regacc
    mov ebx,[snesmmap]
    mov ax,[ebx+ecx]
    xor ebx,ebx
    ret
.regacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov ax,[ebx+ecx]
    xor ebx,ebx
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptr]
    call [ebx]
    mov ah,al
    inc ecx
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptr]
    call [ebx]
    mov bl,al
    dec ecx
    mov al,ah
    mov ah,bl
    xor ebx,ebx
    ret
.invaccess
    xor ax,ax
    ret

NEWSYM membank0w8
    test cx,8000h
    jnz .romacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov [ebx+ecx],al
    xor ebx,ebx
    ret
.romacc
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptw]
    call dword near [ebx]
    xor ebx,ebx
    ret
.invaccess
    ret

NEWSYM membank0w16                                                      ;0002CD53
    test cx,8000h
    jnz .romacc
    cmp cx,2000h
    jae .regs
    mov ebx,[wramdata]
    mov [ebx+ecx],ax
    xor ebx,ebx
    ret
.romacc
    ret
.regs
    cmp cx,47FFh
    ja .invaccess
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptw]
    call dword near [ebx]
    mov al,ah
    inc ecx
    mov ebx,ecx
    shl ebx,2
    add ebx,[regptw]
    call dword near [ebx]
    dec ecx
    xor ebx,ebx
    ret
.invaccess
    ret

NEWSYM memaccessbankr8                                                  ;0002CD98
    mov ebx,[snesmmap+ebx*4]
    mov al,[ebx+ecx]
    xor ebx,ebx
    ret

NEWSYM memaccessbankr16                                                 ;0002CDA5
    mov ebx,[snesmmap+ebx*4]
    mov ax,[ebx+ecx]
    xor ebx,ebx
    ret

NEWSYM memaccessbankw8                                                  ;0002CDB3
    ret

NEWSYM memaccessbankw16                                                 ;0002CDB4
    ret

;*******************************************************
; SRAM Access Bank (70h)
;*******************************************************

NEWSYM sramaccessbankr8                                                 ;0002CDB5
    cmp word[ramsize],0
    je .noaccess
    push cx
    and cx,[ramsizeand]
    mov ebx,[sram]
    mov al,[ebx+ecx]
    pop cx
    xor ebx,ebx
    ret
.noaccess
    xor al,al
    ret

NEWSYM sramaccessbankr16                                                ;0002CDDA
    cmp word[ramsize],0
    je .noaccess
    mov ebx,[sram]
    push cx
    and cx,[ramsizeand]
    mov al,[ebx+ecx]
    inc cx
    and cx,[ramsizeand]
    mov ah,[ebx+ecx]
    pop cx
    xor ebx,ebx
    ret
.noaccess
    xor ax,ax
    ret

NEWSYM sramaccessbankw8                                                 ;0002CE0C
    cmp word[ramsize],0
    je .noaccess
    mov ebx,[sram]
    push cx
    and cx,[ramsizeand]
    mov [ebx+ecx],al
    pop cx
    xor ebx,ebx
.noaccess
    ret

NEWSYM sramaccessbankw16                                                ;0002CE2E
    cmp word[ramsize],0
    je .noaccess
    mov ebx,[sram]
    push cx
    and cx,[ramsizeand]
    mov [ebx+ecx],al
    inc cx
    and cx,[ramsizeand]
    mov [ebx+ecx],ah
    pop cx
    xor ebx,ebx
.noaccess
    ret

;*******************************************************
; WorkRAM/ExpandRAM Access Bank (7Eh)
;*******************************************************

NEWSYM wramaccessbankr8                                                 ;0002CE5C
    mov ebx,[wramdata]
    mov al,[ebx+ecx]
    xor ebx,ebx
    ret

NEWSYM wramaccessbankr16                                                ;0002CE68
    mov ebx,[wramdata]
    mov ax,[ebx+ecx]
    xor ebx,ebx
    ret

NEWSYM wramaccessbankw8                                                 ;0002CE75
    mov ebx,[wramdata]
    mov [ebx+ecx],al
    xor ebx,ebx
    ret

NEWSYM wramaccessbankw16                                                ;0002CE81
    mov ebx,[wramdata]
    mov [ebx+ecx],ax
    xor ebx,ebx
    ret

;*******************************************************
; ExpandRAM Access Bank (7Fh)
;*******************************************************
NEWSYM eramaccessbankr8                                                 ;0002CE8E
    mov ebx,[ram7f]
    mov al,[ebx+ecx]
    xor ebx,ebx
    ret

NEWSYM eramaccessbankr16                                                ;0002CE9A
    mov ebx,[ram7f]
    mov ax,[ebx+ecx]
    xor ebx,ebx
    ret

NEWSYM eramaccessbankw8                                                 ;0002CEA7
    mov ebx,[ram7f]
    mov [ebx+ecx],al
    xor ebx,ebx
    ret

NEWSYM eramaccessbankw16                                                ;0002CEB3
    mov ebx,[ram7f]
    mov [ebx+ecx],ax
    xor ebx,ebx
    ret

;*******************************************************
; Invalid Access Bank (710000h-7DFFFFh)
;*******************************************************
NEWSYM invaccessbank                                                    ;0002CEC0
    xor eax,eax
    mov byte[invalid],1
    mov [invopcd],bl
    mov al,[previdmode]
    mov ah,0
    int 10h
    mov ah,9
    mov edx,.invalidbank
    int 21h
    xor eax,eax
    mov al,[invopcd]
    call printhex8
    mov ah,2
    mov dl,13
    int 21h
    mov ah,2
    mov dl,10
    int 21h
    jmp DosExit

.invalidbank db 'Invalid Bank Access : $'
    ret
