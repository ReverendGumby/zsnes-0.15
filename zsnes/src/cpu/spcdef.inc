;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.








; move al at address ebx

; branch instructions
%macro spcbrancher 0
      inc ebp
      jmp spcopcdone
.branch
      movsx ebx,byte [ebp]
      inc ebp
      add ebp,ebx
      jmp spcopcdone
%endmacro      

; tcall instruction
%macro spctcall 1
      mov ebx,ebp
      sub ebx,spcRam
      mov eax,[spcS]
      mov [spcRam+eax],bh
      dec byte [spcS]
      mov eax,[spcS]
      mov [spcRam+eax],bl
      dec byte [spcS]
      mov bx,[spcextraram+%1]
      add ebx,spcRam
      mov ebp,ebx
      jmp spcopcdone
%endmacro

; SET1 instruction
%macro set1 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      or al,%1
      WriteByte
      jmp spcopcdone
%endmacro

; CLR1 instruction
%macro clr1 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      and al,%1
      WriteByte
      jmp spcopcdone
%endmacro

; BBS instruction
%macro bbs 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      test byte[ebx],%1
      jnz .dp0jump
      add ebp,2
      jmp spcopcdone
.dp0jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      jmp spcopcdone
%endmacro

; BBC instruction
%macro bbc 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      test byte[ebx],%1
      jz .dp0jump
      add ebp,2
      jmp spcopcdone
.dp0jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      jmp spcopcdone
%endmacro

; OR A, instruction
%macro SPC_OR_A 1
      or byte [spcA], %1
      mov al,[spcA]
      mov [spcNZ],al
      jmp spcopcdone
%endmacro

; AND A, instruction
%macro SPC_AND_A 1
      and byte [spcA], %1
      mov al,[spcA]
      mov [spcNZ],al
      jmp spcopcdone
%endmacro

; EOR A, instruction
%macro SPC_EOR_A 1
      xor byte [spcA], %1
      mov al,[spcA]
      mov [spcNZ],al
      jmp spcopcdone
%endmacro

; CMP A, instruction
%macro SPC_CMP_A 1
      cmp byte [spcA], %1
      cmc
      SPCSetFlagnzc
%endmacro

%macro SPC_ADC_A 1
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc byte [spcA], %1
      SPCSetFlagnvhzc
%endmacro

%macro SPC_SBC_A 1
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb byte [spcA], %1
      cmc
      SPCSetFlagnvhzc
%endmacro

%macro SPC_MOV_A 0
      mov byte [spcA], al
      mov [spcNZ],al
      jmp spcopcdone
%endmacro
