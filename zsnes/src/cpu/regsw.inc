;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


EXTSYM initsfxregsw,reg420Bw,reg420Cw,regptw,initSA1regsw,SDD1Reset
EXTSYM SPC7110Reset,RTCReset2
EXTSYM xat,xdbt,xdt,xpbt,xst,xxt,xyt
EXTSYM DSPMem,WDSPReg0C,WDSPReg0D,WDSPReg0F,WDSPReg1C,WDSPReg1F
EXTSYM WDSPReg2C,WDSPReg2F,WDSPReg3C,WDSPReg3D,WDSPReg3F,WDSPReg4F
EXTSYM WDSPReg5F,WDSPReg6C,WDSPReg6F,WDSPReg7D,WDSPReg7F
EXTSYM debstop
EXTSYM cachesprites,curblank,processsprites,sprleftpr,sprlefttot
EXTSYM nmirept,spcRam
EXTSYM HIRQCycNext,HIRQNextExe,HIRQSkip
EXTSYM cycpb268,cycpb358,cycpbl,cycpblt,opexec268,opexec268cph
EXTSYM opexec358,opexec358cph
EXTSYM printhex8






NEWSYM initregw                                                         ;0002384B
    ; Fill register pointer with invalid register accesses
    mov edi,[regptw]
    add edi,8000h
    mov ecx,3000h
    mov eax,regINVALIDw
.loopa
    mov [edi],eax
    add edi,4
    loop .loopa
    ; Set all valid register accesses
    setregw 2100h*4,reg2100w
    setregw 2101h*4,reg2101w
    setregw 2102h*4,reg2102w
    setregw 2103h*4,reg2103w
    setregw 2104h*4,reg2104w
    setregw 2105h*4,reg2105w
    setregw 2106h*4,reg2106w
    setregw 2107h*4,reg2107w
    setregw 2108h*4,reg2108w
    setregw 2109h*4,reg2109w
    setregw 210Ah*4,reg210Aw
    setregw 210Bh*4,reg210Bw
    setregw 210Ch*4,reg210Cw
    setregw 210Dh*4,reg210Dw
    setregw 210Eh*4,reg210Ew
    setregw 210Fh*4,reg210Fw
    setregw 2110h*4,reg2110w
    setregw 2111h*4,reg2111w
    setregw 2112h*4,reg2112w
    setregw 2113h*4,reg2113w
    setregw 2114h*4,reg2114w
    setregw 2115h*4,reg2115w
    setregw 2116h*4,reg2116w
    setregw 2117h*4,reg2117w
    setregw 2118h*4,reg2118w
    setregw 2119h*4,reg2119w
    setregw 211Ah*4,reg211Aw
    setregw 211Bh*4,reg211Bw
    setregw 211Ch*4,reg211Cw
    setregw 211Dh*4,reg211Dw
    setregw 211Eh*4,reg211Ew
    setregw 211Fh*4,reg211Fw
    setregw 2120h*4,reg2120w
    setregw 2121h*4,reg2121w
    setregw 2122h*4,reg2122w
    setregw 2123h*4,reg2123w
    setregw 2124h*4,reg2124w
    setregw 2125h*4,reg2125w
    setregw 2126h*4,reg2126w
    setregw 2127h*4,reg2127w
    setregw 2128h*4,reg2128w
    setregw 2129h*4,reg2129w
    setregw 212Ah*4,reg212Aw
    setregw 212Bh*4,reg212Bw
    setregw 212Ch*4,reg212Cw
    setregw 212Dh*4,reg212Dw
    setregw 212Eh*4,reg212Ew
    setregw 212Fh*4,reg212Fw
    setregw 2130h*4,reg2130w
    setregw 2131h*4,reg2131w
    setregw 2132h*4,reg2132w
    setregw 2133h*4,reg2133w
    setregw 2140h*4,reg2140w
    setregw 2141h*4,reg2141w
    setregw 2142h*4,reg2142w
    setregw 2143h*4,reg2143w
    setregw 2180h*4,reg2180w
    setregw 2181h*4,reg2181w
    setregw 2182h*4,reg2182w
    setregw 2183h*4,reg2183w
    setregw 4016h*4,reg4016w
    setregw 4017h*4,reg4017w
    setregw 4200h*4,reg4200w
    setregw 4201h*4,reg4201w
    setregw 4202h*4,reg4202w
    setregw 4203h*4,reg4203w
    setregw 4204h*4,reg4204w
    setregw 4205h*4,reg4205w
    setregw 4206h*4,reg4206w
    setregw 4207h*4,reg4207w
    setregw 4208h*4,reg4208w
    setregw 4209h*4,reg4209w
    setregw 420Ah*4,reg420Aw
    setregw 420Bh*4,reg420Bw
    setregw 420Ch*4,reg420Cw
    setregw 420Dh*4,reg420Dw
    setregw 420Eh*4,reg420Ew
    setregw 4210h*4,reg4210w
    setregw 4211h*4,reg4211w
    setregw 4212h*4,reg4212w
    setregw 4216h*4,reg4216w
    setregw 4300h*4,reg43X0w
    setregw 4301h*4,reg43X1w
    setregw 4302h*4,reg43x2w
    setregw 4303h*4,reg43x3w
    setregw 4304h*4,reg43x4w
    setregw 4305h*4,reg43x5w
    setregw 4306h*4,reg43x6w
    setregw 4307h*4,reg43x7w
    setregw 4308h*4,reg43x8w
    setregw 4309h*4,reg43x9w
    setregw 430Ah*4,reg43XAw
    setregw 4310h*4,reg43X0w
    setregw 4311h*4,reg43X1w
    setregw 4312h*4,reg43x2w
    setregw 4313h*4,reg43x3w
    setregw 4314h*4,reg43x4w
    setregw 4315h*4,reg43x5w
    setregw 4316h*4,reg43x6w
    setregw 4317h*4,reg43x7w
    setregw 4318h*4,reg43x8w
    setregw 4319h*4,reg43x9w
    setregw 431Ah*4,reg43XAw
    setregw 4320h*4,reg43X0w
    setregw 4321h*4,reg43X1w
    setregw 4322h*4,reg43x2w
    setregw 4323h*4,reg43x3w
    setregw 4324h*4,reg43x4w
    setregw 4325h*4,reg43x5w
    setregw 4326h*4,reg43x6w
    setregw 4327h*4,reg43x7w
    setregw 4328h*4,reg43x8w
    setregw 4329h*4,reg43x9w
    setregw 432Ah*4,reg43XAw
    setregw 4330h*4,reg43X0w
    setregw 4331h*4,reg43X1w
    setregw 4332h*4,reg43x2w
    setregw 4333h*4,reg43x3w
    setregw 4334h*4,reg43x4w
    setregw 4335h*4,reg43x5w
    setregw 4336h*4,reg43x6w
    setregw 4337h*4,reg43x7w
    setregw 4338h*4,reg43x8w
    setregw 4339h*4,reg43x9w
    setregw 433Ah*4,reg43XAw
    setregw 4340h*4,reg43X0w
    setregw 4341h*4,reg43X1w
    setregw 4342h*4,reg43x2w
    setregw 4343h*4,reg43x3w
    setregw 4344h*4,reg43x4w
    setregw 4345h*4,reg43x5w
    setregw 4346h*4,reg43x6w
    setregw 4347h*4,reg43x7w
    setregw 4348h*4,reg43x8w
    setregw 4349h*4,reg43x9w
    setregw 434Ah*4,reg43XAw
    setregw 4350h*4,reg43X0w
    setregw 4351h*4,reg43X1w
    setregw 4352h*4,reg43x2w
    setregw 4353h*4,reg43x3w
    setregw 4354h*4,reg43x4w
    setregw 4355h*4,reg43x5w
    setregw 4356h*4,reg43x6w
    setregw 4357h*4,reg43x7w
    setregw 4358h*4,reg43x8w
    setregw 4359h*4,reg43x9w
    setregw 435Ah*4,reg43XAw
    setregw 4360h*4,reg43X0w
    setregw 4361h*4,reg43X1w
    setregw 4362h*4,reg43x2w
    setregw 4363h*4,reg43x3w
    setregw 4364h*4,reg43x4w
    setregw 4365h*4,reg43x5w
    setregw 4366h*4,reg43x6w
    setregw 4367h*4,reg43x7w
    setregw 4368h*4,reg43x8w
    setregw 4369h*4,reg43x9w
    setregw 436Ah*4,reg43XAw
    setregw 4370h*4,reg43X0w
    setregw 4371h*4,reg43X1w
    setregw 4372h*4,reg43x2w
    setregw 4373h*4,reg43x3w
    setregw 4374h*4,reg43x4w
    setregw 4375h*4,reg43x5w
    setregw 4376h*4,reg43x6w
    setregw 4377h*4,reg43x7w
    setregw 4378h*4,reg43x8w
    setregw 4379h*4,reg43x9w
    setregw 437Ah*4,reg43XAw
    ret

; video memory change buffer for caching (65536/16=4096)
NEWSYM vidmemch2, times 4096 db 0                                       ;0002444E
NEWSYM vidmemch4, times 4096 db 0
NEWSYM vidmemch8, times 4096 db 0


;*******************************************************
; Registers    Note : restore AH, ECX, ESI, EDI, *S & DX
;*******************************************************


; Screen display register
reg2100w:                                                               ;0002744E
    mov [vidbright],al
    and byte[vidbright],0Fh
    mov [forceblnk],al
    and byte[forceblnk],80h
    ret

; OAM size register
reg2101w:                                                               ;00027467
    xor ebx,ebx
    mov bl,al
    and bl,07h
    shl bx,14
    mov [objptr],bx
    mov [objptrn],bx
    xor ebx,ebx
    mov bl,al
    and bl,18h
    shr bl,3
    shl bx,13
    add [objptrn],bx
    xor ebx,ebx
    mov bl,al
    shr bl,5
    push ax
    mov al,[.objsize1+ebx]
    mov [objsize1],al
    mov al,[.objsize2+ebx]
    mov [objsize2],al
    mov al,[.objmovs1+ebx]
    mov [objmovs1],al
    mov al,[.objmovs2+ebx]
    mov [objmovs2],al
    mov ax,[.objadds1+ebx*2]
    mov [objadds1],ax
    mov ax,[.objadds2+ebx*2]
    mov [objadds2],ax
    pop ax
.noproc
    ret

.objsize1 db 1,1,1,4,4,16,1,1
.objsize2 db 4,16,64,16,64,64,4,4
.objmovs1 db 2,2,2,2,2,4,2,2
.objmovs2 db 2,4,8,4,8,8,2,2
.objadds1 dw 14,14,14,14,14,12,14,14
.objadds2 dw 14,12,8,12,8,8,14,14

; OAM address register
reg2102w:                                                               ;00027529
    shr word[oamaddr],1
    mov [oamaddr],al
    shl word[oamaddr],1
    mov bx,[oamaddr]
    mov [oamaddrs],bx
    ret

; OAM address register
reg2103w:                                                               ;0002754B
    mov bl,al
    and bl,01h
    shr word[oamaddr],1
    mov [oamaddr+1],bl
    shl word[oamaddr],1
    test al,80h
    jz .nohipri
    mov bx,[oamaddr]
    shr bx,2
    and bl,07Fh
    mov [objhipr],bl
.nohipri
    mov bx,[oamaddr]
    mov [oamaddrs],bx
    ret

; OAM data register
reg2104w:                                                               ;0002758B
    xor ebx,ebx
    mov bx,[oamaddr]
    add ebx,oamram
    mov [ebx],al
    inc word[oamaddr]
    cmp word[oamaddr],543
    ja .overflow
    ret
.overflow
    mov word[oamaddr],0
    ret

; Screen mode register
reg2105w:                                                               ;000275B9
    mov bl,al
    and bl,00000111b
    mov [bgmode],bl
    mov bl,al
    shr bl,3
    and bl,01h
    mov [bg3highst],bl
    mov bl,al
    shr bl,4
    mov [bgtilesz],bl
    ret

; Screen pixelation register
reg2106w:                                                               ;000275DE
    mov bl,al
    and bl,0Fh
    mov [mosaicon],bl
    mov bl,al
    shr bl,4
    mov [mosaicsz],bl
    ret

; BG1 VRAM location register
reg2107w:                                                               ;000275F5
    xor ebx,ebx
    mov bl,al
    shr bl,2
    shl bx,11
    mov [bg1ptr],bx
    mov [bg1ptrb],bx
    mov [bg1ptrc],bx
    mov [bg1ptrd],bx
    mov bl,al
    and bl,00000011b
    mov [bg1scsize],bl
    cmp bl,1
    jne .skipa
    add word[bg1ptrb],800h
    add word[bg1ptrd],800h
.skipa
    cmp bl,2
    jne .skipb
    add word[bg1ptrc],800h
    add word[bg1ptrd],800h
.skipb
    cmp bl,3
    jne .skipc
    add word[bg1ptrb],800h
    add word[bg1ptrc],1000h
    add word[bg1ptrd],1800h
.skipc
    ret

; BG2 VRAM location register
reg2108w:                                                               ;00027676
    xor ebx,ebx
    mov bl,al
    shr bl,2
    shl bx,11
    mov [bg2ptr],bx
    mov [bg2ptrb],bx
    mov [bg2ptrc],bx
    mov [bg2ptrd],bx
    mov bl,al
    and bl,00000011b
    mov [bg2scsize],bl
    cmp bl,1
    jne .skipa
    add word[bg2ptrb],800h
    add word[bg2ptrd],800h
.skipa
    cmp bl,2
    jne .skipb
    add word[bg2ptrc],800h
    add word[bg2ptrd],800h
.skipb
    cmp bl,3
    jne .skipc
    add word[bg2ptrb],800h
    add word[bg2ptrc],1000h
    add word[bg2ptrd],1800h
.skipc                  
    ret

; BG3 VRAM location register
reg2109w:                                                               ;000276F7
    xor ebx,ebx
    mov bl,al
    shr bl,2
    shl bx,11
    mov [bg3ptr],bx
    mov [bg3ptrb],bx
    mov [bg3ptrc],bx
    mov [bg3ptrd],bx
    mov bl,al
    and bl,00000011b
    mov [bg3scsize],bl
    cmp bl,1
    jne .skipa
    add word[bg3ptrb],800h
    add word[bg3ptrd],800h
.skipa
    cmp bl,2
    jne .skipb
    add word[bg3ptrc],800h
    add word[bg3ptrd],800h
.skipb
    cmp bl,3
    jne .skipc
    add word[bg3ptrb],800h
    add word[bg3ptrc],1000h
    add word[bg3ptrd],1800h
.skipc
    ret

; BG4 VRAM location register
reg210Aw:                                                               ;00027778
    xor ebx,ebx
    mov bl,al
    shr bl,2
    shl bx,11
    mov [bg4ptr],bx
    mov [bg4ptrb],bx
    mov [bg4ptrc],bx
    mov [bg4ptrd],bx
    mov bl,al
    and bl,00000011b
    mov [bg4scsize],bl
    cmp bl,1
    jne .skipa
    add word[bg4ptrb],800h
    add word[bg4ptrd],800h
.skipa
    cmp bl,2
    jne .skipb
    add word[bg4ptrc],800h
    add word[bg4ptrd],800h
.skipb
    cmp bl,3
    jne .skipc
    add word[bg4ptrb],800h
    add word[bg4ptrc],1000h
    add word[bg4ptrd],1800h
.skipc
    ret

; BG1 & BG2 VRAM location register
reg210Bw:                                                               ;000277F9
    xor ebx,ebx
    mov bl,al
    and bl,0Fh
    shl bx,13
    mov [bg1objptr],bx
    mov bl,al
    shr bl,4
    shl bx,13
    mov [bg2objptr],bx
    ret

; BG3 & BG4 VRAM location register
reg210Cw:                                                               ;0002781C
    xor ebx,ebx
    mov bl,al
    and bl,0Fh
    shl bx,13
    mov [bg3objptr],bx
    mov bl,al
    shr bl,4
    shl bx,13
    mov [bg4objptr],bx
    ret

; BG1 horizontal scroll register
reg210Dw:                                                               ;0002783F
    mov bl,byte[bg1scrolx+1]
    mov byte[bg1scrolx],bl
    mov byte[bg1scrolx+1],al
    ret

; BG1 vertical scroll register
reg210Ew:                                                               ;00027851
    mov bl,byte[bg1scroly+1]
    mov byte[bg1scroly],bl
    mov byte[bg1scroly+1],al
    ret

; BG2 horizontal scroll register
reg210Fw:                                                               ;00027863
    mov bl,byte[bg2scrolx+1]
    mov byte[bg2scrolx],bl
    mov byte[bg2scrolx+1],al
    ret

; BG2 vertical scroll register
reg2110w:                                                               ;00027875
    mov bl,byte[bg2scroly+1]
    mov byte[bg2scroly],bl
    mov byte[bg2scroly+1],al
    ret

; BG3 horizontal scroll register
reg2111w:                                                               ;00027887
    mov bl,byte[bg3scrolx+1]
    mov byte[bg3scrolx],bl
    mov byte[bg3scrolx+1],al
    ret

; BG3 vertical scroll register
reg2112w:                                                               ;00027899
    mov bl,byte[bg3scroly+1]
    mov byte[bg3scroly],bl
    mov byte[bg3scroly+1],al
    ret

; BG4 horizontal scroll register
reg2113w:                                                               ;000278AB
    mov bl,byte[bg4scrolx+1]
    mov byte[bg4scrolx],bl
    mov byte[bg4scrolx+1],al
    ret

; BG4 vertical scroll register
reg2114w:                                                               ;000278BD
    mov bl,byte[bg4scroly+1]
    mov byte[bg4scroly],bl
    mov byte[bg4scroly+1],al
    ret

; Video port control
reg2115w:                                                               ;000278CF
    mov [vraminctype],al
    test al,00001100b
    jz .skip0
.skip0
    mov bl,al
    and bl,00000011b
    cmp bl,0
    jne .skip1
    mov word[addrincr],2
.skip1
    cmp bl,1
    jne .skip2
    mov word[addrincr],64
.skip2
    cmp bl,2
    jne .skip3
    mov word[addrincr],128
.skip3
    cmp bl,3
    jne .skip4
    mov word[addrincr],256
.skip4
    test al,80h
    jz .from2118
    mov byte[vramincr],0
    jmp .from2119
.from2118
    mov byte[vramincr],1
.from2119
    ret

; Video port address (Low)
reg2116w:                                                               ;0002792D
    shr word[vramaddr],1
    mov byte[vramaddr],al
    shl word[vramaddr],1
    mov byte[vramread],0
    ret

; Video port address (High)
reg2117w:                                                               ;00027948
    shr word[vramaddr],1
    mov byte[vramaddr+1],al
    shl word[vramaddr],1
    mov byte[vramread],0
    ret

; Video port data (Low)
reg2118w:                                                               ;00027963
    mov ebx,[vramaddr]
    add ebx,[vram]
    cmp [ebx],al
    je .nochange
    mov [ebx],al
    mov ebx,[vramaddr]
    shr ebx,4
    mov byte[vidmemch2+ebx],1
    mov byte[vidmemch4+ebx],1
    mov byte[vidmemch8+ebx],1
.nochange
    cmp byte[vramincr],0
    je .noincr
    mov bx,[addrincr]
    add [vramaddr],bx
.noincr
    ret

; Video port data (High)
reg2119w:                                                               ;000279AB
    mov ebx,[vramaddr]
    add ebx,[vram]
    cmp [ebx+1],al
    je .nochange
    mov [ebx+1],al
    mov ebx,[vramaddr]
    shr ebx,4
    mov byte[vidmemch2+ebx],1
    mov byte[vidmemch4+ebx],1
    mov byte[vidmemch8+ebx],1
.nochange
    cmp byte[vramincr],1
    je .incr
    mov bx,[addrincr]
    add [vramaddr],bx
.incr
    ret

; MODE7 settings register
reg211Aw:                                                               ;00027A79
    mov [mode7set],al
    ret

; COS (COSINE) rotate angle / X Expansion
reg211Bw:                                                               ;000279FB
    mov bl,[mode7A+1]
    mov [mode7A],bl
    mov [mode7A+1],al
    ret

; SIN (SIN)    rotate angle / X Expansion & Complement Multiplication Start
reg211Cw:                                                               ;00027A0D
    mov bl,[mode7B+1]
    mov [mode7B],bl
    mov [mode7B+1],al
    push edx
    push eax
    xor bh,bh
    mov bl,al
    mov ax,[mode7A]
    test bl,80h
    jz .pos
    mov bh,0FFh
.pos
    imul bx
    mov [compmult],ax
    mov [compmult+2],dl
    pop eax
    pop edx
    ret

; SIN (SIN)    rotate angle / Y Expansion
reg211Dw:                                                               ;00027A43
    mov bl,[mode7C+1]
    mov [mode7C],bl
    mov [mode7C+1],al
    ret

; COS (COSINE) rotate angle / Y Expansion
reg211Ew:                                                               ;00027A55
    mov bl,[mode7D+1]
    mov [mode7D],bl
    mov [mode7D+1],al
    ret

; Center position X (13-bit data only)
reg211Fw:                                                               ;00027A67
    mov bl,[mode7X0+1]
    mov [mode7X0],bl
    mov [mode7X0+1],al
    ret

; Center position Y (13-bit data only)
reg2120w:                                                               ;00027A79
    mov bl,[mode7Y0+1]
    mov [mode7Y0],bl
    mov [mode7Y0+1],al
    ret

; Colour # (or palette) selection register
reg2121w:                                                               ;00027A8B
    xor bh,bh
    mov bl,al
    shl bx,1
    mov [cgaddr],bx
    and word[cgaddr],01FFh
    ret

; Colour data register
reg2122w:                                                               ;00027AA3
    xor ebx,ebx
    mov bx,[cgaddr]
    cmp [cgram+ebx],al
    je .nomod
    mov [cgram+ebx],al
    mov byte[cgmod],1
.nomod
    inc word[cgaddr]
    and word[cgaddr],01FFh
    ret

; Window mask settings register [W12SEL]
reg2123w:                                                               ;00027AD2
    mov bl,al
    and bl,0Fh
    mov [winbg1en],bl
    mov bl,al
    shr bl,4
    mov [winbg2en],bl
    ret

; Window mask settings register [W34SEL]
reg2124w:                                                               ;00027AE9
    mov bl,al
    and bl,0Fh
    mov [winbg3en],bl
    mov bl,al
    shr bl,4
    mov [winbg4en],bl
    ret

; Window mask settings register [WOBJSEL]
reg2125w:                                                               ;00027B00
    mov bl,al
    and bl,0Fh
    mov [winobjen],bl
    mov bl,al
    shr bl,4
    mov [wincolen],bl
    ret

; Window 1 left position register
reg2126w:
    mov [winl1],al
    ret

; Window 1 right position register
reg2127w:
    mov [winr1],al
    ret

; Window 2 left position register
reg2128w:
    mov [winl2],al
    ret

; Window 2 right position register
reg2129w:
    mov [winr2],al
    ret

; Mask logic settings for Window 1 & 2 per screen
reg212Aw:                                                               ;00027B2F
    mov [winlogica],al
    ret

; Mask logic settings for Colour Windows & OBJ Windows
reg212Bw:
    mov [winlogicb],al
    ret

; Main screen designation
reg212Cw:
    mov [scrnon],al
    ret

; Sub-screen designation
reg212Dw:
    mov [scrnon+1],al
    ret

; Window mask main screen designation register
reg212Ew:                                                               ;00027B47
    mov [winenabm],al
    ret

; Window mask sub screen designation register
reg212Fw:
    mov [winenabs],al
    ret

; Fixed color addition or screen addition register
reg2130w:                                                               ;00027B53
    mov [scaddset],al    
    ret

; Addition/subtraction for screens, BGs, & OBJs
reg2131w:
    mov [scaddtype],al
    ret

; Fixed colour data for fixed colour +/-
reg2132w:                                                               ;00027B5F
    mov bl,al
    and bl,1Fh
    test al,20h
    jz .nored
    mov [coladdr],bl
.nored
    test al,40h
    jz .nogreen
    mov [coladdg],bl
.nogreen
    test al,80h
    jz .noblue
    mov [coladdb],bl
.noblue
    ret

; Screen mode/video select register
reg2133w:
    test al,04h
    jnz .line239
    mov word[resolutn],224
    ret
.line239
    mov word[resolutn],239
    ret

; Sound Register #1
reg2140w:                                                               ;00027B9B
    mov byte[spcRam+0F4h],al
    ret

; Sound Register #2
reg2141w:                                                               ;00027BA1
    mov byte[spcRam+0F5h],al
    ret

; Sound Register #3
reg2142w:                                                               ;00027BA7
    mov byte[spcRam+0F6h],al
    ret

; Sound Register #4
reg2143w:                                                               ;00027BAD
    mov byte[spcRam+0F7h],al
    ret

; Read/write WRAM register
reg2180w:                                                               ;00027BB3
    mov ebx,[wramrwadr]
    add ebx,[wramdata]
    mov [ebx],al
    inc dword[wramrwadr]
    and dword[wramrwadr],01FFFFh
    ret

; WRAM data register (low byte)
reg2181w:                                                               ;00027BD2
    mov byte[wramrwadr],al
    ret

; WRAM data register (middle byte)
reg2182w:                                                               ;00027BD8
    mov byte[wramrwadr+1],al
    ret

; WRAM data register (high byte)
reg2183w:                                                               ;00027BDE
    and al,01h
    mov byte[wramrwadr+2],al
    ret

; Joystick 1 & 2 status bytes
reg4016w:                                                               ;00027BE6
    mov byte[JoyAPos],0
    ret

; Joystick 1 & 2 status bytes
reg4017w:                                                               ;00027BEE
    mov byte[JoyBPos],0
    ret

; Counter enable
reg4200w:                                                               ;00027BF6
    mov byte[INTEnab],al
    ret

; Programmable I/O port (out-port)
reg4201w:                                                               ;00027BFC
    ret

; Multiplicand 'A'
reg4202w:                                                               ;00027BFD
    mov [multa],al
    ret

; Multiplier 'B'
reg4203w:                                                               ;00027c03
    push edx
    push eax
    xor ah,ah
    xor bh,bh
    mov bl,[multa]
    mul bx
    mov [multres],ax
    pop eax
    pop edx
    ret

; Dividend C (Low)
reg4204w:                                                               ;00027C1B
    mov [diva],al
    ret

; Dividend C (High)
reg4205w:                                                               ;00027C21
    mov [diva+1],al
    ret

; Divisor B
reg4206w:                                                               ;00027C27
    cmp al,0
    je .divby0
    push eax
    push edx
    xor ebx,ebx
    xor edx,edx
    mov bl,al
    mov ax,[diva]
    div bx
    mov [divres],ax
    mov [multres],dx
    pop edx
    pop eax
    ret
.divby0
    push eax
    mov word[divres],0FFFFh
    mov ax,[diva]
    mov [multres],ax
    pop eax
    ret

; Video horizontal IRQ beam position/pointer (Low)
reg4207w:                                                               ;00027C64
    ret

; Video horizontal IRQ beam position/pointer (High)
reg4208w:                                                               ;00027C65
    ret

; Video vertical IRQ beam position/pointer (Low)
reg4209w:                                                               ;00027C66
    mov [VIRQLoc],al
    ret

; Video vertical IRQ beam position/pointer (High)
reg420Aw:                                                               ;00027C6C
    and al,01h
    mov [VIRQLoc+1],al
    ret

; Cycle speed register
reg420Dw:                                                               ;00027C74
    test al,01h
    jnz .speed358
    ; 2.68 Mhz
    mov al,[opexec268]
    mov byte[cycpl],al      ; 2.68 Mhz
    mov al,[opexec268cph]
    mov byte[cycphb],al     ; 2.68 Mhz
    and byte[xirqb],00h
    mov bl,[cycpb268]
    mov byte[cycpblt],bl  ; percentage of CPU/SPC to run
    ret
.speed358
    ; 3.58 Mhz
    mov al,[opexec358]
    mov byte[cycpl],al      ; 3.58 Mhz
    mov al,[opexec358cph]
    mov byte[cycphb],al     ; 3.58 Mhz
    or byte[xirqb],80h
    mov bl,[cycpb358]
    mov byte[cycpblt],bl  ; percentage of CPU/SPC to run
    ret

; ???
reg420Ew:                                                               ;00027CC8
    ret

; NMI Check register
reg4210w:                                                               ;00027CC9
    ret

; Video IRQ register
reg4211w:                                                               ;00027CCA
    ret

; Status register
reg4212w:                                                               ;00027CCB
    ret

; Product of Multiplication Result or Remainder of Divide Result (Low)
reg4216w:                                                               ;00027CCC
    ret

; DMA Control register
reg43X0w:                                                               ;00027CCD
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    ret

; DMA Destination register
reg43X1w:                                                               ;00027CDE
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    ret

; Source address (Low)
reg43x2w:                                                               ;00027CEF
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    mov [dmadata+ebx+6],al
    ret

; Source address (High)
reg43x3w:                                                               ;00027D06
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    mov [dmadata+ebx+6],al
    ret

; Source bank address
reg43x4w                                                                ;00027D1D
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    ret

; DMA transfer size & HDMA address register (Low)
reg43x5w:                                                               ;00027D2E
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    ret

; DMA transfer size & HDMA address register (High)
reg43x6w:                                                               ;00027D3F
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    ret

; DMA transfer size & HDMA address register (Bank)
reg43x7w:                                                               ;00027D50
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    ret

; Table Address of A-BUS by DMA < A2 Table Address (Low)
reg43x8w:                                                               ;00027D61
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    push eax
    mov al,[dmadata-6+ebx]
    mov [dmadata+ebx],al
    pop eax
    ret

; Table Address of A-BUS by DMA < A2 Table Address (High)
reg43x9w:                                                               ;00027D7A
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    push eax
    mov al,[dmadata-6+ebx]
    mov [dmadata+ebx],al
    pop eax
    ret

; Number of lines for HDMA transfer
reg43XAw:                                                               ;00027D93
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov [dmadata+ebx],al
    ret

regINVALIDw:     ; Invalid Register                                     ;00027DA4
    ret


regexiter:                                                              ;00027DA5
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr2
    mov eax,[snesmmap+ebx*4]
    jmp .nextaddr
.loweraddr2
    mov eax,[snesmap2+ebx*4]
.nextaddr
    mov ebx,esi
    sub ebx,eax                 ; subtract program counter by address
    mov [.invaddr],bx
    mov bl,[xpb]
    mov [.invbank],bl

    mov al,[previdmode]
    mov ah,0
    int 10h
    mov byte[invalid],1
    mov [invreg],cx
    mov ah,9
    mov edx,.invalidreg
    int 21h
    xor eax,eax
    mov ax,[invreg]
    call printhex
    mov ah,9
    mov edx,.invalidreg
    int 21h
    xor eax,eax
    mov al,[.invbank]
    call printhex8
    mov ax,[.invaddr]
    call printhex
    mov ah,2
    mov dl,13
    int 21h
    mov ah,2
    mov dl,10
    int 21h
    mov eax,[numinst]
    call printnum
    jmp DosExit

.invalidreg db 'Invalid Write Register : $'
.invalidaddr db ' at address $'
.invbank db 0
.invaddr db 0
