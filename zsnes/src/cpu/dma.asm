;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM memtabler8,regptw,snesmap2,snesmmap,debstop3
;EXTSYM soundcycleft,pexecs2
EXTSYM memtablew8,regptr
EXTSYM dmadata
EXTSYM hdmatype
EXTSYM nexthdma
EXTSYM curhdma,curypos,disablehdma,hdmadata,hdmadelay,hdmaearlstart
EXTSYM resolutn
EXTSYM memtabler16






;*******************************************************
; Transfer DMA                     Inits & Transfers DMA
;*******************************************************
; DMA transfer register

;NEWSYM AddrNoIncr, db 0

%macro ExecSPCCycles 0
    xor ebx,ebx
    mov bx,[esi+5]
    inc bx
    inc ebx
    shr ebx,2
    mov [soundcycleft],ebx
    or ebx,ebx
    jz .nocycles
    xor ebx,ebx
    xor ecx,ecx
    call pexecs2
.nocycles
%endmacro

NEWSYM transdma                                                         ;0002CF16
    push eax
;   ExecSPCCycles

    mov al,[esi]
    test al,80h
    jnz near transdmappu2cpu

    ; set address increment value
    mov word[.addrincr],0
    test al,00001000b
    jnz .skipaddrincr
    test al,00010000b
    jnz .autodec
    mov word[.addrincr],1
    jmp .skipaddrincr
.autodec
    mov word[.addrincr],0FFFFh
.skipaddrincr

    ; get address order to be written
    xor ebx,ebx
    and al,00000111b
    mov bl,al
    shl bl,3
    add ebx,.addrwrite
    mov edi,ebx

    ; get pointer #1
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bl,[edi]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [.regptra],eax

    ; get pointer #2
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bl,[edi+2]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [.regptrb],eax

    ; get pointer #3
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bl,[edi+4]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [.regptrc],eax

    ; get pointer #4
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bl,[edi+6]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [.regptrd],eax

    mov dx,[esi+5]      ; Number of bytes to transfer
    xor ebx,ebx
    mov bl,[esi+4]      ; Bank #
    xor ecx,ecx
    mov cx,[esi+8]      ; address offset # ?
    mov [.curbank],bl
    mov word[esi+5],0

.againloop
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtabler8+ebx*4]
    add cx,[.addrincr]
    call dword near [.regptra]
    dec dx
    jz .findma
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtabler8+ebx*4]
    add cx,[.addrincr]
    call dword near [.regptrb]
    dec dx
    jz .findma
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtabler8+ebx*4]
    add cx,[.addrincr]
    call dword near [.regptrc]
    dec dx
    jz .findma
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtabler8+ebx*4]
    add cx,[.addrincr]
    call dword near [.regptrd]
    dec dx
    jnz near .againloop
.findma
    mov [esi+8],cx
    pop eax
    ret

.curbank   db 0                                                         ;0002D066
.addrwrite dw 0,0,0,0, 0,1,0,1, 0,0,0,0, 0,0,1,1, 0,1,2,3, 0,0,0,0, 0,0,0,0, ;0002D067
           dw 0,0,0,0
.addrincr  dw 0                                                         ;0002D0A7
; pointer address of registers
.regptra   dd 0                                                         ;0002D0A9
.regptrb   dd 0
.regptrc   dd 0
.regptrd   dd 0

NEWSYM transdmappu2cpu
    ; set address increment value
    mov word[.addrincr],0
    test al,00001000b
    jnz .skipaddrincr
    test al,00010000b
    jnz .autodec
    mov word[.addrincr],1
    jmp .skipaddrincr
.autodec
    mov word[.addrincr],0FFFFh
.skipaddrincr

    ; get address order to be written
    xor ebx,ebx
    and al,00000111b
    mov bl,al
    shl bl,3
    add ebx,.addrwrite
    mov edi,ebx

    ; get pointer #1
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi]
    shl ebx,2
    add ebx,[regptr]
    mov eax,[ebx]
    mov [.regptra],eax

    ; get pointer #2
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi+2]
    shl ebx,2
    add ebx,[regptr]
    mov eax,[ebx]
    mov [.regptrb],eax

    ; get pointer #3
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi+4]
    shl ebx,2
    add ebx,[regptr]
    mov eax,[ebx]
    mov [.regptrc],eax

    ; get pointer #4
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi+6]
    shl ebx,2
    add ebx,[regptr]
    mov eax,[ebx]
    mov [.regptrd],eax

    mov dx,[esi+5]      ; Number of bytes to transfer
    xor ebx,ebx
    mov bl,[esi+4]      ; Bank #
    xor ecx,ecx
    mov cx,[esi+8]      ; address offset # ?
    mov [.curbank],bl
    mov word[esi+5],0

.againloop
    call dword near [.regptra]
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtablew8+ebx*4]
    add cx,[.addrincr]
    dec dx
    jz .findma
    call dword near [.regptrb]
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtablew8+ebx*4]
    add cx,[.addrincr]
    dec dx
    jz .findma
    call dword near [.regptrc]
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtablew8+ebx*4]
    add cx,[.addrincr]
    dec dx
    jz .findma
    call dword near [.regptrd]
    xor ebx,ebx
    mov bl,[.curbank]
    call [memtablew8+ebx*4]
    add cx,[.addrincr]
    dec dx
    jnz near .againloop

.findma
    mov [esi+8],cx
    pop eax
    ret

.curbank   db 0
.addrwrite dw 0,0,0,0, 0,1,0,1, 0,0,0,0, 0,0,1,1, 0,1,2,3, 0,0,0,0, 0,0,0,0,
           dw 0,0,0,0
.addrincr  dw 0
; pointer address of registers
.regptra   dd 0
.regptrb   dd 0
.regptrc   dd 0
.regptrd   dd 0



    
; DMA enable register
; use dmadata for input on dma
NEWSYM reg420Bw                                                         ;0002D255
    push esi
    push edi
    push ecx
    push edx
    mov esi,dmadata
    test al,01h
    jz .notransa
    call transdma
.notransa
    add esi,16
    test al,02h
    jz .notransb
    call transdma
.notransb
    add esi,16
    test al,04h
    jz .notransc
    call transdma
.notransc
    add esi,16
    test al,08h
    jz .notransd
    call transdma
.notransd
    add esi,16
    test al,10h
    jz .notranse
    call transdma
.notranse
    add esi,16
    test al,20h
    jz .notransf
    call transdma
.notransf
    add esi,16
    test al,40h
    jz .notransg
    call transdma
.notransg
    add esi,16
    test al,80h
    jz .notransh
    call transdma
.notransh
    pop edx
    pop ecx
    pop edi
    pop esi
    ret

;*******************************************************
; HDMA Settings
;*******************************************************
NEWSYM setuphdma                                                        ;0002D2D5
    push eax

    ; transfer old address to new address
    mov ax,[esi+2]
    mov [esi+8],ax
    mov [edx+17],ax
    ; get address order to be written
    xor ebx,ebx
    xor eax,eax
    xor ecx,ecx
    mov al,[esi]
    and al,00000111b
    mov ah,[.addrnumt+eax]
    mov [edx+16],ah
    mov bl,al
    shl bl,3
    add ebx,.addrwrite
    mov edi,ebx

    ; get pointer #1
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [edx],eax

    ; get pointer #2
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi+2]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [edx+4],eax

    ; get pointer #3
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi+4]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [edx+8],eax

    ; get pointer #4
    xor ebx,ebx
    mov bl,[esi+1]      ; PPU memory - 21xx
    mov bh,21h
    add bx,[edi+6]
    shl ebx,2
    add ebx,[regptw]
    mov eax,[ebx]
    mov [edx+12],eax

    xor ebx,ebx
    mov byte[esi+10],0
    pop eax
    or [hdmatype],ah
    ret

.addrwrite dw 0,0,0,0, 0,1,0,1, 0,0,0,0, 0,0,1,1, 0,1,2,3, 0,1,2,3, 0,1,2,3
           dw 0,1,2,3
.addrnumt  db 1,2,2,4,4,4,4,4

NEWSYM reg420Cw                                                         ;0002D3BA
    mov [curhdma],al
    ret

; HDMA enable register
NEWSYM starthdma                                                        ;0002D3C0
    mov al,[curhdma]
NEWSYM startnexthdma
    mov [nexthdma],al
    cmp al,0
    je near .nohdma
    push ebx
    push esi
    push edi
    push ecx
    push edx
    mov esi,dmadata
    mov edx,hdmadata
    mov ah,01h
    test al,01h
    jz .notransa
    call setuphdma
.notransa
    add esi,16
    add edx,19
    mov ah,02h
    test al,02h
    jz .notransb
    call setuphdma
.notransb
    add esi,16
    add edx,19
    mov ah,04h
    test al,04h
    jz .notransc
    call setuphdma
.notransc
    add esi,16
    add edx,19
    mov ah,08h
    test al,08h
    jz .notransd
    call setuphdma
.notransd
    add esi,16
    add edx,19
    mov ah,10h
    test al,10h
    jz .notranse
    call setuphdma
.notranse
    add esi,16
    add edx,19
    mov ah,20h
    test al,20h
    jz .notransf
    call setuphdma
.notransf
    add esi,16
    add edx,19
    mov ah,40h
    test al,40h
    jz .notransg
    call setuphdma
.notransg
    add esi,16
    add edx,19
    mov ah,80h
    test al,80h
    jz .notransh
    call setuphdma
.notransh
    pop edx
    pop ecx
    pop edi
    pop esi
    pop ebx
.nohdma
    ret

NEWSYM dohdma
    xor ebx,ebx
    test byte[esi],40h
    jnz near indirectaddr
    push eax
    test byte[esi+10],07Fh
    jnz near .nozero
    test byte[esi+10],80h
    jnz near .noincr
    test [hdmatype],ah
    jnz .noincr
    mov bl,[edx+16]
    add word[edx+17],bx
.noincr
    mov bl,ah
    not bl
    and [hdmatype],bl
    mov bl,[esi+4]
    mov cx,[edx+17]
    call dword near [memtabler8+ebx*4]
    inc word[edx+17]
    mov byte[esi+10],al
    test al,0FFh
    jnz .yeszero
    xor [nexthdma],ah
    jmp .finhdma2
.yeszero
    cmp byte[esi+10],80h
    ja near hdmatype2
    mov al,[edx+16]
    mov [.tempdecr],al
    xor ebx,ebx
    xor ecx,ecx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    call dword near [memtabler8+ebx*4]
    call dword near [edx]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    inc cx
    call dword near [memtabler8+ebx*4]
    call dword near [edx+4]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    add cx,2
    call dword near [memtabler8+ebx*4]
    call dword near [edx+8]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    add cx,3
    call dword near [memtabler8+ebx*4]
    call dword near [edx+12]
    jmp .finhdma
.nozero
    test byte[esi+10],80h
    jnz near hdmatype2
.finhdma
    mov ax,[edx+17]
    mov [esi+8],ax
    pop eax
    dec byte[esi+10]
    ret
.finhdma2
    mov ax,[edx+17]
    mov [esi+8],ax
    pop eax
    ret

.tempdecr db 0

NEWSYM hdmatype2
    mov al,[edx+16]
    mov [.tempdecr],al
    xor ebx,ebx
    xor ecx,ecx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    inc word[edx+17]
    call dword near [memtabler8+ebx*4]
    call dword near [edx]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    inc word[edx+17]
    call dword near [memtabler8+ebx*4]
    call dword near [edx+4]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    inc word[edx+17]
    call dword near [memtabler8+ebx*4]
    call dword near [edx+8]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+4]
    mov cx,[edx+17]         ; increment/decrement/keep pointer location
    inc word[edx+17]
    call dword near [memtabler8+ebx*4]
    call dword near [edx+12]
.finhdma
    mov ax,[edx+17]
    mov [esi+8],ax
    pop eax
    dec byte[esi+10]
    ret

.tempdecr db 0

NEWSYM indirectaddr
    push eax
    test byte[esi+10],07Fh
    jnz near .nozero
    test [hdmatype],ah
    jnz .noincr
    add word[edx+17],2
.noincr
    mov bl,ah
    not bl
    and [hdmatype],bl
    mov bl,[esi+4]
    mov cx,[edx+17]
    call dword near [memtabler8+ebx*4]
    inc word[edx+17]
    mov byte[esi+10],al
    push eax
    mov bl,[esi+4]
    mov cx,[edx+17]
    call dword near [memtabler16+ebx*4]
    mov [esi+5],ax
    pop eax
    test al,0FFh
    jnz .yeszero
    xor [nexthdma],ah
    jmp .finhdma2
.yeszero
    cmp byte[esi+10],80h
    ja near hdmatype2indirect
    mov al,[edx+16]
    mov [.tempdecr],al
    xor ebx,ebx
    xor ecx,ecx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    call dword near [memtabler8+ebx*4]
    call dword near [edx]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    inc cx
    call dword near [memtabler8+ebx*4]
    call dword near [edx+4]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    add cx,2
    call dword near [memtabler8+ebx*4]
    call dword near [edx+8]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    add cx,3
    call dword near [memtabler8+ebx*4]
    call dword near [edx+12]
    jmp .finhdma
.nozero
    test byte[esi+10],80h
    jnz near hdmatype2indirect
.finhdma
    mov ax,[edx+17]
    mov [esi+8],ax
    pop eax
    dec byte[esi+10]
    ret
.finhdma2
    mov ax,[edx+17]
    mov [esi+8],ax
    pop eax
    ret

.tempdecr db 0
.fname2 db 9,'vram2.dat',0

NEWSYM hdmatype2indirect
    mov al,[edx+16]
    mov [.tempdecr],al
    xor ebx,ebx
    xor ecx,ecx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    inc word[esi+5]
    call dword near [memtabler8+ebx*4]
    call dword near [edx]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    inc word[esi+5]
    call dword near [memtabler8+ebx*4]
    call dword near [edx+4]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    inc word[esi+5]
    call dword near [memtabler8+ebx*4]
    call dword near [edx+8]
    dec byte[.tempdecr]
    jz .finhdma
    xor ebx,ebx
    mov bl,[esi+7]
    mov cx,[esi+5]         ; increment/decrement/keep pointer location
    inc word[esi+5]
    call dword near [memtabler8+ebx*4]
    call dword near [edx+12]
.finhdma
    pop eax
    dec byte[esi+10]
    ret

.tempdecr db 0

NEWSYM exechdma                                                         ;0002D7B8
    mov al,[nexthdma]
    cmp al,0
    je near .nohdma
    push ebx
    push esi
    push edi
    push ecx
    push edx
    mov esi,dmadata
    mov edx,hdmadata
    mov ah,01h
    test al,01h
    jz .notransa
    call dohdma
.notransa
    add esi,16
    add edx,19
    mov ah,02h
    test al,02h
    jz .notransb
    call dohdma
.notransb
    add esi,16
    add edx,19
    mov ah,04h
    test al,04h
    jz .notransc
    call dohdma
.notransc
    add esi,16
    add edx,19
    mov ah,08h
    test al,08h
    jz .notransd
    call dohdma
.notransd
    add esi,16
    add edx,19
    mov ah,10h
    test al,10h
    jz .notranse
    call dohdma
.notranse
    add esi,16
    add edx,19
    mov ah,20h
    test al,20h
    jz .notransf
    call dohdma
.notransf
    add esi,16
    add edx,19
    mov ah,40h
    test al,40h
    jz .notransg
    call dohdma
.notransg
    add esi,16
    add edx,19
    mov ah,80h
    test al,80h
    jz .notransh
    call dohdma
.notransh
    pop edx
    pop ecx
    pop edi
    pop esi
    pop ebx
.nohdma
    ret
