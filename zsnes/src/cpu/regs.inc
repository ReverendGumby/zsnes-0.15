;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


EXTSYM SFXEnable,regptr,initsfxregsr,initSA1regs,SA1Enable,initSDD1regs
EXTSYM SPC7110Enable,initSPC7110regs,RTCEnable,RTCReset
EXTSYM curypos,cycpl,debuggeron,pdh,Reg4212hack
EXTSYM vram
EXTSYM romispal
EXTSYM reg1read,spcon
EXTSYM reg2read
EXTSYM reg3read
EXTSYM reg4read
EXTSYM snesmouse
EXTSYM cycphb
EXTSYM DosExit,invalid,numinst,previdmode,printhex,printnum
EXTSYM pl1keya,pl1keyb,pl2contrl,pl2keya,pl2keyb






NEWSYM initregr                                                         ;00021DE3
    ; Fill register pointer with invalid register accesses
    inc byte[eax]               ; WHY?
    mov edi,[regptr]
    add edi,8000h
    mov ecx,3000h
    mov eax,regINVALID
.loopa
    mov [edi],eax
    add edi,4
    loop .loopa
    ; Set all valid register accesses
    setreg 2134h*4,reg2134r
    setreg 2135h*4,reg2135r
    setreg 2136h*4,reg2136r
    setreg 2137h*4,reg2137r
    setreg 2138h*4,reg2138r
    setreg 2139h*4,reg2139r
    setreg 213Ah*4,reg213Ar
    setreg 213Bh*4,reg213Br
    setreg 213Ch*4,reg213Cr
    setreg 213Dh*4,reg213Dr
    setreg 213Eh*4,reg213Er
    setreg 213Fh*4,reg213Fr
    setreg 2140h*4,reg2140r
    setreg 2141h*4,reg2141r
    setreg 2142h*4,reg2142r
    setreg 2143h*4,reg2143r
    setreg 2180h*4,reg2180r
    setreg 4016h*4,reg4016r
    setreg 4017h*4,reg4017r
    setreg 4210h*4,reg4210r
    setreg 4211h*4,reg4211r
    setreg 4212h*4,reg4212r
    setreg 4213h*4,reg4213r
    setreg 4214h*4,reg4214r
    setreg 4215h*4,reg4215r
    setreg 4216h*4,reg4216r
    setreg 4217h*4,reg4217r
    setreg 4218h*4,reg4218r
    setreg 4219h*4,reg4219r
    setreg 421Ah*4,reg421Ar
    setreg 421Bh*4,reg421Br
    setreg 421Ch*4,reg421Cr
    setreg 421Dh*4,reg421Dr
    setreg 421Eh*4,reg421Er
    setreg 421Fh*4,reg421Fr

    setreg 4300h*4,reg43XXr
    setreg 4301h*4,reg43XXr
    setreg 4302h*4,reg43XXr
    setreg 4303h*4,reg43XXr
    setreg 4304h*4,reg43XXr
    setreg 4305h*4,reg43XXr
    setreg 4306h*4,reg43XXr
    setreg 4307h*4,reg43XXr
    setreg 4308h*4,reg43XXr
    setreg 4309h*4,reg43XXr
    setreg 430Ah*4,reg43XXr
    setreg 4310h*4,reg43XXr
    setreg 4311h*4,reg43XXr
    setreg 4312h*4,reg43XXr
    setreg 4313h*4,reg43XXr
    setreg 4314h*4,reg43XXr
    setreg 4315h*4,reg43XXr
    setreg 4316h*4,reg43XXr
    setreg 4317h*4,reg43XXr
    setreg 4318h*4,reg43XXr
    setreg 4319h*4,reg43XXr
    setreg 431Ah*4,reg43XXr
    setreg 4320h*4,reg43XXr
    setreg 4321h*4,reg43XXr
    setreg 4322h*4,reg43XXr
    setreg 4323h*4,reg43XXr
    setreg 4324h*4,reg43XXr
    setreg 4325h*4,reg43XXr
    setreg 4326h*4,reg43XXr
    setreg 4327h*4,reg43XXr
    setreg 4328h*4,reg43XXr
    setreg 4329h*4,reg43XXr
    setreg 432Ah*4,reg43XXr
    setreg 4330h*4,reg43XXr
    setreg 4331h*4,reg43XXr
    setreg 4332h*4,reg43XXr
    setreg 4333h*4,reg43XXr
    setreg 4334h*4,reg43XXr
    setreg 4335h*4,reg43XXr
    setreg 4336h*4,reg43XXr
    setreg 4337h*4,reg43XXr
    setreg 4338h*4,reg43XXr
    setreg 4339h*4,reg43XXr
    setreg 433Ah*4,reg43XXr
    setreg 4340h*4,reg43XXr
    setreg 4341h*4,reg43XXr
    setreg 4342h*4,reg43XXr
    setreg 4343h*4,reg43XXr
    setreg 4344h*4,reg43XXr
    setreg 4345h*4,reg43XXr
    setreg 4346h*4,reg43XXr
    setreg 4347h*4,reg43XXr
    setreg 4348h*4,reg43XXr
    setreg 4349h*4,reg43XXr
    setreg 434Ah*4,reg43XXr
    setreg 4350h*4,reg43XXr
    setreg 4351h*4,reg43XXr
    setreg 4352h*4,reg43XXr
    setreg 4353h*4,reg43XXr
    setreg 4354h*4,reg43XXr
    setreg 4355h*4,reg43XXr
    setreg 4356h*4,reg43XXr
    setreg 4357h*4,reg43XXr
    setreg 4358h*4,reg43XXr
    setreg 4359h*4,reg43XXr
    setreg 435Ah*4,reg43XXr
    setreg 4360h*4,reg43XXr
    setreg 4361h*4,reg43XXr
    setreg 4362h*4,reg43XXr
    setreg 4363h*4,reg43XXr
    setreg 4364h*4,reg43XXr
    setreg 4365h*4,reg43XXr
    setreg 4366h*4,reg43XXr
    setreg 4367h*4,reg43XXr
    setreg 4368h*4,reg43XXr
    setreg 4369h*4,reg43XXr
    setreg 436Ah*4,reg43XXr
    setreg 4370h*4,reg43XXr
    setreg 4371h*4,reg43XXr
    setreg 4372h*4,reg43XXr
    setreg 4373h*4,reg43XXr
    setreg 4374h*4,reg43XXr
    setreg 4375h*4,reg43XXr
    setreg 4376h*4,reg43XXr
    setreg 4377h*4,reg43XXr
    setreg 4378h*4,reg43XXr
    setreg 4379h*4,reg43XXr
    setreg 437Ah*4,reg43XXr
    ret

; global variables
NEWSYM invreg,    dw 0                                                      ;000226AC
NEWSYM sndrot,    db 0             ; rotates to use A,X or Y for sound skip ;000226AE
NEWSYM sndrot2,   db 0             ; rotates a random value for sound skip  ;000226AF
NEWSYM INTEnab,   db 0             ; enables NMI(7)/VIRQ(5)/HIRQ(4)/JOY(0)  ;000226B0
NEWSYM NMIEnab,   db 1             ; controlled in e65816 loop.  Sets to 81h ;000226B1
NEWSYM VIRQLoc,   dw 0             ; VIRQ Y location                        ;000226B2
NEWSYM vidbright, db 0             ; screen brightness (0 .. 15)            ;000226B4
NEWSYM previdbr,  db 0             ; previous screen brightness             ;000226B5
NEWSYM forceblnk, db 80h           ; force blanking on/off ($80=on)         ;000226B6
NEWSYM objptr,    dd 0             ; pointer to object data in VRAM         ;000226B7
NEWSYM objptrn,   dd 0             ; pointer2 to object data in VRAM        ;000226BB
NEWSYM objsize1,  db 1             ; 1 = 8dot, 4=16 dot, 16=32 dot, 64=64dot ;000226BF
NEWSYM objsize2,  db 4             ; large object size                      ;000226C0
NEWSYM objmovs1,  db 2             ; number of bytes to move/paragraph      ;000226C1
NEWSYM objadds1,  dw 14            ; number of bytes to add/paragraph       ;000226C2
NEWSYM objmovs2,  db 2             ; number of bytes to move/paragraph      ;000226C4
NEWSYM objadds2,  dw 14            ; number of bytes to add/paragraph       ;000226C5
NEWSYM oamaddr,   dw 0             ; oam address                            ;000226C7
NEWSYM oamaddrs,  dw 0             ; oam address at beginning of vblank     ;000226C9
NEWSYM objhipr,   db 0             ; highest priority object #              ;000226CB
NEWSYM bgmode,    db 0             ; graphics mode ( 0 .. 7 )               ;000226CC
NEWSYM bg3highst, db 0             ; is 1 if background 3 has the highest priority ;000226CD
NEWSYM bgtilesz,  db 0             ; 0 = 8x8, 1 = 16x16, bit 0=bg1,bit1=bg2,etc ;000226CE
NEWSYM mosaicon,  db 0             ; mosaic on, bit 0=bg1,bit1=bg2, etc     ;000226CF
NEWSYM mosaicsz,  db 0             ; mosaic size in pixels                  ;000226D0
NEWSYM bg1ptr,    dw 0             ; pointer to background1                 ;000226D1
NEWSYM bg2ptr,    dw 0             ; pointer to background2
NEWSYM bg3ptr,    dw 0             ; pointer to background3
NEWSYM bg4ptr,    dw 0             ; pointer to background4
NEWSYM bg1ptrb,   dw 0             ; pointer to background1                 ;000226D9
NEWSYM bg2ptrb,   dw 0             ; pointer to background2
NEWSYM bg3ptrb,   dw 0             ; pointer to background3
NEWSYM bg4ptrb,   dw 0             ; pointer to background4
NEWSYM bg1ptrc,   dw 0             ; pointer to background1                 ;000226E1
NEWSYM bg2ptrc,   dw 0             ; pointer to background2
NEWSYM bg3ptrc,   dw 0             ; pointer to background3
NEWSYM bg4ptrc,   dw 0             ; pointer to background4
NEWSYM bg1ptrd,   dw 0             ; pointer to background1                 ;000226E9
NEWSYM bg2ptrd,   dw 0             ; pointer to background2
NEWSYM bg3ptrd,   dw 0             ; pointer to background3
NEWSYM bg4ptrd,   dw 0             ; pointer to background4
NEWSYM bg1scsize, db 0             ; bg #1 screen size (0=1x1,1=1x2,2=2x1,3=2x2)    ;000226F1
NEWSYM bg2scsize, db 0             ; bg #2 screen size (0=1x1,1=1x2,2=2x1,3=2x2)
NEWSYM bg3scsize, db 0             ; bg #3 screen size (0=1x1,1=1x2,2=2x1,3=2x2)
NEWSYM bg4scsize, db 0             ; bg #4 screen size (0=1x1,1=1x2,2=2x1,3=2x2)
NEWSYM bg1objptr, dw 0             ; pointer to tiles in background1        ;000226F5
NEWSYM bg2objptr, dw 0             ; pointer to tiles in background2
NEWSYM bg3objptr, dw 0             ; pointer to tiles in background3
NEWSYM bg4objptr, dw 0             ; pointer to tiles in background4
NEWSYM bg1scrolx, dw 0             ; background 1 x position                ;000226FD
NEWSYM bg2scrolx, dw 0             ; background 2 x position
NEWSYM bg3scrolx, dw 0             ; background 3 x position
NEWSYM bg4scrolx, dw 0             ; background 4 x position
NEWSYM bg1sx,     dw 0             ; Temporary Variable for Debugging purposes
NEWSYM bg1scroly, dw 0             ; background 1 y position                ;00022708
NEWSYM bg2scroly, dw 0             ; background 2 y position
NEWSYM bg3scroly, dw 0             ; background 3 y position
NEWSYM bg4scroly, dw 0             ; background 4 y position
NEWSYM addrincr,  dw 2             ; vram increment (2,64,128,256)          ;0002270F
NEWSYM vramincr,  db 0             ; 0 = inrement at 2118/2139, 1 = 2119,213A ;00022711
NEWSYM vramread,  db 0             ; 0 = address set, 1 = already read once ;00022712
NEWSYM vramaddr,  dd 0             ; vram address                           ;00022713
NEWSYM cgaddr,    dw 0             ; cg (palette) address                   ;00022717
NEWSYM cgmod,     db 0             ; if cgram is modified or not            ;00022719
NEWSYM scrnon,    dw 0             ; main & sub screen on
NEWSYM scrndis,   db 0             ; which background is disabled       ;0002271C
NEWSYM resolutn,  dw 224           ; screen resolution                  ;0002271D
NEWSYM multa,     db 0             ; multiplier A                       ;0002271F
NEWSYM diva,      dw 0             ; divisor C
NEWSYM divres,    dw 0             ; quotent of divc/divb               ;00022722
NEWSYM multres,   dw 0             ; result of multa * multb/remainder of divc/divb ;00022724
NEWSYM latchx,    dw 0             ; latched x value
NEWSYM latchy,    dw 0             ; latched y value                    ;00022728
NEWSYM latchxr,   db 0             ; low or high byte read for x value
NEWSYM latchyr,   db 0             ; low or high byte read for y value
NEWSYM frskipper, db 0             ; used to control frame skipping
NEWSYM winl1,     db 0             ; window 1 left position             ;0002272D
NEWSYM winr1,     db 0             ; window 1 right position
NEWSYM winl2,     db 0             ; window 2 left position
NEWSYM winr2,     db 0             ; window 2 right position
NEWSYM winbg1en,  db 0             ; Win1 on (IN/OUT) or Win2 on (IN/OUT) on BG1    ;00022731
NEWSYM winbg2en,  db 0             ; Win1 on (IN/OUT) or Win2 on (IN/OUT) on BG2
NEWSYM winbg3en,  db 0             ; Win1 on (IN/OUT) or Win2 on (IN/OUT) on BG3
NEWSYM winbg4en,  db 0             ; Win1 on (IN/OUT) or Win2 on (IN/OUT) on BG4
NEWSYM winobjen,  db 0             ; Win1 on (IN/OUT) or Win2 on (IN/OUT) on sprites
NEWSYM wincolen,  db 0             ; Win1 on (IN/OUT) or Win2 on (IN/OUT) on backarea
NEWSYM winlogica, db 0             ; Window logic type for BG1 to 4         ;00022737
NEWSYM winlogicb, db 0             ; Window logic type for Sprites and Backarea
NEWSYM winenabm,  db 0             ; Window logic enable for main screen    ;00022739
NEWSYM winenabs,  db 0             ; Window logic enable for sub screen
NEWSYM mode7set,  db 0             ; mode 7 settings                        ;0002273B
NEWSYM mode7A,    dw 0             ; A value for Mode 7                     ;0002273C
NEWSYM mode7B,    dw 0             ; B value for Mode 7                     ;0002273E
NEWSYM mode7C,    dw 0             ; C value for Mode 7                     ;00022740
NEWSYM mode7D,    dw 0             ; D value for Mode 7                     ;00022742
NEWSYM mode7X0,   dw 0             ; Center X for Mode 7                     ;00022744
NEWSYM mode7Y0,   dw 0             ; Center Y for Mode 7                     ;00022746
NEWSYM JoyAPos,   db 0             ; Old-Style Joystick Read Position for Joy 1 & 3 ;00022748
NEWSYM JoyBPos,   db 0             ; Old-Style Joystick Read Position for Joy 2 & 4 ;00022749
NEWSYM compmult,  dd 0             ; Complement Multiplication for Mode 7   ;0002274A
NEWSYM joyalt,    db 0             ; temporary joystick alternation         ;0002274E

NEWSYM wramrwadr, dd 0             ; continuous read/write to wram address  ;0002274F
NEWSYM dmadata, times 129 db 0h  ; dma data (written from ports 43xx)       ;00022753
NEWSYM irqon,     db 0             ; if IRQ has been called (80h) or not (0)
NEWSYM nexthdma,  db 0             ; HDMA data to execute once vblank ends  ;000227D5
NEWSYM curhdma,   db 0             ; Currently executed hdma                ;000227D6
NEWSYM hdmadata, times 8*19 db 0   ; 4 dword register addresses, # of bytes to  ;000227D7
                           ; transfer/line, address increment (word)
NEWSYM hdmatype,  db 0             ; if first time executing hdma or not    ;0002286F
NEWSYM coladdr,   db 0             ; red value of color to add              ;00022870
NEWSYM coladdg,   db 0             ; green value of color to add
NEWSYM coladdb,   db 0             ; blue value of color to add
NEWSYM colnull,   db 0             ; keep this 0 (when accessing colors by dword)
NEWSYM scaddset,  db 0             ; screen/fixed color addition settings   ;00022874
NEWSYM scaddtype, db 0             ; which screen to add/sub                ;00022875
NEWSYM Voice0Disable, db 1         ; Disable Voice 0                        ;00022876
NEWSYM Voice1Disable, db 1         ; Disable Voice 1                        ;00022877
NEWSYM Voice2Disable, db 1         ; Disable Voice 2                        ;00022878
NEWSYM Voice3Disable, db 1         ; Disable Voice 3                        ;00022879
NEWSYM Voice4Disable, db 1         ; Disable Voice 4                        ;0002287A
NEWSYM Voice5Disable, db 1         ; Disable Voice 5                        ;0002287B
NEWSYM Voice6Disable, db 1         ; Disable Voice 6                        ;0002287C
NEWSYM Voice7Disable, db 1         ; Disable Voice 7                        ;0002287D

NEWSYM oamram,  times 1024 db 0    ; OAMRAM (544 bytes)                     ;0002287E
NEWSYM cgram,   times 512 db 0     ; CGRAM                                  ;00022C7E
NEWSYM pcgram,  times 512 db 0     ; Previous CGRAM                         ;00022E7E

NEWSYM vraminctype,  db 0                                                   ;0002307E

; New variables
;NEWSYM vramincby8on,   db 0        ; if increment by 8 is on                ;000
;NEWSYM vramincby8left, db 0        ; how many left
;NEWSYM vramincby8totl, db 0        ; how many in total (32,64,128)
;NEWSYM vramincby8rowl, db 0        ; how many left in that row (start at 8)
;NEWSYM vramincby8ptri, dw 0        ; increment by how many when rowl = 0

;NEWSYM nexthprior,     db 0
;NEWSYM doirqnext,      db 0
;
;NEWSYM vramincby8var,  dw 0
;NEWSYM screstype,      db 0
;NEWSYM extlatch,       db 0
;NEWSYM cfield,         db 0
;NEWSYM interlval,      db 0

;NEWSYM HIRQLoc,   dw 0             ; HIRQ X location
;
;NEWSYM KeyOnStA, db 0
;NEWSYM KeyOnStB, db 0
;
NEWSYM tempdat, times 506 db 0     ; expandable area

num2writeppureg equ $-sndrot

;NEWSYM scrndis,   db 0             ; which background is disabled

;NEWSYM oamaddr,   dd 0             ; oam address

;NEWSYM bg1ptrx,   dd 0             ; pointer to background1
;NEWSYM bg2ptrx,   dd 0             ; pointer to background2
;NEWSYM bg3ptrx,   dd 0             ; pointer to background3
;NEWSYM bg4ptrx,   dd 0             ; pointer to background4

;NEWSYM bg1ptry,   dd 0             ; pointer to background1
;NEWSYM bg2ptry,   dd 0             ; pointer to background2
;NEWSYM bg3ptry,   dd 0             ; pointer to background3
;NEWSYM bg4ptry,   dd 0             ; pointer to background4

;NEWSYM BG116x16t, db 0
;NEWSYM BG216x16t, db 0
;NEWSYM BG316x16t, db 0
;NEWSYM BG416x16t, db 0
;NEWSYM SPC700read, dd 0
;NEWSYM SPC700write, dd 0

; Multiply Result Low
reg2134r:                                                               ;00023279
    mov al,[compmult]
    ret

; Multiply Result Middle
reg2135r:                                                               ;0002327F
    mov al,[compmult+1]
    ret

; Multiply Result High                                                  ;00023285
reg2136r:
    mov al,[compmult+2]
    ret

; Software latch for horizontal/vertical counter
reg2137r:                                                               ;0002328B
    push ax
    push dx
    xor eax,eax
    xor ebx,ebx
    mov al,dh
    mov bx,339
    mul bx
    xor ebx,ebx
    mov bl,[cycpl]
    div bx
    mov [latchx],ax
    pop dx
    pop ax
    mov bx,[curypos]
    mov [latchy],bx
    xor al,al
    ret

; Read OAM Data (Low, High)
reg2138r:                                                               ;000232C2
    xor ebx,ebx
    mov bx,word[oamaddr]
    add ebx,oamram
    mov al,[ebx]
    inc word[oamaddr]
    cmp word[oamaddr],543
    ja .wrapoam
    ret
.wrapoam
    mov word[oamaddr],0
    ret

; Read VRAM Data (Low)
reg2139r:                                                               ;000232F0
    xor ebx,ebx
    mov bx,[vramaddr]
    add ebx,[vram]
    mov al,[ebx]
    cmp byte[vramincr],0
    je .noincr
    cmp byte[vramread],0
    je .noincr2
    mov bx,[addrincr]
    add [vramaddr],bx
.noincr
    ret
.noincr2
    mov byte[vramread],1
    ret

; Read VRAM Data (High)
reg213Ar:                                                               ;0002332A
    xor ebx,ebx
    mov bx,[vramaddr]
    add ebx,[vram]
    mov al,[ebx+1]
    cmp byte[vramincr],1
    je .noincr
    cmp byte[vramread],0
    je .noincr2
    mov bx,[addrincr]
    add [vramaddr],bx
.noincr
    ret
.noincr2
    mov byte[vramread],1
    ret

; Read CGRAM Data
reg213Br:                                                               ;00023365
    xor ebx,ebx
    mov bx,[cgaddr]
    mov al,[cgram+ebx]
    inc word[cgaddr]
    and word[cgaddr],01FFh
    ret

; H counter data by external or software latch
reg213Cr:                                                               ;00023385
    cmp byte[latchxr],1
    je .highv
    mov al,byte[latchx]
    mov byte[latchxr],1
    ret
.highv
    mov al,byte[latchx+1]
    mov byte[latchxr],0
    ret

; V counter data by external or software latch
reg213Dr:                                                               ;000233A8
    cmp byte[latchyr],1
    je .highv
    mov al,byte[latchy]
    mov byte[latchyr],1
    ret
.highv
    mov al,byte[latchy+1]
    mov byte[latchyr],0
    ret

; PPU Status Flag & Version number (OBJ over flags)
reg213Er:                                                               ;000233CB
    mov al,01h
    ret

; PPU Status Flag & Version number (NTSC/PAL/EXTRN Latch flag)
reg213Fr:                                                               ;000233CE
    mov al,[romispal]
    shl al,4
    or al,01h
    mov byte[latchxr],0
    mov byte[latchyr],0
    ret

; Sound Reg #1
reg2140r:                                                               ;000233E7
    cmp byte[spcon],0
    je .nosound
    mov al,[reg1read]
    ret
.nosound
    ; Find for D0
    mov ebx,esi
    mov al,5
.tryagain
    cmp byte[ebx],0D0h
    je .foundit
    inc ebx
    dec al
    jnz .tryagain
    jmp .notfound
.foundit
    mov byte[ebx],0EAh
    mov byte[ebx+1],0EAh
.notfound
    inc byte[sndrot2]
    cmp byte[sndrot2],3
    jne .a
    mov byte[sndrot2],0
.a
    xor al,al
    test byte[sndrot2],01h
    jz .n
    mov al,[xa]
.n
    ret

; Sound Reg #2
reg2141r:                                                               ;00023437
    cmp byte[spcon],0
    je .nosound
    mov al,[reg2read]
    ret
.nosound
    ; Find for D0
    mov ebx,esi
    mov al,3
.tryagain
    cmp byte[ebx],0D0h
    je .foundit
    inc ebx
    dec al
    jnz .tryagain
    jmp .notfound
.foundit
    mov byte[ebx],0EAh
    mov byte[ebx+1],0EAh
.notfound
    xor byte[sndrot],01h
    mov al,[xa+1]
    test byte[sndrot],01h
    jz .n
    mov al,[xa]
.n
    ret

; Sound Reg #3
reg2142r:                                                               ;0002347B
    cmp byte[spcon],0
    je .nosound
    mov al,[reg3read]
    ret
.nosound
    ; Find for D0
    mov ebx,esi
    mov al,3
.tryagain
    cmp byte[ebx],0D0h
    je .foundit
    inc ebx
    dec al
    jnz .tryagain
    jmp .notfound
.foundit
    mov byte[ebx],0EAh
    mov byte[ebx+1],0EAh
.notfound
    mov al,[xa]
    test byte[sndrot],01h
    jz .n
    mov al,[xa+1]
.n
    ret

; Sound Reg #4
reg2143r:                                                               ;000234B8
    cmp byte[spcon],0
    je .nosound
    mov al,[reg4read]
    ret
.nosound
    ; Find for D0
    mov ebx,esi
    mov al,3
.tryagain
    cmp byte[ebx],0D0h
    je .foundit
    inc ebx
    dec al
    jnz .tryagain
    jmp .notfound
.foundit
    mov byte[ebx],0EAh
    mov byte[ebx+1],0EAh
.notfound
    mov al,[xa+1]
    ret

; WRAM Read
reg2180r:                                                               ;000234E7
    mov ebx,[wramrwadr]
    add ebx,[wramdata]
    mov al,[ebx]
    inc dword[wramrwadr]
    and dword[wramrwadr],01FFFFh
    ret

%macro JoyHelp 4
    cmp byte[%1],%2
    jne %%yes
    test byte[%3],%4
    je %%yes
    mov al,1
%%yes
%endmacro

; Joystick Data for controller 1 and 2

reg4016r:                                                               ;00023506
    xor al,al
    JoyHelp JoyAPos, 0,  pl1keya, 080h
    JoyHelp JoyAPos, 1,  pl1keya, 040h
    JoyHelp JoyAPos, 2,  pl1keya, 020h
    JoyHelp JoyAPos, 3,  pl1keya, 010h
    JoyHelp JoyAPos, 4,  pl1keya, 008h
    JoyHelp JoyAPos, 5,  pl1keya, 004h
    JoyHelp JoyAPos, 6,  pl1keya, 002h
    JoyHelp JoyAPos, 7,  pl1keya, 001h
    JoyHelp JoyAPos, 8,  pl1keyb, 080h
    JoyHelp JoyAPos, 9,  pl1keyb, 040h
    JoyHelp JoyAPos, 10, pl1keyb, 020h
    JoyHelp JoyAPos, 11, pl1keyb, 010h
    cmp byte[JoyAPos], 16
    jne .not16
    or al,1
.not16
    cmp byte[JoyAPos], 20
    jne .not20
    dec byte[JoyAPos]
.not20
    inc byte[JoyAPos]
    mov al,1
    or al,10h
    ret

; Joystick Data for controller 2 and 4
reg4017r:                                                               ;0002361D
    xor al,al
    cmp byte[pl2contrl],0
    je .done
    xor al,al
    JoyHelp JoyBPos, 0,  pl2keya, 080h
    JoyHelp JoyBPos, 1,  pl2keya, 040h
    JoyHelp JoyBPos, 2,  pl2keya, 020h
    JoyHelp JoyBPos, 3,  pl2keya, 010h
    JoyHelp JoyBPos, 4,  pl2keya, 008h
    JoyHelp JoyBPos, 5,  pl2keya, 004h
    JoyHelp JoyBPos, 6,  pl2keya, 002h
    JoyHelp JoyBPos, 7,  pl2keya, 001h
    JoyHelp JoyBPos, 8,  pl2keyb, 080h
    JoyHelp JoyBPos, 9,  pl2keyb, 040h
    JoyHelp JoyBPos, 10, pl2keyb, 020h
    JoyHelp JoyBPos, 11, pl2keyb, 010h
    cmp byte[JoyBPos], 16
    jne .not16
    or al,1
.not16
    cmp byte[JoyBPos], 20
    jne .not20
    dec byte[JoyBPos]
.not20
    inc byte[JoyBPos]
    mov al,1
    or al,10h
.done
    ret

; NMI Check Register
reg4210r:                                                               ;00023743
    mov al,[NMIEnab]
    cmp byte[curnmi],0
    jne .nmi
    mov byte[NMIEnab],01h
.nmi
    ret

; Video IRQ Register
reg4211r:                                                               ;00023759
    mov al,[irqon]
    mov byte[irqon],0
    ret

; H/V Blank Flag & Joystick Controller Enable Flag
; bit 7 = vblank, 0=out,1=in, bit 6 = hblank, 0=out,1=in, bit 0 = joystick on
reg4212r:                                                               ;00023766
    xor al,al
;    cmp byte[debuggeron],1
;    je .debugger
    mov bx,[resolutn]
    cmp word[curypos],bx
    jb .novbl
    or al,80h
    jmp .nohblank
.novbl
    cmp dh,[cycphb]
    jae .nohblank
    or al,40h
.nohblank
    xor byte[joyalt],01h
    or al,[joyalt]
    ret

; Programmable I/O port
reg4213r:                                                               ;00023797
    xor al,al
    ret

; Quotent of Divide Result (Low)
reg4214r:                                                               ;0002379A
    mov al,[divres]
    ret

; Quotent of Divide Result (High)
reg4215r:                                                               ;000237A0
    mov al,[divres+1]
    ret

; Product of Multiplication Result or Remainder of Divide Result (Low)
reg4216r:                                                               ;000237A6
    mov al,[multres]
    ret

; Product of Multiplication Result or Remainder of Divide Result (High)
reg4217r:                                                               ;000237AC
    mov al,[multres+1]
    ret

; Joystick 1 Low
; bit7=X,bit6=Y,bit5=L,bit4=R
reg4218r:                                                               ;000237B2
    mov al,[pl1keyb]
    ret

; Joystick 1 High
; bit7=A,bit6=B,bit5=Sel,bit4=Start,bit3=up,bit2=down,bit1=left,bit0=right
reg4219r:                                                               ;000237B8
    mov al,[pl1keya]
    ret

; Joystick 2 Low
reg421Ar:                                                               ;000237BE
    mov al,[pl2keyb]
    ret

; Joystick 2 High
reg421Br:                                                               ;000237C4
    mov al,[pl2keya]
    ret

; Joystick 3 Low
reg421Cr:                                                               ;000237CA
    xor al,al
    ret

; Joystick 3 High
reg421Dr:                                                               ;000237CD
    xor al,al
    ret

; Joystick 4 Low
reg421Er:                                                               ;000237D0
    xor al,al
    ret

; Joystick 4 High
reg421Fr:                                                               ;000237D3
    xor al,al
    ret

; DMA Reader
reg43XXr:                                                               ;000237D6
    xor ebx,ebx
    mov bx,cx
    sub bx,4300h
    mov al,[dmadata+ebx]
    ret

regINVALID:     ; Invalid Register                                      ;000237E7
    xor al,al
;    mov al,30h
    ret
    mov al,[previdmode]
    mov ah,0
    int 10h
    mov byte[invalid],1
    mov [invreg],cx
    mov ah,9
    mov edx,.invalidreg
    int 21h
    xor eax,eax
    mov ax,[invreg]
    call printhex
    mov ah,2
    mov dl,13
    int 21h
    mov ah,2
    mov dl,10
    int 21h
    mov eax,[numinst]          ;Temporary
    call printnum
    jmp DosExit

.invalidreg db 'Invalid Read Register : $'

