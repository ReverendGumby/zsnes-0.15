;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTERN spcsave,dspsave
EXTSYM StringLength
EXTSYM Get_Time
EXTSYM objhipr
EXTSYM KeyRewind
EXTSYM xa,timer2upd,prevoamptr,ReadHead
EXTSYM prevedi,SA1xpc,SA1RAMArea,sa1dmaptr
EXTSYM DSP1COp,C4WFXVal,C41FXVal,Op00Multiplicand,Op10Coefficient,Op04Angle
EXTSYM Op08X,Op18X,Op28X,Op0CA,Op1CAZ,Op02FX,Op0AVS,Op06X,Op0DX,Op03F,Op14Zr
EXTSYM Op0EH,DSP1Type,Op01m
EXTSYM Voice0Status
EXTSYM Interror,MessageOn,MsgCount,Msgptr,StartGUI,cbitmode,debuggeron,romdata
EXTSYM frameskip,initvideo,newgfx16b,oldhandSBo,oldhandSBs,soundon,cvidmode
EXTSYM nextframe
EXTSYM vidbuffer,vidbufferofsa,vidbufferofsb,disable65816sh,GUISaveVars,virqnodisable
EXTSYM KeySaveState,KeyLoadState,KeyQuickExit,KeyQuickLoad,KeyQuickRst,GUIDoReset
EXTSYM KeyOnStA,KeyOnStB,ProcessKeyOn,printnum,sramsavedis,DSPDisable,C4Enable
EXTSYM IRQHack,HIRQLoc,Offby1line,KeyQuickSnapShot
EXTSYM csounddisable,videotroub,Open_File,Close_File,Read_File,ResetTripleBuf
EXTSYM Write_File,Output_Text,Create_File,Check_Key,Get_Key,Change_Dir,InitPreGame
EXTSYM OSPort
;    EXTSYM tempblah,romdata
EXTSYM Curtableaddr,DeInitSPC,InitSB,PICMaskP,SBHandler,SBInt
EXTSYM SBIrq,curcyc,debugdisble,dmadata,guioff,memtabler8,SetupPreGame
EXTSYM memtablew8,regaccessbankr8,showmenu,snesmap2,snesmmap,DeInitPostGame
EXTSYM spcPCRam,startdebugger,xp,xpb,xpc,tablead,tableadb,tableadc
;    EXTSYM oamram
EXTSYM memtabler16,memaccessbankr848mb,memaccessbankr1648mb
EXTSYM nextmenupopup
EXTSYM MovieProcessing
EXTSYM MovieFileHand, PrintStr
EXTSYM OSExit,DosExit,InitDir,InitDrive,createnewcfg,fnames,gotoroot,previdmode
EXTSYM ramsize,sfxramdata,sram,SRAMDrive,SRAMDir,welcome
;    EXTSYM tempstore
EXTSYM printhex
EXTSYM ModemInitStat, DeInitModem
EXTSYM deinitipx
EXTSYM deinitvideo
EXTSYM BRRBuffer,DSPMem,PrepareSaveState,ResetState
EXTSYM fnamest,sndrot,spcRam,spcRamDP,tableA,unpackfunct,vram,wramdata
EXTSYM zsmesg,num2writecpureg,initrevst
EXTSYM C4Ram
EXTSYM SPC7110Enable
EXTSYM SA1Mode,PHnum2writesa1reg,SaveSA1,RestoreSA1,UpdateBanksSDD1
EXTSYM SDD1Enable
EXTSYM CapturePicture,PrevPicture,NoPictureSave
EXTSYM BRRPlace0,SfxCPB,SfxCROM,SfxLastRamAdr,SfxMemTable,Totalbyteloaded
EXTSYM SfxRAMBR,SfxRAMMem,SfxROMBR,SfxRomBuffer,Voice0Freq
EXTSYM cycpbl,cycpbl2,cycpblt,cycpblt2,irqon,nexthdma
EXTSYM repackfunct,spcon,versn,headerhack,initpitch
EXTSYM SPCMultA,PHnum2writespc7110reg
EXTSYM multchange,vidmemch2
EXTSYM romispal
EXTSYM dssel
EXTSYM scrndis,sprlefttot,sprleftpr,processsprites,cachesprites
EXTSYM NextLineStart,FlipWait,LastLineStart
EXTSYM opcjmptab
EXTSYM cpuoverptr
EXTSYM CheatOn,INTEnab,JoyAPos,JoyBPos,JoyCRead,NMIEnab,NumCheats,CurrentExecSA1
EXTSYM ReadInputDevice,StartDrawNewGfx,VIRQLoc,cachevideo,cfield
EXTSYM cheatdata,curblank,curnmi,curypos,cycpl,doirqnext,drawline
EXTSYM execatzerovirq,exechdma,hdmadelay,intrset,newengen,oamaddr
EXTSYM oamaddrs,processmouse,resolutn,showvideo,snesmouse,starthdma
EXTSYM switchtonmi,switchtovirq,totlines,updatetimer,SA1Swap,SA1DoIRQ
EXTSYM JoyAOrig,JoyANow,JoyBOrig,JoyBNow,JoyCOrig,JoyCNow,JoyDOrig,JoyDNow
EXTSYM JoyEOrig,JoyENow,chaton,chatstrL,chatRTL,chatstrR,SA1Message
EXTSYM MultiTapStat,MovieCounter,idledetectspc,SA1Control,SA1Enable,SA1IRQEnable
EXTSYM SPC700read,SPC700write,numspcvblleft,spc700idle,SA1Status,SA1IRQExec
EXTSYM ForceNewGfxOff,LethEnData,C4Pause,GUIQuit
EXTSYM IRAM,SA1Ptr,SA1BWPtr
EXTSYM scrnon,scaddset
EXTSYM outofmemfix,yesoutofmemory
EXTSYM CNetType,Latency,LatencyLeft,NetSwap
;    EXTSYM vesa2selec
EXTSYM RemoteSendChar,RemoteGetChar,pl1neten,pl2neten,pl3neten,pl4neten
EXTSYM pl5neten,RemoteSendEAX,prevp1net,prevp2net,prevp3net,prevp4net
EXTSYM RemoteGetEAX,cnetplaybuf,netdelayed,cnetptrtail,cnetptrhead
EXTSYM ChatProgress,RecvProgress,chatTL,WritetochatBuffer,NetAddChar
EXTSYM PreparePacket, SendPacket, NoInputRead, RemoteDisconnect
EXTSYM SendPacketUDP
EXTSYM ChatNick
EXTSYM JoyRead,ChatType2,chatstrR2,chatstrR3,chatstrR4,chatstrR5
EXTSYM chatRTL2,chatRTL3,chatRTL4,chatRTL5
EXTSYM NetLoadState
EXTSYM ProcessMovies
EXTSYM C4VBlank
EXTSYM dsp1teststuff
EXTSYM ReturnFromSPCStall,SPCStallSetting,cycpb268,cycpb358,HIRQSkip,scanlines
EXTSYM smallscreenon,ScreenScale
EXTSYM MainLoop,NumberOfOpcodes,SfxCLSR,SfxSCMR,SfxPOR
EXTSYM sfx128lineloc,sfx160lineloc,sfx192lineloc,sfxobjlineloc,sfxclineloc
EXTSYM PLOTJmpa,PLOTJmpb,FxTable,FxTableb,FxTablec,FxTabled
EXTSYM SfxPBR,SCBRrel,SfxSCBR,SfxCOLR,hdmaearlstart,SFXCounter
EXTSYM fxbit01,fxbit01pcal,fxbit23,fxbit23pcal,fxbit45,fxbit45pcal,fxbit67,fxbit67pcal
EXTSYM SfxSFR
EXTSYM cpucycle,debstop,switchtonmideb
EXTSYM changeexecloop

NEWSYM start65816                                                       ;00016611
    mov byte[debuggeron],0

    call initvideo

    ; clear keyboard presses
    mov esi,pressed
    mov ecx,128
    mov al,0
.loopa
    mov [esi],al
    inc esi
    loop .loopa

    mov byte[exiter],0

    ; set up interrupt handler
    ; get old handler pmode mode address
    ; Process stuff such as sound init, interrupt initialization
    mov ax,204h
    mov bl,09h
    int 31h
    jc near interror
    mov [oldhand9s],cx
    mov [oldhand9o],edx
    mov ax,204h
    mov bl,08h
    int 31h
    jc near interror
    mov [oldhand8s],cx
    mov [oldhand8o],edx
.nofs
    cmp byte[soundon],0
    je .nosound
    mov ax,204h
    mov bl,[SBInt]
    int 31h
    jc near Interror
    mov [oldhandSBs],cx
    mov [oldhandSBo],edx
.nosound

NEWSYM reexecute                                                        ;00016A1E
    ; set new handler
    cmp byte[soundon],0
    je .nosound2
    mov ax,205h
    mov bl,[SBInt]
    mov ecx,cs
    mov edx,SBHandler
    int 31h
    jc near interror

    call InitSB

.nosound2
    mov ax,205h
    mov bl,09h
    mov ecx,cs			; Requires CS rather than DS
    mov edx,handler9h
    int 31h
    jc near interror

    mov ax,205h
    mov bl,08h
    mov ecx,cs			; Requires CS rather than DS
    mov edx,handler8h
    int 31h
    jc near interror
    call init60hz               ; Set timer to 60/50Hz
.nofs2

    xor eax,eax
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    jmp .skiplower
.loweraddr
    mov esi,[snesmap2+ebx*4]
.skiplower
    mov [initaddrl],esi
    add esi,eax                 ; add program counter to address
    mov dl,[xp]                 ; set flags
    mov dh,[curcyc]             ; set cycles

    mov edi,[Curtableaddr]
    mov ebp,[spcPCRam]

    call execute

    ; de-init variables (copy to variables)

    mov [spcPCRam],ebp
    mov [Curtableaddr],edi
    mov [xp],dl
    mov [curcyc],dh

    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr2
    mov eax,[snesmmap+ebx*4]
    jmp .skiplower2
.loweraddr2
    mov eax,[snesmap2+ebx*4]
.skiplower2
    sub esi,eax                 ; subtract program counter by address
    mov [xpc],si

    ; de-init interrupt handler
    mov cx,[oldhand9s]
    mov edx,[oldhand9o]
    mov ax,205h
    mov bl,09h
    int 31h
    jc near interror

    mov cx,[oldhand8s]
    mov edx,[oldhand8o]
    mov ax,205h
    mov bl,08h
    int 31h
    jc near interror

    ; DeINITSPC
    cmp byte[soundon],0
    je .nosoundb
    call DeInitSPC
    mov cx,[oldhandSBs]
    mov edx,[oldhandSBo]
    mov ax,205h
    mov bl,[SBInt]
    int 31h
    jc near interror
.nosoundb

;    ; clear all keys
;    call Check_Key
;    cmp al,0
;    je .nokeys
;.yeskeys
;    call Get_Key
;    call Check_Key
;    cmp al,0
;    jne .yeskeys
;.nokeys

;    mov edi,memtabler8+40h*4
;    mov ecx,30h
;    mov eax,memaccessbankr848mb
;    rep stosd
;    mov edi,memtabler16+40h*4
;    mov ecx,30h
;    mov eax,memaccessbankr1648mb
;    rep stosd

    call init18_2hz

    test byte[pressed+60],1
    jnz near savestate
    test byte[pressed+62],1
    jnz near loadstate
    cmp byte[debugdisble],0
    jne .nodebugger
    test byte[pressed+59],1
    jne near startdebugger
.nodebugger
    ;; deinit video
    mov al,[previdmode]
    mov ah,0
    int 10h

    cmp word[ramsize],0
    je .nosram
    mov ah,3Ch
    mov cx,0
    mov edx,fnames+1
    int 21h
    mov bx,ax
    mov ah,40h
    xor ecx,ecx
    mov cx,[ramsize]
    mov edx,[sram]
    int 21h
    mov ah,3Eh
    int 21h
.nosram
    jmp DosExit

NEWSYM interror
    mov al,[previdmode]
    mov ah,0
    int 10h
    mov edx,.nohand         ;use extended
    mov ah,9                ;DOS- API
    int 21h
    mov ax,4C00h
    int 21h

.nohand db 'Cannot process interrupt handler!',13,10,'$'

; global variables
NEWSYM invalid, db 0                                                    ;00016895
NEWSYM invopcd, db 0
                                                                        ;00016897
NEWSYM pressed, times 128 db 0          ; keyboard pressed keys in scancode
NEWSYM exiter, db 0                                                     ;00016917
NEWSYM oldhand9o, dd 0
NEWSYM oldhand9s, dw 0
NEWSYM oldhand8o, dd 0
NEWSYM oldhand8s, dw 0
NEWSYM opcd,      dd 0                                                  ;00016924
NEWSYM pdh,       db 0                                                  ;00016928
NEWSYM pcury,     dw 0
NEWSYM timercount, dd 0
NEWSYM initaddrl, dd 0                  ; initial address location      ;0001692F
NEWSYM nextframe, db 0                                                  ;00016933
NEWSYM curfps,    db 0                  ; frame/sec for current screen  ;00016934

;*******************************************************
; Save/Load States
;*******************************************************

NEWSYM savestate                                                        ;00016935
    mov byte[pressed+1],0
    mov byte[pressed+60],2
    sub dword[Curtableaddr],tableA
    sub dword[spcPCRam],spcRam
    sub dword[spcRamDP],spcRam
    call PrepareSaveState
    ; Save State
    mov ah,3Ch
    mov cx,0
    mov edx,fnamest+1
    int 21h
    ; Save 65816 status, etc.
    mov bx,ax
    mov ah,40h
    mov ecx,40h
    mov edx,zsmesg
    int 21h
    ; Save SNES PPU Register status
    mov ah,40h
    mov ecx,3019
    mov edx,sndrot
    int 21h
    ; Save RAM (WRAM(128k),VRAM(64k),SRAM)
    mov ah,40h
    xor ecx,ecx
    mov cx,[ramsize]
    add ecx,32768+65536+65536+32768
    mov edx,[wramdata]
    int 21h
    cmp byte[spcon],0
    je .nospcon
    ; Save SPC stuff
    mov ah,40h
    mov ecx,spcsave
    mov edx,spcRam
    int 21h
    ; Save DSP stuff
    mov ah,40h
    mov ecx,dspsave
    mov edx,BRRBuffer
    int 21h
    ; Save DSP Mem
    mov ah,40h
    mov ecx,256
    mov edx,DSPMem
    int 21h
.nospcon
    mov dword[Msgptr],.savemsg
    mov eax,[MsgCount]
    mov [MessageOn],eax
    mov ah,3Eh
    int 21h

    add dword[Curtableaddr],tableA
    add dword[spcPCRam],spcRam
    add dword[spcRamDP],spcRam
    call ResetState
    jmp reexecute

.fname db 9,'vram.dat',0
.savemsg db 'STATE SAVED.',0

NEWSYM loadstate                                                        ;00016A35
    mov byte[pressed+1],0
    mov byte[pressed+62],2
    mov ax,3D00h
    mov cx,0
    mov edx,fnamest+1
    int 21h
    jc near .nofile

    ; Load 65816 status, etc.
    mov bx,ax
    mov ah,3Fh
    mov ecx,40h
    mov edx,zsmesg
    int 21h
    ; Load SNES PPU Register status
    mov ah,3Fh
    mov ecx,3019    ; 3019
    mov edx,sndrot
    int 21h
    ; Load RAM (WRAM(128k),VRAM(64k),SRAM)
    mov ah,3Fh
    xor ecx,ecx
    mov cx,[ramsize]
    add ecx,32768+65536+65536+32768
    mov edx,[wramdata]
    int 21h
    cmp byte[spcon],0
    je .nospcon
    ; Load SPC stuff
    mov ah,3Fh
    mov ecx,spcsave
    mov edx,spcRam
    int 21h
    ; Load DSP stuff
    mov ah,3Fh
    mov ecx,dspsave
    mov edx,BRRBuffer
    int 21h
    ; Load DSP Mem
    mov ah,3Fh
    mov ecx,256
    mov edx,DSPMem
    int 21h
.nospcon
    mov ah,3Eh
    int 21h

    ; Clear Cache Check
    mov esi,vidmemch2
    mov ecx,4096+4096+4096
.next
    mov byte[esi],1
    inc esi
    dec ecx
    jnz .next
    mov dword[Msgptr],.loadmsg
    mov eax,[MsgCount]
    mov [MessageOn],eax
    add dword[Curtableaddr],tableA
    add dword[spcPCRam],spcRam
    add dword[spcRamDP],spcRam
    call ResetState
    call changeexecloop
    jmp reexecute
.nofile
    mov dword[Msgptr],.nfndmsg
    mov eax,[MsgCount]
    mov [MessageOn],eax
    jmp reexecute

.loadmsg db 'STATE LOADED.',0
.nfndmsg db 'UNABLE TO LOAD.',0

;*******************************************************
; Int 08h vector
;*******************************************************

; sets to either 60Hz or 50Hz depending on PAL/NTSC
NEWSYM init60hz                                                         ;00016B50
    cmp byte[romispal],0
    jne .dopal
    mov al,00110110b
    out 43h,al
    mov ax,19886        ; 65536/(60/((65536*24+176)/(60*60*24)))
    mov dword[timercount],19886
    out 40h,al
    mov al,ah
    out 40H,al
    ret
.dopal
    mov al,00110110b
    out 43h,al
    mov ax,23863        ; 65536/(50/((65536*24+175)/(60*60*24)))
    mov dword[timercount],23863
    out 40h,al
    mov al,ah
    out 40H,al
    ret

NEWSYM init18_2hz                                                       ;00016B8B
    mov al,00110110b
    out 43H,al
    mov ax,0
    mov dword[timercount],65536
    out 40H,al
    mov al,ah
    out 40H,al
    ret

NEWSYM handler8h                                                        ;00016BA4
    cli
    inc word[t1cc]
    inc byte[nextframe]
    push eax
    mov eax,[timercount]
    sub dword[timeradj],eax
    jnc .noupd
    pop eax
    add dword[timeradj],65536
    jmp far [oldhand8o]
.noupd
    mov al,20h
    out 20h,al
    pop eax
    sti
    iretd

NEWSYM timeradj, dd 65536                                               ;00016BD8
NEWSYM t1cc,     dw 0                                                   ;00016BDC

;*******************************************************
; Int 09h vector
;*******************************************************

NEWSYM handler9h                                                        ;00016BDE
    cli
    push ecx
    push eax
    push ebx

    xor ebx,ebx
    mov ecx,1000
.nocmd
    in al,64H                 ; get keyboard status
    test al,2                 ; 1=command available
    loopne .nocmd
    mov al,0ADh
    out 64H,al
.nocmd2
    in al,64H
    mov bl,al
    test al,2
    loopne .nocmd2

    in al,60H                 ; get keyboard scan code
    mov bl,al
    xor bh,bh
    test bl,80h               ; check if bit 7 is on (key released)
    jnz .keyrel
    cmp byte[pressed+ebx],0
    jne .skipa
    mov byte[pressed+ebx],1        ; if not, set key to pressed
.skipa
    test byte[pressed+60],1
    jz .not60
    mov byte[pressed+1],1
    jmp .done
.not60
    cmp byte[debugdisble],0
    jnz .not59
    test byte[pressed+59],1
    jz .not59
    mov byte[pressed+1],1
    jmp .done
.not59
    test byte[pressed+62],1
    jz .done
    mov byte[pressed+1],1
    jmp .done
.keyrel
    and bl,7Fh
    mov byte[pressed+ebx],0        ; if not, set key to pressed
.done
    mov al,20H                ; turn off interrupt mode
    out 20H,al
    mov ecx,1000
.nocmd3
    in al,64H
    test al,2
    loopne .nocmd3
    mov al,0AEh
    out 64H,al
    pop ebx                          ; Pop registers off
    pop eax                          ; stack in correct
    pop ecx
    sti
    iretd

;*******************************************************
; 65816 execution
;*******************************************************

%macro FlipCheck 0
   cmp byte[FlipWait],0
   je %%noflip
   push edx
   push eax
   mov dx,3DAh             ;VGA status port
   in al,dx
   test al,8
   jz %%skipflip
   push ebx
   push ecx
   mov ax,4F07h
   mov bh,00h
   mov bl,00h
   xor cx,cx
   mov dx,[NextLineStart]
   mov [LastLineStart],dx
   int 10h
   mov byte[FlipWait],0
   pop ecx
   pop ebx
%%skipflip
   pop eax
   pop edx
%%noflip
%endmacro


NEWSYM execute                                                          ;00016C80
NEWSYM execloop
    sub byte[cycpbl],100
    jnc skipallspc
    mov al,[cycpblt]
    mov bl,[ebp]
    add [cycpbl],al
   inc ebp
   jmp dword near [opcjmptab+ebx*4]
NEWSYM spcopcdone                                                       ;00016C9F
   xor ebx,ebx
skipallspc
   mov bl,[esi]
   inc esi
   sub dh,[cpucycle+ebx]
   jc cpuover
   jmp dword near [edi+ebx*4]
cpuovers
    ret

GLOBAL sz_execloop
sz_execloop equ $-execloop

NEWSYM execloopns                                                       ;00016CB0
   mov bl,[esi]
   inc esi
   sub dh,[cpucycle+ebx]
   jc .cpuovers
   jmp dword near [edi+ebx*4]
.cpuovers
   jmp [cpuoverptr]


NEWSYM cpuover                                                          ;00016CC4
    dec esi
    test byte[exiter],01h
    jnz cpuovers
    inc word[curypos]
    add dh,[cycpl]
    call updatetimer

    mov ax,[resolutn]
    cmp [curypos],ax
    je .nmi

    mov ax,[totlines]
    cmp word[curypos],ax
    ja near .overy

    ; check for VIRQ/HIRQ/NMI
    test dl,04h
    jnz .startirq
    test byte[INTEnab],20h
    jz .novirq
    mov ax,[VIRQLoc]
    cmp [curypos],ax
    je .drawline
.notres
    jmp .startirq
.novirq
    test byte[INTEnab],10h
    jnz .drawline
.startirq
    mov ax,[resolutn]
    cmp [curypos],ax
    jb .hdma

    test byte[pressed+1],1
    jnz cpuovers

    mov bl,[esi]
    inc esi
    jmp [edi+ebx*4]
.hdma
    call exechdma
    call drawline
    mov bl,[esi]
    inc esi
    jmp [edi+ebx*4]

.nmi
    mov byte[debstop],1
    mov ax,[oamaddrs]
    mov [oamaddr],ax
    call ReadInputDevice
    call showvideo
    xor ebx,ebx
    mov byte[NMIEnab],81h
    test byte[INTEnab],80h
    jz .nonmi

    cmp byte[intrset],1
    jne .nointrset
    mov byte[intrset],2
.nointrset
    jmp switchtonmi
.nonmi
    mov bl,[esi]
    inc esi
    jmp [edi+ebx*4]

.overy
    mov word[curypos],0

    mov byte[NMIEnab],01h

    test byte[pressed+1],1
    jnz near cpuovers

    mov bl,[esi]
    inc esi
    call cachevideo
    call starthdma
    jmp [edi+ebx*4]

.drawline
    xor ebx,ebx
    mov ax,[resolutn]
    cmp [curypos],ax
    jnc .undery
    call exechdma
    call drawline
.undery
    cmp byte[intrset],1
    jne .nointrset2
    mov byte[intrset],2
.nointrset2
    jmp switchtovirq

;*******************************************************
; Execute a Single 65816 instruction (debugging purpose)
;*******************************************************
NEWSYM execsingle                                                       ;00016E0E
    mov byte[exiter],1
    mov bl,[esi]
    inc esi
    sub dh,[cpucycle+ebx]
    jc .dhp
    mov [pdh],dh
    xor dh,dh
    jmp [edi+ebx*4]

.dhp
    add dh,[cycpl]
    mov [pdh],dh
    call updatetimer
    xor dh,dh
    inc word[curypos]
    mov ax,[resolutn]
    cmp word[curypos],ax
    je .nmi
    cmp word[curypos],261
    ja .overy

    ; check for VIRQ/HIRQ/NMI
    test dl,04h
    jnz .noirq
    test byte[INTEnab],20h
    jz .novirq
    mov ax,[VIRQLoc]
    cmp word[curypos],ax
    je near .virq
    jmp .noirq
.novirq
    test byte[INTEnab],10h
    jnz near .virq
.noirq
    mov ax,[resolutn]
    cmp word[curypos],ax
    jb .drawline
    jmp dword near [edi+ebx*4]

.drawline
    call exechdma
    call drawline
    jmp dword near [edi+ebx*4]

.nmi
    mov byte[NMIEnab],81h
    test byte[INTEnab],80h
    jz .nonmi
    dec esi
    cmp byte[intrset],1
    jne .nointrset
    mov byte[intrset],2
.nointrset
    jmp switchtonmi
.nonmi
    jmp dword near [edi+ebx*4]

.overy
    mov word[curypos],0
    mov byte[NMIEnab],01h
    add dword[opcd],170*262
    call cachevideo
    call starthdma

    jmp dword near [edi+ebx*4]

.virq
    mov ax,[resolutn]
    cmp word[curypos],ax
    jnb .nodrawline
.dohdma2
    call exechdma
    call drawline
.nodrawline
    dec esi
    cmp byte[intrset],1
    jne .nointrset4
    mov byte[intrset],2
.nointrset4
    jmp switchtovirq









