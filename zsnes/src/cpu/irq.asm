;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM curnmi,execloop,initaddrl,nmiv,snesmap2
EXTSYM snesmmap,stackand,stackor,wramdata
EXTSYM xe,xirqb,xpb,xpc,xs
EXTSYM irqon,irqv
EXTSYM irqv8
EXTSYM nmiv8






;        NMI     Hardware        00FFFA,B    00FFEA,B     3  -> 000108
;        RES     Hardware        00FFFC.D    00FFFC,D     1
;        BRK     Software        00FFFE,F    00FFE6,7    N/A
;        IRQ     Hardware        00FFFE,F    00FFEE,F     4  -> 00010C


;*******************************************************
; SwitchToNMI/VIRQ                        Calls NMI/VIRQ
;*******************************************************
NEWSYM switchtonmi                                                      ;0005C1F1
    mov byte[curnmi],1
    test byte[xe],1
    jne near NMIemulmode

    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr0
    mov eax,[snesmmap+ebx*4]
    jmp .gotaddr
.loweraddr0
    mov eax,[snesmap2+ebx*4]
.gotaddr
    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx

    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpb]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh

    mov bl,byte[xirqb]
    mov [xpb],bl
    xor eax,eax
    mov ax,[nmiv]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop

NEWSYM NMIemulmode                                                      ;0005C2F5
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr0
    mov eax,[snesmmap+ebx*4]
    jmp .gotaddr
.loweraddr0
    mov eax,[snesmap2+ebx*4]
.gotaddr

    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx

    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh

    mov bl,[xpb]
    xor eax,eax
    mov ax,[nmiv8]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop

NEWSYM switchtovirq                                                     ;0005C3C7
    mov byte[irqon],80h
    test byte[xe],1
    jne near IRQemulmode

    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr0
    mov eax,[snesmmap+ebx*4]
    jmp .gotaddr
.loweraddr0
    mov eax,[snesmap2+ebx*4]
.gotaddr

    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx

    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpb]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh

    mov bl,byte[xirqb]
    mov [xpb],bl
    xor eax,eax
    mov ax,[irqv]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop

NEWSYM IRQemulmode                                                      ;0005C4CC
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr0
    mov eax,[snesmmap+ebx*4]
    jmp .gotaddr
.loweraddr0
    mov eax,[snesmap2+ebx*4]
.gotaddr

    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx

    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh

    mov bl,[xpb]
    xor eax,eax
    mov ax,[irqv8]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    jmp execloop
