;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


EXTSYM thing1,thing2






;*******************************************************
; Flag Setting Macros
;*******************************************************


%macro endloop 0
   jmp execloop
%endmacro

; Sets flags n and z according to al
%macro flagsetnz8b 0
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
    endloop
%endmacro

; Sets flags n and z according to ax
%macro flagsetnz16b 0
    and eax,0FFFFh
    and dl,01111101b
    or dl,[thing2+eax]
    endloop
%endmacro

; Sets flags n and z according to al
%macro flagsetnz8bnel 0
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
%endmacro

; Sets flags n and z according to ax
%macro flagsetnz16bnel 0
    and eax,0FFFFh
    and dl,01111101b
    or dl,[thing2+eax]
%endmacro

; Sets flags N V . . . . Z C according to flags
%macro flagsetnvzc8b 0
    jo .over
    jc .carry
    xor eax,eax
    mov al,[xa]
    and dl,00111100b
    or dl,[thing1+eax]
    endloop
.over
    jc .overcarry
    xor eax,eax
    mov al,[xa]
    and dl,00111100b
    or dl,[thing1+eax]
    or dl,01000000b
    endloop
.carry
    xor eax,eax
    mov al,[xa]
    and dl,00111100b
    or dl,[thing1+eax]
    or dl,00000001b
    endloop
.overcarry
    xor eax,eax
    mov al,[xa]
    and dl,00111100b
    or dl,[thing1+eax]
    or dl,01000001b
    endloop
%endmacro

%macro flagsetnvzc16b 0
    jo .over
    jc .carry
    xor eax,eax
    mov ax,[xa]
    and dl,00111100b
    or dl,[thing2+eax]
    endloop
.over
    jc .overcarry
    xor eax,eax
    mov ax,[xa]
    and dl,00111100b
    or dl,[thing2+eax]
    or dl,01000000b
    endloop
.carry
    xor eax,eax
    mov ax,[xa]
    and dl,00111100b
    or dl,[thing2+eax]
    or dl,00000001b
    endloop
.overcarry
    xor eax,eax
    mov ax,[xa]
    and dl,00111100b
    or dl,[thing2+eax]
    or dl,01000001b
    endloop
%endmacro

; Sets flags N V . . . . Z C according to flags
%macro flagsetnvzcs8b 0
    flagsetnvzc8b
%endmacro

%macro flagsetnvzcs16b 0
    flagsetnvzc16b
%endmacro

; Sets flags N V . . . . Z C for 16-bit decimal mode only
%macro flagsetnvzcd 0
    lahf
    jo .overflow
    test word[xa],0FFFFh
    jz .zero
    and ah,81h
    and dl,00111100b
    or dl,ah
    endloop
.zero
    and ah,81h
    and dl,00111100b
    or dl,ah
    or dl,00000010b
    endloop
.overflow
    test word[xa],0FFFFh
    jz .overzero
    and ah,81h
    and dl,00111100b
;    or dl,[thing1+eax]
    or dl,ah
    or dl,01000000b
    endloop
.overzero
    and ah,81h
    and dl,00111100b
    or dl,ah
    or dl,01000010b
    endloop
%endmacro

; Sets flags N . . . . . Z C according to flags and don't jump to execloop
%macro flagsetnzc8b 0
    jc .carry
    and eax,0FFh
    and dl,01111100b
    or dl,[thing1+eax]
    jmp near .nc
.carry
    and eax,0FFh
    and dl,01111100b
    or dl,[thing1+eax]
    or dl,00000001b
.nc
%endmacro

; Sets flags N . . . . . Z C according to flags and don't jump to execloop
%macro flagsetnzc 0
    jc .carry
    and eax,0FFFFh
    and dl,01111100b
    or dl,[thing2+eax]
    jmp near .nc
.carry
    and eax,0FFFFh
    and dl,01111100b
    or dl,[thing2+eax]
    or dl,00000001b
.nc
%endmacro

; Sets flags N . . . . . Z C according to flags and jump to execloop
%macro flagsetnzcel8b 0
    lahf
    jz .zero
    and ah,81h
    and dl,01111100b
    or dl,ah
    mov ah,cl
    endloop
.zero
    and ah,81h
    and dl,01111100b
    or dl,ah
    or dl,00000010b
    mov ah,cl
    endloop
%endmacro

%macro flagsetnzcel16b 0
    flagsetnzcel8b
%endmacro

;*******************************************************
; Opcode Instructions
;*******************************************************

%macro ADCMacro8bnd 0
    test dl,00000001b
    clc
    jz .cc
    stc
.cc
    adc [xa],al
    flagsetnvzc8b
%endmacro

%macro ADCMacro16bnd 0
    test dl,00000001b
    clc
    jz .cc
    stc
.cc
    adc [xa],ax
    flagsetnvzc16b
%endmacro

%macro ADCMacro8bd 0
    test dl,00000001b
    clc
    jz near .cc
    stc
.cc
    mov cl,al
    mov al,[xa]
    adc al,cl
    daa
    mov [xa],al
    flagsetnvzc8b
    endloop
%endmacro

%macro ADCMacro16bd 0
    mov cx,ax
    test dl,00000001b
    clc
    jz .cc
    stc
.cc
    mov al,[xa]
    adc al,cl
    daa
    mov [xa],al
    mov al,[xa+1]
    adc al,ch
    daa
    mov [xa+1],al
    flagsetnvzcd
%endmacro

%macro ANDMacro8b 0
    and al,[xa]
    mov [xa],al
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
    endloop
%endmacro

%macro ANDMacro16b 0
    and ax,[xa]
    mov [xa],ax
    and eax,0FFFFh
    and dl,01111101b
    or dl,[thing2+eax]
    endloop
%endmacro

%macro ASLMacro8b 0
    shl al,1
    flagsetnzc8b
%endmacro

%macro ASLMacro16b 0
    shl ax,1
    flagsetnzc
%endmacro

%macro JumpMacro 0
    movsx eax,byte[esi]
    add esi,eax
.skip
    inc esi
    endloop
%endmacro

%macro BITMacroim8b 0
    mov al,[esi]
    inc esi
    test [xa],al
    jz .zero
    and dl,11111101b
    endloop
.zero
    or dl,00000010b
    endloop
%endmacro

%macro BITMacroim16b 0
    mov ax,[esi]
    add esi,2
    test [xa],ax
    jz .zero
    and dl,11111101b
    endloop
.zero
    or dl,00000010b
    endloop
%endmacro

%macro BITMacro8b 0
    test [xa],al
    jz .zero
    and al,0C0h
    and dl,00111101b
    or dl,al
    endloop
.zero
    or dl,00000010b
    and al,0C0h
    and dl,00111111b
    or dl,al
    endloop
%endmacro

%macro BITMacro16b 0
    test [xa],ax
    jz .zero
    and ah,0C0h
    and dl,00111101b
    or dl,ah
    endloop
.zero
    or dl,00000010b
    and ah,0C0h
    and dl,00111111b
    or dl,ah
    endloop
%endmacro

%macro BRKMacro 0
    inc esi
    test byte[xe],1
    jne near BRKemulmode
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr2
    mov eax,[snesmmap+ebx*4]
    jmp .nextaddr
.loweraddr2
    mov eax,[snesmap2+ebx*4]
.nextaddr
    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpb]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh
    mov bl,byte[xirqb]
    mov [xpb],bl
    xor eax,eax
    mov ax,[brkv]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
BRKemulmode
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr2
    mov eax,[snesmmap+ebx*4]
    jmp .nextaddr
.loweraddr2
    mov eax,[snesmap2+ebx*4]
.nextaddr
    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh
    mov bl,[xpb]
    xor eax,eax
    mov ax,[brkv8]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro BRLMacro 0
    mov ax,[xpc]
    mov bl,[xpb]
    test ax,8000h
    jz .loweraddr
    mov eax,[snesmmap+ebx*4]
    jmp .higheraddr
.loweraddr
    mov eax,[snesmap2+ebx*4]
.higheraddr
    mov ebx,esi
    sub ebx,eax
    add bx,2
    xor eax,eax
    add bx,[esi]
    mov ax,bx
    xor ebx,ebx
    mov [xpc],ax
    mov bl,[xpb]
    test ax,8000h
    jz .loweraddr2
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr2
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro CMPMacro8b 0
    cmp [xa],al
    cmc
    mov cl,ah
    flagsetnzcel8b
%endmacro

%macro CMPMacro16b 0
    cmp [xa],ax
    cmc
    mov cl,ah
    flagsetnzcel16b
%endmacro

%macro COPMacro 0
    inc esi
    test byte[xe],1
    jne near COPemulmode
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr2
    mov eax,[snesmmap+ebx*4]
    jmp .nextaddr
.loweraddr2
    mov eax,[snesmap2+ebx*4]
.nextaddr
    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpb]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh
    mov bl,byte[xirqb]
    mov [xpb],bl
    xor eax,eax
    mov ax,[copv]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
COPemulmode
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr2
    mov eax,[snesmmap+ebx*4]
    jmp .nextaddr
.loweraddr2
    mov eax,[snesmap2+ebx*4]
.nextaddr
    mov ebx,esi
    sub ebx,eax
    mov [xpc],bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],dl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh
    mov bl,[xpb]
    xor eax,eax
    mov ax,[copv8]
    mov [xpc],ax
    and dl,11110011b
    or dl,00000100b
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro CPXMacro8b 0
    cmp [xx],al
    cmc
    mov cl,ah
    flagsetnzcel8b
%endmacro

%macro CPXMacro16b 0
    cmp [xx],ax
    cmc
    mov cl,ah
    flagsetnzcel16b
%endmacro

%macro CPYMacro8b 0
    cmp [xy],al
    cmc
    mov cl,ah
    flagsetnzcel8b
%endmacro

%macro CPYMacro16b 0
    cmp [xy],ax
    cmc
    mov cl,ah
    flagsetnzcel16b
%endmacro

%macro EORMacro8b 0
    xor al,[xa]
    mov [xa],al
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
    endloop
%endmacro

%macro EORMacro16b 0
    xor ax,[xa]
    mov [xa],ax
    and eax,0FFFFh
    and dl,01111101b
    or dl,[thing2+eax]
    endloop
%endmacro

%macro JMLMacro 0
    mov cx,[esi]
    xor eax,eax
    call membank0r16
    add cx,2
    push eax
    call membank0r8
    mov bl,al
    pop eax
    mov [xpc],ax
    mov [xpb],bl
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro JMPMacro4C 0
    xor eax,eax
    mov ax,[esi]
    mov bl,[xpb]
    mov [xpc],ax
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro JMPMacro6C 0
    mov cx,[esi]
    xor eax,eax
    call membank0r16
    mov [xpc],ax
    mov bl,[xpb]
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro JMPMacro7C 0
    mov cx,[esi]
    xor eax,eax
    add cx,[xx]
    mov bl,[xpb]
    call dword near [memtabler16+ebx*4]
    mov [xpc],ax
    mov bl,[xpb]
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro JMPMacro5C 0
    xor eax,eax
    mov bl,[esi+2]
    mov ax,[esi]
    mov [xpb],bl
    mov [xpc],ax
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro JSLMacro 0
    mov ebx,esi
    sub ebx,[initaddrl]
    add bx,2
    mov [xpc],bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpb]
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    mov [xs],bx
    xor eax,eax
    xor bh,bh
    mov ax,[esi]
    mov bl,[esi+2]
    mov [xpc],ax
    mov [xpb],bl
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro JSRMacro20 0
    mov ebx,esi
    sub ebx,[initaddrl]
    inc bx
    mov [xpc],bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    xor eax,eax
    mov [xs],bx
    mov ax,[esi]
    xor bh,bh
    mov [xpc],ax
    mov bl,[xpb]
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro JSRMacroFC 0
    mov ebx,esi
    sub ebx,[initaddrl]
    inc bx
    mov [xpc],bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[xpc+1]
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    mov cl,[xpc]
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    mov [xs],bx
    xor eax,eax
    xor bh,bh
    mov cx,[esi]
    mov bl,[xpb]
    add cx,[xx]
    call dword near [memtabler16+ebx*4]
    mov [xpc],ax
    mov bl,[xpb]
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro LDAMacro8b 0
    mov [xa],al
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
    endloop
    endloop
%endmacro

%macro LDAMacro16b 0
    mov [xa],ax
    flagsetnz16b
    endloop
%endmacro

%macro LDXMacro8b 0
    mov [xx],al
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
    endloop
    endloop
%endmacro

%macro LDXMacro16b 0
    mov [xx],ax
    flagsetnz16b
    endloop
%endmacro

%macro LDYMacro8b 0
    mov [xy],al
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
    endloop
    endloop
%endmacro

%macro LDYMacro16b 0
    mov [xy],ax
    flagsetnz16b
    endloop
%endmacro

%macro LSRMacro8b 0
    shr al,1
    flagsetnzc8b
%endmacro

%macro LSRMacro16b 0
    shr ax,1
    flagsetnzc
%endmacro

%macro MVNMacro 0
    mov ax,[esi]
    mov [xdb],al
    mov bl,ah
    mov cx,[xx]
    call dword near [memtabler8+ebx*4]
    mov bl,[xdb]
    mov cx,[xy]
    call dword near [memtablew8+ebx*4]
    inc word[xx]
    inc word[xy]
    dec word[xa]
    cmp word[xa],0FFFFh
    je .endmove
    dec esi
    endloop
.endmove
    add esi,2
    endloop
%endmacro

%macro MVPMacro 0
    mov ax,[esi]
    mov [xdb],al
    mov bl,ah
    mov cx,[xx]
    call dword near [memtabler8+ebx*4]
    mov bl,[xdb]
    mov cx,[xy]
    call dword near [memtablew8+ebx*4]
    dec word[xx]
    dec word[xy]
    dec word[xa]
    cmp word[xa],0FFFFh
    je .endmove
    dec esi
    endloop
.endmove
    add esi,2
    endloop
%endmacro

%macro ORAMacro8b 0
    or al,[xa]
    mov [xa],al
    and eax,0FFh
    and dl,01111101b
    or dl,[thing1+eax]
    endloop
%endmacro

%macro ORAMacro16b 0
    or ax,[xa]
    mov [xa],ax
    flagsetnz16b
%endmacro

%macro PUSHMacro8b 1
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,%1
    mov [eax+ebx],cl
    dec bx
    or bx,[stackor]
    mov [xs],bx
    xor bh,bh
    endloop
%endmacro

%macro PUSHMacro8bp 0
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,dl
    mov [eax+ebx],cl
    dec bx
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh
    endloop
%endmacro

%macro PUSHMacro16b 2
    mov eax,[wramdata]
    mov bx,[xs]
    mov cx,%1
    mov [eax+ebx],ch
    dec bx
    or bx,[stackor]
    mov [eax+ebx],cl
    dec bx
    or bx,[stackor]
    mov [xs],bx
    xor bh,bh
    endloop
%endmacro

%macro PEAMacro 0
    mov eax,[wramdata]
    mov bx,[xs]
    mov cl,[esi+1]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov cl,[esi]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    add esi,2
    xor bh,bh
    endloop
%endmacro

%macro PEIMacro 0
    xor ah,ah
    mov al,[esi]
    mov cx,[xd]
    inc esi
    add cx,ax
    call membank0r16
    mov cx,ax
    mov eax,[wramdata]
    mov bx,[xs]
    mov [eax+ebx],ch
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor bh,bh
    mov byte[0x22705],3
    endloop
%endmacro

%macro PERMacro 0
    mov bl,[xpb]
    mov ax,[xpc]
    test ax,8000h
    jz .loweraddr
    mov eax,[snesmmap+ebx*4]
    mov ebx,esi
    sub ebx,eax
    add bx,[esi]
    add esi,2
    add bx,2
    mov cx,bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov [eax+ebx],ch
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor ebx,ebx
    endloop
.loweraddr
    mov eax,[snesmap2+ebx*4]
    mov ebx,esi
    sub ebx,eax
    add bx,[esi]
    add esi,2
    add bx,2
    mov cx,bx
    mov eax,[wramdata]
    mov bx,[xs]
    mov [eax+ebx],ch
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [eax+ebx],cl
    dec bx
    and bx,word[stackand]
    or bx,word[stackor]
    mov [xs],bx
    xor ebx,ebx
    endloop
%endmacro

%macro POPMacro8b 1
    mov eax,[wramdata]
    mov bx,[xs]
    inc bx
    and bx,word[stackand]
    mov cl,[eax+ebx]
    mov [xs],bx
    xor bh,bh
    mov al,cl
    mov %1,al
    flagsetnz8b
    endloop
%endmacro

%macro POPMacro16b 1
    mov eax,[wramdata]
    mov bx,[xs]
    inc bx
    and bx,word[stackand]
    mov cl,[eax+ebx]
    mov [xs],bx
    inc bx
    and bx,word[stackand]
    mov ch,[eax+ebx]
    mov [xs],bx
    xor bh,bh
    mov ax,cx
    mov %1,ax
    flagsetnz16b
%endmacro

%macro POPMacroP 0
    mov eax,[wramdata]
    mov bx,[xs]
    inc bx
    and bx,word[stackand]
    mov cl,[eax+ebx]
    mov [xs],bx
    xor bh,bh
    mov dl,cl
    test byte[xe],01h
    jnz .emul
    mov bl,dl
    mov edi,[tablead+ebx*4]
    test dl,00010000b
    jnz .setx
    endloop
.setx
    mov byte[xx+1],0
    mov byte[xy+1],0
    endloop
.emul
    or dl,00110000b
    mov bl,dl
    mov edi,[tablead+ebx*4]
    endloop
%endmacro

%macro REPMacro 0
    mov al,[esi]
    inc esi
    not al
    and dl,al
    test byte[xe],01h
    jnz .emul
    mov bl,dl
    mov edi,[tablead+ebx*4]
    endloop
.emul
    or dl,00110000b
    mov bl,dl
    mov edi,[tablead+ebx*4]
    endloop
%endmacro

%macro ROLMacro8b 0
    clc
    test dl,00000001b
    jz .cc
    stc
.cc
    rcl al,1
    flagsetnzc8b
%endmacro

%macro ROLMacro16b 0
    clc
    test dl,00000001b
    jz .cc
    stc
.cc
    rcl ax,1
    flagsetnzc
%endmacro

%macro RORMacro8b 0
    clc
    test dl,00000001b
    jz .cc
    stc
.cc
    rcr al,1
    flagsetnzc8b
%endmacro

%macro RORMacro16b 0
    clc
    test dl,00000001b
    jz .cc
    stc
.cc
    rcr ax,1
    flagsetnzc
%endmacro

%macro RTIMacro 0
    mov byte[curnmi],0
    test byte[xe],1
    jne near emulRTI
    mov eax,[wramdata]
    mov bx,[xs]
    inc bx
    and bx,word[stackand]
    mov dl,[eax+ebx]
    inc bx
    and bx,word[stackand]
    mov cl,[eax+ebx]
    inc bx
    and bx,word[stackand]
    mov ch,[eax+ebx]
    inc bx
    and bx,word[stackand]
    mov al,[eax+ebx]
    mov [xpb],al
    mov [xs],bx
    xor bh,bh
    xor eax,eax
    mov ax,cx
    mov bl,dl
    mov edi,[tablead+ebx*4]
    mov bl,[xpb]
    mov [xpc],ax
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    test dl,00010000b
    jnz .setx
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    test dl,00010000b
    jnz .setx
    endloop
.setx
    mov byte[xx+1],0
    mov byte[xy+1],0
    endloop

emulRTI
    mov eax,[wramdata]
    mov bx,[xs]
    inc bx
    and bx,word[stackand]
    mov dl,[eax+ebx]
    or dl,00110000b
    inc bx
    and bx,word[stackand]
    mov cl,[eax+ebx]
    inc bx
    and bx,word[stackand]
    mov ch,[eax+ebx]
    mov [xs],bx
    xor bh,bh
    xor eax,eax
    mov ax,cx
    mov bl,dl
    mov edi,[tablead+ebx*4]
    xor bl,bl
    mov [xpc],ax
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro RTLMacro 0
    mov eax,[wramdata]
    mov bx,[xs]
    inc bx
    and bx,word[stackand]
    mov cl,[eax+ebx]
    inc bx
    and bx,word[stackand]
    mov ch,[eax+ebx]
    inc bx
    and bx,word[stackand]
    mov al,[eax+ebx]
    mov [xpb],al
    mov [xs],bx
    xor bh,bh
    xor eax,eax
    mov ax,cx
    inc ax
    mov bl,[xpb]
    mov [xpc],ax
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro RTSMacro 0
    mov eax,[wramdata]
    mov bx,[xs]
    inc bx
    and bx,word[stackand]
    mov cl,[eax+ebx]
    inc bx
    and bx,word[stackand]
    mov ch,[eax+ebx]
    mov [xs],bx
    xor bh,bh
    xor eax,eax
    mov ax,cx
    inc ax
    mov [xpc],ax
    mov bl,[xpb]
    test ax,8000h
    jz .loweraddr
    mov esi,[snesmmap+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
.loweraddr
    mov esi,[snesmap2+ebx*4]
    mov [initaddrl],esi
    add esi,eax
    endloop
%endmacro

%macro SBCMacro8bnd 0
    test dl,00000001b
    clc
    jnz .cc
    stc
.cc
    sbb [xa],al
    cmc
    flagsetnvzcs8b
    endloop
%endmacro

%macro SBCMacro16bnd 0
    test dl,00000001b
    clc
    jnz .cc
    stc
.cc
    sbb [xa],ax
    cmc
    flagsetnvzcs16b
    endloop
%endmacro

%macro SBCMacro8bd 0
    test dl,00000001b
    clc
    jnz .cc
    stc
.cc
    mov cl,al
    mov al,[xa]
    sbb al,cl
    das
    mov [xa],al
    cmc
    flagsetnvzcs8b
    endloop
%endmacro

%macro SBCMacro16bd 0
    mov cx,ax
    test dl,00000001b
    clc
    jnz .cc
    stc
.cc
    mov al,[xa]
    sbb al,cl
    das
    mov [xa],al
    mov al,[xa+1]
    sbb al,ch
    das
    mov [xa+1],al
    cmc
    flagsetnvzcd
    endloop
%endmacro

%macro SEPMacro 0
    mov al,[esi]
    inc esi
    or dl,al
    mov bl,dl
    mov edi,[tablead+ebx*4]
    test dl,00010000b
    jnz .setx
    endloop
.setx
    mov byte[xx+1],0
    mov byte[xy+1],0
    endloop
%endmacro

%macro STAMacro8b 0
    mov al,[xa]
%endmacro

%macro STAMacro16b 0
    mov ax,[xa]
%endmacro

%macro STXMacro8b 0
    mov al,[xx]
%endmacro

%macro STXMacro16b 0
    mov ax,[xx]
%endmacro

%macro STYMacro8b 0
    mov al,[xy]
%endmacro

%macro STYMacro16b 0
    mov ax,[xy]
%endmacro

%macro WAIMacro 0
    cmp byte[intrset],1
    jne .notws
    dec esi
    endloop
.notws
    test byte[intrset],0FFh
    jz .waitstate
    cmp byte[intrset],2
    je .stopint
    dec esi
    endloop
.waitstate
    mov byte[intrset],1
    dec esi
    endloop
.stopint
    mov byte[intrset],0
    endloop
%endmacro

%macro XCEMacro 0
    mov al,dl
    and dl,11111110b
    and al,00000001b
    or dl,[xe]
    mov [xe],al
    test byte[xe],01h
    jnz .emul
    mov word[stackand],0FFFFh
    mov word[stackor],0000h
    endloop
.emul
    or dl,00110000b
    mov bl,dl
    mov edi,[tablead+ebx*4]
    mov byte[xx+1],0
    mov byte[xy+1],0
    mov byte[xs+1],1
    mov word[stackand],01FFh
    mov word[stackor],0100h
    endloop
%endmacro
