;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM spcRam, spcRamDP, spcA, spcS, spcX, spcY, spcP, spcNZ
EXTSYM DSPMem,spcWptr,debstop,disablespcclr,SPCSkipXtraROM,SPC700sh
EXTSYM cycpbl,spcRptr
EXTSYM spcextraram
EXTSYM reg1read,reg2read,reg3read,reg4read
EXTSYM timeron,timincr0,timincr1,timincr2,timinl0,timinl1,timinl2,timrcall
;EXTSYM spc700read
EXTSYM dspRptr,dspWptr
EXTSYM changeexecloop,curexecstate,SA1Enable,tableadb,spcopcdone

%include "cpu/regsw.mac"
%include "cpu/spcdef.inc"
%include "cpu/spcaddr.inc"






; SPC 700 Emulation by _Demo_
; Version 2.0

; Little info on functions :
; Write byte : write al at [ebx]
; Read byte : read al from [ebx]
; update timer : update the timers, called every scanline


ALIGN32

%macro WriteByte 0
  cmp ebx,0ffh+spcRam
  ja %%extramem
  cmp ebx,0f0h+spcRam
  jb %%normalmem
  sub ebx,spcRam
  call dword near [spcWptr+ebx*4-0f0h*4]
  jmp %%finished
%%extramem
  cmp ebx,0ffc0h+spcRam
  jb %%normalmem
  sub ebx,spcRam
  sub ebx,0ffc0h
  mov [spcextraram+ebx],al
  jmp near %%finished
;  test byte[spcRam+0F1h],80h
;  jnz .finished
;  push ecx
;  mov cl,[DSPMem+06Ch]
;  test cl,20h
;  pop ecx
;  jz .finished
%%normalmem
  mov [ebx],al
%%finished
%endmacro

%macro ReadByte 0
  cmp ebx,0ffh+spcRam
  ja .rnormalmem
  cmp ebx,0f0h+spcRam
  jb .rnormalmem2
  sub ebx,spcRam
  call dword near [spcRptr+ebx*4-0f0h*4]
  jmp .rfinished
.rnormalmem
;  cmp ebx,0ffc0h+spcRam
;  jb .rnormalmem2
;  test byte [DSPMem+6Ch],10h
;  jz .rnormalmem2
;  mov al,[spcextraram+ebx-0FFC0h-spcRam]
;  jmp .rfinished
.rnormalmem2
   mov al,[ebx]
.rfinished
%endmacro

;NEWSYM timer2upd, dd 0
; This function is called every scanline (262*60 times/sec)
; Make it call 0.9825 times (393/400) (skip when divisible by 64)
; 2 8khz, 1 64khz

NEWSYM updatetimer                                              ;000DD387
;      inc dword[timer2upd]
;      cmp dword[timer2upd],400
;      jne .nowrap
;      mov dword[timer2upd],0
;.nowrap
;.again
;      mov eax,dword[timer2upd]
;      shr eax,6
;      shl eax,6
;      cmp eax,dword[timer2upd]
;      je near .noin2d


.another
      xor byte[timrcall],01h
      test byte[timrcall],01h
      jz short .notimer
      test byte[timeron],1
      jz short .noin0
      dec byte[timinl0]
      jnz short .noin0
      inc byte[spcRam+0FDh]
      mov al,[timincr0]
      mov [timinl0],al
;      cmp byte[spcRam+0FDh],1
;      jne .noin0
;      reenablespc
;      mov dword[cycpbl],0
.noin0
      test byte[timeron],2
      jz short .noin1
      dec byte[timinl1]
      jnz short .noin1
      inc byte[spcRam+0FEh]
      mov al,[timincr1]
      mov [timinl1],al
;      cmp byte[spcRam+0FEh],1
;      jne .noin1
;      reenablespc
;      mov dword[cycpbl],0
.noin1
.notimer
      test byte[timeron],4
      jz short .noin2d2
      dec byte[timinl2]
      jnz short .noin2
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
;      cmp byte[spcRam+0FFh],1
;      jne .noin2
;      reenablespc
;      mov dword[cycpbl],0
.noin2
      dec byte[timinl2]
      jnz short .noin2b
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
;      cmp byte[spcRam+0FFh],1
;      jne .noin2b
;      reenablespc
;      mov dword[cycpbl],0
.noin2b
      dec byte[timinl2]
      jnz short .noin2c
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
;      cmp byte[spcRam+0FFh],1
;      jne .noin2c
;      reenablespc
;      mov dword[cycpbl],0
.noin2c
      dec byte[timinl2]
      jnz short .noin2d
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
;      cmp byte[spcRam+0FFh],1
;      jne .noin2d
;      reenablespc
;      mov dword[cycpbl],0
.noin2d
.noin2d2
;      inc dword[timer2upd]
;      cmp dword[timer2upd],31
;      jne .nowrap
;      mov dword[timer2upd],0
;      jmp .again
;.nowrap

;      inc dword[timer2upd]
;      cmp dword[timer2upd],60
;      jne .noanother
;      mov dword[timer2upd],0
;      jmp .another
;.noanother

      ret


; SPC Write Registers
; DO NOT MODIFY DX OR ECX!
NEWSYM SPCRegF0                 ;000DD443
      mov [spcRam+0F0h],al
      ret
NEWSYM SPCRegF1                 ;000DD449
;      cmp byte[disablespcclr],1
;      je .No23Clear
      test al,10h
      jz .No01Clear
      mov byte [spcRam+0F4h],0
      mov byte [spcRam+0F5h],0
.No01Clear
      test al,20h
      jz .No23Clear
      mov byte [spcRam+0F6h],0
      mov byte [spcRam+0F7h],0
.No23Clear
;      cmp byte[SPCSkipXtraROM],1
;      je near .AfterNoROM
;      test al,80h
;      jz .NoROM
;      push eax
;      push ebx
;      xor eax,eax
;.loopa
;      mov bl,[SPCROM+eax]
;      mov [spcRam+0FFC0h+eax],bl
;      inc eax
;      cmp eax,040h
;      jne .loopa
;      pop ebx
;      pop eax
;      jmp .AfterNoROM
;.NoROM
;      push eax
;      push ebx
;      xor eax,eax
;.loopb
;      mov bl,[spcextraram+eax]
;      mov [spcRam+0FFC0h+eax],bl
;      inc eax
;      cmp eax,040h
;      jne .loopb
;      pop ebx
;      pop eax
;.AfterNoROM
      and al,0Fh
      mov [spcRam+ebx],al
      mov [timeron],al
      ret
NEWSYM SPCRegF2                 ;000DD47B
      mov [spcRam+0F2h],al
      push eax
      push ebx
      xor eax,eax
      mov al,[spcRam+0F2h]
      mov bl,[DSPMem+eax]
      mov [spcRam+0F3h],bl
      pop ebx
      pop eax
      ret
NEWSYM SPCRegF3                 ;000DD498
      push ebx
      xor ebx,ebx
      mov bl,[spcRam+0F2h]
      and bl,07fh
      call dword near [dspWptr+ebx*4]
      pop ebx
      mov [spcRam+ebx],al
      ret
NEWSYM SPCRegF4                 ;000DD4B3
      mov [reg1read],al
;      inc dword[spc700read]
      ret
NEWSYM SPCRegF5                 ;000DD4B9
      mov [reg2read],al
;      inc dword[spc700read]
      ret
NEWSYM SPCRegF6                 ;000DD4BF
      mov [reg3read],al
;      inc dword[spc700read]
      ret
NEWSYM SPCRegF7                 ;000DD4C5
      mov [reg4read],al
;      inc dword[spc700read]
      ret
NEWSYM SPCRegF8                 ;000DD4CB
      mov [spcRam+ebx],al
      ret
NEWSYM SPCRegF9                 ;000DD4D2
      mov [spcRam+ebx],al
      ret
NEWSYM SPCRegFA                 ;000DD4D9
      mov [timincr0],al
;      test byte[timinl0],0FFh
;      jne .nowrite
      mov [timinl0],al
;.nowrite
      mov [spcRam+ebx],al
      ret
NEWSYM SPCRegFB                 ;000DD4EA
      mov [timincr1],al
;      test byte[timinl1],0FFh
;      jne .nowrite
      mov [timinl1],al
;.nowrite
      mov [spcRam+ebx],al
      ret
NEWSYM SPCRegFC                 ;000DD4FB
      mov [timincr2],al
;      test byte[timinl2],0FFh
;      jne .nowrite
      mov [timinl2],al
;.nowrite
      mov [spcRam+ebx],al
      ret
NEWSYM SPCRegFD                 ;000DD50C
      ret
NEWSYM SPCRegFE                 ;000DD50D
      ret
NEWSYM SPCRegFF                 ;000DD50E
      ret

; SPC Read Registers
; DO NOT MODIFY ANY REG!
; return data true al
NEWSYM RSPCRegF0                ;000DD50F
      mov al,[spcRam+0f0h]
      ret
NEWSYM RSPCRegF1                ;000DD515
      mov al,[spcRam+0f1h]
      ret
NEWSYM RSPCRegF2                ;000DD51B
      mov al,[spcRam+0f2h]
      ret
NEWSYM RSPCRegF3                ;000DD521
      mov al,[spcRam+0f3h]
      push ebx
      xor ebx,ebx
      mov bl,[spcRam+0f2h]
      call [dspRptr+ebx*4]
      pop ebx
      ret
NEWSYM RSPCRegF4                ;000DD538
      mov al,[spcRam+0f4h]
      ret
NEWSYM RSPCRegF5                ;000DD53E
      mov al,[spcRam+0f5h]
      ret
NEWSYM RSPCRegF6                ;000DD544
      mov al,[spcRam+0f6h]
      ret
NEWSYM RSPCRegF7                ;000DD54A
      mov al,[spcRam+0f7h]
      ret
NEWSYM RSPCRegF8                ;000DD550
      mov al,[spcRam+0f8h]
      ret
NEWSYM RSPCRegF9                ;000DD556
      mov al,[spcRam+0f9h]
      ret
NEWSYM RSPCRegFA                ;000DD55C
      mov al,[spcRam+0fah]
      ret
NEWSYM RSPCRegFB                ;000DD562
      mov al,[spcRam+0fbh]
      ret
NEWSYM RSPCRegFC                ;000DD568
      mov al,[spcRam+0fch]
      ret
NEWSYM RSPCRegFD                ;000DD56E
      mov al,[spcRam+0fdh]
;      and al,0Fh
;      cmp byte[spcRam+0fdh],0
;      je spcnextskip
      mov byte [spcRam+0fdh],0
;      mov byte [spcnumread],0
      ret
NEWSYM RSPCRegFE                ;000DD57B
      mov al,[spcRam+0feh]
;      and al,0Fh
;      cmp byte[spcRam+0feh],0
;      je spcnextskip
      mov byte [spcRam+0fdh],0
;      mov byte [spcnumread],0
      ret
NEWSYM RSPCRegFF                ;000DD588
      mov al,[spcRam+0ffh]
;      and al,0Fh
;      cmp byte[spcRam+0ffh],0
;      je spcnextskip
      mov byte [spcRam+0fdh],0
;      mov byte [spcnumread],0
      ret
;NEWSYM spcnextskip
;      inc byte[spcnumread]
;      cmp byte[spcnumread],5
;      je near haltspc
;      ret

;************************************************

%macro SPCSetFlagnzc 0
  js .setsignflag
  jz .setzeroflag
  mov byte [spcNZ],1
  jc .setcarryflag
  and byte [spcP],0FEh
  jmp spcopcdone
.setsignflag
  mov byte [spcNZ],80h
  jz .setzeroflag
  jc .setcarryflag
  and byte [spcP],0FEh
  jmp spcopcdone
.setzeroflag
  mov byte [spcNZ],0
  jc .setcarryflag
  and byte [spcP],0FEh
  jmp spcopcdone
.setcarryflag
  or byte [spcP],1
  jmp spcopcdone
%endmacro

%macro SPCSetFlagnzcnoret 0
  js short .setsignflag
  jz short .setzeroflag
  mov byte [spcNZ],1
  jc short .setcarryflag
  and byte [spcP],0FEh
  jmp .skipflags
.setsignflag
  mov byte [spcNZ],80h
  jz short .setzeroflag
  jc short .setcarryflag
  and byte [spcP],0FEh
  jmp .skipflags
.setzeroflag
  mov byte [spcNZ],0
  jc short .setcarryflag
  and byte [spcP],0FEh
  jmp .skipflags
.setcarryflag
  or byte [spcP],1
.skipflags
%endmacro

;spcNF    db 0     ; The Negative Flag  128 or 127 80h
;spcOF    db 0     ; The Overflow Flag   64 or 191 40h
;spcDPF   db 0     ; Direct Page Flag    32 or 223 20h
;spcUF    db 0     ; The Unused Flag ?   16 or 239 10h
;spcHCF   db 0     ; The Half Carry Flag  8 or 247 08h
;spcIF    db 0     ; The interrupt flag   4 or 251 04h
;spcZF    db 0     ; The Zero Flag        2 or 253 02h
;spcCF    db 0     ; The Carry Flag       1 or 254 01h
;      test byte[spcP],08h
;      jz .nohcarry
;      or ah,00010000b
;.nohcarry

%macro SPCSetFlagnvhzc 0
  lahf
  js .setsignflag
  jz .setzeroflag
  mov byte [spcNZ],1
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags 
.setsignflag
  mov byte [spcNZ],80h
  jz .setzeroflag
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setzeroflag
  mov byte [spcNZ],0
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setoverflowflag
  or byte [spcP],01h
  jmp .skipflags
.skipflags
  and byte [spcP],0F6h
  test ah,01h
  jz .noCarry
  or byte [spcP],1
  test ah,10h
  jz .nohf
  or byte [spcP],8
.noCarry
  test ah,10h
  jz .nohf
  or byte [spcP],8
.nohf
jmp spcopcdone
%endmacro

%macro SPCSetFlagnvhzcnoret 0
  lahf
  js .setsignflag
  jz .setzeroflag
  mov byte [spcNZ],1
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags 
.setsignflag
  mov byte [spcNZ],80h
  jz .setzeroflag
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setzeroflag
  mov byte [spcNZ],0
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setoverflowflag
  or byte [spcP],01h
  jmp .skipflags
.skipflags
  and byte [spcP],0F6h
  test ah,01h
  jz .noCarry
  or byte [spcP],1
  test ah,10h
  jz .nohf
  or byte [spcP],8
.noCarry
  test ah,10h
  jz .nohf
  or byte [spcP],8
.nohf
%endmacro

%macro spcgetdp_imm 0
      mov bl,[ebp+1]
      add ebx,[spcRamDP]
      mov ah,[ebp]
      mov al,[ebx]
      add ebp,2
%endmacro

%macro spcaddrDPbDb_DPbSb 0
      xor ecx,ecx
      mov cl,[ebp]
      mov bl,[ebp+1]
      add ebx,[spcRamDP]
      add ebp,2
      add ecx,[spcRamDP]
      mov al,[ebx]
      mov ah,[ecx]
%endmacro

%macro spcaddrmembit 0
      mov bx,[ebp]

      mov cl,bl
      add ebp,2
      shr bx,3
      and cl,7
      mov al,[spcRam+ebx]
      shr al,cl
%endmacro

%macro spcROLstuff 0
      rcl al,1
      jc %%setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp %%skipflags
%%setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
%%skipflags
%endmacro

%macro spcRORstuff 0
      rcr al,1
      jc %%setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp %%skipflags
%%setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
%%skipflags
%endmacro

;000DD595   Op00
NEWSYM Op00       ; NOP
      jmp spcopcdone
;000DD59A   Op10
NEWSYM Op10       ; BPL Branch on N=0
      test byte [spcNZ],128
      jz .branch
      spcbrancher
;  CLRP               20    1     2   clear direct page flag    ..0..... 
;000DD5B5   Op20
NEWSYM Op20       ; CLRP Clear direct page flag
      and byte [spcP],11011111b
      mov dword [spcRamDP],spcRam
      jmp spcopcdone
;000DD5CB   Op30
NEWSYM Op30       ; BMI Branch on N=1
      test byte [spcNZ],128
      jnz .branch
      spcbrancher
;  SETP               40    1     2   set dorect page flag      ..1..0..
;000DD5E6   Op40
NEWSYM Op40       ; SETP Set Direct Page Flag  (Also clear interupt flag?)
      or byte [spcP],00100000b
      and byte [spcP],11111011b
      mov dword [spcRamDP],spcRam
      add dword [spcRamDP],100h
      jmp spcopcdone
;000DD60D   Op50
NEWSYM Op50       ; BVC Branch on V=0
      test byte [spcP],64
      jz .branch
      spcbrancher
;  CLRC               60    1     2   clear carry flag          .......0 
;000DD628   Op60
NEWSYM Op60       ; CLRC Clear carry flag
      and byte [spcP],11111110b
      jmp spcopcdone
;000DD634   Op70
NEWSYM Op70       ; BVS Branch on V=1
      test byte [spcP],64
      jnz .branch
      spcbrancher
;  SETC               80    1     2   set carry flag            .......1 
;000DD64F   Op80
NEWSYM Op80       ; SETC Set carry flag
      or byte [spcP],00000001b
      jmp spcopcdone
;000DD65B   Op90
NEWSYM Op90       ; BCC Branc on c=0
      test byte [spcP],1
      jz .branch
      spcbrancher
;  EI                 A0    1     3  set interrup enable flag   .....1.. 
;000DD676   OpA0
NEWSYM OpA0       ; EI set interrupt flag
      or byte [spcP],00000100b
      jmp spcopcdone
;000DD682   OpB0
NEWSYM OpB0       ; BCS Branch on C=1
      test byte [spcP],1
      jnz .branch
      spcbrancher
;  DI                 C0    1     3  clear interrup enable flag .....0..
;000DD69D   OpC0
NEWSYM OpC0       ; DI clear interrupt flag
      and byte [spcP],11111011b
      jmp spcopcdone
;000DD6A9   OpD0
NEWSYM OpD0       ; BNE branch on Z=0
      test byte [spcNZ],255
      jnz .branch
      spcbrancher
;  CLRV               E0    1     2   clear V and H             .0..0... 
;000DD6C4   OpE0
NEWSYM OpE0       ; CLRV clear V and H
      and byte [spcP],10111111b
      and byte [spcP],11110111b
      jmp spcopcdone
;000DD6D7   OpF0
NEWSYM OpF0       ; BEQ Branch on Z=1
      test byte [spcNZ],0FFh
      jz .branch
      spcbrancher
;000DD6F2   Op01
NEWSYM Op01       ; TCALL 0
      spctcall 30
;000DD730   Op11
NEWSYM Op11       ; TCALL 1
      spctcall 28
;000DD76E   Op21
NEWSYM Op21       ; TCALL 2
      spctcall 26
;000DD7AC   Op31
NEWSYM Op31       ; TCALL 3
      spctcall 24
;000DD7EA   Op41
NEWSYM Op41       ; TCALL 4
      spctcall 22
;000DD828   Op51
NEWSYM Op51       ; TCALL 5
      spctcall 20
;000DD866   Op61
NEWSYM Op61       ; TCALL 6
      spctcall 18
;000DD8A4   Op71
NEWSYM Op71       ; TCALL 7
      spctcall 16
;000DD8E2   Op81
NEWSYM Op81       ; TCALL 8
      spctcall 14
;000DD920   Op91
NEWSYM Op91       ; TCALL 9
      spctcall 12
;000DD95E   OpA1
NEWSYM OpA1       ; TCALL A
      spctcall 10
;000DD99C   OpB1
NEWSYM OpB1       ; TCALL B
      spctcall 08
;000DD9DA   OpC1
NEWSYM OpC1       ; TCALL C
      spctcall 06
;000DDA18   OpD1
NEWSYM OpD1       ; TCALL D
      spctcall 04
;000DDA56   OpE1
NEWSYM OpE1       ; TCALL E
      spctcall 02
;000DDA94   OpF1
NEWSYM OpF1       ; TCALL F
      spctcall 00
;000DDAD2   Op02
NEWSYM Op02       ; SET1 direct page bit 0
      set1 1 
;000DDB28   Op22
NEWSYM Op22       ; SET1 direct page bit 1
      set1 2
;000DDB7E   Op42
NEWSYM Op42       ; SET1 direct page bit 2
      set1 4
;000DDBD4   Op62
NEWSYM Op62       ; SET1 direct page bit 3
      set1 8
;000DDC2A   Op82
NEWSYM Op82       ; SET1 direct page bit 4
      set1 16
;000DDC80   OpA2
NEWSYM OpA2       ; SET1 direct page bit 5
      set1 32
;000DDCD6   OpC2
NEWSYM OpC2       ; SET1 direct page bit 6
      set1 64
;000DDD2C   OpE2
NEWSYM OpE2       ; SET1 direct page bit 7
      set1 128
;000DDD82   Op12
NEWSYM Op12       ; CLR1 direct page bit 0
      clr1 255-1
;000DDDD8   Op32
NEWSYM Op32       ; CLR1 direct page bit 1
      clr1 255-2
;000DDE2E   Op52
NEWSYM Op52       ; CLR1 direct page bit 2
      clr1 255-4
;000DDE84   Op72
NEWSYM Op72       ; CLR1 direct page bit 3
      clr1 255-8
;000DDEDA   Op92
NEWSYM Op92       ; CLR1 direct page bit 4
      clr1 255-16
;000DDF30   OpB2
NEWSYM OpB2       ; CLR1 direct page bit 5
      clr1 255-32
;000DDF86   OpD2
NEWSYM OpD2       ; CLR1 direct page bit 6
      clr1 255-64
;000DDFDC   OpF2
NEWSYM OpF2       ; CLR1 direct page bit 7
      clr1 255-128
;000DE032   Op03
NEWSYM Op03       ; BBS direct page bit 0
      bbs 1
;000DE05C   Op23
NEWSYM Op23       ; BBS direct page bit 1
      bbs 2
;000DE086   Op43
NEWSYM Op43       ; BBS direct page bit 2
      bbs 4
;000DE0B0   Op63
NEWSYM Op63       ; BBS direct page bit 3
      bbs 8
;000DE0DA   Op83
NEWSYM Op83       ; BBS direct page bit 4
      bbs 16
;000DE104   OpA3
NEWSYM OpA3       ; BBS direct page bit 5
      bbs 32
;000DE12E   OpC3
NEWSYM OpC3       ; BBS direct page bit 6
      bbs 64
;000DE158   OpE3
NEWSYM OpE3       ; BBS direct page bit 7
      bbs 128
;000DE182   Op13
NEWSYM Op13       ; BBC direct page bit 0
      bbc 1
;000DE1AC   Op33
NEWSYM Op33       ; BBC direct page bit 1
      bbc 2
;000DE1D6   Op53
NEWSYM Op53       ; BBC direct page bit 2
      bbc 4
;000DE200   Op73
NEWSYM Op73       ; BBC direct page bit 3
      bbc 8
;000DE22A   Op93
NEWSYM Op93       ; BBC direct page bit 4
      bbc 16
;000DE254   OpB3
NEWSYM OpB3       ; BBC direct page bit 5
      bbc 32
;000DE27E   OpD3
NEWSYM OpD3       ; BBC direct page bit 6
      bbc 64
;000DE2A8   OpF3
NEWSYM OpF3       ; BBC direct page bit 7
      bbc 128
;000DE2D2   Op04
NEWSYM Op04       ; OR A,dp   A <- A OR (dp)    N.....Z.
      SPCaddr_DP
      SPC_OR_A al
;000DE2F3   Op14
NEWSYM Op14       ; OR A,dp+X    A <- A OR (dp+X)     N.....Z.
      SPCaddr_DP_X
      SPC_OR_A al
;000DE31A   Op24
NEWSYM Op24       ; AND A,dp     A <- A AND (dp)      N.....Z.
      SPCaddr_DP
      SPC_AND_A al
;000DE33B   Op34
NEWSYM Op34       ; AND A,dp+x   A <- A AND (dp+X)    N.....Z.
      SPCaddr_DP_X
      SPC_AND_A al
;000DE362   Op44
NEWSYM Op44       ; EOR A,dp     A <- A EOR (dp)      N.....Z.
      SPCaddr_DP
      SPC_EOR_A al
;000DE383   Op54
NEWSYM Op54       ; EOR A,dp+x   A <- A EOR (dp+X)    N.....Z.
      SPCaddr_DP_X
      SPC_EOR_A al
;000DE3AA   Op64
NEWSYM Op64       ; CMP A,dp     A-(dp)               N.....ZC
      SPCaddr_DP
      SPC_CMP_A al
;000DE40E   Op74
NEWSYM Op74       ; CMP A,dp+x   A-(dp+X)             N.....ZC
      SPCaddr_DP_X
      SPC_CMP_A al
;000DE478   Op84
NEWSYM Op84       ; ADC A,dp     A <- A+(dp)+C        NV..H.ZC
      SPCaddr_DP
      SPC_ADC_A al
;000DE517   Op94
NEWSYM Op94       ; ADC A,dp+x   A <- A+(dp+X)+C      NV..H.ZC
      SPCaddr_DP_X
      SPC_ADC_A al
;000DE5BC   OpA4
NEWSYM OpA4       ; SBC A,dp     A <- A-(dp)-!C       NV..H.ZC
      SPCaddr_DP
      SPC_SBC_A al
;000DE65C   OpB4
NEWSYM OpB4       ; SBC A,dp+x   A <- A-(dp+X)-!C     NV..H.ZC
      SPCaddr_DP_X
      SPC_SBC_A al
;000DE702   OpC4
NEWSYM OpC4       ; MOV dp,A     A -> (dp)            ........
      mov bl,[ebp]
      mov al, byte [spcA]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      jmp spcopcdone
;000DE759   OpD4
NEWSYM OpD4       ; MOV dp+x,A   A -> (dp+X)          ........
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al, byte [spcA]
      WriteByte
      jmp spcopcdone
;000DE7B6   OpE4
NEWSYM OpE4       ; MOV A,dp     A <- (dp)            N......Z
;      SPCaddr_DP_ReadByte
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      SPC_MOV_A
;000DE7F3   OpF4
NEWSYM OpF4       ; MOV A,dp+x   A <- (dp+X)          N......Z
;      SPCaddr_DP_X_ReadByte
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      SPC_MOV_A
;000DE836   Op05
NEWSYM Op05       ; OR A,labs   A <- A OR (abs)       N.....Z.
;      SPCaddr_LABS
      mov bx,[ebp]
      mov al,[spcRam+ebx]
      add ebp,2
      SPC_OR_A al
;000DE85B   Op15
NEWSYM Op15       ; OR A,labs+x  A <- A OR (abs+X)    N.....Z.
      SPCaddr_LABS_X bx
      SPC_OR_A al
;000DE887   Op25
NEWSYM Op25       ; AND A,labs   A <- A AND (abs)     N.....Z.
      SPCaddr_LABS
      SPC_AND_A al
;000DE8AC   Op35
NEWSYM Op35       ; AND A,labs+X A <- A AND (abs+X)   N.....Z.
      SPCaddr_LABS_X bx
      SPC_AND_A al
;000DE8D8   Op45
NEWSYM Op45       ; EOR A,labs   A <- A EOR (abs)     N.....Z.
      SPCaddr_LABS
      SPC_EOR_A al
;000DE8FD   Op55
NEWSYM Op55       ; EOR A,labs+X A <- A EOR (abs+X)   N.....Z.
      SPCaddr_LABS_X bl
      SPC_EOR_A al
;000DE928   Op65
NEWSYM Op65       ; CMP A,labs   A-(abs)              N.....ZC
      SPCaddr_LABS
      SPC_CMP_A al
;000DE990   Op75
NEWSYM Op75       ; CMP A,labs+X A-(abs+X)            N.....ZC
      SPCaddr_LABS_X bx
      SPC_CMP_A al
;000DE9FF   Op85
NEWSYM Op85       ; ADC A,labs   A <- A+(abs)+C       NV..H.ZC
      SPCaddr_LABS
      SPC_ADC_A al
;000DEAA2   Op95
NEWSYM Op95       ; ADC A,labs+X A <- A+(abs+X)+C     NV..H.ZC
      SPCaddr_LABS_X bl
      SPC_ADC_A al
;000DEB4B   OpA5
NEWSYM OpA5       ; SBC A,labs   A <- A-(abs)-!C      NV..H.ZC
      SPCaddr_LABS
      SPC_SBC_A al
;000DEBEF   OpB5
NEWSYM OpB5       ; SBC A,labs+x A <- A-(abs+X)-!C    NV..H.ZC
      SPCaddr_LABS_X bl
      SPC_SBC_A al
;000DEC99   OpC5
NEWSYM OpC5       ; MOV labs,A   A -> (abs)           ........
      mov bx,[ebp]
      mov al, byte [spcA]
      add ebp,2
      add ebx,spcRam
      WriteByte
      jmp spcopcdone
;000DECF6   OpD5
NEWSYM OpD5       ; MOV labs+X,A A -> (abs+X)         ........
      mov bl,[spcX]
      add bx,[ebp]
      mov al, byte [spcA]
      add ebp,2
      add ebx,spcRam
      WriteByte
      jmp spcopcdone
;000DED59   OpE5
NEWSYM OpE5       ; MOV A,labs   A <- (abs)           N......Z
;      SPCaddr_LABS_ReadByte
      mov bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      SPC_MOV_A
;000DED9C   OpF5
NEWSYM OpF5       ; MOV A,labs+X A <- (abs+X)         N......Z
;      SPCaddr_LABS_X_ReadByte
      mov bl,[spcX]
      add bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      SPC_MOV_A
;000DEDE5   Op06
NEWSYM Op06       ; OR A,(X)     A <- A OR (X)        N.....Z.
      SPCaddr__X_
      SPC_OR_A al
;000DEE08   Op16
NEWSYM Op16       ; OR A,labs+Y  A <- A OR (abs+Y)    N......Z.
      SPCaddr_LABS_Y
      SPC_OR_A al
;000DEE33   Op26
NEWSYM Op26       ; AND A,(X)    A <- A AND (X)       N......Z.
      SPCaddr__X_
      SPC_AND_A al
;000DEE56   Op36
NEWSYM Op36       ; AND A,labs+Y A <- A AND (abs+Y)   N......Z.
      SPCaddr_LABS_Y
      SPC_AND_A al
;000DEE81   Op46
NEWSYM Op46       ; EOR A,(X)    A <- A EOR (X)       N......Z.
      SPCaddr__X_
      SPC_EOR_A al
;000DEEA4   Op56
NEWSYM Op56       ; EOR A,labs+Y A <- A EOR (abs+Y)   N......Z.
      SPCaddr_LABS_Y
      SPC_EOR_A al
;000DEECF   Op66
NEWSYM Op66       ; CMP A,(X)    A-(X)                N......ZC
      SPCaddr__X_
      SPC_CMP_A al
;000DEF35   Op76
NEWSYM Op76       ; CMP A,labs+Y A-(abs+Y)            N......ZC
      SPCaddr_LABS_Y
      SPC_CMP_A al
;000DEFA3   Op86
NEWSYM Op86       ; ADC A,(X)    A <- A+(X)+C         NV..H..ZC
      SPCaddr__X_
      SPC_ADC_A al
;000DF044   Op96
NEWSYM Op96       ; ADC A,labs+Y A <- A+(abs+Y)+C     NV..H..ZC
      SPCaddr_LABS_Y
      SPC_ADC_A al
;000DF0ED   OpA6
NEWSYM OpA6       ; SBC A,(X)    A <- A-(X)-!C        NV..H..ZC
      SPCaddr__X_
      SPC_SBC_A al
;000DF18F   OpB6
NEWSYM OpB6       ; SBC A,labs+Y A <- A-(abs+Y)-!C    NV..H..ZC
;      SPCaddr_LABS_Y
      mov bl,[spcY]
      add bx,[ebp]
      mov al,[spcRam+ebx]
      add ebp,2
      SPC_SBC_A al
;000DF239   OpC6
NEWSYM OpC6       ; MOV (X),A    A -> (X)             ........
      mov bl,[spcX]
      add ebx,[spcRamDP]
      mov al, byte [spcA]
      WriteByte
      jmp spcopcdone
;000DF292   OpD6
NEWSYM OpD6       ; MOV labs+Y,A A -> (abs+Y)         ........
      mov bl,[spcY]
      mov al, byte [spcA]
      add bx,[ebp]
      add ebp,2
      add ebx,spcRam
      WriteByte
      jmp spcopcdone
;000DF2F5   OpE6
NEWSYM OpE6       ; MOV A,(X)    A <- (X)             N......Z
;      SPCaddr__X__ReadByte
      mov bl,[spcX]
      add ebx,[spcRamDP]
      ReadByte
      SPC_MOV_A
;000DF334   OpF6
NEWSYM OpF6       ; MOV A,labs+Y A <- (abs+Y)         N......Z
;      SPCaddr_LABS_Y_ReadByte
      mov bl,[spcY]
      add bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      SPC_MOV_A
;000DF37D   Op07
NEWSYM Op07       ; OR A,(dp+X)  A <- A OR ((dp+X+1)(dp+X))  N......Z.
      SPCaddr_bDP_Xb
      SPC_OR_A bl
;000DF3AD   Op17
NEWSYM Op17       ; OR A,(dp)+Y  A <- A OR ((dp+1)(dp)+Y)   N......Z.
      SPCaddr_bDPb_Y
      SPC_OR_A bl
;000DF3DE   Op27
NEWSYM Op27       ; AND A,(dp+X) A <- A AND ((dp+X+1)(dp+X)) N......Z.
      SPCaddr_bDP_Xb
      SPC_AND_A bl
;000DF40E   Op37
NEWSYM Op37       ; AND A,(dp)+Y A <- A AND ((dp+1)(dp)+Y)  N......Z.
      SPCaddr_bDPb_Y
      SPC_AND_A bl
;000DF43F   Op47
NEWSYM Op47       ; EOR A,(dp+X) A <- A EOR ((dp+X+1)(dp+X)) N......Z.
      SPCaddr_bDP_Xb
      SPC_EOR_A bl
;000DF46F   Op57
NEWSYM Op57       ; EOR A,(dp)+Y A <- A EOR ((dp+1)(dp)+Y)  N......Z.
      SPCaddr_bDPb_Y
      SPC_EOR_A bl
;000DF4A0   Op67
NEWSYM Op67       ; CMP A,(dp+X) A-((dp+X+1)(dp+X))      N......ZC
      SPCaddr_bDP_Xb
      SPC_CMP_A bl
;000DF513   Op77
NEWSYM Op77       ; CMP A,(dp)+Y A-((dp+1)(dp)+Y)        N......ZC
      SPCaddr_bDPb_Y
      SPC_CMP_A bl
;000DF587   Op87
NEWSYM Op87       ; ADC A,(dp+X) A <- A+((dp+X+1)(dp+X)) NV..H..ZC
      SPCaddr_bDP_Xb
      SPC_ADC_A bl
;000DF635   Op97
NEWSYM Op97       ; ADC A,(dp)+Y A <- A+((dp+1)(dp)+Y)   NV..H..ZC
;      SPCaddr_bDPb_Y
      mov bl,[ebp]
      xor eax,eax
      add ebx,[spcRamDP]
      mov ax,[ebx]
      inc ebp
      add ax,[spcY]
      mov bl,[spcRam+eax]
      SPC_ADC_A bl
;000DF6E4   OpA7
NEWSYM OpA7       ; SBC A,(dp+X) A <- A-((dp+X+1)(dp+X))-!C NV..H..ZC
      SPCaddr_bDP_Xb
      SPC_SBC_A bl
;000DF793   OpB7
NEWSYM OpB7       ; SBC A,(dp)+Y A <- A-((dp+1)(dp)+Y)-!C   NV..H..ZC
;      SPCaddr_bDPb_Y
      mov bl,[ebp]
      xor eax,eax
      add ebx,[spcRamDP]
      mov ax,[ebx]
      inc ebp
      add ax,[spcY]
      mov bl,[spcRam+eax]
      SPC_SBC_A bl
;000DF843   OpC7
NEWSYM OpC7       ; MOV (dp+X),A A -> ((dp+X+1)(dp+X))     ........
      mov bl,[ebp]
      add bl,[spcX]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax, word [ebx]
      mov ebx,eax
      add ebx,spcRam
      mov al, byte [spcA]
      WriteByte
      jmp spcopcdone
;000DF8AD   OpD7
NEWSYM OpD7       ; MOV (dp)+Y,A A -> ((dp+1)(dp)+Y)       ........
      mov bl,[ebp]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax, word [ebx]
      add ax,[spcY]
      mov ebx,eax
      add ebx,spcRam
      mov al, byte [spcA]
      WriteByte
      jmp spcopcdone
;000DF918   OpE7
NEWSYM OpE7       ; MOV A,(dp+X) A <- ((dp+X+1)(dp+X))     N......Z
;      SPCaddr_bDP_Xb_ReadByte
      mov bl,[ebp]
      add bl,[spcX]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax, word [ebx]
      mov ebx,eax
      add ebx,spcRam
      ReadByte
      SPC_MOV_A
;000DF968   OpF7
NEWSYM OpF7       ; MOV A,(dp)+Y A <- ((dp+1)(dp)+Y)       N......Z
;      SPCaddr_bDPb_Y_ReadByte
      xor eax,eax
      mov al,[ebp]
      add eax,[spcRamDP]
      inc ebp
      mov bx,[eax]
      add bx,[spcY]
      add ebx,spcRam
      ReadByte
      SPC_MOV_A
;000DF9B7   Op08
NEWSYM Op08       ; OR A,#inm    A <- A OR inm            N......Z.
      mov bl,[ebp]
      inc ebp
      or byte [spcA], bl
      mov al,[spcA]
      mov [spcNZ],al
      jmp spcopcdone
;000DF9D0   Op18
NEWSYM Op18       ; OR dp,#inm   (dp) <- (dp) OR inm         N......Z.
      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      or al,ah
      mov [spcNZ],al
      WriteByte
      jmp spcopcdone
;000DFA33   Op28
NEWSYM Op28       ; AND A,#inm   A <- A AND inm           N......Z.
      mov bl,[ebp]
      inc ebp
      and byte [spcA], bl
      mov al,[spcA]
      mov [spcNZ],al
      jmp spcopcdone
;000DFA4C   Op38
NEWSYM Op38       ; AND dp,#inm  (dp) <- (dp) AND inm        N......Z.
      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      and al,ah
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000DFAAF   Op48
NEWSYM Op48       ; EOR A,#inm   A <- A EOR inm           N......Z.
      mov bl,[ebp]
      inc ebp
      xor byte [spcA], bl
      mov al,[spcA]
      mov [spcNZ],al
      jmp spcopcdone
;000DFAC8   Op58
NEWSYM Op58      ; EOR dp,#inm  (dp) <- (dp) EOR inm        N......Z.
      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      xor al,ah
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000DFB2B   Op68
NEWSYM Op68       ; CMP A,#inm   A-inm                   N......ZC
      mov bl,[ebp]
      inc ebp
      cmp byte [spcA], bl
      cmc
      SPCSetFlagnzc
;000DFB87   Op78
NEWSYM Op78       ; CMP dp,#inm  (dp)-inm                N......ZC
      mov bl,[ebp+1]
      add ebx,[spcRamDP]
      mov ah,[ebp]
      mov al,[ebx]
      add ebp,2
      cmp al,ah
      cmc
      SPCSetFlagnzcnoret
      jmp spcopcdone
;000DFBEF   Op88
NEWSYM Op88       ; ADC A,#inm   A <- A+inm+C            NV..H..ZC
      mov bl,[ebp]
      inc ebp
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc byte [spcA], bl
      SPCSetFlagnvhzc
;000DFC86   Op98
NEWSYM Op98       ; ADC dp,#inm  (dp) <- (dp)+inm+C         NV..H..ZC
;      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      mov bl,[ebp+1]
      mov ah,[ebp]
      add ebx,[spcRamDP]
      mov al,[ebx]
      add ebp,2
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc al, ah
      SPCSetFlagnvhzcnoret
      WriteByte
      jmp spcopcdone
;000DFD6C   OpA8
NEWSYM OpA8       ; SBC A,#inm   A <- A-inm-!C           NV..H..ZC
      mov bl,[ebp]
      inc ebp
;      SPC_SBC_A
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb byte [spcA], bl
      cmc
      SPCSetFlagnvhzc
;000DFE04   OpB8
NEWSYM OpB8       ; SBC dp,#inm  (dp) <- (dp)-inm-!C        NV..H..ZC
;      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      mov bl,[ebp+1]
      mov ah,[ebp]
      add ebx,[spcRamDP]
      mov al,[ebx]
      add ebp,2
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb al,ah
      cmc
      SPCSetFlagnvhzcnoret
      WriteByte
      jmp spcopcdone
;000DFEEB   OpC8
NEWSYM OpC8       ; CMP X,#inm   X-inm                   N......ZC
      mov bl,[ebp]
      inc ebp
      cmp [spcX],bl
      cmc
      SPCSetFlagnzc
;000DFF47   OpD8
NEWSYM OpD8       ; MOV dp,X     X -> (dp)                 ........
      mov bl,[ebp]
      mov al, byte [spcX]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      jmp spcopcdone
;000DFF9E   OpE8
NEWSYM OpE8       ;  MOV A,#inm  A <- inm                  N......Z
      mov bl,[ebp]
      inc ebp
;      SPC_MOV_A
      mov byte [spcA], bl
      mov al,bl
      mov [spcNZ],al
      jmp spcopcdone
;000DFFB4   OpF8
NEWSYM OpF8       ;  MOV X,dp    X <- (dp)                 N......Z
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      mov byte [spcX], al
      mov [spcNZ],al
      jmp spcopcdone
;000DFFF1   Op09
NEWSYM Op09       ; OR dp(d),dp(s)  (dp(d))<-(dp(d)) OR (dp(s))  N......Z.
      spcaddrDPbDb_DPbSb
      or al,ah
      mov [spcNZ], al
      WriteByte
      xor ecx,ecx
      jmp spcopcdone
;000E0060   Op19
NEWSYM Op19       ; OR (X),(Y)   (X) <- (X) OR (Y)          N......Z.
;      spcaddrDPbXb_bYb
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      or [ebx],al
      mov al,[ebx]
      mov [spcNZ],al
      jmp spcopcdone
;000E008A   Op29
NEWSYM Op29       ; AND dp(d),dp(s) (dp(d))<-(dp(d)) AND (dp(s)) N......Z.
      spcaddrDPbDb_DPbSb
      and al,ah
      mov [spcNZ], al
      WriteByte
      xor ecx,ecx
      jmp spcopcdone
;000E00F9   Op39
NEWSYM Op39       ; AND (X),(Y)  (X) <- (X) AND (Y)         N......Z.
;      spcaddrDPbXb_bYb
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      and [spcRam+ebx], al
      mov al,[spcRam+ebx]
      mov [spcNZ],al
      jmp spcopcdone
;000E012B   Op49
NEWSYM Op49       ; EOR dp(d),dp(s) (dp(d))<-(dp(d)) EOR (dp(s)) N......Z.
      spcaddrDPbDb_DPbSb
      xor al,ah
      mov [spcNZ], al
      WriteByte
      xor ecx,ecx
      jmp spcopcdone
;000E019A   Op59
NEWSYM Op59       ; EOR (X),(Y)  (X) <- (X) EOR (Y)         N......Z.
;      spcaddrDPbXb_bYb
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      xor [ebx], al
      mov al,[ebx]
      mov [spcNZ],al
      jmp spcopcdone
;000E01C4   Op69
NEWSYM Op69       ; CMP dp(d),dp(s) (dp(d))-(dp(s))         N......ZC
      spcaddrDPbDb_DPbSb
      cmp al,ah
      cmc
      SPCSetFlagnzcnoret
      WriteByte
      xor ecx,ecx
      jmp spcopcdone
;000E027B   Op79
NEWSYM Op79       ; CMP (X),(Y)  (X)-(Y)                 N......ZC
;      spcaddrDPbXb_bYb Op79b:
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      cmp [ebx], al
      cmc
      SPCSetFlagnzc
;000E02EB   Op89
NEWSYM Op89       ; ADC dp(d),dp(s) (dp(d))<-(dp(d))+(dp(s))+C  NV..H..ZC
      spcaddrDPbDb_DPbSb
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc al, ah
      SPCSetFlagnvhzcnoret
      WriteByte
      xor ecx,ecx
      jmp spcopcdone
;000E03DD   Op99
NEWSYM Op99       ; ADC (X),(Y)  (X) <- (X)+(Y)+C          NV..H..ZC
;      spcaddrDPbXb_bYb Op99b:
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc [ebx],al
      SPCSetFlagnzc
;000E0457   OpA9
NEWSYM OpA9       ; SBC dp(d),dp(s) (dp(d))<-(dp(d))-(dp(s))-!C NV..H..ZC
      spcaddrDPbDb_DPbSb
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb al,ah
      SPCSetFlagnvhzcnoret
      WriteByte
      xor ecx,ecx
      jmp spcopcdone
;000E0549   OpB9
NEWSYM OpB9       ; SBC (X),(Y)  (X) <- (X)-(Y)-!C         NV..H..ZC
;      spcaddrDPbXb_bYb OpB9b:
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb [ebx],al
      cmc
      SPCSetFlagnzc
;000E05C4   OpC9
NEWSYM OpC9       ; MOV labs,X   X -> (abs)                ........
      mov bx,[ebp]
      mov al, byte [spcX]
      add ebp,2
      add ebx,spcRam
      WriteByte
      jmp spcopcdone
;000E0621   OpD9
NEWSYM OpD9       ; MOV dp+Y,X   X -> (dp+Y)               ........
      mov bl,[ebp]
      mov al, byte [spcX]
      add bl,[spcY]
      inc ebp
      add ebx,[spcRamDP]
      WriteByte
      jmp spcopcdone
;000E067E   OpE9
NEWSYM OpE9       ; MOV X,labs   X <- (abs)                N......Z
      mov bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      mov byte [spcX], al
      mov [spcNZ],al
      jmp spcopcdone
;000E06C1   OpF9
NEWSYM OpF9       ; MOV X,dp+Y   X <- (dp+Y)               N......Z
      mov bl,[ebp]
      add bl,[spcY]
      inc ebp
      add ebx,[spcRamDP]
      ReadByte
      mov byte [spcX], al
      mov [spcNZ],al
      jmp spcopcdone
;000E0704   Op0A
NEWSYM Op0A       ; OR1 C,mem.bit   C <- C OR  (mem.bit)      ........C
      spcaddrmembit
      and al,01h
      or [spcP],al
      xor ecx,ecx
      jmp spcopcdone
;000E072E   Op1A
NEWSYM Op1A       ; DECW dp   Decrement dp memory pair  N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      dec ax
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      jmp .SkipFlag
.YesNeg
      mov byte [spcNZ],80h
      jmp .SkipFlag
.YesZero
      mov byte [spcNZ],0
      jmp .SkipFlag
.SkipFlag
      push ebx
      WriteByte
      pop ebx
      inc ebx
      mov al,ah
      WriteByte
      jmp spcopcdone
;000E07FD   Op2A
NEWSYM Op2A       ; OR1 C,/mem.bit  C <- C OR  !(mem.bit)     ........C
      spcaddrmembit
      and al,01h
      xor al,01h
      or [spcP],al
      xor ecx,ecx
      jmp spcopcdone
;000E0829   Op3A
NEWSYM Op3A       ; INCW dp   Increment dp memory pair  N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      inc ax
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      jmp .SkipFlag
.YesNeg
      mov byte [spcNZ],80h
      jmp .SkipFlag
.YesZero
      mov byte [spcNZ],0
      jmp .SkipFlag
.SkipFlag
      push ebx
      WriteByte
      pop ebx
NEWSYM Op3Ab
      inc ebx
      mov al,ah
      WriteByte
      jmp spcopcdone
;000E08F8   Op4A
NEWSYM Op4A       ; AND1 C,mem.bit  C <- C AND (mem.bit)      ........C
      mov bx,[ebp]

      mov cl,bl
      add ebp,2
      shr bx,3
      and cl,7
      mov al,[spcRam+ebx]
      shr al,cl
      or al,0FEh
      and [spcP],al
      xor ecx,ecx
      jmp spcopcdone
; looks like there is the Carry flag checked in op5a..
;000E0922   Op5A
NEWSYM Op5A       ; CMPW YA,dp   YA - (dp+1)(dp)        N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov bl,[spcA]
      mov bh,[spcY]
      cmp bx,ax
      cmc
      SPCSetFlagnzc
;000E0990   Op6A
NEWSYM Op6A       ; AND1 C,/mem.bit C <- C AND !(mem.bit)     ........C
      mov bx,[ebp]

      mov cl,bl
      add ebp,2
      shr bx,3
      and cl,7
      mov al,[spcRam+ebx]
      shr al,cl
      or al,0FEh
      xor al,01h
      and [spcP],al
      xor ecx,ecx
      jmp spcopcdone
;000E09BC   Op7A
NEWSYM Op7A       ; ADDW YA,dp   YA  <- YA + (dp+1)(dp)   NV..H..ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov bl,[spcA]
      mov bh,[spcY]
      add bx,ax
      mov [spcA],bl
      mov [spcY],bh
      SPCSetFlagnvhzc
;000E0A66   Op8A
NEWSYM Op8A       ; EOR1 C,mem.bit  C <- C EOR (mem.bit)      ........C
      spcaddrmembit
      and al,01h
      xor [spcP],al
      xor ecx,ecx
      jmp spcopcdone
;000E0A90   Op9A
NEWSYM Op9A       ; SUBW YA,dp   YA  <- YA - (dp+1)(dp)   NV..H..ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov bl,[spcA]
      mov bh,[spcY]
      sub bx,ax
      cmc
      mov [spcA],bl
      mov [spcY],bh
      SPCSetFlagnvhzc
;000E0B3B   OpAA
NEWSYM OpAA       ; MOV1 C,mem.bit  C <- (mem.bit)
      spcaddrmembit
      and byte[spcP],0FEh
      and al,01h
      or [spcP],al
      xor ecx,ecx
      jmp spcopcdone
;000E0B6C   OpBA
NEWSYM OpBA       ; MOVW YA,dp   YA  - (dp+1)(dp)       N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov [spcA],al
      mov [spcY],ah
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      jmp spcopcdone
.YesNeg
      mov byte [spcNZ],80h
      jmp spcopcdone
.YesZero
      mov byte [spcNZ],0
      jmp spcopcdone
;000E0BB4   OpCA
NEWSYM OpCA       ; MOV1 mem.bit,C  C -> (mem.bit)            .........
      push edx
      mov bx,[ebp]
      mov al,[spcP]

      mov cl,bl
      mov ah,01h
      and cl,7
      shr bx,3
      shl ah,cl
      and al,01h
      add ebp,2
      shl al,cl
      mov ecx,spcRam
      ; al = carry flag positioned in correct location, ah = 1 positioned
      xor ah,0FFh
      add ecx,ebx
      mov dl,[ecx]
      and dl,ah
      or dl,al
      mov al,dl
      mov ebx,ecx
      WriteByte
      pop edx
      xor ecx,ecx
      jmp spcopcdone
;000E0C34   OpDA
NEWSYM OpDA       ; MOVW dp,YA   (dp+1)(dp) - YA         .........
      mov bl,[ebp]
      inc ebp
      add ebx,[spcRamDP]
      mov al,[spcA]
      push ebx
      WriteByte
      pop ebx
      inc ebx
      mov al,[spcY]
      WriteByte
      jmp spcopcdone
;000E0CD6   OpEA
NEWSYM OpEA       ; NOT1 mem.bit    complement (mem.bit)      .........
      push edx
      mov bx,[ebp]

      mov cl,bl
      mov ah,01h
      and cl,7
      shr bx,3
      shl ah,cl
      mov ecx,spcRam
      add ebp,2
      add ecx,ebx
      mov dl,[ecx]
      xor dl,ah
      mov al,dl
      mov ebx,ecx
      WriteByte
      pop edx
      xor ecx,ecx
      jmp spcopcdone
;000E0D48   OpFA
NEWSYM OpFA       ; MOV dp(d),dp(s) (dp(d)) <- (dp(s))        ........
      xor ecx,ecx
      mov bl,[ebp+1]
      mov cl,[ebp]
      add ebx,[spcRamDP]
      add ecx,[spcRamDP]
      add ebp,2
      mov al,[ecx]
      WriteByte
      xor ecx,ecx
      jmp spcopcdone
;000E0DAE   Op0B
NEWSYM Op0B       ; ASL dp    C << (dp)   <<0       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      shl al,1
      SPCSetFlagnzcnoret
      WriteByte
      jmp spcopcdone
;000E0E50   Op1B
NEWSYM Op1B       ; ASL dp+X  C << (dp+X) <<0       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      shl al,1
      SPCSetFlagnzcnoret
      WriteByte
      jmp spcopcdone
;000E0EF8   Op2B
NEWSYM Op2B       ; ROL dp    C << (dp)   <<C       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op2Bb
      mov al,[ebx]
      clc
      spcROLstuff
      WriteByte
      jmp spcopcdone
.Op2Bb
      mov al,[ebx]
      stc
      spcROLstuff
      WriteByte
      jmp spcopcdone
;000E0FE7   Op3B
NEWSYM Op3B       ; ROL dp+X  C << (dp+X) <<C       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op3Bb
      mov al,[ebx]
      clc
      spcROLstuff
      WriteByte
      jmp spcopcdone
.Op3Bb
      mov al,[ebx]
      stc
      spcROLstuff
      WriteByte
      jmp spcopcdone
;000E10DC   Op4B
NEWSYM Op4B       ; LSR dp    0 >> (dp)   <<C       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      shr al,1
      SPCSetFlagnzcnoret
      WriteByte
      jmp spcopcdone
;000E117E   Op5B
NEWSYM Op5B       ; LSR dp+X  0 >> (dp+X) <<C       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      inc ebp
      add ebx,[spcRamDP]
      mov al,[ebx]
      shr al,1
      SPCSetFlagnzcnoret
      WriteByte
      jmp spcopcdone
;000E1226   Op6B
NEWSYM Op6B       ; ROR dp    C >> (dp)   <<C       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op6Bb
      mov al,[ebx]
      clc
      spcRORstuff
      WriteByte
      jmp spcopcdone
.Op6Bb
      mov al,[ebx]
      stc
      spcRORstuff
      WriteByte
      jmp spcopcdone
;000E1315   Op7B
NEWSYM Op7B       ; ROR dp+X  C >> (dp+X) <<C       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op7Bb
      mov al,[ebx]
      clc
      spcRORstuff
      WriteByte
      jmp spcopcdone
.Op7Bb
      mov al,[ebx]
      stc
      spcRORstuff
      WriteByte
      jmp spcopcdone
;000E140A   Op8B
NEWSYM Op8B       ;  DEC dp   -- (dp)               N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      dec al
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000E1465   Op9B
NEWSYM Op9B       ;  DEC dp+X -- (dp+X)             N......Z.
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      dec al
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000E14C6   OpAB
NEWSYM OpAB       ; INC dp    ++ (dp)               N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      inc al
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000E1521   OpBB
NEWSYM OpBB       ; INC dp+X  ++ (dp+X)             N......Z.
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      inc al
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000E1582   OpCB
NEWSYM OpCB       ; MOV dp,Y  Y -> (dp)                 ........
      mov bl,[ebp]
      mov al, byte [spcY]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      jmp spcopcdone
;000E15D9   OpDB
NEWSYM OpDB       ; MOV dp+X,Y   X -> (dp+X)               ........
      mov bl,[ebp]
      add bl,[spcX]
      mov al, byte [spcY]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      jmp spcopcdone
;000E1636   OpEB
NEWSYM OpEB       ; MOV Y,dp  Y <- (dp)                 N......Z
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      mov byte [spcY], al
      mov [spcNZ],al
      jmp spcopcdone
;000E1673   OpFB
NEWSYM OpFB       ; MOV Y,dp+X   Y <- (dp+X)               N......Z
      mov bl,[ebp]
      add bl,[spcX]
      inc ebp
      add ebx,[spcRamDP]
      ReadByte
      mov byte [spcY], al
      mov [spcNZ],al
      jmp spcopcdone
;000E16B6   Op0C
NEWSYM Op0C       ; ASL labs  C << (abs)  <<0       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      shl al,1
      SPCSetFlagnzcnoret
      WriteByte
      jmp spcopcdone
;000E175E   Op1C
NEWSYM Op1C       ; ASL A  C << A      <<0       N......ZC
      shl byte [spcA],1
      SPCSetFlagnzc
;000E17B5   Op2C
NEWSYM Op2C       ; ROL labs  C << (abs)  <<C       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      test byte [spcP],01h
      jnz near .Op2Cb
      mov al,[ebx]
      clc
      spcROLstuff
      WriteByte
      jmp spcopcdone
.Op2Cb
      mov al,[ebx]
      stc
      spcROLstuff
      WriteByte
      jmp spcopcdone
;000E18AA   Op3C
NEWSYM Op3C       ; ROL A  C << A      <<C       N......ZC
      test byte [spcP],01h
      jnz near .Op3Cb
      clc
      rcl byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp spcopcdone
.setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
      jmp spcopcdone
.Op3Cb
      stc
      rcl byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag2
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp spcopcdone
.setcarryflag2
      or byte[spcP],01h
      mov [spcNZ],al
      jmp spcopcdone
;000E1917   Op4C
NEWSYM Op4C       ; LSR labs  0 >> (abs)  <<C       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      shr al,1
      SPCSetFlagnzcnoret
      WriteByte
      jmp spcopcdone
;000E19BF   Op5C
NEWSYM Op5C       ; LSR A  0 >> A      <<C       N......ZC
      shr byte [spcA],1
      SPCSetFlagnzc
;000E1A16   Op6C
NEWSYM Op6C       ; ROR labs  C >> (abs)  <<C       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      test byte [spcP],01h
      jnz near .Op6Cb
      mov al,[ebx]
      clc
      spcRORstuff
      WriteByte
      jmp spcopcdone
.Op6Cb
      mov al,[ebx]
      stc
      spcRORstuff
      WriteByte
      jmp spcopcdone
;000E1B0B   Op7C
NEWSYM Op7C       ; ROR A  C >> A      <<C       N......ZC
      test byte [spcP],01h
      jnz near .Op7Cb
      clc
      rcr byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp spcopcdone
.setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
      jmp spcopcdone
.Op7Cb
      stc
      rcr byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag2
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp spcopcdone
.setcarryflag2
      or byte[spcP],01h
      mov [spcNZ],al
      jmp spcopcdone
;000E1B78   Op8C
NEWSYM Op8C       ; DEC labs  -- (abs)              N......Z.
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      dec al
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000E1BD9   Op9C
NEWSYM Op9C       ; DEC A  -- A                  N......Z.
      dec byte [spcA]
      mov al,byte [spcA]
      mov [spcNZ],al
      jmp spcopcdone
;000E1BEE   OpAC
NEWSYM OpAC       ; INC labs  ++ (abs)              N......Z.
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      inc al
      mov [spcNZ], al
      WriteByte
      jmp spcopcdone
;000E1C4F   OpBC
NEWSYM OpBC       ; INC A  ++ A                  N......Z.
      inc byte [spcA]
      mov al,byte [spcA]
      mov [spcNZ],al
      jmp spcopcdone
;000E1C64   OpCC
NEWSYM OpCC       ; MOV labs,Y   Y -> (abs)                ........
      mov bx,[ebp]
      mov al, byte [spcY]
      add ebp,2
      add ebx,spcRam
      WriteByte
      jmp spcopcdone
;000E1CC1   OpDC
NEWSYM OpDC       ; DEC Y  -- Y                  N......Z.
      dec byte [spcY]
      mov al,byte [spcY]
      mov [spcNZ],al
      jmp spcopcdone
;000E1CD6   OpEC
NEWSYM OpEC       ; MOV Y,labs   Y <- (abs)                N......Z
      mov bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      mov byte [spcY],al
      mov [spcNZ],al
      jmp spcopcdone
;000E1D19   OpFC
NEWSYM OpFC       ; INC Y  ++ Y                  N......Z.
      inc byte [spcY]
      mov al,byte [spcY]
      mov [spcNZ],al
      jmp spcopcdone
;000E1D2E   Op0D
NEWSYM Op0D       ; PUSH PSW     push PSW to stack       .........
      mov eax,[spcS]
      mov bl,[spcP]
      and bl,01111101b
      test byte [spcNZ],80h
      jnz .NegSet
      cmp byte [spcNZ],0
      je .ZeroSet
      dec byte [spcS]
      mov [spcRam+eax],bl
      jmp spcopcdone
.NegSet
      or bl,80h
      dec byte [spcS]
      mov [spcRam+eax],bl
      jmp spcopcdone
.ZeroSet
      or bl,02h
      dec byte [spcS]
      mov [spcRam+eax],bl
      jmp spcopcdone
;000E1D87   Op1D
NEWSYM Op1D       ; DEC X     -- X                  N......Z.
      dec byte [spcX]
      mov al,byte [spcX]
      mov [spcNZ],al
      jmp spcopcdone
;000E1D9C   Op2D
NEWSYM Op2D       ; PUSH A       push A to stack         .........
      mov eax,[spcS]
      mov bl,[spcA]
      dec byte [spcS]
      mov [spcRam+eax],bl
      jmp spcopcdone
;000E1DB8   Op3D
NEWSYM Op3D       ; INC X     ++ X                  N......Z.
      inc byte [spcX]
      mov al,byte [spcX]
      mov [spcNZ],al
      jmp spcopcdone
;000E1DCD   Op4D
NEWSYM Op4D       ; PUSH X       push X to stack         .........
      mov eax,[spcS]
      mov bl,[spcX]
      dec byte [spcS]
      mov [spcRam+eax],bl
      jmp spcopcdone
;000E1DE9   Op5D
NEWSYM Op5D       ; MOV X,A      X <- A                   N......Z
      mov al,[spcA]
      mov [spcX],al
      mov [spcNZ],al
      jmp spcopcdone
;000E1DFD   Op6D
NEWSYM Op6D       ; PUSH Y    push Y to stack         .........
      mov eax,[spcS]
      mov bl,[spcY]
      dec byte [spcS]
      mov [spcRam+eax],bl
      jmp spcopcdone
;000E1E19   Op7D
NEWSYM Op7D       ; MOV A,X      A <- X                   N......Z
      mov al,[spcX]
      mov [spcA],al
      mov [spcNZ],al
      jmp spcopcdone
;000E1E2D   Op8D
NEWSYM Op8D       ; MOV Y,#inm   Y <- inm                  N......Z
      mov bl,[ebp]
      mov [spcY],bl
      inc ebp
      mov al,bl
      mov [spcNZ],al
      jmp spcopcdone
;000E1E43   Op9D
NEWSYM Op9D       ; MOV X,SP     X <- SP                  N......Z
      mov al,[spcS]
      mov [spcX],al
      mov [spcNZ],al
      jmp spcopcdone
;000E1E57   OpAD
NEWSYM OpAD       ; CMP Y,#inm   Y-inm                   N......ZC
      mov bl,[ebp]
      inc ebp
      cmp [spcY],bl
      cmc
      SPCSetFlagnzc
;000E1EB3   OpBD
NEWSYM OpBD       ; MOV SP,X     SP <- X                   ........
      mov al,[spcX]
      mov [spcS],al
      jmp spcopcdone
;000E1EC2   OpCD
NEWSYM OpCD       ; MOV X,#inm   X <- inm                  N......Z
      mov bl,[ebp]
      mov [spcX],bl
      inc ebp
      mov al,bl
      mov [spcNZ],al
      jmp spcopcdone
;000E1ED8   OpDD
NEWSYM OpDD       ; MOV A,Y      A <- Y                   N......Z
      mov al,[spcY]
      mov [spcA],al
      mov [spcNZ],al
      jmp spcopcdone
;  NOTC               ED    1     3   complement carry flag     .......C 
;000E1EEC   OpED
NEWSYM OpED       ; NOTC         complement carry flag     .......C 
      xor byte [spcP],00000001b
      jmp spcopcdone
;000E1EF8   OpFD
NEWSYM OpFD       ; MOV Y,A      Y <- A                   N......Z
      mov al,[spcA]
      mov [spcY],al
      mov [spcNZ],al
      jmp spcopcdone
;000E1F0C   Op0E
NEWSYM Op0E       ; TSET1 labs   test and set bits with A   N......Z.
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      mov ah,al
      and ah,[spcA]
      mov [spcNZ],ah
      or al,[spcA]
      WriteByte
      jmp spcopcdone
;000E1F7A   Op1E
NEWSYM Op1E       ; CMP X,labs   X-(abs)                 N......ZC
      mov bx,[ebp]
      add ebp,2
      mov al,[spcRam+ebx]
      cmp byte [spcX], al
      cmc
      SPCSetFlagnzc
;000E1FE2   Op2E
NEWSYM Op2E       ; CBNE dp,rel  compare A with (dp) then BNE   ...
      mov bl,[ebp]
      add ebx,[spcRamDP]
      mov al,[ebx]
      cmp byte [spcA], al
      jne .Jump
      add ebp,2
      jmp spcopcdone
.Jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      jmp spcopcdone
;000E2011   Op3E
NEWSYM Op3E       ; CMP X,dp     X-(dp)                  N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      cmp byte [spcX], al
      cmc
      SPCSetFlagnzc
;000E2075   Op4E
NEWSYM Op4E       ; TCLR1     test and clear bits with A N......Z.
      mov bx,[ebp]
      add ebx,[spcRamDP]
      add ebp,2
      mov al,[ebx]
      mov ah,al
      and ah,[spcA]
      mov [spcNZ],ah
      mov ah,[spcA]
      not ah
      and al,ah
      WriteByte
      jmp spcopcdone
;000E20E7   Op5E
NEWSYM Op5E       ; CMP Y,labs   Y-(abs)                 N......ZC
      mov bx,[ebp]
      mov al,[spcRam+ebx]
      add ebp,2
      cmp byte [spcY], al
      cmc
      SPCSetFlagnzc
;000E214F   Op6E
NEWSYM Op6E       ; DBNZ   decrement memory (dp) then JNZ ...
      mov bl,[ebp]
      add ebx,[spcRamDP]
      dec byte[ebx]
      jnz .Jump
      add ebp,2
      jmp spcopcdone
.Jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      jmp spcopcdone
;000E2178   Op7E
NEWSYM Op7E       ; CMP Y,dp     Y-(dp)                  N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      cmp byte [spcY], al
      cmc
      SPCSetFlagnzc
;000E21DC   Op8E
NEWSYM Op8E       ; POP PSW   pop PSW from stack     (Restored)
      mov byte [spcNZ],0
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcP],bl
      test byte [spcP],2
      jz .ZeroNo
      mov byte [spcNZ],0
      jmp .DoNeg
.ZeroNo
      or byte [spcNZ],1
.DoNeg
      test byte [spcP],80h
      jz .NoNeg
      or byte [spcNZ],80h
.NoNeg
      mov dword [spcRamDP],spcRam
      test byte[spcP],32
      jnz .setpage1
      jmp spcopcdone
.setpage1
      add dword [spcRamDP],100h
      jmp spcopcdone
;000E224D   Op9E
NEWSYM Op9E       ; DIV YA,X     Q:A B:Y <- YA / X       NV..H..Z.
   push edx
   mov ah,[spcY]
   mov al,[spcA]
   xor bh,bh
   xor dx,dx
   mov bl,[spcX]
   cmp bl,0
   je NoDiv
   div bx
   mov byte[spcA],al
   mov byte[spcY],dl
   cmp ah,0
   jne Over
   and byte [spcP],191-16
   pop edx
   mov [spcNZ],al
   jmp spcopcdone
NEWSYM NoDiv
   mov byte [spcA],0ffh
   mov byte [spcY],0ffh
   or byte [spcP],16
   and byte[spcP],255-64
   pop edx
   jmp spcopcdone
NEWSYM Over
   or byte [spcP],64
   and byte[spcP],255-16
   pop edx
   mov [spcNZ],al
   jmp spcopcdone
;000E22C9   OpAE
NEWSYM OpAE       ; POP A     pop A from stack        .........
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcA],bl
      jmp spcopcdone
;000E22E5   OpBE
NEWSYM OpBE       ; DAS A     decimal adjust for sub  N......ZC
;      jmp spcopcdone
      ; copy al flags into AH
      xor ah,ah
      test byte[spcNZ],80h
      jz .noneg
      or ah,10000000b
.noneg
      test byte[spcP],01h
      jz .nocarry
      or ah,00000001b
.nocarry
      test byte[spcNZ],0FFh
      jnz .nozero
      or ah,01000000b
.nozero
      test byte[spcP],08h
      jz .nohcarry
      or ah,00010000b
.nohcarry
      mov al,[spcA]
      sahf
      das
      mov [spcA],al
      SPCSetFlagnzc
;000E2374   OpCE
NEWSYM OpCE       ; POP X     pop X from stack        .........
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcX],bl
      jmp spcopcdone
;000E2390   OpDE
NEWSYM OpDE       ; CBNE dp+X,rel   compare A with (dp+X) then BNE ...
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      mov al,[ebx]
      cmp byte [spcA], al
      jne .Jump
      add ebp,2
      jmp spcopcdone
.Jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      jmp spcopcdone
;000E23C5   OpEE
NEWSYM OpEE       ; POP Y     pop Y from stack        .........
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcY],bl
      jmp spcopcdone
;000E23E1   OpFE
NEWSYM OpFE       ; DBNZ Y,rel   decrement Y then JNZ           ...
      dec byte [spcY]
      jnz .Jump
      inc ebp
      jmp spcopcdone
.Jump
      movsx ebx,byte [ebp]
      add ebp,ebx
      inc ebp
      jmp spcopcdone
;000E23FB   Op0F
NEWSYM Op0F       ; BRK       software interrupt       ...1.0.. 
;      inc dword[spc700read]
      dec ebp
      jmp spcopcdone
;000E2401   Op1F
NEWSYM Op1F       ; JMP (labs+X)    PC <- (abs+X+1)(abs+X)         ...
      mov bx,[ebp]
      add bx,[spcX]
      xor eax,eax
      add ebp,2
      mov ax, word [spcRam+ebx]
      mov ebp,spcRam
      add ebp,eax
      jmp spcopcdone
;000E2427   Op2F
NEWSYM Op2F       ; BRA rel      branch always                  ...
      movsx ebx,byte [ebp]
      inc ebp
      add ebp,ebx
      jmp spcopcdone
;000E2433   Op3F
NEWSYM Op3F       ; CALL labs    subroutine call          ........
      ; calculate PC
      mov ecx,ebp
      add ecx,2
      sub ecx,spcRam
      mov eax,[spcS]
      mov [spcRam+eax],ch
      dec byte [spcS]
      mov eax,[spcS]
      mov [spcRam+eax],cl
      dec byte [spcS]
      ; set new PC
      mov cx,[ebp]
      add ecx,spcRam
      mov ebp,ecx
      xor ecx,ecx
      jmp spcopcdone
;000E2476   Op4F
NEWSYM Op4F       ; PCALL upage  upage call               ........ 
      ; calculate PC
      mov ecx,ebp
      inc ecx
      sub ecx,spcRam
      mov eax,[spcS]
      mov [spcRam+eax],ch
      dec byte [spcS]
      mov eax,[spcS]
      mov [spcRam+eax],cl
      dec byte [spcS]
      ; set new PC
      xor ecx,ecx
      mov cl,[ebp]
      add ecx,spcRam
      add ecx,0ff00h
      mov ebp,ecx
      xor ecx,ecx
      jmp spcopcdone
; I'm not sure about this one and JMP labs+X...
;000E24BB   Op5F
NEWSYM Op5F       ; JMP labs     jump to new location           ...
      mov bx,[ebp]
      add ebp,2
      mov ebp,spcRam
      add ebp,ebx
      jmp spcopcdone
;000E24D1   Op6F
NEWSYM Op6F       ; ret          ret from subroutine   ........
      xor ecx,ecx
      inc byte [spcS]
      mov eax,[spcS]
      mov cl,[spcRam+eax]
      inc byte [spcS]
      mov eax,[spcS]
      mov ch,[spcRam+eax]
      add ecx,spcRam
      mov ebp,ecx
      xor ecx,ecx
      jmp spcopcdone
;000E2504   Op7F
NEWSYM Op7F       ; ret1         return from interrupt   (Restored)
      xor ecx,ecx
      inc byte [spcS]
      mov eax,[spcS]
      mov cl,[spcRam+eax]
      mov [spcP],cl
      test byte [spcP],80h
      jz .NoNeg
      or byte [spcNZ],80h      
.NoNeg
      test byte [spcP],2
      jz .NoZero
      mov byte [spcNZ],0
      jmp .YesZero
.NoZero
      or byte [spcNZ],1
.YesZero
      inc byte [spcS]
      mov eax,[spcS]
      mov cl,[spcRam+eax]
      inc byte [spcS]
      mov eax,[spcS]
      mov ch,[spcRam+eax]
      add ecx,spcRam
      mov ebp,ecx
      ; set direct page
      mov dword [spcRamDP],spcRam
      test byte[spcP],32
      jz .nodp
      add dword [spcRamDP],100h
.nodp
      xor ecx,ecx
      jmp spcopcdone
;000E2597   Op8F
NEWSYM Op8F       ; MOV dp,#inm  (dp) <- inm               ........
      mov bl,[ebp+1]
      mov al,[ebp]
      add ebx,[spcRamDP]
      add ebp,2
      WriteByte
      jmp spcopcdone
;000E25F1   Op9F
NEWSYM Op9F       ; XCN A     A(7-4) <-> A(3-0)     N......Z.
      ror byte [spcA],4
      mov al,byte[spcA]
      mov [spcNZ],al
      jmp spcopcdone
;000E2607   OpAF
NEWSYM OpAF       ; MOV (X)+,A   A -> (X) with auto inc    ........
      mov bl,[spcX]
      add ebx,[spcRamDP]
      mov al, byte [spcA]
      inc byte [spcX]
      WriteByte
      jmp spcopcdone
;000E2666   OpBF
NEWSYM OpBF       ; MOV A,(X)+  A <- (X) with auto inc    N......Z
      mov bl,[spcX]
      add ebx,[spcRamDP]
      ReadByte
      inc byte [spcX]
      mov byte [spcA],al
      mov [spcNZ],al
      jmp spcopcdone
;000E26AB   OpCF
NEWSYM OpCF       ; MUL YA       YA(16 bits) <- Y * A    N......Z.
      mov al,[spcY]
      mov bl,[spcA]
      mul bl
      mov [spcA],al
      mov [spcY],ah
      ; ??? (Is the n flag set on YA or A?)
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      jmp spcopcdone
.YesNeg
      mov byte [spcNZ],80h
      jmp spcopcdone
.YesZero
      mov byte [spcNZ],0
      jmp spcopcdone
;000E26F3   OpDF
NEWSYM OpDF       ; DAA A        decimal adjust for add  N......ZC
      ; copy al flags into AH
      xor ah,ah
      test byte[spcNZ],80h
      jz .noneg
      or ah,10000000b
.noneg
      test byte[spcP],01h
      jz .nocarry
      or ah,00000001b
.nocarry
      test byte[spcNZ],0FFh
      jnz .nozero
      or ah,01000000b
.nozero
      test byte[spcP],08h
      jz .nohcarry
      or ah,00010000b
.nohcarry
      mov al,[spcA]
      sahf
      daa
      mov [spcA],al
      SPCSetFlagnzc
;000E2782   OpEF
NEWSYM OpEF       ; SLEEP        standby SLEEP mode      .........
      dec ebp
      jmp spcopcdone
;000E2788   OpFF
NEWSYM OpFF       ; STOP         standby STOP mode       .........
;      inc dword[spc700read]
      dec ebp
      jmp spcopcdone
;000E278E
NEWSYM Invalidopcode ; Invalid Opcode
      dec ebp
      jmp spcopcdone


;spcNF    db 0     ; The Negative Flag  128 or 127
;spcOF    db 0     ; The Overflow Flag   64 or 191
;spcDPF   db 0     ; Direct Page Flag    32 or 223
;spcUF    db 0     ; The Unused Flag ?   16 or 239
;spcHCF   db 0     ; The Half Carry Flag  8 or 247
;spcIF    db 0     ; The interrupt flag   4 or 251
;spcZF    db 0     ; The Zero Flag        2 or 253
;spcCF    db 0     ; The Carry Flag       1 or 254



