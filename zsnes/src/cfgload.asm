;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM pl1selk,pl1startk,pl1upk,pl2Ak,pl2Bk,pl2Lk,pl2Rk
EXTSYM pl2Xk,pl2Yk,pl2contrl,pl2downk,pl2leftk,pl2rightk
EXTSYM pl2selk,pl2startk,pl2upk
EXTSYM SoundQuality
EXTSYM cvidmode,enterpress,frameskip
EXTSYM per2exec,pl1Ak,pl1Bk,pl1Lk,pl1Rk,pl1Xk,pl1Yk
EXTSYM pl1contrl,pl1downk,pl1leftk,pl1rightk,soundon
EXTSYM spcon






NEWSYM getcfg                                                   ;00000EC2
    mov byte[.forceauto],0
    ; open file
    mov ax,3D00h
    mov edx,.cfgfname
    int 21h
    ; return ax = file handle, carry = error
    jc .done
    mov [.fileloc],ax
    mov byte[.eofile],0
.noteof
    call .readstring
    cmp dword[.stralen],1
    jbe .skipstpr
    call .splitstring
.skipstpr
    cmp byte[.eofile],0
    je .noteof
    
    ; close file
    mov bx,[.fileloc]
    mov ah,3Eh
    int 21h
.done
    ret

.readstring:
    mov dword[.strlen],0
    mov dword[.stralen],0
    mov byte[.ignore],0
.startloop
    ; read file
    mov bx,[.fileloc]
    mov ah,3Fh
    mov ecx,1
    mov edx,.cchar
    int 21h
    cmp eax,0
    je .endoffile
    cmp byte[.cchar],';'
    je .comment
    cmp byte[.cchar],13
    je .eoline
    cmp byte[.cchar],10
    je .startloop
    mov ecx,[.strlen]
    mov al,[.cchar]
    cmp ecx,127
    ja .nocopy
    cmp byte[.ignore],1
    je .nocopy
    mov [.string+ecx],al
    inc dword[.stralen]
.nocopy
    inc dword[.strlen]
    jmp .startloop
    ret
.comment
    inc dword[.strlen]
    mov byte[.ignore],1
    jmp .startloop
    ret
.eoline
    ret
.endoffile
    mov byte[.eofile],1
    ret

.splitstring:
    ; search for ='s
    mov ecx,[.stralen]
    dec ecx
    xor eax,eax
    xor ebx,ebx
.next
    cmp byte[.string+eax],'='
    jne .noeq
    inc ebx
.noeq
    inc eax
    dec ecx
    jnz .next
    cmp ebx,1
    je .onequal
    ret

.onequal
    xor eax,eax
    xor ebx,ebx
    mov ecx,[.stralen]
    mov dword[.strlena],0
    mov dword[.strlenb],0
.loopa
    mov dl,[.string+eax]
    cmp dl,'='
    je .nextb
    cmp dl,' '
    je .skipcopy
    cmp dl,'a'
    jb .nocapneeded
    cmp dl,'z'
    ja .nocapneeded
    sub dl,'a'-'A'
.nocapneeded
    mov [.stringa+ebx],dl
    inc dword[.strlena]
    inc ebx
.skipcopy
    inc eax
    dec ecx
    jnz .loopa
    ret
.nextb
    inc eax
    dec ecx
    jnz .noblank
    ret
.noblank
    xor ebx,ebx
.loopb
    mov dl,[.string+eax]
    cmp dl,' '
    je .skipcopyb
    cmp dl,'a'
    jb .nocapneededb
    cmp dl,'z'
    ja .nocapneededb
    sub dl,'a'-'A'
.nocapneededb
    mov [.stringb+ebx],dl
    inc dword[.strlenb]
    inc ebx
.skipcopyb
    inc eax
    dec ecx
    jnz .loopb
    cmp dword[.strlena],0
    je .nostr
    cmp dword[.strlenb],0
    je .nostr
    jmp .process
.nostr
    ret

.process
    mov ecx,[.strlena]
    cmp ecx,[.stra]
    jne .nostra
    mov edx,.stra+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostra
    call .getframeskip
.nostra
    mov ecx,[.strlena]
    cmp ecx,[.strb]
    jne .nostrb
    mov edx,.strb+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostrb
    call .getautoframeskip
.nostrb
    mov ecx,[.strlena]
    cmp ecx,[.strc]
    jne .nostrc
    mov edx,.strc+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostrc
    call .getplayer1device
.nostrc
    mov ecx,[.strlena]
    cmp ecx,[.strd]
    jne .nostrd
    mov edx,.strd+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostrd
    call .getplayer2device
.nostrd
    mov ecx,[.strlena]
    cmp ecx,[.stre]
    jne .nostre
    mov edx,.stre+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostre
    call .getscankey1
.nostre
    mov ecx,[.strlena]
    cmp ecx,[.strf]
    jne .nostrf
    mov edx,.strf+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostrf
    call .getscankey2
.nostrf
    mov ecx,[.strlena]
    cmp ecx,[.strg]
    jne .nostrg
    mov edx,.strg+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostrg
    call .getsound
.nostrg
    mov ecx,[.strlena]
    cmp ecx,[.strh]
    jne .nostrh
    mov edx,.strh+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostrh
    call .getsoundrate
.nostrh
    mov ecx,[.strlena]
    cmp ecx,[.stri]
    jne .nostri
    mov edx,.stri+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostri
    call .getvideo
.nostri
    mov ecx,[.strlena]
    cmp ecx,[.strj]
    jne .nostrj
    mov edx,.strj+4
    mov eax,.stringa
    call .cmpstr
    cmp bl,1
    je .nostrj
    call .getexecute
.nostrj
    ret

.cmpstr
    xor bl,bl
.loopcmp
    mov bh,[eax]
    cmp [edx],bh
    jne .nocmp
    inc eax
    inc edx
    dec ecx
    jnz .loopcmp
    ret
.nocmp
    mov bl,1
    ret

.getframeskip
    cmp dword[.strlenb],1
    jne .noframeskip
    mov al,[.stringb]
    cmp al,'0'
    jb .noframeskip
    cmp al,'9'
    ja .noframeskip
    cmp byte[.forceauto],1
    je .noframeskip
    sub al,47
    mov [frameskip],al
.noframeskip
    ret

.getautoframeskip
    cmp dword[.strlenb],1
    jne .noautoframeskip
    cmp byte[.stringb],'1'
    jne .noautoframeskip
    mov byte[.forceauto],1
    mov byte[frameskip],0
.noautoframeskip
    ret

.getplayer1device
    cmp dword[.strlenb],1
    jne .noplay1
    mov al,[.stringb]
    cmp al,'0'
    jb .noplay1
    cmp al,'3'
    ja .noplay1
    sub al,48
    mov [pl1contrl],al
.noplay1
    ret

.getplayer2device
    cmp dword[.strlenb],1
    jne .noplay2
    mov al,[.stringb]
    cmp al,'0'
    jb .noplay2
    cmp al,'3'
    ja .noplay2
    sub al,48
    mov [pl2contrl],al
.noplay2
    ret

.getscankey1
    mov ecx,.stringb
    xor ebx,ebx
    xor eax,eax
.checknextscan1
    cmp dword[.strlenb],0
    je near .noscankey1
    mov bl,[ecx]
    inc ecx
    dec dword[.strlenb]
    cmp bl,','
    je near .finstr1
    cmp bl,'0'
    jb near .nextstr1
    cmp bl,'9'
    ja near .nextstr1
    sub bl,48
    mov dl,10
    mul dl
    add al,bl
    cmp dword[.strlenb],0
    je near .finstr1
    jmp .nextstr1
.finstr1
    ; determine which variable according to bh & write
    ; In order of Right, Left, Down, Up, Start, Select, A, B, X, Y, L, R
    cmp bh,0
    jne .nextaa
    mov [pl1rightk],al
.nextaa
    cmp bh,1
    jne .nextba
    mov [pl1leftk],al
.nextba
    cmp bh,2
    jne .nextca
    mov [pl1downk],al
.nextca
    cmp bh,3
    jne .nextda
    mov [pl1upk],al
.nextda
    cmp bh,4
    jne .nextea
    mov [pl1startk],al
.nextea
    cmp bh,5
    jne .nextfa
    mov [pl1selk],al
.nextfa
    cmp bh,6
    jne .nextga
    mov [pl1Bk],al
.nextga
    cmp bh,7
    jne .nextha
    mov [pl1Yk],al
.nextha
    cmp bh,8
    jne .nextia
    mov [pl1Ak],al
.nextia
    cmp bh,9
    jne .nextja
    mov [pl1Xk],al
.nextja
    cmp bh,10
    jne .nextka
    mov [pl1Lk],al
.nextka
    cmp bh,11
    jne .nextla
    mov [pl1Rk],al
.nextla
    xor eax,eax
    inc bh
.nextstr1
    jmp .checknextscan1
.noscankey1
    ret

.getscankey2
    mov ecx,.stringb
    xor ebx,ebx
    xor eax,eax
.checknextscan2
    cmp dword[.strlenb],0
    je near .noscankey2
    mov bl,[ecx]
    inc ecx
    dec dword[.strlenb]
    cmp bl,','
    je near .finstr2
    cmp bl,'0'
    jb near .nextstr2
    cmp bl,'9'
    ja near .nextstr2
    sub bl,48
    mov dl,10
    mul dl
    add al,bl
    cmp dword[.strlenb],0
    je near .finstr2
    jmp .nextstr2
.finstr2
    ; determine which variable according to bh & write
    ; In order of Right, Left, Down, Up, Start, Select, A, B, X, Y, L, R
    cmp bh,0
    jne .nextaa2
    mov [pl2rightk],al
.nextaa2
    cmp bh,1
    jne .nextba2
    mov [pl2leftk],al
.nextba2
    cmp bh,2
    jne .nextca2
    mov [pl2downk],al
.nextca2
    cmp bh,3
    jne .nextda2
    mov [pl2upk],al
.nextda2
    cmp bh,4
    jne .nextea2
    mov [pl2startk],al
.nextea2
    cmp bh,5
    jne .nextfa2
    mov [pl2selk],al
.nextfa2
    cmp bh,6
    jne .nextga2
    mov [pl2Bk],al
.nextga2
    cmp bh,7
    jne .nextha2
    mov [pl2Yk],al
.nextha2
    cmp bh,8
    jne .nextia2
    mov [pl2Ak],al
.nextia2
    cmp bh,9
    jne .nextja2
    mov [pl2Xk],al
.nextja2
    cmp bh,10
    jne .nextka2
    mov [pl2Lk],al
.nextka2
    cmp bh,11
    jne .nextla2
    mov [pl2Rk],al
.nextla2
    xor eax,eax
    inc bh
.nextstr2
    jmp .checknextscan2
.noscankey2
    ret

.getsound
    cmp dword[.strlenb],1
    jne .nosound
    mov al,[.stringb]
    cmp al,'1'
    jne .nosound
    mov byte[spcon],1        ; SPC Enabled
    mov byte[soundon],1
.nosound
    ret

.getsoundrate
    cmp dword[.strlenb],1
    jne .nosoundrate
    mov al,[.stringb]
    cmp al,'0'
    jb .nosoundrate
    cmp al,'3'
    ja .nosoundrate
    sub al,48
    mov [SoundQuality],al
.nosoundrate
    ret

.getvideo
    cmp dword[.strlenb],1
    jne .novideo
    mov al,[.stringb]
    cmp al,'0'
    jb .novideo
    cmp al,'3'
    ja .novideo
    sub al,48
    mov [cvidmode],al
.novideo
    ret

.getexecute
    cmp dword[.strlenb],0
    je .noexecute
    mov cl,[.strlenb]
    mov esi,.stringb
    mov byte[.per2exec],0
.getnextperc
    mov bl,10
    mov al,[.per2exec]
    cmp al,100
    jae .noexecute
    mul bl
    mov [.per2exec],al
    mov al,[esi]
    cmp al,'0'
    jb .noexecute
    cmp al,'9'
    ja .noexecute
    sub al,48
    add [.per2exec],al
    inc esi                     ; next character
    dec cl
    jz .nonextperc
    jmp .getnextperc
.nonextperc
    cmp byte[.per2exec],100
    ja .noexecute
    cmp byte[.per2exec],50
    jb .noexecute
    mov al,[.per2exec]
    mov [per2exec],al
    dec byte[per2exec]
.noexecute
    ret

.per2exec db 0
.fileloc dw 0
.eofile  db 0
.ignore  db 0
.strlen  dd 0
.stralen dd 0    ; actual string length
.strlena dd 0
.strlenb dd 0
.cchar   db 0
.forceauto db 0
.string  times 128 db 0  ; full string
.stringa times 128 db 0
.stringb times 128 db 0
.cfgfname db 'zsnes.cfg',0
.stra dd 9
      db 'FRAMESKIP'
.strb dd 13
      db 'AUTOFRAMESKIP'
.strc dd 13
      db 'PLAYER1DEVICE'
.strd dd 13
      db 'PLAYER2DEVICE'
.stre dd 8
      db 'SCANKEY1'
.strf dd 8
      db 'SCANKEY2'
.strg dd 5
      db 'SOUND'
.strh dd 9
      db 'SOUNDRATE'
.stri dd 9
      db 'VIDEOMODE'
.strj dd 7
      db 'EXECUTE'

