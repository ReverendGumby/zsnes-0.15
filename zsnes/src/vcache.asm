;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM MessageOn,MsgCount,Msgptr,Voice0Disable,Voice0Status,Voice1Disable
EXTSYM Voice1Status,Voice2Disable,Voice2Status,Voice3Disable,Voice3Status
EXTSYM Voice4Disable,Voice4Status,Voice5Disable,Voice5Status,Voice6Disable
EXTSYM Voice6Status,Voice7Disable,Voice7Status,bgcmsung,bgmode,cbackofsaddr
EXTSYM cbitmode,cgmod,debuggeron,disableeffects,frameskip,frskipper,newgfxerror2
EXTSYM maxbr,modeused,mousexloc,mouseyloc,newengen,newgfx16b,newgfxerror
EXTSYM nextdrawallng,oamaddr,pal16b,pal16bxcl,pressed,prevbright,prevpal
EXTSYM scaddsngb,scaddtngb,scaddtngbx,scfbl,scrndis,snesmouse,sprprdrn
EXTSYM t1cc,vidbright,vidbuffer,vidbufferm,vidbufferofsa,vidbufferofsb
EXTSYM vidmemch2,statefileloc,fnamest,GUIClick,MousePRClick,ngmsdraw,cvidmode
EXTSYM KeyDisableSC0,KeyDisableSC1,KeyDisableSC2,KeyDisableSC3,KeyDisableSC4
EXTSYM KeyDisableSC5,KeyDisableSC6,KeyDisableSC7,KeyFastFrwrd,SRAMSave5Sec
EXTSYM KeyBGDisble0,KeyBGDisble1,KeyBGDisble2,KeyBGDisble3,KeySprDisble
EXTSYM KeyResetAll,KeyExtraEnab,KeyWinDisble,KeyNewGfxSwt,KeyOffsetMSw
EXTSYM KeyStateSlc0,KeyStateSlc1,KeyStateSlc2,KeyStateSlc3,KeyStateSlc4
EXTSYM KeyStateSlc5,KeyStateSlc6,KeyStateSlc7,KeyStateSlc8,KeyStateSlc9
EXTSYM DSPMem,SprValAdd,dsp1ptr,dsp1array,FastFwdToggle,SaveSramData
EXTSYM ngextbg,Mode7HiRes,Check60hz,Get_MouseData,Get_MousePositionDisplacement
EXTSYM WindowDisables,scanlines,romispal
EXTSYM MusicRelVol,MusicVol,WDSPReg0C,WDSPReg1C
EXTSYM DSPOp02,Op02AAS,Op02AZS,Op02CX,Op02CY,Op02FX,Op02FY
EXTSYM Op02FZ,Op02LES,Op02LFE,Op02VOF,Op02VVA
EXTSYM CurRecv
EXTSYM CNetType
EXTSYM KeySlowDown
EXTSYM chaton
EXTSYM KeyFRateDown,KeyFRateUp,KeyVolUp,KeyVolDown,FPSOn
EXTSYM bg1ptr,bg2ptr,bg3ptr,bg4ptr,bg3scrolx,bg1sx,cachebg1,resolutn
EXTSYM curypos,oamram,objhipr,objptr,objptrn,objsize1,objsize2
EXTSYM sprleftpr,sprlefttot,vcache4b
EXTSYM objadds1,objadds2,objmovs1,objmovs2,tltype4b,vidmemch4,vram
EXTSYM bgptr,bgptrc,bgptrd,curtileptr,vcache2b
EXTSYM vcache8b,vidmemch8
EXTSYM offsetmshl
EXTSYM tltype2b
EXTSYM tltype8b


; Process stuff & Cache sprites

NEWSYM fskipped,     db 0                                               ;0002D886

NEWSYM cachevideo                                                       ;0002D887
    mov al,[vidbright]
    mov [maxbr],al
    mov byte[cgmod],1
    xor al,al
    mov [curblank],al
    cmp byte[debuggeron],0
    je .nodebugger
    mov byte[curblank],40h
    mov al,40h
    jmp .nofrskip
.nodebugger

    cmp byte[frameskip],0
    jne .frameskip
.t1ccwait
    cmp word[t1cc],0
    jz .t1ccwait
    dec word[t1cc]
.t1ccwait2
    cmp word[t1cc],0
    jz .noskip2
    mov byte[curblank],40h
    inc byte[fskipped]
    mov al,40h
    cmp byte[fskipped],12
    jb near .nofrskip
    mov word[t1cc],0
    mov byte[curblank],0
    mov byte[fskipped],0
    jmp .nofrskip
.noskip2
    mov byte[fskipped],0
    jmp .nofrskip
.frameskip
    inc byte[frskipper]
    push ebx
    mov bl,byte[frameskip]
.fastforb
    cmp byte[frskipper],bl
    pop ebx
    jae .nofrskip
    mov byte[curblank],40h
    mov al,40h
    jmp .frskip
.nofrskip
    mov byte[frskipper],0
.frskip
    push ebx
    push esi
    push edi
    push edx
    mov cl,[bgmode]
    mov al,01h
    shl al,cl
    mov [cachedmode],al
    test byte[pressed+2],1
    je .nodis1
    xor byte[scrndis],01h
    mov byte[pressed+2],2
.nodis1
    test byte[pressed+3],1
    je .nodis2
    xor byte[scrndis],02h
    mov byte[pressed+3],2
.nodis2
    test byte[pressed+4],1
    je .nodis3
    xor byte[scrndis],04h
    mov byte[pressed+4],2
.nodis3
    test byte[pressed+5],1
    je .nodis4
    xor byte[scrndis],08h
    mov byte[pressed+5],2
.nodis4
    test byte[pressed+6],1
    je .nodis5
    xor byte[scrndis],10h
    mov byte[pressed+6],2
.nodis5
    test byte[pressed+7],1
    je .nodis6
    xor byte[scrndis],20h
    add byte[bg1sx],1
    mov byte[pressed+7],2
.nodis6
    test byte[pressed+63],1
    je .nodis7
    xor byte[Voice0Disable],01h
    mov byte[Voice0Status],0
    mov byte[pressed+63],2
.nodis7
    test byte[pressed+64],1
    je .nodis8
    xor byte[Voice1Disable],01h
    mov byte[Voice1Status],0
    mov byte[pressed+64],2
.nodis8
    test byte[pressed+65],1
    je .nodis9
    xor byte[Voice2Disable],01h
    mov byte[Voice2Status],0
    mov byte[pressed+65],2
.nodis9
    test byte[pressed+66],1
    je .nodis10
    xor byte[Voice3Disable],01h
    mov byte[Voice3Status],0
    mov byte[pressed+66],2
.nodis10
    test byte[pressed+67],1
    je .nodis11
    xor byte[Voice4Disable],01h
    mov byte[Voice4Status],0
    mov byte[pressed+67],2
.nodis11
    test byte[pressed+68],1
    je .nodis12
    xor byte[Voice5Disable],01h
    mov byte[Voice5Status],0
    mov byte[pressed+68],2
.nodis12
    test byte[pressed+87],1
    je .nodis13
    xor byte[Voice6Disable],01h
    mov byte[Voice6Status],0
    mov byte[pressed+87],2
.nodis13
    test byte[pressed+88],1
    je .nodis14
    xor byte[Voice7Disable],01h
    mov byte[Voice7Status],0
    mov byte[pressed+88],2
.nodis14
    cmp byte[curblank],0h
    jne near yesblank

NEWSYM docache
    xor ebx,ebx
    mov bl,[bgmode]
    shl bl,2
    add ebx,colormodedef
    mov [colormodeofs],ebx
    xor ebx,ebx
    mov bl,[bgmode]
    mov al,[colormodedef+ebx*4]
    mov [curcolbg1],al
    mov ah,[colormodedef+ebx*4+1]
    mov [curcolbg2],ah
    mov al,[colormodedef+ebx*4]
    mov [curcolbg3],al
    mov ah,[colormodedef+ebx*4+1]
    mov [curcolbg4],ah
    mov ax,[bg1ptr]
    mov [curbgofs1],ax
    mov ax,[bg2ptr]
    mov [curbgofs2],ax
    mov ax,[bg3ptr]
    mov [curbgofs3],ax
    mov ax,[bg4ptr]
    mov [curbgofs4],ax
    push es
    mov eax,ds
    mov es,eax
    ; clear # of sprites & bg cache
    mov edi,cachebg1
    mov ecx,64*5+16*4
    xor eax,eax
    rep stosd
    test byte[scrndis],10h
    jnz .nosprites
    call cachesprites
    call processsprites

.nosprites
    ; fill background with 0's unless 16-bit/new graphics engine mode is on
    cmp byte[cbitmode],1
    je .skipbgclear
    mov edi,[vidbuffer]
    xor eax,eax
    add edi,16
    mov dl,240
.loopa
    mov ecx,64
    rep stosd
    add edi,32
    dec dl
    jnz .loopa
.skipbgclear
    xor ecx,ecx
    pop es
NEWSYM yesblank
    pop edx
    pop edi
    pop esi
    pop ebx
    ret

NEWSYM cachedmode,   db 0                                               ;0002DBC5
NEWSYM tempfname,    db 'vram.bin',0
NEWSYM scrnsizebyte, dw 1024,2048,2048,4096
NEWSYM colormodedef, db 1,1,1,1, 2,2,1,0, 2,2,0,0, 3,2,0,0,             ;0002DBD7
               db 3,1,0,0, 2,1,0,0, 2,0,0,0, 0,0,0,0
NEWSYM colormodeofs, dd 0                                               ;0002DBF7
NEWSYM curblank,     db 80h     ; current blank state (40h = skip fill) ;0002DBFB
NEWSYM addr2add,     dd 0                                               ;0002DBFC
NEWSYM cachebg1,    times 64 db 0                                       ;0002DC00
NEWSYM cachebg2,    times 64 db 0
NEWSYM cachebg3,    times 64 db 0
NEWSYM cachebg4,    times 64 db 0
NEWSYM sprlefttot,  times 256 db 0     ; total sprites left             ;0002DD00
NEWSYM sprleftpr,   times 256 db 0     ; sprites left for priority 0    ;0002DE00
NEWSYM sprleftpr1,  times 256 db 0     ; sprites left for priority 1
NEWSYM sprleftpr2,  times 256 db 0     ; sprites left for priority 2
NEWSYM sprleftpr3,  times 256 db 0     ; sprites left for priority 3
NEWSYM spritetable, times 256*512 db 0  ; sprite table (flip/pal/xloc/vbufptr)38*7
NEWSYM curbgofs1,   dw 0                                                ;0004E200
NEWSYM curbgofs2,   dw 0
NEWSYM curbgofs3,   dw 0
NEWSYM curbgofs4,   dw 0
NEWSYM curcolbg1,   db 0                                                ;0004E208
NEWSYM curcolbg2,   db 0
NEWSYM curcolbg3,   db 0
NEWSYM curcolbg4,   db 0

;*******************************************************
; Process Sprites
;*******************************************************
; Use oamram for object table
NEWSYM processsprites                                                   ;0004E20C
    ; set obj pointers
    cmp byte[objsize1],1
    jne .16dot1
    mov ebx,.process8x8sprite
    mov [.size1ptr],ebx
    jmp .fin1
.16dot1
    cmp byte[objsize1],4
    jne .32dot1
    mov ebx,.process16x16sprite
    mov [.size1ptr],ebx
    jmp .fin1
.32dot1
    cmp byte[objsize1],16
    jne .64dot1
    mov ebx,.process32x32sprite
    mov [.size1ptr],ebx
    jmp .fin1
.64dot1
    mov ebx,.process64x64sprite
    mov [.size1ptr],ebx
.fin1
    cmp byte[objsize2],1
    jne .16dot2
    mov ebx,.process8x8sprite
    mov [.size2ptr],ebx
    jmp .fin2
.16dot2
    cmp byte[objsize2],4
    jne .32dot2
    mov ebx,.process16x16sprite
    mov [.size2ptr],ebx
    jmp .fin2
.32dot2
    cmp byte[objsize2],16
    jne .64dot2
    mov ebx,.process32x32sprite
    mov [.size2ptr],ebx
    jmp .fin2 
.64dot2 
    mov ebx,.process64x64sprite
    mov [.size2ptr],ebx
.fin2
    ; set pointer adder
    xor eax,eax
    xor ebx,ebx
    mov al,[objhipr]
    shl ax,2
    mov ebx,eax
    sub bx,4
    and bx,01FCh
    mov dword[addr2add],0
    mov byte[.prileft],4
    mov byte[.curpri],0
    mov [.objvramloc],ecx
.startobject
    ; do 1st priority
    mov byte[.objleft],128
.objloop
    mov ecx,[objptr]
    cmp byte[.curpri],0
    jz .notpri
    cmp byte[.curpri],1
    jz .notpri
    mov ecx,[objptrn]
.notpri
    shl ecx,1
    mov [.objvramloc],ecx
    shl ecx,1
    ; get object information
.objinfo
    xor ecx,ecx
    mov cx,[oamram+ebx+2]
    mov dl,ch
    shr dl,4
    and dl,03h
    cmp dl,[.curpri]
    jnz .nextobj
    push ebx
    mov dl,[oamram+ebx+1]       ; y
    inc dl
    ; set up pointer to esi
    mov dh,ch
    and ch,01h
    shr dh,1
    shl ecx,6
    add ecx,[.objvramloc]
    and ecx,01FFFFh
    add ecx,[vcache4b]
    mov esi,ecx
    ; get x
    mov al,[oamram+ebx]         ; x
    ; get double bits
    mov cl,bl
    shr ebx,4           ; /16
    shr cl,1
    and cl,06h
    mov ah,[oamram+ebx+512]
    shr ah,cl
    and ah,03h
    mov ch,ah
    and ch,01h
    mov cl,al
    ; process object
    ; esi = pointer to 8-bit object, dh = stats (1 shifted to right)
    ; cx = x position, dl = y position
    cmp cx,384
    jb .noadder
    add cx,65535-511
.noadder
    cmp cx,256
    jge .returnfromptr
    cmp cx,-64
    jle .returnfromptr
    test ah,02h
    jz .size1
    jmp dword near [.size2ptr]
.size1
    jmp dword near [.size1ptr]
.returnfromptr
    pop ebx
    ; next object
.nextobj
    sub bx,4
    and bx,01FCh
    dec byte[.objleft]
    jnz near .objinfo
    add dword[addr2add],256
    inc byte[.curpri]
    dec byte[.prileft]
    jnz near .startobject
    ret

.objvramloc dd 0                                                        ;0004E3DF
.curpri  db 0                                                           ;0004E3E3
.trypri  db 0                                                           ;0004E3E4
.objleft db 0                                                           ;0004E3E5
.prileft db 0                                                           ;0004E3E6
.size1ptr dd 0                                                          ;0004E3E7
.size2ptr dd 0
          dd 0

.reprocesssprite                                                        ;0004E3F3
    cmp cx,-8
    jle .next
    cmp cx,256
    jge .next
    add cx,8
.reprocessspriteb
    cmp dl,[resolutn]
    jae .overflow
    xor ebx,ebx
    xor eax,eax
    mov bl,dl
    mov al,byte[sprlefttot+ebx]
    cmp al,37
    ja near .overflow
    inc byte[sprlefttot+ebx]
    add ebx,[addr2add]
    inc byte[sprleftpr+ebx]
    sub ebx,[addr2add]
    shl ebx,9
    shl eax,3
    add ebx,eax
    mov [spritetable+ebx],cx
    mov [spritetable+ebx+2],esi
    mov al,[.statusbit]
    mov byte[spritetable+ebx+6],dh
    mov byte[spritetable+ebx+7],al
.overflow
    inc dl
    add esi,8
    dec byte[.numleft2do]
    jnz .reprocessspriteb
    sub cx,8
    ret
.next
    add dl,8
    add esi,64
    ret

.reprocessspriteflipy
    cmp cx,-8
    jle .nextb
    cmp cx,256
    jge .nextb
    add cx,8
.reprocessspriteflipyb
    cmp dl,[resolutn]
    jae .overflow2
    xor ebx,ebx
    xor eax,eax
    mov bl,dl
    mov al,byte[sprlefttot+ebx]
    cmp al,37
    ja near .overflow2
    inc byte[sprlefttot+ebx]
    add ebx,[addr2add]
    inc byte[sprleftpr+ebx]
    sub ebx,[addr2add]
    shl ebx,9
    shl eax,3
    add ebx,eax
    mov [spritetable+ebx],cx
    mov [spritetable+ebx+2],esi
    mov al,[.statusbit]
    mov byte[spritetable+ebx+6],dh
    mov byte[spritetable+ebx+7],al
.overflow2
    inc dl
    sub esi,8
    dec byte[.numleft2do]
    jnz .reprocessspriteflipyb
    sub cx,8
    ret
.nextb
    add dl,8
    sub esi,64
    ret

.statusbit db 0

.process8x8sprite:                                                      ;0004E50E
    test dh,40h
    jnz .8x8flipy
    mov [.statusbit],dh
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    call .reprocesssprite
    jmp .returnfromptr
.8x8flipy
    mov [.statusbit],dh
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add esi,56
    call .reprocessspriteflipy
    jmp .returnfromptr
.numleft2do db 0

.process16x16sprite:
    mov [.statusbit],dh
    test dh,20h
    jnz near .16x16flipx
    test dh,40h
    jnz .16x16flipy
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    call .reprocesssprite
    sub dl,8
    add cx,8
    mov byte[.numleft2do],8
    call .reprocesssprite
    sub cx,8
    add esi,64*14
    mov byte[.numleft2do],8
    call .reprocesssprite
    sub dl,8
    add cx,8
    mov byte[.numleft2do],8
    call .reprocesssprite
    jmp .returnfromptr
.16x16flipy
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add dl,8
    add esi,56
    call .reprocessspriteflipy
    add esi,128
    sub dl,8
    add cx,8
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    add esi,128
    sub dl,16
    sub cx,8
    add esi,64*14
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    add esi,128
    sub dl,8
    add cx,8
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    jmp .returnfromptr
.16x16flipx
    test dh,40h
    jnz .16x16flipyx
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add cx,8
    call .reprocesssprite
    sub dl,8
    sub cx,8
    mov byte[.numleft2do],8
    call .reprocesssprite
    add esi,64*14
    add cx,8
    mov byte[.numleft2do],8
    call .reprocesssprite
    sub dl,8
    sub cx,8
    mov byte[.numleft2do],8
    call .reprocesssprite
    jmp .returnfromptr
.16x16flipyx
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add cx,8
    add dl,8
    add esi,56
    call .reprocessspriteflipy
    add esi,128
    sub dl,8
    sub cx,8
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    add esi,128
    add esi,64*14
    sub dl,16
    add cx,8
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    add esi,128
    sub dl,8
    sub cx,8
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    jmp .returnfromptr

;*******************************************************
; Sprite increment/draw macros
;*******************************************************

%macro nextsprite2right 0
    sub dl,8
    add cx,8
    mov byte[.numleft2do],8
    call .reprocesssprite
%endmacro

%macro nextsprite2rightflipy 0
    add esi,128
    sub dl,8
    add cx,8
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
%endmacro

%macro nextsprite2rightflipx 0
    sub dl,8
    sub cx,8
    mov byte[.numleft2do],8
    call .reprocesssprite
%endmacro

%macro nextsprite2rightflipyx 0
    add esi,128
    sub dl,8
    sub cx,8
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
%endmacro

;*******************************************************
; 32x32 sprites routines
;*******************************************************

%macro nextline32x32 0
    sub cx,24
    add esi,64*12
    mov byte[.numleft2do],8
    call .reprocesssprite
    nextsprite2right
    nextsprite2right
    nextsprite2right
%endmacro

.process32x32sprite:                                                    ;0004E71D
    mov [.statusbit],dh
    test dh,20h
    jnz near .32x32flipx
    test dh,40h
    jnz near .32x32flipy
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    call .reprocesssprite
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextline32x32
    nextline32x32
    nextline32x32
    jmp .returnfromptr

%macro nextline32x32flipy 0
    sub cx,24
    add esi,64*12+128
    sub dl,16
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
%endmacro

.32x32flipy
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add dl,24
    add esi,56
    call .reprocessspriteflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextline32x32flipy
    nextline32x32flipy
    nextline32x32flipy
    jmp .returnfromptr

%macro nextline32x32flipx 0
    add cx,24
    add esi,64*12
    mov byte[.numleft2do],8
    call .reprocesssprite
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
%endmacro

.32x32flipx
    test dh,40h
    jnz near .32x32flipyx
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add cx,24
    call .reprocesssprite
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextline32x32flipx
    nextline32x32flipx
    nextline32x32flipx
    jmp .returnfromptr

%macro nextline32x32flipyx 0
    add cx,24
    add esi,64*12+128
    sub dl,16
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
%endmacro

.32x32flipyx
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add cx,24
    add dl,24
    add esi,56
    call .reprocessspriteflipy
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextline32x32flipyx
    nextline32x32flipyx
    nextline32x32flipyx
    jmp .returnfromptr

;*******************************************************
; 64x64 sprites routines
;*******************************************************

%macro nextline64x64 0
    sub cx,56
    add esi,64*8
    mov byte[.numleft2do],8
    call .reprocesssprite
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
%endmacro

.process64x64sprite:                                                    ;0004ED38
    mov [.statusbit],dh
    test dh,20h
    jnz near .64x64flipx
    test dh,40h
    jnz near .64x64flipy
    mov [.statusbit],dh
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    call .reprocesssprite
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextsprite2right
    nextline64x64
    nextline64x64
    nextline64x64
    nextline64x64
    nextline64x64
    nextline64x64
    nextline64x64
    jmp .returnfromptr

%macro nextline64x64flipy 0
    sub cx,56
    add esi,64*8+128
    sub dl,16
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
%endmacro

.64x64flipy
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add dl,56
    add esi,56
    call .reprocessspriteflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextsprite2rightflipy
    nextline64x64flipy
    nextline64x64flipy
    nextline64x64flipy
    nextline64x64flipy
    nextline64x64flipy
    nextline64x64flipy
    nextline64x64flipy
    jmp .returnfromptr

%macro nextline64x64flipx 0
    add cx,56
    add esi,64*8
    mov byte[.numleft2do],8
    call .reprocesssprite
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
%endmacro

.64x64flipx
    test dh,40h
    jnz near .64x64flipyx
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add cx,56
    call .reprocesssprite
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextsprite2rightflipx
    nextline64x64flipx
    nextline64x64flipx
    nextline64x64flipx
    nextline64x64flipx
    nextline64x64flipx
    nextline64x64flipx
    nextline64x64flipx
    jmp .returnfromptr

%macro nextline64x64flipyx 0
    add cx,56
    add esi,64*8+128
    sub dl,16
    mov byte[.numleft2do],8
    call .reprocessspriteflipy
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
%endmacro

.64x64flipyx
    and dh,07h
    mov byte[.numleft2do],8
    shl dh,4
    add dh,128
    add cx,56
    add dl,56
    add esi,56
    call .reprocessspriteflipy
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextsprite2rightflipyx
    nextline64x64flipyx
    nextline64x64flipyx
    nextline64x64flipyx
    nextline64x64flipyx
    nextline64x64flipyx
    nextline64x64flipyx
    nextline64x64flipyx
    jmp .returnfromptr

;*******************************************************
; Cache Sprites
;*******************************************************
; Use oamram for object table, copy from vram -> vcache4b
; 16x16 sprite, to move = 2, to add = 14, 32x32 = 4,12, 64x64 = 8,8

NEWSYM cachesprites                                                     ;000504B1
    ; initialize obj size cache
    mov dword[.objptr],oamram
    add dword[.objptr],512
    mov esi,dword[.objptr]
    mov al,[esi]
    mov [.curobjtype],al
    mov byte[.objleftinbyte],4
    ; Initialize oamram pointer
    mov esi,oamram
    add esi,2

    ; process pointers (.objptra = source, .objptrb = dest)
.trynextgroup
    xor ebx,ebx
    mov bx,[objptr]
    mov ecx,ebx
    shr ecx,4
    mov [.nbg],cx
    mov edi,[vram]
    add edi,ebx
    mov [.objptra],edi
    shl ebx,1
    add ebx,[vcache4b]
    mov [.objptrb],ebx

;    xor ebx,ebx
;    mov bx,[objptrn]
;    mov ecx,ebx
;    shr ecx,4
;    mov [.nbg2],cx
;    mov edi,[vram]
;    add edi,ebx
;    mov [.objptra2],edi
;    shl ebx,1
;    add ebx,[vcache4b]
;    mov [.objptrb2],ebx
;
    xor ebx,ebx

    ; process objects
    mov byte[.objleft],128
;    mov dword[.sprnum],3
.nextobj
    ; process sprite sizes
    test byte[.curobjtype],02h
    jz .dosprsize1
    mov al,[objsize2]
    mov [.num2do],al
    mov ax,[objadds2]
    mov [.byte2add],ax
    mov al,[objmovs2]
    mov [.byte2move],al
    mov [.byteb4add],al
    jmp .exitsprsize
.dosprsize1
    mov al,[objsize1]
    mov [.num2do],al
    mov ax,[objadds1]
    mov [.byte2add],ax
    mov al,[objmovs1]
    mov [.byte2move],al
    mov [.byteb4add],al
.exitsprsize
    shr byte[.curobjtype],2
    dec byte[.objleftinbyte]
    jnz .skipobjproc
    mov byte[.objleftinbyte],4
    inc dword[.objptr]
    mov ebx,[.objptr]
    mov al,[ebx]
    mov [.curobjtype],al
.skipobjproc
    mov bx,[esi]
    and bh,1h
    mov [.curobj],bx
.nextobject
    xor ebx,ebx
    mov bx,[.curobj]
    mov cx,bx
    shl bx,1
    add bx,[.nbg]
    and bx,4095
    test word[vidmemch4+ebx],0101h
    jz near .nocache
    mov word[vidmemch4+ebx],0000h
    push esi
    mov bx,cx
    shl bx,5
    mov esi,[.objptra]
    add esi,ebx
    shl bx,1
    mov edi,[.objptrb]
    add edi,ebx
    ; convert from [esi] to [edi]
    mov byte[.rowleft],8
.donext

    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0

    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    mov al,[esi+16]                ; bitplane 2
    cmp al,0
    je .skipconvc
    test al,01h
    jz .skipc0
    or ah,04h
.skipc0
    test al,02h
    jz .skipc1
    or bl,04h
.skipc1
    test al,04h
    jz .skipc2
    or bh,04h
.skipc2
    test al,08h
    jz .skipc3
    or cl,04h
.skipc3
    test al,10h
    jz .skipc4
    or ch,04h
.skipc4
    test al,20h
    jz .skipc5
    or dl,04h
.skipc5
    test al,40h
    jz .skipc6
    or dh,04h
.skipc6
    test al,80h
    jz .skipc7
    or byte[.a],04h
.skipc7
.skipconvc

    mov al,[esi+17]                ; bitplane 3
    cmp al,0
    je .skipconvd
    test al,01h
    jz .skipd0
    or ah,08h
.skipd0
    test al,02h
    jz .skipd1
    or bl,08h
.skipd1
    test al,04h
    jz .skipd2
    or bh,08h
.skipd2
    test al,08h
    jz .skipd3
    or cl,08h
.skipd3
    test al,10h
    jz .skipd4
    or ch,08h
.skipd4
    test al,20h
    jz .skipd5
    or dl,08h
.skipd5
    test al,40h
    jz .skipd6
    or dh,08h
.skipd6
    test al,80h
    jz .skipd7
    or byte[.a],08h
.skipd7
.skipconvd

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov [edi+1],dh
    mov al,[.a]
    mov [edi],al

    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext
    pop esi
.nocache
    inc word[.curobj]
    dec byte[.byteb4add]
    jnz .skipbyteadd
    mov ax,[.byte2add]
    add word[.curobj],ax
    mov al,[.byte2move]
    mov byte[.byteb4add],al
.skipbyteadd
    dec byte[.num2do]
    jnz near .nextobject
    add esi,4
    dec byte[.objleft]
    jnz near .nextobj
    ret

.objptra dd 0                                                           ;00050795
.objptrb dd 0                                                           ;00050799
.nbg     dw 0                                                           ;0005079D
.objleft db 0                                                           ;0005079F
.rowleft db 0
.a       db 0
.objptr dd 0                                                            ;000507A2
.objleftinbyte db 0
.curobjtype db 0                                                        ;000507A7
.num2do db 1                                                            ;000507A8
.curobj dw 0                                                            ;000507A9
.byteb4add db 2
.byte2move db 0
.byte2add  dw 0

;*******************************************************
; Cache 2-Bit
;*******************************************************
NEWSYM cachetile2b                                                      ;000507AF
    ; Keep high word ecx 0
    push eax
    xor ecx,ecx
    push edx
    mov byte[.nextar],1
    push ebx
    ; get tile info location
    test al,20h
    jnz .highptr
    shl eax,6   ; x 64 for each line
    add ax,[bgptr]
    jmp .loptr
.highptr
    and al,1Fh
    shl eax,6   ; x 64 for each line
    add ax,[bgptrc]
.loptr
    add eax,[vram]
    mov bx,[curtileptr]
    shr bx,4
    mov byte[.count],32
    mov [.nbg],bx
    ; do loop
.cacheloop
    mov si,[eax]
    and esi,03FFh
    add si,[.nbg]
    and esi,4095
    test byte[vidmemch2+esi],01h
    jz near .nocache
    mov byte[vidmemch2+esi],00h
    mov edi,esi
    shl esi,4
    shl edi,6
    add esi,[vram]
    add edi,[vcache2b]
    push eax
    mov byte[.rowleft],8
.donext

    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0

    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov al,[.a]
    mov [edi+1],dh
    mov [edi],al

    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext
    pop eax
.nocache
    add eax,2
    dec byte[.count]
    jnz near .cacheloop

    cmp byte[.nextar],0
    je .skipall
    mov bx,[bgptrc]
    cmp [bgptrd],bx
    je .skipall
    add eax,2048-64
    mov byte[.count],32
    mov byte[.nextar],0
    jmp .cacheloop
.skipall
    pop ebx
    pop edx
    pop eax
    ret

.nbg     dw 0
.count   db 0
.a       db 0
.rowleft db 0
.nextar  db 0

NEWSYM cache2bit
    ret

;*******************************************************
; Cache 4-Bit
;*******************************************************

; esi = pointer to tile location vram
; edi = pointer to graphics data (cache & non-cache)
; ebx = external pointer
; tile value : bit 15 = flipy, bit 14 = flipx, bit 10-12 = palette, 0-9=tile#

NEWSYM cachetile4b                                                      ;00050955
    ; Keep high word ecx 0
    push eax
    xor ecx,ecx
    push edx
    mov byte[.nextar],1
    push ebx
    ; get tile info location
    test al,20h
    jnz .highptr
    shl eax,6   ; x 64 for each line
    add ax,[bgptr]
    jmp .loptr
.highptr
    and al,1Fh
    shl eax,6   ; x 64 for each line
    add ax,[bgptrc]
.loptr
    add eax,[vram]
    mov bx,[curtileptr]
    shr bx,5
    mov byte[.count],32
    mov [.nbg],bx

    ; do loop
.cacheloop
    mov si,[eax]
    and esi,03FFh
    add si,[.nbg]
    shl esi,1
    and esi,4095
    test word[vidmemch4+esi],0101h
    jz near .nocache
    mov word[vidmemch4+esi],0000h
    mov edi,esi
    shl esi,4
    shl edi,5
    add esi,[vram]
    add edi,[vcache4b]
    push eax
    mov byte[.rowleft],8
.donext

    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0

    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    mov al,[esi+16]                ; bitplane 2
    cmp al,0
    je .skipconvc
    test al,01h
    jz .skipc0
    or ah,04h
.skipc0
    test al,02h
    jz .skipc1
    or bl,04h
.skipc1
    test al,04h
    jz .skipc2
    or bh,04h
.skipc2
    test al,08h
    jz .skipc3
    or cl,04h
.skipc3
    test al,10h
    jz .skipc4
    or ch,04h
.skipc4
    test al,20h
    jz .skipc5
    or dl,04h
.skipc5
    test al,40h
    jz .skipc6
    or dh,04h
.skipc6
    test al,80h
    jz .skipc7
    or byte[.a],04h
.skipc7
.skipconvc

    mov al,[esi+17]                ; bitplane 3
    cmp al,0
    je .skipconvd
    test al,01h
    jz .skipd0
    or ah,08h
.skipd0
    test al,02h
    jz .skipd1
    or bl,08h
.skipd1
    test al,04h
    jz .skipd2
    or bh,08h
.skipd2
    test al,08h
    jz .skipd3
    or cl,08h
.skipd3
    test al,10h
    jz .skipd4
    or ch,08h
.skipd4
    test al,20h
    jz .skipd5
    or dl,08h
.skipd5
    test al,40h
    jz .skipd6
    or dh,08h
.skipd6
    test al,80h
    jz .skipd7
    or byte[.a],08h
.skipd7
.skipconvd

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov al,[.a]
    mov [edi+1],dh
    mov [edi],al

    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext
    pop eax
.nocache
    add eax,2
    dec byte[.count]
    jnz near .cacheloop

    cmp byte[.nextar],0
    je .skipall
    mov bx,[bgptrc]
    cmp [bgptrd],bx
    je .skipall
    add eax,2048-64
    mov byte[.count],32
    mov byte[.nextar],0
    jmp .cacheloop
.skipall
    pop ebx
    pop edx
    pop eax
    ret

.nbg     dw 0
.count   db 0
.a       db 0
.rowleft db 0
.nextar  db 0

NEWSYM cache4bit                                                        ;00050B86
    ret
;*******************************************************
; Cache 8-Bit
;*******************************************************
; tile value : bit 15 = flipy, bit 14 = flipx, bit 10-12 = palette, 0-9=tile#
NEWSYM cachetile8b                                                      ;00050B87
    ; Keep high word ecx 0
    push eax
    xor ecx,ecx
    push edx
    mov byte[.nextar],1
    push ebx
    ; get tile info location
    test al,20h
    jnz .highptr
    shl eax,6   ; x 64 for each line
    add ax,[bgptr]
    jmp .loptr
.highptr
    and al,1Fh
    shl eax,6   ; x 64 for each line
    add ax,[bgptrc]
.loptr
    add eax,[vram]
    mov bx,[curtileptr]
    shr bx,6
    mov byte[.count],32
    mov [.nbg],bx

    ; do loop
.cacheloop
    mov si,[eax]
    and esi,03FFh
    add si,[.nbg]
    shl esi,2
    and esi,4095
    test dword[vidmemch8+esi],01010101h
    jz near .nocache
    mov dword[vidmemch8+esi],00000000h
    mov edi,esi
    shl esi,4
    shl edi,4
    add esi,[vram]
    add edi,[vcache8b]
    push eax
    mov byte[.rowleft],8
.donext
    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0

    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    mov al,[esi+16]                ; bitplane 2
    cmp al,0
    je .skipconvc
    test al,01h
    jz .skipc0
    or ah,04h
.skipc0
    test al,02h
    jz .skipc1
    or bl,04h
.skipc1
    test al,04h
    jz .skipc2
    or bh,04h
.skipc2
    test al,08h
    jz .skipc3
    or cl,04h
.skipc3
    test al,10h
    jz .skipc4
    or ch,04h
.skipc4
    test al,20h
    jz .skipc5
    or dl,04h
.skipc5
    test al,40h
    jz .skipc6
    or dh,04h
.skipc6
    test al,80h
    jz .skipc7
    or byte[.a],04h
.skipc7
.skipconvc

    mov al,[esi+17]                ; bitplane 3
    cmp al,0
    je .skipconvd
    test al,01h
    jz .skipd0
    or ah,08h
.skipd0
    test al,02h
    jz .skipd1
    or bl,08h
.skipd1
    test al,04h
    jz .skipd2
    or bh,08h
.skipd2
    test al,08h
    jz .skipd3
    or cl,08h
.skipd3
    test al,10h
    jz .skipd4
    or ch,08h
.skipd4
    test al,20h
    jz .skipd5
    or dl,08h
.skipd5
    test al,40h
    jz .skipd6
    or dh,08h
.skipd6
    test al,80h
    jz .skipd7
    or byte[.a],08h
.skipd7
.skipconvd

    mov al,[esi+32]                ; bitplane 4
    cmp al,0
    je .skipconve
    test al,01h
    jz .skipe0
    or ah,10h
.skipe0
    test al,02h
    jz .skipe1
    or bl,10h
.skipe1
    test al,04h
    jz .skipe2
    or bh,10h
.skipe2
    test al,08h
    jz .skipe3
    or cl,10h
.skipe3
    test al,10h
    jz .skipe4
    or ch,10h
.skipe4
    test al,20h
    jz .skipe5
    or dl,10h
.skipe5
    test al,40h
    jz .skipe6
    or dh,10h
.skipe6
    test al,80h
    jz .skipe7
    or byte[.a],10h
.skipe7
.skipconve

    mov al,[esi+33]                ; bitplane 5
    cmp al,0
    je .skipconvf
    test al,01h
    jz .skipf0
    or ah,20h
.skipf0
    test al,02h
    jz .skipf1
    or bl,20h
.skipf1
    test al,04h
    jz .skipf2
    or bh,20h
.skipf2
    test al,08h
    jz .skipf3
    or cl,20h
.skipf3
    test al,10h
    jz .skipf4
    or ch,20h
.skipf4
    test al,20h
    jz .skipf5
    or dl,20h
.skipf5
    test al,40h
    jz .skipf6
    or dh,20h
.skipf6
    test al,80h
    jz .skipf7
    or byte[.a],20h
.skipf7
.skipconvf

    mov al,[esi+48]                ; bitplane 6
    cmp al,0
    je .skipconvg
    test al,01h
    jz .skipg0
    or ah,40h
.skipg0
    test al,02h
    jz .skipg1
    or bl,40h
.skipg1
    test al,04h
    jz .skipg2
    or bh,40h
.skipg2
    test al,08h
    jz .skipg3
    or cl,40h
.skipg3
    test al,10h
    jz .skipg4
    or ch,40h
.skipg4
    test al,20h
    jz .skipg5
    or dl,40h
.skipg5
    test al,40h
    jz .skipg6
    or dh,40h
.skipg6
    test al,80h
    jz .skipg7
    or byte[.a],40h
.skipg7
.skipconvg

    mov al,[esi+49]                ; bitplane 7
    cmp al,0
    je .skipconvh
    test al,01h
    jz .skiph0
    or ah,80h
.skiph0
    test al,02h
    jz .skiph1
    or bl,80h
.skiph1
    test al,04h
    jz .skiph2
    or bh,80h
.skiph2
    test al,08h
    jz .skiph3
    or cl,80h
.skiph3
    test al,10h
    jz .skiph4
    or ch,80h
.skiph4
    test al,20h
    jz .skiph5
    or dl,80h
.skiph5
    test al,40h
    jz .skiph6
    or dh,80h
.skiph6
    test al,80h
    jz .skiph7
    or byte[.a],80h
.skiph7
.skipconvh

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov [edi+1],dh
    mov al,[.a]
    mov [edi],al
    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext
    pop eax
.nocache
    add eax,2
    dec byte[.count]
    jnz near .cacheloop

    cmp byte[.nextar],0
    je .skipall
    mov bx,[bgptrc]
    cmp [bgptrd],bx
    je .skipall
    add eax,2048-64
    mov byte[.count],32
    mov byte[.nextar],0
    jmp .cacheloop
.skipall
    pop ebx
    pop edx
    pop eax
    ret

.nbg     dw 0
.count   db 0
.a       db 0
.rowleft db 0
.nextar  db 0

NEWSYM cache8bit                                                        ;00050EC7
    ret

;*******************************************************
; Cache 2-Bit 16x16 tiles
;*******************************************************

NEWSYM cachetile2b16x16                                                 ;00050EC8
    ; Keep high word ecx 0
    push eax
    xor ecx,ecx
    push edx
    mov byte[.nextar],1
    push ebx
    ; get tile info location
    test al,20h
    jnz .highptr
    shl eax,6   ; x 64 for each line
    add ax,[bgptr]
    jmp .loptr
.highptr
    and al,1Fh
    shl eax,6   ; x 64 for each line
    add ax,[bgptrc]
.loptr
    add eax,[vram]
    mov bx,[curtileptr]
    shr bx,4
    mov byte[.count],32
    mov [.nbg],bx
    ; do loop
.cacheloop
    mov si,[eax]
    and esi,03FFh
    add si,[.nbg]
    mov byte[.tileleft],4
.nextof4
    and esi,4095
    test byte[vidmemch2+esi],01h
    jz near .nocache
    mov byte[vidmemch2+esi],00h
    push esi
    mov edi,esi
    shl esi,4
    shl edi,6
    add esi,[vram]
    add edi,[vcache2b]
    push eax
    mov byte[.rowleft],8
.donext

    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0

    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov al,[.a]
    mov [edi+1],dh
    mov [edi],al

    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext
    pop eax
    pop esi
.nocache
    inc esi
    cmp byte[.tileleft],3
    jne .noadd
    add esi,14
.noadd
    dec byte[.tileleft]
    jnz near .nextof4
    add eax,2
    dec byte[.count]
    jnz near .cacheloop

    cmp byte[.nextar],0
    je .skipall
    mov bx,[bgptrc]
    cmp [bgptrd],bx
    je .skipall
    add eax,2048-64
    mov byte[.count],32
    mov byte[.nextar],0
    jmp .cacheloop
.skipall
    pop ebx
    pop edx
    pop eax
    ret

.nbg      dw 0
.count    db 0
.a        db 0
.rowleft  db 0
.nextar   db 0
.tileleft db 0

NEWSYM cache2bit16x16                                                   ;00051093
    ret

;*******************************************************
; Cache 4-Bit 16x16 tiles
;*******************************************************

NEWSYM cachetile4b16x16                                                 ;00051094
    ; Keep high word ecx 0
    push eax
    xor ecx,ecx
    push edx
    mov byte[.nextar],1
    push ebx
    ; get tile info location
    test al,20h
    jnz .highptr
    shl eax,6   ; x 64 for each line
    add ax,[bgptr]
    jmp .loptr
.highptr
    and al,1Fh
    shl eax,6   ; x 64 for each line
    add ax,[bgptrc]
.loptr
    add eax,[vram]
    mov bx,[curtileptr]
    shr bx,5
    mov byte[.count],32
    mov [.nbg],bx

    ; do loop
.cacheloop
    mov si,[eax]
    and esi,03FFh
    add si,[.nbg]
    shl esi,1
    mov byte[.tileleft],4
.nextof4
    and esi,4095
    test word[vidmemch4+esi],0101h
    jz near .nocache
    mov word[vidmemch4+esi],0000h
    push esi
    mov edi,esi
    shl esi,4
    shl edi,5
    add esi,[vram]
    add edi,[vcache4b]
    push eax
    mov byte[.rowleft],8
.donext

    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0

    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    mov al,[esi+16]                ; bitplane 2
    cmp al,0
    je .skipconvc
    test al,01h
    jz .skipc0
    or ah,04h
.skipc0
    test al,02h
    jz .skipc1
    or bl,04h
.skipc1
    test al,04h
    jz .skipc2
    or bh,04h
.skipc2
    test al,08h
    jz .skipc3
    or cl,04h
.skipc3
    test al,10h
    jz .skipc4
    or ch,04h
.skipc4
    test al,20h
    jz .skipc5
    or dl,04h
.skipc5
    test al,40h
    jz .skipc6
    or dh,04h
.skipc6
    test al,80h
    jz .skipc7
    or byte[.a],04h
.skipc7
.skipconvc

    mov al,[esi+17]                ; bitplane 3
    cmp al,0
    je .skipconvd
    test al,01h
    jz .skipd0
    or ah,08h
.skipd0
    test al,02h
    jz .skipd1
    or bl,08h
.skipd1
    test al,04h
    jz .skipd2
    or bh,08h
.skipd2
    test al,08h
    jz .skipd3
    or cl,08h
.skipd3
    test al,10h
    jz .skipd4
    or ch,08h
.skipd4
    test al,20h
    jz .skipd5
    or dl,08h
.skipd5
    test al,40h
    jz .skipd6
    or dh,08h
.skipd6
    test al,80h
    jz .skipd7
    or byte[.a],08h
.skipd7
.skipconvd

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov al,[.a]
    mov [edi+1],dh
    mov [edi],al


    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext
    pop eax
    pop esi
.nocache
    add esi,2
    cmp byte[.tileleft],3
    jne .noadd
    add esi,28
.noadd
    dec byte[.tileleft]
    jnz near .nextof4
    add eax,2
    dec byte[.count]
    jnz near .cacheloop

    cmp byte[.nextar],0
    je .skipall
    mov bx,[bgptrc]
    cmp [bgptrd],bx
    je .skipall
    add eax,2048-64
    mov byte[.count],32
    mov byte[.nextar],0
    jmp .cacheloop
.skipall
    pop ebx
    pop edx
    pop eax
    ret

.nbg     dw 0
.count   db 0
.a       db 0
.rowleft db 0
.nextar  db 0
.tileleft db 0

NEWSYM cache4bit16x16                                                   ;000512F0
    ret

;*******************************************************
; Cache 8-Bit 16x16 tiles
;*******************************************************

NEWSYM cachetile8b16x16                                                 ;000512F1
    ; Keep high word ecx 0
    push eax
    xor ecx,ecx
    push edx
    mov byte[.nextar],1
    push ebx
    ; get tile info location
    test al,20h
    jnz .highptr
    shl eax,6   ; x 64 for each line
    add ax,[bgptr]
    jmp .loptr
.highptr
    and al,1Fh
    shl eax,6   ; x 64 for each line
    add ax,[bgptrc]
.loptr
    add eax,[vram]
    mov bx,[curtileptr]
    shr bx,6
    mov byte[.count],32
    mov [.nbg],bx

    ; do loop
.cacheloop
    mov si,[eax]
    and esi,03FFh
    add si,[.nbg]
    shl esi,2
    mov byte[.tileleft],4
.nextof4
    and esi,4095
    test dword[vidmemch8+esi],01010101h
    jz near .nocache
    mov dword[vidmemch8+esi],00000000h
    push esi
    mov edi,esi
    shl esi,4
    shl edi,4
    add esi,[vram]
    add edi,[vcache8b]
    push eax
    mov byte[.rowleft],8
.donext
    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0

    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    mov al,[esi+16]                ; bitplane 2
    cmp al,0
    je .skipconvc
    test al,01h
    jz .skipc0
    or ah,04h
.skipc0
    test al,02h
    jz .skipc1
    or bl,04h
.skipc1
    test al,04h
    jz .skipc2
    or bh,04h
.skipc2
    test al,08h
    jz .skipc3
    or cl,04h
.skipc3
    test al,10h
    jz .skipc4
    or ch,04h
.skipc4
    test al,20h
    jz .skipc5
    or dl,04h
.skipc5
    test al,40h
    jz .skipc6
    or dh,04h
.skipc6
    test al,80h
    jz .skipc7
    or byte[.a],04h
.skipc7
.skipconvc

    mov al,[esi+17]                ; bitplane 3
    cmp al,0
    je .skipconvd
    test al,01h
    jz .skipd0
    or ah,08h
.skipd0
    test al,02h
    jz .skipd1
    or bl,08h
.skipd1
    test al,04h
    jz .skipd2
    or bh,08h
.skipd2
    test al,08h
    jz .skipd3
    or cl,08h
.skipd3
    test al,10h
    jz .skipd4
    or ch,08h
.skipd4
    test al,20h
    jz .skipd5
    or dl,08h
.skipd5
    test al,40h
    jz .skipd6
    or dh,08h
.skipd6
    test al,80h
    jz .skipd7
    or byte[.a],08h
.skipd7
.skipconvd

    mov al,[esi+32]                ; bitplane 4
    cmp al,0
    je .skipconve
    test al,01h
    jz .skipe0
    or ah,10h
.skipe0
    test al,02h
    jz .skipe1
    or bl,10h
.skipe1
    test al,04h
    jz .skipe2
    or bh,10h
.skipe2
    test al,08h
    jz .skipe3
    or cl,10h
.skipe3
    test al,10h
    jz .skipe4
    or ch,10h
.skipe4
    test al,20h
    jz .skipe5
    or dl,10h
.skipe5
    test al,40h
    jz .skipe6
    or dh,10h
.skipe6
    test al,80h
    jz .skipe7
    or byte[.a],10h
.skipe7
.skipconve

    mov al,[esi+33]                ; bitplane 5
    cmp al,0
    je .skipconvf
    test al,01h
    jz .skipf0
    or ah,20h
.skipf0
    test al,02h
    jz .skipf1
    or bl,20h
.skipf1
    test al,04h
    jz .skipf2
    or bh,20h
.skipf2
    test al,08h
    jz .skipf3
    or cl,20h
.skipf3
    test al,10h
    jz .skipf4
    or ch,20h
.skipf4
    test al,20h
    jz .skipf5
    or dl,20h
.skipf5
    test al,40h
    jz .skipf6
    or dh,20h
.skipf6
    test al,80h
    jz .skipf7
    or byte[.a],20h
.skipf7
.skipconvf

    mov al,[esi+48]                ; bitplane 6
    cmp al,0
    je .skipconvg
    test al,01h
    jz .skipg0
    or ah,40h
.skipg0
    test al,02h
    jz .skipg1
    or bl,40h
.skipg1
    test al,04h
    jz .skipg2
    or bh,40h
.skipg2
    test al,08h
    jz .skipg3
    or cl,40h
.skipg3
    test al,10h
    jz .skipg4
    or ch,40h
.skipg4
    test al,20h
    jz .skipg5
    or dl,40h
.skipg5
    test al,40h
    jz .skipg6
    or dh,40h
.skipg6
    test al,80h
    jz .skipg7
    or byte[.a],40h
.skipg7
.skipconvg

    mov al,[esi+49]                ; bitplane 7
    cmp al,0
    je .skipconvh
    test al,01h
    jz .skiph0
    or ah,80h
.skiph0
    test al,02h
    jz .skiph1
    or bl,80h
.skiph1
    test al,04h
    jz .skiph2
    or bh,80h
.skiph2
    test al,08h
    jz .skiph3
    or cl,80h
.skiph3
    test al,10h
    jz .skiph4
    or ch,80h
.skiph4
    test al,20h
    jz .skiph5
    or dl,80h
.skiph5
    test al,40h
    jz .skiph6
    or dh,80h
.skiph6
    test al,80h
    jz .skiph7
    or byte[.a],80h
.skiph7
.skipconvh

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov [edi+1],dh
    mov al,[.a]
    mov [edi],al
    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext
    pop eax
    pop esi
.nocache
    add esi,4
    cmp byte[.tileleft],3
    jne .noadd
    add esi,56
.noadd
    dec byte[.tileleft]
    jnz near .nextof4
    add eax,2
    dec byte[.count]
    jnz near .cacheloop

    cmp byte[.nextar],0
    je .skipall
    mov bx,[bgptrc]
    cmp [bgptrd],bx
    je .skipall
    add eax,2048-64
    mov byte[.count],32
    mov byte[.nextar],0
    jmp .cacheloop
.skipall
    pop ebx
    pop edx
    pop eax
    ret

.nbg      dw 0
.count    db 0
.a        db 0
.rowleft  db 0
.nextar   db 0
.tileleft db 0

NEWSYM cache8bit16x16                                                   ;0005165C
    ret
