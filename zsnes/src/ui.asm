;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM getcfg,soundon,SBHDMA,StereoSound,init,GUIRestoreVars,GUIClick,MouseDis
EXTSYM ConvertJoyMap,ConvertJoyMap1,ConvertJoyMap2,printhex,InitSPC
EXTSYM StartUp,PrintStr,WaitForKey,PrintChar,MMXCheck,ZFileSystemInit
EXTSYM SPCDisable,SystemInit,allocmem
EXTSYM xa
EXTSYM SBPort,SBInt,SBIrq,SBDMA,SBDMAPage,SBHDMAPage,getenv
EXTSYM ram7fa,wramdataa,spritetable
EXTSYM malloc,free
EXTSYM StateBackup
EXTSYM OSPort
EXTSYM ADSRGAINSwitch,FPUCopy,ScreenScale,SoundQuality
EXTSYM debugger,pl1contrl,pl2contrl,romtype,smallscreence
EXTSYM smallscreenon,spcon
EXTSYM statefileloc,LatestSave
EXTSYM Open_File, Get_File_Date, Close_File, Change_Dir, Get_Dir
EXTSYM romloadskip
EXTSYM cfgloadgdir,cfgloadsdir
EXTSYM init18_2hz

%include "betauser.mac"






; Function 0501h
; User Interface
; Search for CMDLINE= for commandline entry

NEWSYM zstart
    mov    ax,901h             ;enable interrupts
    int    31h

;*******************************************************
; Allocate Memory, ebx = size,eax returned = LFB pointer
;*******************************************************
    mov ax,0FFFFh
    mov edx,68h * 10000h
    int 21h
    jc short .outofmemory

    mov edx,welcome         ;welcome message
    mov ah,09h
    int 21h

    cld                     ;clear direction flag

    call StartUp

    ; get previous video mode
    xor ecx,ecx
    push es
    mov ax,[selc0040]
    mov es,eax
    mov al,[es:49h]
    mov [previdmode],al
    pop es

    push es
    call allocptr           ;partition allocated memory
    call getcfg
    call tparms
    pop es

    cmp byte [soundon],0
    jz short .skipcmdline
    call getblaster
.skipcmdline

    jmp init

.outofmemory
    mov edx,outofmem
    mov ah,09h
    int 21h
    jmp DosExit

NEWSYM outofmem, db 'You don',39,'t have enough memory to run this program!',13,10,'$'
%define ZVERSION '150'
;%define ZBETA    0
NEWSYM welcome


%ifdef ZBETA
                 db 'ZSNES v0.',ZVERSION,' beta (c)1997, Compiled under NASM and WDOSX',13,10
                 db 'PRIVATE BETA VERSION!!!  PLEASE DO NOT DISTRIBUTE!!!  Thank you!',13,10
                 db 'Private Beta is Registered to : ',USERNAMEN,13,10
%else
                 db 'ZSNES v0.',ZVERSION,' beta (c)1997, Compiled under NASM and WDOSX',13,10
%endif
                 db '  Programmers : zsKnight, _Demo_',13,10,13,10,'$'


; global variables
NEWSYM string,  times 128 db 0                                          ;000000FE
NEWSYM fname,   times 128 db 0                                          ;0000017E
NEWSYM fnames,  times 128 db 0                                          ;000001FE
NEWSYM fnamest, times 128 db 0                                          ;0000027E
    db 0

NEWSYM selc0040,      dw 0                                              ;000002FF
NEWSYM selcA000,      dw 0
NEWSYM selcB800,      dw 0
NEWSYM filefound,     db 0              ; Parameter String Found        ;00000305
NEWSYM prevdash,      db 0              ; Previous Dash Value
NEWSYM frameskip,     db 0              ; 0 = Auto, 1-10 = Skip 0 .. 9
NEWSYM per2exec,      db 100            ; percentage of opcodes to execute
NEWSYM vidbuffer,     dd 0              ; video buffer (1024x239 = 244736) ;00000309
NEWSYM headdata,      dd 0              ; load data == rom data         ;0000030D
NEWSYM romdata,       dd 0              ; rom data  (4MB = 4194304)     ;00000311
NEWSYM wramdata,      dd 0              ; stack (64K = 32768)           ;00000315
NEWSYM ram7f,         dd 0              ; ram @ 7f = 65536              ;00000319
NEWSYM vram,          dd 0              ; vram = 65536                  ;0000031D
NEWSYM sram,          dd 0              ; sram = 32768                  ;00000321
NEWSYM debugbuf,      dd 0              ; debug buffer = 80x1000 = 80000;00000325
NEWSYM regptr,        dd 0              ; pointer to registers          ;00000329
NEWSYM regptw,        dd 0              ; pointer to registers          ;0000032D
NEWSYM vcache2b,      dd 0              ; 2-bit video cache
NEWSYM vcache4b,      dd 0              ; 4-bit video cache
NEWSYM vcache8b,      dd 0              ; 8-bit video cache

NEWSYM romispal,      db 0              ; 0 = NTSC, 1 = PAL             ;0000033D
NEWSYM enterpress,    db 0              ; if enter is to be issued (0 = yes)

NEWSYM previdmode,    db 0              ; previous video mode           ;0000033F
NEWSYM cvidmode,      db 1              ; video mode, 0=320x240, 1=256x256
NEWSYM cbitmode,      db 0              ; bit mode, 0=8bit, 1=16bit     ;00000341
NEWSYM opexec268,     db 171            ; # of opcodes/scanline in 2.68Mhz mode
NEWSYM opexec358,     db 180            ; # of opcodes/scanline in 3.58Mhz mode (228/180)
NEWSYM opexec268cph,  db 42             ; # of opcodes/hblank in 2.68Mhz mode
NEWSYM opexec358cph,  db 45             ; # of opcodes/hblank in 3.58Mhz mode (56/50)
NEWSYM debugdisble,   db 1              ; debugger disable.  0 = no, 1 = yes
;NEWSYM BetaUser, db 37,38,210,56,78,23,7,7,0

;*******************************************************
; Get Blaster            Locates SET BLASTER environment
;*******************************************************
NEWSYM getblaster
    xor eax,eax
    mov ax,[es:02Ch]
    push es
    mov es,eax
    xor esi,esi
    xor ebx,ebx
    xor dh,dh

    mov byte[.cursetting],0
.a
    mov dl,[es:esi]
    cmp dl,'a'
    jb .nocap
    cmp dl,'z'
    ja .nocap
    sub dl,'a'-'A'
.nocap
    inc esi
    cmp bl,8
    jnc .sbfound
    inc bl
    cmp [.string2s+ebx-1],dl
    jnz .src
    inc dh
    jmp .src
.sbfound
    mov byte[.blfound],1
    cmp bl,dh
    jnz .src
    cmp dl,'A'
    jne .afound
    mov byte[.cursetting],1
    mov word[SBPort],0
    jmp .src
.afound
    cmp dl,'I'
    jne .ifound
    mov byte[.cursetting],2
    mov byte[SBIrq],0
    jmp .src
.ifound
    cmp dl,'D'
    jne .dfound
    mov byte[.cursetting],3
    mov byte[SBDMA],0
    jmp .src
.dfound
    cmp dl,'H'
    jne .hfound
    mov byte[.cursetting],4
    mov byte[SBHDMA],0
    jmp .src
.hfound
    cmp dl,' '
    je .src2
    cmp dl,0
    je .src2
    jmp .src3
.src2
    mov byte[.cursetting],0
    jmp .src
.src3
    cmp byte[.cursetting],1
    jne .nproca
    shl word[SBPort],4
    sub dl,48
    add byte[SBPort],dl
    add dl,48
.nproca
    cmp byte[.cursetting],2
    jne .nproci
    sub dl,48
    mov [SBIrq],dl
    add dl,48
.nproci
    cmp byte[.cursetting],3
    jne .nprocd
    sub dl,48
    mov [SBDMA],dl
    add dl,48
.nprocd
    cmp byte[.cursetting],4
    jne .nproch
    sub dl,48
    mov [SBHDMA],dl
    add dl,48
.nproch
.src
    cmp dl,0
    jne near .a
    cmp byte[es:esi],0
    jz .eos
    xor bl,bl
    xor dh,dh
    jmp .a
.eos
    pop es
    cmp byte[.blfound],0
    je near .nosound

    mov edx, .blasterstr
    mov ah,09h
    int 21h
    mov al,[SBIrq]
    add al,08h
    mov [SBInt],al
    cmp byte[SBDMA],0
    jne .dma0
    mov byte[SBDMAPage],87h
.dma0
    cmp byte[SBDMA],1
    jne .dma1
    mov byte[SBDMAPage],83h
.dma1
    cmp byte[SBDMA],2
    jne .dma2
    mov byte[SBDMAPage],81h
.dma2
    cmp byte[SBDMA],3
    jne .dma3
    mov byte[SBDMAPage],82h
.dma3
    cmp byte[SBHDMA],5
    jne .hdma5
    mov byte[SBHDMAPage],8Bh
.hdma5
    cmp byte[SBHDMA],6
    jne .hdma6
    mov byte[SBHDMAPage],89h
.hdma6
    cmp byte[SBHDMA],7
    jne .nosound
    mov byte[SBHDMAPage],8Ah
.nosound
    ret

.string2s db 'BLASTER='
.blfound  db 0
.blasterstr db 'SET BLASTER environment found!',10,13,10,13,'$'
.cursetting db 0

; Find Selector
findselec:                                              ;0000054C
    mov ax, 2
    int 31h
    jnc .proceed
    mov edx, .noselector
    mov ah,09h
    int 21h
    jmp DosExit
.proceed
    ret
.noselector db 'Cannot find selector!',10,13,'$'

;*******************************************************
; Allocate Pointer    Sets variables with pointer values
;*******************************************************

NEWSYM allocptr
    ; Set up memory values
    mov eax,esp
    add eax,250*1000            ; feels arbitrary...
    mov [vidbuffer],eax
    add eax,1024*239
    mov [headdata],eax
    mov [romdata],eax
    add eax,4*1024*1024 + 32*1024
    mov [wramdata],eax
    add eax,65536
    mov [ram7f],eax
    add eax,65536
    mov [vram],eax
    add eax,65536
    mov [sram],eax
    add eax,32768
    mov [debugbuf],eax
    add eax,80*1000

    mov [regptr],eax
    sub dword[regptr],8000h   ; Since register address starts @ 2000h
    add eax,0C000h
    mov [regptw],eax
    sub dword[regptw],8000h   ; Since register address starts @ 2000h
    add eax,0C000h

    ; 2-bit = 256k
    mov dword[vcache2b],eax
    add eax,256*1024
    ; 4-bit = 128k
    mov dword[vcache4b],eax
    add eax,128*1024
    ; 8 bit = 64k
    mov dword[vcache8b],eax
    add eax,64*1024
    ret

;*******************************************************

NEWSYM StartUp
    mov bx,0A000h
    call findselec
    mov [selcA000],ax
    mov bx,0B800h
    call findselec
    mov [selcB800],ax
    mov bx,0040h
    call findselec
    mov [selc0040],ax
    ret

;*******************************************************
; Print Numbers                Prints # in EAX to screen
;*******************************************************
NEWSYM printnum                                                 ;00000642
    ; process through each digit
    push edx
    push eax
    push ebx
    push cx
    xor edx,edx           ; clear high byte
    xor cx,cx             ; clear counter variable
    mov ebx,10
  .loopa
    div ebx              ; get quotent and remainder
    push edx              ; store number to stack
    inc cl
    xor edx,edx
    test eax,0FFFFFFFFh
    jnz .loopa
  .loopb
    pop edx              ; get number back from stack
    add dl,30h          ; adjust to ASCII value
    mov ah,02h
    int 21h
    dec cl
    jnz .loopb
    pop cx
    pop ebx
    pop eax
    pop edx
    ret

;*******************************************************
; Check Parameter          This Processes the Parameters
;*******************************************************

NEWSYM checkparm                                                ;00000671
    push cx
    push esi
    mov cl,[string]
    xor ch,ch
    mov esi,string+1
    mov ah,2
    mov al,[esi]
    cmp al,'-'
    jz .dash

    mov al,[prevdash]
    cmp al,1
    jz .is1
    cmp al,2
    jz .is2
    cmp al,3
    jz .is3
    cmp al,4
    jz .is4
    cmp al,5
    jz .is5
    cmp al,6
    jz .is6
    mov al,[filefound]
    test al,0FFh
    jz .notfound
    jmp .badparam
.notfound

    mov byte[filefound],1
    xor ebx,ebx
    inc cl
.copyit
    mov al,[string+ebx]
    mov [fname+ebx],al
    inc ebx
    loop .copyit

    xor ebx,ebx
    xor ah,ah
    mov cl,[fname]
    mov [fnames],cl
    mov [fnamest],cl
    mov dl,cl
    inc ebx
.loopc
    mov al,[fname+ebx]
    mov [fnames+ebx],al
    mov [fnamest+ebx],al
    inc ebx
    inc ah
    loop .loopc
    ; find for '.' or '\'
    mov cl,dl
    mov edx,ebx
.loopz
    dec edx
    mov al,[fnames+edx]
    cmp al,'\'
    je .addext
    cmp al,'.'
    je .addb
    dec cl
    jnz .loopz
    jmp .addext
.addb
    mov ebx,edx
.addext
    mov byte[fnames+ebx],'.'
    mov byte[fnamest+ebx],'.'
    inc ebx
    mov byte[fnames+ebx],'S'
    mov byte[fnamest+ebx],'Z'
    inc ebx
    mov byte[fnames+ebx],'R'
    mov byte[fnamest+ebx],'S'
    inc ebx
    mov byte[fnames+ebx],'M'
    mov byte[fnamest+ebx],'T'
    mov dword[statefileloc],ebx
    add ah,4
    mov [fnames],ah
    mov [fnamest],ah
    pop esi
    pop cx
    ret

.is1
    cmp cl,1
    jnz .badframe
    mov al,[esi]
    cmp al,'0'
    jc .badframe
    cmp al,'9'
    ja .badframe
    sub al,'0'-1
    mov [frameskip],al
    mov byte[prevdash],0
    pop esi
    pop cx
    ret
.badframe
    mov edx,.invalidframe
    mov ah,09h
    int 21h
    jmp DosExit

.is3
    cmp cl,1
    jnz .badcvid
    mov al,[esi]
    cmp al,'0'
    jc .badcvid
    cmp al,'3'
    ja .badcvid
    sub al,'0'
    mov [cvidmode],al
    mov byte[prevdash],0
    pop esi
    pop cx
    ret
.badcvid
    mov edx,.invalidcvid
    mov ah,09h
    int 21h
    jmp DosExit

.is4
    cmp cl,1
    jnz .badrate
    mov al,[esi]
    cmp al,'0'
    jc .badrate
    cmp al,'3'
    ja .badrate
    sub al,'0'
    mov [SoundQuality],al
    mov byte[prevdash],0
    pop esi
    pop cx
    ret
.badrate
    mov edx,.invalidrate
    mov ah,09h
    int 21h
    jmp DosExit

.is2
    cmp cl,0
    jz .badpct
    mov byte[per2exec],0
    mov byte[prevdash],0
.nextdigit
    mov bl,10
    mov al,[per2exec]
    cmp al,100
    jnc .badpct
    mul bl
    mov [per2exec],al
    mov al,[esi]
    cmp al,'0'
    jc .badpct
    cmp al,'9'
    ja .badpct
    sub al,'0'
    add [per2exec],al
    inc esi
    dec cl
    test cl,0FFh
    jz .85d
    cmp byte[esi],' '
    jnz .nextdigit
.85d
    cmp byte[per2exec],100
    ja .badpct
    cmp byte[per2exec],50
    jc .badpct
    dec byte[per2exec]
    pop esi
    pop cx
    ret
.badpct
    mov edx,.invalidpct
    mov ah,09h
    int 21h
    jmp DosExit

.is5
    cmp cl,1
    jnz .badplyr
    mov al,[esi]
    cmp al,'0'
    jc .badplyr
    cmp al,'3'
    ja .badplyr
    sub al,'0'
    mov [pl1contrl],al
    mov byte[prevdash],0
    pop esi
    pop cx
    ret
.is6
    cmp cl,1
    jnz .badplyr
    mov al,[esi]
    cmp al,'0'
    jc .badplyr
    cmp al,'3'
    ja .badplyr
    sub al,'0'
    mov [pl2contrl],al
    mov byte[prevdash],0
    pop esi
    pop cx
    ret
.badplyr
    mov edx,.invalidplyr
    mov ah,09h
    int 21h
    jmp DosExit

.dash
    cmp cl,2
    jnz .badparam
    mov al,[esi+1]
    cmp al,'a'
    jb .nocap
    cmp al,'z'
    ja .nocap
    sub al,'a'-'A'
.nocap
    cmp al,'F'
    jnz .notf
    mov byte[prevdash],1
    jmp .done
.notf
    cmp byte[debugdisble],0
    jnz .notd
    cmp al,'D'
    jnz .notd
    mov byte[debugger],1
    jmp .done
.notd
    cmp al,'L'
    jnz .notl
    mov byte[romtype],1
    jmp .done
.notl
    cmp al,'H'
    jnz .noth
    mov byte[romtype],2
    jmp .done
.noth
    cmp al,'P'
    jnz .notp
    mov byte[prevdash],2
    jmp .done
.notp
    cmp al,'A'
    jnz .nota
    mov byte[frameskip],0
    jmp .done
.nota
    cmp al,'S'
    jnz .nots
    mov byte[spcon],1
    mov byte[soundon],1
    jmp .done
.nots
    cmp al,'V'
    jnz .notv
    mov byte[prevdash],3
    jmp .done
.notv
    cmp al,'R'
    jnz .notr
    mov byte[prevdash],4
    jmp .done
.notr
    cmp al,'1'
    jnz .not1
    mov byte[prevdash],5
    jmp .done
.not1
    cmp al,'2'
    jnz .not2
    mov byte[prevdash],6
    jmp .done
.not2
    cmp al,'E'
    jnz .note
    mov byte[enterpress],1
    jmp .done
.note
    jmp .badparam

.done
    pop esi
    pop cx
    ret

.badparam
    mov edx,.invalidparam
    mov ah,09h
    int 21h
    mov ah,02h
.nextch
    lodsb
    mov dl,al
    int 21h
    loop .nextch
    mov dl,13
    int 21h
    mov dl,10
    int 21h
    jmp DosExit

.invalidparam db 'Invalid parameter : $'
.invalidframe db 'Frame Skip must be a value of 0 to 9!',13,10,'$'
.invalidrate  db 'Sound Sampling Rate must be a value of 0 to 3!',13,10,'$'
.invalidcvid  db 'Invalid Video Mode!',13,10,'$'
.invalidpct   db 'Percentage of instructions to execute must be a number from 50 to 100!',13,10,'$'
.invalidplyr  db 'Player Input must be a value from 0 to 3!',13,10,'$'

;*******************************************************
; Get Parameters             Get Parameters Individually
;*******************************************************

NEWSYM tparms                                                   ;00000ADE
    mov esi,80h
    xor edi,edi

    mov cl,[es:esi]
    test cl,0FFh
    jz .nochars
    inc esi

.nextarg
    mov al,[es:esi]
    cmp al,' '
    jnz .notspc
    inc esi
    dec cl
    test cl,0FFh
    jz .donestring
    jmp .nextarg
.notspc
    inc byte[.numparam]

.copyit
    mov al,[es:esi]
    cmp al,' '
    jz .checkit
    inc byte[string]
    inc edi
    mov [string+edi],al
    inc esi
    dec cl
    test cl,0FFh
    jz .eos
    jmp .copyit

.checkit
    call checkparm

    mov byte[string],0
    xor edi,edi
    jmp .nextarg

.eos
    call checkparm

  .donestring
    test byte[.numparam],0FFh
    jz .nochars
    mov al,byte[filefound]
    test al,0FFh
    jz .nostring
    ret

  .nostring
    mov edx,.nostr
    mov ah,09h
    int 21h
    jmp DosExit

  .nochars
    mov edx,.noparams
    mov ah,09h
    int 21h
    jmp DosExit

.numparam db 0

.noparams db 'Usage : ZSNES [-d,-f #, ... ] <filename.SMC>',13,10
          db '   Eg : ZSNES -s -r 2 game.smc',13,10,13,10
          db '  -1 #    Select Player 1 Input :',13,10
          db '            1 = Keyboard  2 = Joystick  3 = Gamepad',13,10
          db '  -2 #    Select Player 2 Input :',13,10
          db '            0 = None      1 = Keyboard  2 = Joystick  3 = Gamepad',13,10
          db '  -a      Turn on auto frame skip',13,10
          db '  -e      Skip enter key press at the beginning',13,10
          db '  -f #    Turn on frame skip [0..9]',13,10
          db '  -h      Force HiROM',13,10
          db '  -l      Force LoROM',13,10
          db '  -r #    Set Sampling Sound Blaster Sampling Rate & Bit :',13,10
          db '             0 = 8000Hz 1 = 11025Hz 2 = 22050Hz 3 = 44100Hz',13,10
          db '  -s      Enable SPC700/DSP emulation (buggy due to lack of docs)',13,10
          db '  -v #    Select Video Mode :',13,10
          db '            0 = 320x240x256           1 = 256x256x256 (default)',13,10
          db '            2 = 320x240x256 VESA2     3 = 320x240x65536(VESA2) ',13,10,'$$'

.nostr db 'This emulator will not work without a filename!',13,10,'$'

NEWSYM DosExit          ; Terminate Program     ;00000EB7
    call init18_2hz ;015CCFh
    mov    ax,4c00h            ;terminate
    int    21h

