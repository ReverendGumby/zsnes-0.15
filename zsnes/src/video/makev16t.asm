;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM dualstartprocess,dualwinbg,dualwinsp,dwinptrproc,pwinbgenab
EXTSYM pwinbgtype,pwinspenab,pwinsptype,winbgdata,winlogicb,winonbtype
EXTSYM winonstype,winspdata,interlval,FPUCopy
EXTSYM bg1scrolx,bg1scroly,curmosaicsz,curypos,drawmode716t
EXTSYM mode7set,mosaicon,mosaicsz,scrnon,winbg1en,winenabm
EXTSYM drawmode716textbg,drawmode716textbg2
EXTSYM extbgdone
EXTSYM drawmode716tb,drawmode716b,drawmode716extbg,drawmode716extbg2
EXTSYM cursprloc,drawsprites16b,scrndis,winonsp
EXTSYM scaddtype
EXTSYM alreadydrawn,bg1cachloc,bg1tdabloc,bg1tdatloc,bg1vbufloc,bg1xposloc
EXTSYM bg1yaddval,bgmode,bgtilesz,colormodeofs,curbgnum
EXTSYM draw16x1616b,draw8x816b,drawn,winenabs
EXTSYM curbgpr,draw16x1616tms,draw8x816tms
EXTSYM bg3highst,currentobjptr,curvidoffset,cwinenabm
EXTSYM preparesprpr,procbackgrnd,setpalette16b,spritetable
EXTSYM sprleftpr,sprlefttot
EXTSYM numwin,scaddset,wincolen,windowdata,winl1,winl2
EXTSYM winon,winr1,winr2
EXTSYM vidbuffer
EXTSYM FPUZero,coladdb,coladdg,coladdr,vesa2_bpos,vesa2_gpos,vesa2_rpos
EXTSYM vidbright
EXTSYM winptrref
EXTSYM fulladdtab,pal16b,vesa2_clbit
EXTSYM csprbit
EXTSYM sprclprio
EXTSYM csprprlft,sprsingle,sprpriodata
EXTSYM pal16bcl,pal16bxcl
EXTSYM bgofwptr,bgsubby,bshifter,domosaic16b,dowindow16b,temp,tempcach,temptile
EXTSYM tileleft16b,xtravbuf,yadder
EXTSYM yrevadder,vcache2b,vcache4b,vcache8b
EXTSYM vcache2ba,vcache4ba,vcache8ba,draw8x816boffset
EXTSYM osm2dis,draw16x816
EXTSYM hirestiledat,res512switch
EXTSYM bg1objptr,bg1ptr,bg1ptrc,bg3ptr,bg3scrolx,bg3scroly,cachesingle
EXTSYM colormoded2,offsetenab,offsetmclr,offsetmcol,offsetmodeptr
EXTSYM offsetmptr,offsetmshl,offsetmtst,offsetptra,offsetptrb,posyscroll
EXTSYM prevoffsetdat,prevtempcache,tempoffset,vidmemch2,vidmemch4
EXTSYM vidmemch8,vram
EXTSYM ofsmcptr,ofsmady,ofsmadx,yposng,yposngom,flipyposng,flipyposngom
EXTSYM ofsmtptr,ofsmmptr,ofsmcyps,bgtxadd,bg1ptrx,bg1ptry
EXTSYM a16x16xinc,a16x16yinc

%include "video/vidmacro.mac"





; clearback16bts clearback16bdual

;*******************************************************
; DrawLine 16bit Transparent      Draws the current line
;*******************************************************
; use curypos+bg1scroly for y location and bg1scrolx for x location
; use bg1ptr(b,c,d) for the pointer to the tile number contents
; use bg1objptr for the pointer to the object tile contents

NEWSYM procmode716tsub                                                  ;00056571
    test word[scrnon+1],01h
    jz near .noback1
    test word[scrnon],01h
    jnz near .noback1
    mov byte[winon],0
    test word[winenabm],0001h
    jz near .nobackwin1
    test word[winenabm],0100h
    jnz near .nobackwin1
    procwindow [winbg1en]
.nobackwin1
    xor eax,eax
    xor edx,edx
    mov ax,[curypos]
    test byte[mode7set],02h
    jz .noflip
    mov ax,261
    sub ax,[curypos]
    dec ax
.noflip
    mov byte[curmosaicsz],1
    test byte[mosaicon],1
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
    xor bh,bh
    div bx
    xor edx,edx
    mul bx
.nomos
    add ax,[bg1scroly]
    mov dx,[bg1scrolx]
    call drawmode716t
.noback1
    ret

NEWSYM procmode716tmain                                                 ;000566E2
    test word[scrnon],01h
    jz near .noback1
    mov byte[winon],0
    test word[winenabm],0001h
    jz near .nobackwin1
    test word[winenabm],0100h
    jnz near .nobackwin1
    procwindow [winbg1en]
.nobackwin1
    xor eax,eax
    xor edx,edx
    mov ax,[curypos]
    test byte[mode7set],02h
    jz .noflip
    mov ax,261
    sub ax,[curypos]
    dec ax
.noflip
    mov byte[curmosaicsz],1
    test byte[mosaicon],1
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
    xor bh,bh
    div bx
    xor edx,edx
    mul bx
.nomos
    add ax,[bg1scroly]
    mov dx,[bg1scrolx]
    test byte[scaddset],02h
    jz .noscrnadd
    test word[scrnon+1],01h
    jnz near .mode7b
.noscrnadd
    call drawmode716t
.noback1
    ret
.mode7b
    call drawmode716tb
    ret

NEWSYM procspritessub16t                                                ;00056862
    test byte[scrndis],10h
    jnz .nosprites
    test byte[scrnon+1],10h
    jz .nosprites
    test byte[scrnon],10h
    jnz .nosprites
    xor ebx,ebx
    mov bl,[curypos]
    add ebx,[cursprloc]
    mov cl,[ebx]
    add dword[cursprloc],256
    cmp cl,0
    je .nosprites
    call drawsprites16b
.nosprites
    ret

NEWSYM procspritesmain16t                                               ;000568A2
    test byte[scrndis],10h
    jnz .nosprites
    test byte[scrnon],10h
    jz .nosprites
    xor ebx,ebx
    mov bl,[curypos]
    add ebx,[cursprloc]
    mov cl,[ebx]
    add dword[cursprloc],256
    cmp cl,0
    je .nosprites
    test byte[scrnon+1],10h
    jnz .spritesubmain
    test byte[scaddtype],10h
    jz .nospriteadd
    call drawsprites16t
.nosprites
    ret
.nospriteadd
    call drawsprites16b
    ret
.spritesubmain
    call drawsprites16bt
    ret

NEWSYM drawbackgrndsub16t                                               ;000568F7
    mov esi,[colormodeofs]
    mov bl,[ebp+esi+0]
    cmp bl,0
    je near .noback
    mov al,[curbgnum]
    test byte[scrnon+1],al
    jz near .noback
    test byte[scrnon],al
    jnz near .noback
    test byte[alreadydrawn],al
    jnz near .noback
    test byte[scrndis],al
    jnz near .noback
    mov byte[winon],0
    test byte[winenabs],al
    jz near .nobackwin
    procwindow [winbg1en+ebp]
.nobackwin
    mov bl,[curbgnum]
    mov byte[curmosaicsz],1
    test byte[mosaicon],bl
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
.nomos
    mov esi,[bg1vbufloc+ebp*4]
    mov edi,[bg1tdatloc+ebp*4]
    mov edx,[bg1tdabloc+ebp*4]
    mov ebx,[bg1cachloc+ebp*4]
    mov eax,[bg1xposloc+ebp*4]
    mov cl,[curbgnum]
    test byte[bgtilesz],cl
    jnz .16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x816b
    cmp byte[drawn],33
    jne .notalldrawn
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawn
.noback
    ret
.16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x1616b
    cmp byte[drawn],33
    jne .notalldrawnb
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnb
    ret

NEWSYM drawbackgrndmain16t                                              ;00056ABB
    mov esi,[colormodeofs]
    mov bl,[ebp+esi+0]
    cmp bl,0
    je near .noback
    mov al,[curbgnum]
    test byte[scrnon],al
    jz near .noback
    test byte[alreadydrawn],al
    jnz near .noback
    test byte[scrndis],al
    jnz near .noback
    mov byte[winon],0
    test byte[winenabm],al
    jz near .nobackwin
    procwindow [winbg1en+ebp]
.nobackwin
    mov bl,[curbgnum]
    mov byte[curmosaicsz],1
    test byte[mosaicon],bl
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
.nomos
    mov esi,[bg1vbufloc+ebp*4]
    mov edi,[bg1tdatloc+ebp*4]
    mov edx,[bg1tdabloc+ebp*4]
    mov ebx,[bg1cachloc+ebp*4]
    mov eax,[bg1xposloc+ebp*4]
    mov cl,[curbgnum]
;    cmp byte[curbgpr],0h
;    jne .test2
;    test byte[scaddtype],cl
;    jnz .transp
;.test2
    test byte[scaddset],02h
    jz .noscrnadd
    test byte[scrnon+1],cl
    jnz near .mainandsub
.noscrnadd
    test byte[scaddtype],cl
    jnz .transp
    test byte[bgtilesz],cl
    jnz .16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x816b
    cmp byte[drawn],33
    jne .notalldrawn
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawn
.noback
    ret
.16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x1616b
    cmp byte[drawn],33
    jne .notalldrawnb
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnb
    ret
.transp
    test byte[bgtilesz],cl
    jnz .16x16b
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x816t
    cmp byte[drawn],33
    jne .notalldrawnc
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnc
    ret
.16x16b
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x1616t
    cmp byte[drawn],33
    jne .notalldrawnd
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnd
    ret
.mainandsub
    test byte[bgtilesz],cl
    jnz .16x16c
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x816bt
    cmp byte[drawn],33
    jne .notalldrawne
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawne
    ret
.16x16c
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x1616bt
    cmp byte[drawn],33
    jne .notalldrawnf
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnf
    ret

NEWSYM transpbuf, times 512+64 db 0              ; Transparent buffer   ;00056D24


NEWSYM drawline16t                                                      ;00056F64
    cmp byte[bgmode],7
    je near processmode716t
    push esi
    push edi
    push ebx
    push edx
    push ebp
    ; current video offset
    mov dword[curvidoffset],transpbuf+32
    ; set palette
    call setpalette16b
    ; clear back area w/ back color
    procwindowback
    call clearback16bts
    ; clear registers
    xor eax,eax
    xor ecx,ecx
    ; get current sprite table
    xor ebx,ebx
    mov bl,[curypos]
    shl ebx,9
    add ebx,spritetable
    mov [currentobjptr],ebx
    mov dword[cursprloc],sprleftpr
; process backgrounds
; do background 2
    mov byte[curbgnum],02h
    mov ebp,01h
    call procbackgrnd
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call procbackgrnd
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call procbackgrnd
; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call procbackgrnd

    test byte[scaddset],02h
    jz near .noscrnadd
; draw backgrounds
    mov byte[curbgpr],0h
; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndsub16t
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndsub16t
; do background 3
    mov byte[curbgpr],20h
    cmp byte[bg3highst],1
    je .bg3nothigh
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndsub16t
.bg3nothigh
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndsub16t
; do background 2
    mov byte[curbgpr],0h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndsub16t
    call procspritessub16t
    call procspritessub16t
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndsub16t
    call procspritessub16t
; do background 2
    mov byte[curbgpr],20h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndsub16t
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndsub16t
    call procspritessub16t
; do background 3
    cmp byte[bg3highst],1
    jne .bg3high
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndsub16t
.bg3high
.noscrnadd

NEWSYM NextDrawLine16bt                                                 ;000571BF
    ; calculate current video offset
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,9
    shl ebx,6
    add esi,ebx
    add esi,32
    add esi,[vidbuffer]
    mov [curvidoffset],esi
    ; clear back area w/ back color
    call clearback16t
    mov byte[curbgpr],0h
; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain16t
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndmain16t
; do background 3
    mov byte[curbgpr],20h
    cmp byte[bg3highst],1
    je .bg3nothighb
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain16t
.bg3nothighb
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndmain16t
; do background 2
    mov byte[curbgpr],0h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndmain16t
    call procspritesmain16t
    call procspritesmain16t
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndmain16t
    call procspritesmain16t
; do background 2
    mov byte[curbgpr],20h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndmain16t
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndmain16t
    call procspritesmain16t
    cmp byte[bg3highst],1
    jne .bg3highb
; do background 3
    mov byte[curbgpr],20h
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain16t
.bg3highb
    pop ebp
    pop edx
    pop ebx
    pop edi
    pop esi
    xor eax,eax
    xor ecx,ecx
    ret

NEWSYM processmode716t                                                  ;000572D5
    push esi
    push edi
    push ebx
    push edx
    push ebp
    ; current video offset
    mov dword[curvidoffset],transpbuf+32
    ; set palette
    call setpalette16b
    ; clear back area w/ back color
    procwindowback
    call clearback16bts
    ; clear registers
    xor eax,eax
    xor ecx,ecx
    ; get current sprite table
    xor ebx,ebx
    mov bl,[curypos]
    shl ebx,9
    add ebx,spritetable
    mov [currentobjptr],ebx
    mov dword[cursprloc],sprleftpr
    test byte[scaddset],02h
    jz .nosubscr
    call procspritessub16t
    call procmode716tsub
    call procspritessub16t
    call procspritessub16t
    call procspritessub16t
.nosubscr

NEWSYM processmode716t2                                                 ;00057419
    ; calculate current video offset
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,9
    shl ebx,6
    add esi,ebx
    add esi,32
    add esi,[vidbuffer]
    mov [curvidoffset],esi
    ; get current sprite table
    xor ebx,ebx
    mov bl,[curypos]
    shl ebx,9
    add ebx,spritetable
    mov [currentobjptr],ebx
    mov dword[cursprloc],sprleftpr
    ; clear back area w/ back color
    call clearback16t
    ; clear registers
    xor eax,eax
    xor ecx,ecx
    call procspritesmain16t
    call procmode716tmain
    call procspritesmain16t
    call procspritesmain16t
    call procspritesmain16t
    pop ebp
    pop edx
    pop ebx
    pop edi
    pop esi
    ret

;*******************************************************
; Clear Backarea, with 0s
;*******************************************************

NEWSYM clearback16bts                                                   ;00057487
    cmp dword[vesa2_rpos],0
    je near clearback16bts0
    cmp byte[winon],0
    je near clearback16bts0b
.noclear
    xor eax,eax
    mov al,[coladdr]
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_rpos]
    shl ax,cl
    mov bx,ax
    xor eax,eax
    mov al,[coladdg]
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_gpos]
    shl ax,cl
    add bx,ax
    xor eax,eax
    mov al,[coladdb]
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_bpos]
    shl ax,cl
    add bx,ax
.useprevpal
    mov ax,bx
    cmp eax,0
    je .nowin
    jmp dowindowback16b
.nowin
    mov esi,[curvidoffset]
    mov ecx,128
.loopc
    mov [esi],eax
    add esi,4
    dec ecx
    jnz .loopc
    xor eax,eax
    ret

NEWSYM clearback16bts0b                                                 ;0005752F
    test byte[scaddtype],80h
    jz near clearback16bts0
    xor eax,eax
    mov al,[coladdr]
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_rpos]
    shl ax,cl
    mov bx,ax
    xor eax,eax
    mov al,[coladdg]
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_gpos]
    shl ax,cl
    add bx,ax
    xor eax,eax
    mov al,[coladdb]
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_bpos]
    shl ax,cl
    add bx,ax
.useprevpal2
    mov ax,bx
    shl eax,16
    mov ax,bx
    mov esi,[curvidoffset]
    mov ecx,128
.loopc
    mov [esi],eax
    add esi,4
    dec ecx
    jnz .loopc
    xor eax,eax
    ret

NEWSYM clearback16bts0                                                  ;000575C1
    mov esi,[curvidoffset]
    mov ecx,128
.loopc
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .loopc
    ret

NEWSYM dowindowback16b                                                  ;000575DC
    mov ebx,windowdata
    mov edi,[curvidoffset]
    xor edx,edx
    xor ch,ch
.getnext
    mov cl,[ebx]
    cmp dl,cl
    je .procnext
.dorest
    sub cl,dl
    cmp ch,0
    ja .nodraw
.loopa
    mov word[edi+edx*2],ax
    inc dl
    dec cl
    jnz .loopa
.procnext
    add ch,[ebx+1]
    add ebx,2
    test byte[numwin],0FFh
    jz .finishwin
    dec byte[numwin]
    jnz .getnext
    xor cl,cl
    jmp .dorest
.nodraw
.loopb
    mov word[edi+edx*2],0
    inc dl
    dec cl
    jnz .loopb
    jmp .procnext
.finishwin
    xor eax,eax
    ret

;*******************************************************
; Clear Backarea, 16-bit mode w/ transparency
;*******************************************************
NEWSYM clearback16t                                                     ;00057637
    test byte[scaddtype],20h
    jz near .backcopy
    mov ax,[pal16b]
    mov esi,[curvidoffset]
    mov ebp,transpbuf+32
    and ax,[vesa2_clbit]
    shr ax,1
    test byte[scaddtype],40h
    jz .fulladd
    mov ecx,128
.loopa
    mov ebx,[ebp]
    and bx,[vesa2_clbit]
    shr bx,1
    add bx,ax
    mov [esi],bx
    shr ebx,16
    and bx,word[vesa2_clbit]
    shr bx,1
    add bx,ax
    mov [esi+2],bx
    add ebp,4
    add esi,4
    dec ecx
    jnz .loopa
    xor eax,eax
    ret
.fulladd
    mov ecx,256
    xor ebx,ebx
.loopc
    mov bx,[ebp]
    and bx,[vesa2_clbit]
    shr bx,1
    add bx,ax
    mov bx,[fulladdtab+ebx*2]
    mov [esi],bx
    add ebp,2
    add esi,2
    dec ecx
    jnz .loopc
    xor eax,eax
    ret
.backcopy
    mov esi,[curvidoffset]
    mov ecx,128
    mov ebp,transpbuf+32
.loopc2
    mov eax,[ebp]
    mov [esi],eax
    add ebp,4
    add esi,4
    dec ecx
    jnz .loopc2
    ret

NEWSYM drawsprites16bt                                                  ;00057700
    mov esi,[currentobjptr]
    xor ebx,ebx
    xor eax,eax
.loopobj
    mov edi,[curvidoffset]
    test byte[esi+7],20h
    jnz near .drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    shl bx,1
    mov esi,[esi+2]
    mov ebp,ebx
    add edi,ebx
    add ebp,transpbuf+32
    drawspr16bt 0, 16
    drawspr16bt 1, 14
    drawspr16bt 2, 12
    drawspr16bt 3, 10
    drawspr16bt 4, 8
    drawspr16bt 5, 6
    drawspr16bt 6, 4
    drawspr16bt 7, 2
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    shl bx,1
    mov esi,[esi+2]
    mov ebp,ebx
    add edi,ebx
    add ebp,transpbuf+32
    drawspr16bt 7, 16
    drawspr16bt 6, 14
    drawspr16bt 5, 12
    drawspr16bt 4, 10
    drawspr16bt 3, 8
    drawspr16bt 2, 6
    drawspr16bt 1, 4
    drawspr16bt 0, 2
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

NEWSYM drawsprites16t                                                   ;00057902
    test byte[scaddtype],40h
    jz near drawspritesfulladd
    test byte[scaddtype],80h
    jnz near drawspritesfulladd
    mov esi,[currentobjptr]
    mov edi,[curvidoffset]
    xor ebx,ebx
    xor eax,eax
.loopobj
    test byte[esi+7],20h
    jnz near .drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    shl bx,1
    mov esi,[esi+2]
    cmp ch,12*16
    jae near .transparentobjnf
    drawsprgrp drawspr16ta
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    shl bx,1
    mov esi,[esi+2]
    cmp ch,12*16
    jae near .transparentobj
    drawsprgrpf drawspr16ta
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.transparentobjnf
    mov ebp,ebx
    add edi,ebx
    xor edx,edx
    add ebp,transpbuf+32
    drawsprgrp drawspr16tb
    pop esi
    mov edi,[curvidoffset]
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.transparentobj
    mov ebp,ebx
    add edi,ebx
    xor edx,edx
    add ebp,transpbuf+32
    drawsprgrpf drawspr16tb
    pop esi
    mov edi,[curvidoffset]
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

drawspritesfulladd:
    mov esi,[currentobjptr]
    mov edi,[curvidoffset]
    xor ebx,ebx
    xor eax,eax
.loopobj
    test byte[esi+7],20h
    jnz near .drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    shl bx,1
    mov esi,[esi+2]
    cmp ch,12*16
    jae near .transparentobjnf
    drawsprgrp drawspr16ta
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    shl bx,1
    mov esi,[esi+2]
    cmp ch,12*16
    jae near .transparentobj
    drawsprgrpf drawspr16ta
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.transparentobjnf
    test byte[scaddtype],80h
    jnz near .transparentobjnfs
    mov ebp,ebx
    add edi,ebx
    xor edx,edx
    add ebp,transpbuf+32
    drawsprgrp drawspr16tc
    pop esi
    mov edi,[curvidoffset]
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.transparentobjnfs
    mov ebp,ebx
    add edi,ebx
    xor edx,edx
    add ebp,transpbuf+32
    drawsprgrp drawspr16td
    pop esi
    mov edi,[curvidoffset]
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.transparentobj
    test byte[scaddtype],80h
    jnz near .transparentobjs
    mov ebp,ebx
    add edi,ebx
    xor edx,edx
    add ebp,transpbuf+32
    drawsprgrpf drawspr16tc
    pop esi
    mov edi,[curvidoffset]
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.transparentobjs
    mov ebp,ebx
    add edi,ebx
    xor edx,edx
    add ebp,transpbuf+32
    drawsprgrpf drawspr16td
    pop esi
    mov edi,[curvidoffset]
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

NEWSYM draw8x816bt                                                      ;00058730
    mov [temp],al
    mov [bshifter],ah
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .domosaic
.nomosaic
    cmp byte[winon],0
    je .domosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
.domosaic
; tile value : bit 15 = flipy, bit 14 = flipx, bit 13 = priority value
;              bit 10-12 = palette, 0-9=tile#
    mov [temptile],edx
    mov ebp,transpbuf+32
    sub ebp,eax
    sub ebp,eax
    mov byte[tileleft16b],33
    mov byte[drawn],0
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    add edi,2
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    and eax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl eax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    ; Start loop
    drawtilegrp draw8x816bta
.hprior
    add esi,16
    add ebp,16
    inc dl
    cmp dl,20h
    jne .loopc2
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    drawtilegrpf draw8x816bta
    add esi,16
    add ebp,16
    inc dl
    cmp dl,20h
    jne .loopc
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw2
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw2
    ret

NEWSYM draw8x816t                                                       ;00058A9D
    test byte[scaddtype],80h
    jnz near draw8x816toffset
    mov [temp],al
    mov [bshifter],ah
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .domosaic
.nomosaic
    cmp byte[winon],0
    je .domosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
.domosaic
; tile value : bit 15 = flipy, bit 14 = flipx, bit 13 = priority value
;              bit 10-12 = palette, 0-9=tile#
    mov [temptile],edx
    mov ebp,transpbuf+32
    sub ebp,eax
    sub ebp,eax

    test byte[scaddtype],40h
    jz near draw8x8fulladd

    mov byte[tileleft16b],33
    mov byte[drawn],0
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    add edi,2
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    and eax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl eax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    ; Start loop
;    drawtilegrp draw8x816ta
    cmp dword[ebx],0
    je near .loopda
    xor ecx,ecx
    draw8x816ta 0, 0
    draw8x816ta 1, 2
    draw8x816ta 2, 4
    draw8x816ta 3, 6
.loopda
    cmp dword[ebx+4],0
    je near .loopda2
    draw8x816ta 4, 8
    draw8x816ta 5, 10
    draw8x816ta 6, 12
    draw8x816ta 7, 14
.loopda2
.hprior
    add esi,16
    add ebp,16
    inc dl
    cmp dl,20h
    jne .loopc2
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
;    drawtilegrpf draw8x816ta
    cmp dword[ebx+4],0
    je near .rloopa
    draw8x816ta3 7, 0
    draw8x816ta3 6, 2
    draw8x816ta3 5, 4
    draw8x816ta3 4, 6
.rloopa
    cmp dword[ebx+4],0
    je near .rloopa2
    draw8x816ta3 3, 8
    draw8x816ta3 2, 10
    draw8x816ta3 1, 12
    draw8x816ta3 0, 14
.rloopa2
    add esi,16
    add ebp,16
    inc dl
    cmp dl,20h
    jne .loopc
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw2
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw2
    ret

NEWSYM draw8x8fulladd                                                   ;00059067
    mov byte[tileleft16b],33
    mov byte[drawn],0
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    add edi,2
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    and eax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl eax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    ; Start loop
    drawtilegrpfull draw8x816tb
.hprior
    add esi,16
    add ebp,16
    inc dl
    cmp dl,20h
    jne .loopc2
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    drawtilegrpfullf draw8x816tb
    add esi,16
    add ebp,16
    inc dl
    cmp dl,20h
    jne .loopc
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw2
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw2
    ret

;*******************************************************
; Processes & Draws 8x8 tiles, offset mode
;*******************************************************


NEWSYM draw8x816toffset                                                 ;0005947E
    mov [temp],al
    mov [bshifter],ah
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .noclear
.nomosaic
    cmp byte[winon],0
    je .noclear
.domosaic
; tile value : bit 15 = flipy, bit 14 = flipx, bit 13 = priority value
;              bit 10-12 = palette, 0-9=tile#
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
.noclear
    mov [temptile],edx
    mov ebp,transpbuf+32
    sub ebp,eax
    sub ebp,eax
    test byte[scaddtype],40h
    jz near draw8x8fulladdoffset
    jmp draw8x8fulladdoffset
    mov byte[tileleft16b],33
    mov byte[drawn],0
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    add edi,2
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    and eax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl eax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    ; Start loop
    drawtilegrp draw8x816ta2
.hprior
    add esi,16
    add ebp,16
    inc dl
    cmp dl,32
    jne .sametile
    mov edi,[temptile]
.sametile
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    drawtilegrpf draw8x816ta2
    add esi,16
    add ebp,16
    inc dl
    cmp dl,32
    jne .sametile2
    mov edi,[temptile]
.sametile2
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw2
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw2
    ret

NEWSYM draw8x8fulladdoffset                                             ;0005997F
    mov byte[tileleft16b],33
    mov byte[drawn],0
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    add edi,2
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    and eax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl eax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    ; Start loop
;    drawtilegrpfull draw8x816tb
    cmp dword[ebx],0
    je near .loopda
    draw8x816tc 0, 0
    draw8x816tc 1, 2
    draw8x816tc 2, 4
    draw8x816tc 3, 6
.loopda
    cmp dword[ebx+4],0
    je near .loopda2
    draw8x816tc 4, 8
    draw8x816tc 5, 10
    draw8x816tc 6, 12
    draw8x816tc 7, 14
.loopda2
.hprior
    add esi,16
    add ebp,16
    inc dl
    cmp dl,32
    jne .sametile
    mov edi,[temptile]
.sametile
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    drawtilegrpfullf draw8x816tc
    add esi,16
    add ebp,16
    inc dl
    cmp dl,32
    jne .sametile2
    mov edi,[temptile]
.sametile2
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw2
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw2
    ret

;*******************************************************
; Processes & Draws 16x16 tiles in main and sub screen
;*******************************************************
NEWSYM draw16x1616bt                                                    ;00059DF6
    mov byte[drawn],0
    mov [temp],eax
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    xor ebx,ebx
    mov bl,[curypos]
    mov [temptile],edx
    ; set up y adders
    test byte[a16x16yinc],01h
    jz .noincrc
    mov word[yadd],16
    mov word[yflipadd],0
    jmp .yesincrc
.noincrc
    mov word[yadd],0
    mov word[yflipadd],16
.yesincrc
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .noclear
.nomosaic
    cmp byte[winon],0
    je .noclear
.domosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
.noclear
    mov ebp,transpbuf+32
    sub ebp,eax
    sub ebp,eax
    mov byte[tileleft16b],33
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    xor byte[a16x16xinc],1

    test dh,40h
    jnz .noxflip
    test byte[a16x16xinc],01h
    jnz .noincr2
    inc ax
    add edi,2
.noincr2
    jmp .yesxflip
.noxflip
    test byte[a16x16xinc],01h
    jnz .noincr
    add edi,2
    jmp .yesincr
.noincr
    inc ax
.yesincr
.yesxflip
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]

    test dh,80h
    jnz .noyflip
    add ax,word[yadd]
    jmp .yesyflip
.noyflip
    add ax,word[yflipadd]
.yesyflip

    and ax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl ax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax

    ; Start loop
;    drawtilegrp draw8x816bta
    draw8x816bta 0, 0
    draw8x816bta 1, 2
    draw8x816bta 2, 4
    draw8x816bta 3, 6
    draw8x816bta 4, 8
    draw8x816bta 5, 10
    draw8x816bta 6, 12
    draw8x816bta 7, 14
.hprior
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb2
    inc dl
.noincrb2
    cmp dl,20h
    jne .loopc2
    xor dl,dl
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
;    drawtilegrpf draw8x816bta
    draw8x816bta 7, 0
    draw8x816bta 6, 2
    draw8x816bta 5, 4
    draw8x816bta 4, 6
    draw8x816bta 3, 8
    draw8x816bta 2, 10
    draw8x816bta 1, 12
    draw8x816bta 0, 14
.skiploop2b
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb
    inc dl
.noincrb
    cmp dl,20h
    jne .loopc
    xor dl,dl
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

;*******************************************************
; Processes & Draws 16x16 tiles in 2, 4, & 8 bit mode
;*******************************************************

NEWSYM draw16x1616t                                                     ;0005A1B5
    test byte[scaddtype],80h
    jnz near draw16x1616ts
    mov byte[drawn],0
    mov [temp],eax
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    xor ebx,ebx
    mov bl,[curypos]
    mov [temptile],edx
    ; set up y adders
    test byte[a16x16yinc],01h
    jz .noincrc
    mov word[yadd],16
    mov word[yflipadd],0
    jmp .yesincrc
.noincrc
    mov word[yadd],0
    mov word[yflipadd],16
.yesincrc
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .noclear
.nomosaic
    cmp byte[winon],0
    je .noclear
.domosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
.noclear
    mov ebp,transpbuf+32
    sub ebp,eax
    sub ebp,eax
    test byte[scaddtype],40h
    jz near draw16x16fulladd
    mov byte[tileleft16b],33
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    xor byte[a16x16xinc],1
    test dh,40h
    jnz .noxflip
    test byte[a16x16xinc],01h
    jnz .noincr2
    inc ax
    add edi,2
.noincr2
    jmp .yesxflip
.noxflip
    test byte[a16x16xinc],01h
    jnz .noincr
    add edi,2
    jmp .yesincr
.noincr
    inc ax
.yesincr
.yesxflip
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    test dh,80h
    jnz .noyflip
    add ax,word[yadd]
    jmp .yesyflip
.noyflip
    add ax,word[yflipadd]
.yesyflip
    and ax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl ax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    ; Start loop
;    drawtilegrp draw8x816ta
    draw8x816ta 0, 0
    draw8x816ta 1, 2
    draw8x816ta 2, 4
    draw8x816ta 3, 6
    draw8x816ta 4, 8
    draw8x816ta 5, 10
    draw8x816ta 6, 12
    draw8x816ta 7, 14
.hprior
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb2
    inc dl
.noincrb2
    cmp dl,20h
    jne .loopc2
    xor dl,dl
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    ; Start loop
;    drawtilegrpf draw8x816ta
    draw8x816ta 7, 0
    draw8x816ta 6, 2
    draw8x816ta 5, 4
    draw8x816ta 4, 6
    draw8x816ta 3, 8
    draw8x816ta 2, 10
    draw8x816ta 1, 12
    draw8x816ta 0, 14
.skiploop2b
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb
    inc dl
.noincrb
    cmp dl,20h
    jne .loopc
    xor dl,dl
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

NEWSYM yadd,   dw 0                                                     ;0005A7D0
NEWSYM yflipadd,  dw 0                                                  ;0005A7D2

NEWSYM draw16x16fulladd
    mov byte[tileleft16b],33
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    xor byte[a16x16xinc],1

    test dh,40h
    jnz .noxflip
    test byte[a16x16xinc],01h
    jnz .noincr2
    inc ax
    add edi,2
.noincr2
    jmp .yesxflip
.noxflip
    test byte[a16x16xinc],01h
    jnz .noincr
    add edi,2
    jmp .yesincr
.noincr
    inc ax
.yesincr
.yesxflip
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]

    test dh,80h
    jnz .noyflip
    add ax,word[yadd]
    jmp .yesyflip
.noyflip
    add ax,word[yflipadd]
.yesyflip

    and ax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl ax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    ; Start loop
;    drawtilegrpfull draw8x816tb
    draw8x816tb 0, 0
    draw8x816tb 1, 2
    draw8x816tb 2, 4
    draw8x816tb 3, 6
    draw8x816tb 4, 8
    draw8x816tb 5, 10
    draw8x816tb 6, 12
    draw8x816tb 7, 14
.hprior
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb2
    inc dl
.noincrb2
    cmp dl,20h
    jne .loopc2
    xor dl,dl
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
;    drawtilegrpfullf draw8x816tb
    draw8x816tb 7, 0
    draw8x816tb 6, 2
    draw8x816tb 5, 4
    draw8x816tb 4, 6
    draw8x816tb 3, 8
    draw8x816tb 2, 10
    draw8x816tb 1, 12
    draw8x816tb 0, 14
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb
    inc dl
.noincrb
    cmp dl,20h
    jne .loopc
    xor dl,dl
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

;NEWSYM draw16x1616ts
;    mov byte[tileleft16b],33
;    mov dl,[temp]
;.loopa
;    mov ax,[edi]
;    mov dh,ah
;    xor byte[a16x16xinc],1
;
;    test dh,40h
;    jnz .noxflip
;    test byte[a16x16xinc],01h
;    jnz .noincr2
;    inc ax
;    add edi,2
;.noincr2
;    jmp .yesxflip
;.noxflip
;    test byte[a16x16xinc],01h
;    jnz .noincr
;    add edi,2
;    jmp .yesincr
;.noincr
;    inc ax
;.yesincr
;.yesxflip
;    xor dh,[curbgpr]
;    push edi
;    test dh,20h
;    jnz near .hprior
;    inc byte[drawn]
;
;    test dh,80h
;    jnz .noyflip
;    add ax,word[yadd]
;    jmp .yesyflip
;.noyflip
;    add ax,word[yflipadd]
;.yesyflip
;
;    and ax,03FFh                ; filter out tile #
;    mov edi,[tempcach]
;    shl ax,6
;    add edi,eax
;    cmp edi,[bgofwptr]
;    jb .noclip
;    sub edi,[bgsubby]
;.noclip
;    test dh,80h
;    jz .normadd
;    add edi,[yrevadder]
;    jmp .skipadd
;.normadd
;    add edi,[yadder]
;.skipadd
;    test dh,40h
;    jnz near .rloop
;    mov cl,[bshifter]
;    and dh,1Ch
;    mov eax,[ebx]
;    shl dh,cl                    ; process palette # (bits 10-12)
;    add dh,[bgcoloradder]
;    xor eax,eax
;    xor ecx,ecx
;    ; Start loop
;    drawtilegrpfull draw8x816tc
;.hprior
;    pop edi
;    add esi,16
;    add ebp,16
;    test byte[a16x16xinc],01h
;    jnz .noincrb2
;    inc dl
;.noincrb2
;    cmp dl,20h
;    jne .loopc2
;    xor dl,dl
;    mov edi,[temptile]
;.loopc2
;    dec byte[tileleft16b]
;    jnz near .loopa
;    mov dh,byte[curmosaicsz]
;    cmp dh,1
;    jne near domosaic16b
;    cmp byte[winon],0
;    jne near dowindow16b
;    ret
;
;    ; reversed loop
;.rloop
;    mov cl,[bshifter]
;    and dh,1Ch
;    mov eax,[ebx]
;    shl dh,cl                    ; process palette # (bits 10-12)
;    add dh,[bgcoloradder]
;    xor eax,eax
;    xor ecx,ecx
;    drawtilegrpfullf draw8x816tc
;    pop edi
;    add esi,16
;    add ebp,16
;    test byte[a16x16xinc],01h
;    jnz .noincrb
;    inc dl
;.noincrb
;    cmp dl,20h
;    jne .loopc
;    xor dl,dl
;    mov edi,[temptile]
;.loopc
;    dec byte[tileleft16b]
;    jnz near .loopa
;    mov dh,byte[curmosaicsz]
;    cmp dh,1
;    jne near domosaic16b
;    cmp byte[winon],0
;    jne near dowindow16b
;    ret

NEWSYM draw16x1616ts                                                    ;0005AC02
    mov byte[drawn],0
    mov [temp],eax
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    xor ebx,ebx
    mov bl,[curypos]
    mov [temptile],edx
    ; set up y adders
    test byte[a16x16yinc],01h
    jz .noincrc
    mov word[yadd],16
    mov word[yflipadd],0
    jmp .yesincrc
.noincrc
    mov word[yadd],0
    mov word[yflipadd],16
.yesincrc
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .noclear
.nomosaic
    cmp byte[winon],0
    je .noclear
.domosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
.noclear
    mov ebp,transpbuf+32
    sub ebp,eax
    sub ebp,eax
    mov byte[tileleft16b],33
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    xor byte[a16x16xinc],1
    test dh,40h
    jnz .noxflip
    test byte[a16x16xinc],01h
    jnz .noincr2
    inc ax
    add edi,2
.noincr2
    jmp .yesxflip
.noxflip
    test byte[a16x16xinc],01h
    jnz .noincr
    add edi,2
    jmp .yesincr
.noincr
    inc ax
.yesincr
.yesxflip
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    test dh,80h
    jnz .noyflip
    add ax,word[yadd]
    jmp .yesyflip
.noyflip
    add ax,word[yflipadd]
.yesyflip
    and ax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl ax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    ; Start loop
    draw8x816tc 0, 0
    draw8x816tc 1, 2
    draw8x816tc 2, 4
    draw8x816tc 3, 6
    draw8x816tc 4, 8
    draw8x816tc 5, 10
    draw8x816tc 6, 12
    draw8x816tc 7, 14
.hprior
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb2
    inc dl
.noincrb2
    cmp dl,20h
    jne .loopc2
    xor dl,dl
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    xor ecx,ecx
    ; Start loop
    draw8x816tc 7, 0
    draw8x816tc 6, 2
    draw8x816tc 5, 4
    draw8x816tc 4, 6
    draw8x816tc 3, 8
    draw8x816tc 2, 10
    draw8x816tc 1, 12
    draw8x816tc 0, 14
.skiploop2b
    add esi,16
    add ebp,16
    test byte[a16x16xinc],01h
    jnz .noincrb
    inc dl
.noincrb
    cmp dl,20h
    jne .loopc
    xor dl,dl
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret
