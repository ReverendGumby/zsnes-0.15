;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM curblank,selcA000,vidbuffer,resolutn,vesa2selec,vcache4b,vram
EXTSYM objptr
EXTSYM GUIOn,HalfTrans
EXTSYM ClearScreen
EXTSYM Mode7HiRes,mosenng,mosszng,intrlng,mode7hr ;,VESAAddr
EXTSYM GUICPC, newgfx16b
EXTSYM vesa2_clbitng,vesa2_clbitng2,vesa2_clbitng3
EXTSYM granadd
EXTSYM SpecialLine
EXTSYM vidbufferofsb
EXTSYM Super2xSaI
EXTSYM HalfTransB,HalfTransC


;*******************************************************
; CopyModeX         Copies buffer into unchained 320x240
;*******************************************************

NEWSYM copymodex                                                        ;00052AF5
    cmp byte[curblank],40h
    jne .startcopy
    ret
.startcopy
    ; select plane 1
    mov dx,03C4h
    mov ax,0102h        ; set as plane 1
    out dx,ax
    push es
    mov ax,[selcA000]
    mov es,eax

    mov esi,[vidbuffer]
    mov edi,8                  ; Draw @ Y from 9 to 247
    cmp word[resolutn],224
    jne .res239
    mov edi,8*80+8
.res239
     add esi,16+256+32
    cmp byte[whichpage],0
    jne .pageb
    add edi,19200
.pageb
    mov [.startesi],esi
    mov [.startedi],edi
    mov dl,[resolutn]
.loopa
    mov ecx,64
.loopb
    mov al,[esi+0]
    mov [es:edi],al
    add esi,4
    inc edi
    dec ecx
    jnz .loopb
    add esi,32
    add edi,16
    dec dl
    jnz .loopa
    mov dx,03C4h
    mov ax,0202h        ; set as plane 2
    out dx,ax
    mov esi,[.startesi]
    inc esi
    mov edi,[.startedi]
    mov dl,[resolutn]
.loopa2
    mov ecx,64
.loopb2
    mov al,[esi+0]
    mov [es:edi],al
    add esi,4
    inc edi
    dec ecx
    jnz .loopb2
    add esi,32
    add edi,16
    dec dl
    jnz .loopa2
    mov dx,03C4h
    mov ax,0402h        ; set as plane 3
    out dx,ax
    mov esi,[.startesi]
    add esi,2
    mov edi,[.startedi]
    mov dl,[resolutn]
.loopa3
    mov ecx,64
.loopb3
    mov al,[esi+0]
    mov [es:edi],al
    add esi,4
    inc edi
    dec ecx
    jnz .loopb3
    add esi,32
    add edi,16
    dec dl
    jnz .loopa3
    mov dx,03C4h
    mov ax,0802h        ; set as plane 4
    out dx,ax
    mov esi,[.startesi]
    add esi,3
    mov edi,[.startedi]
    mov dl,[resolutn]
.loopa4 
    mov ecx,64
.loopb4
    mov al,[esi+0]
    mov [es:edi],al
    add esi,4
    inc edi
    dec ecx
    jnz .loopb4
    add esi,32
    add edi,16
    dec dl
    jnz .loopa4
    pop es
    cmp byte[whichpage],0
    jne .setpageb
    mov dx,03D4h
    mov al,0Ch
    out dx,al
    inc dx
    mov al,75
    out dx,al
    dec dx
    mov al,0Dh
    out dx,al
    inc dx
    xor al,al
    out dx,al
    mov byte[whichpage],1
    ret
.setpageb
    mov dx,03D4h
    mov al,0Ch
    out dx,al
    inc dx
    xor al,al
    out dx,al
    dec dx
    mov al,0Dh
    out dx,al
    inc dx
    xor al,al
    out dx,al
    mov byte[whichpage],0
    ret
.startesi dd 0
.startedi dd 0

NEWSYM cscopymodex
    ; select all planes
    mov dx,03C4h
    mov ax,0F02h
    out dx,ax
    push es
    mov ax,[selcA000]
    mov es,eax
    xor edi,edi
    mov ecx,65536/4
    xor eax,eax
    rep stosd
    pop es
    ret

NEWSYM whichpage, db 0          ; active page and visual page locations

;*******************************************************
; CopyModeQ           Copies buffer into chained 256x256
;*******************************************************

NEWSYM copymodeq                                                        ;00052CB4
    cmp byte[curblank],40h
    jne .startcopy
    ret
.startcopy
    push es
    mov ax,[selcA000]
    mov es,eax
    mov esi,[vidbuffer]
    mov edi,9*256                ; Draw @ Y from 9 to 247
    cmp word[resolutn],224
    jne .res239
    mov edi,17*256
.res239
    add esi,16+256+32
    xor eax,eax
    mov dl,[resolutn]
.loopa
    mov ecx,64
    rep movsd
    add esi,32
    dec dl
    jnz .loopa
    pop es
    ret
.loopb
    mov al,0
    mov dx,03C8h
    out dx,al
    inc dx
    out dx,al
    out dx,al
    out dx,al
    push es
    mov ax,[selcA000]
    mov es,eax
    xor eax,eax
    mov ecx,4000h
    xor edi,edi
    rep stosd
    pop es
    ret

;*******************************************************
; Copy VESA2 320x240x8b  Copies buffer to 320x240x8bVBE2
;*******************************************************
;     Input:    AX   = 4F07h   VBE Set/Get Display Start Control
;               BH   = 00h          Reserved and must be 00h
;               BL   = 00h          Set Display Start
;                    = 01h          Get Display Start
;                    = 80h          Set Display Start during Vertical
;     Retrace
;               CX   =         First Displayed Pixel In Scan Line
;                              (Set Display Start only)
;               DX   =         First Displayed Scan Line (Set Display Start
;     only)

NEWSYM copyvesa2320x240x8b                                              ;00052D25
    cmp byte[curblank],40h
    jne .startcopy
    ret
.startcopy
    push es
    mov ax,[vesa2selec]
    mov es,eax
    mov esi,[vidbuffer]
    mov edi,32             ; Draw @ Y from 9 to 247
    cmp word[resolutn],224
    jne .res239
    mov edi,8*320+32
.res239
    add esi,16+256+32
    xor eax,eax
    mov dl,[resolutn]
.loopa
    mov ecx,64
    rep movsd
    add esi,32
    add edi,64
    dec dl
    jnz .loopa
    pop es
    ret
.loopb
    mov al,0
    mov dx,03C8h
    out dx,al
    inc dx
    out dx,al
    out dx,al
    out dx,al
    push es
    mov ax,[vesa2selec]
    mov es,eax
    xor eax,eax
    mov ecx,4B00h
    xor edi,edi
    rep stosd
    pop es
    ret

;*******************************************************
; Copy VESA2 320x240x16b Copies buffer to 320x240x16bVB2
;*******************************************************

NEWSYM copyvesa2320x240x16b                                             ;00052D9C
    cmp byte[curblank],40h
    jne .startcopy
    ret
.startcopy
    push es
    mov ax,[vesa2selec]
    mov es,eax
    mov esi,[vidbuffer]
    mov edi,32*2           ; Draw @ Y from 9 to 247
    cmp word[resolutn],224
    jne .res239
    mov edi,8*320*2+32*2
.res239
    add esi,16*2+256*2+32*2
    xor eax,eax
    mov dl,[resolutn]
.loopa
    mov ecx,128
    rep movsd
    add esi,64
    add edi,128
    dec dl
    jnz .loopa
    pop es
    ret
.loopb
    mov al,0
    mov dx,03C8h
    out dx,al
    inc dx
    out dx,al
    out dx,al
    out dx,al
    push es
    mov ax,[vesa2selec]
    mov es,eax
    xor eax,eax
    mov ecx,9600h
    xor edi,edi
    rep stosd
    pop es
    ret

; Temporary

NEWSYM tempcopy                                                         ;00052E13
    ; cache all sprites
    call allcache
    ; copy [vcache4b]+bg1objptr*2 into
    xor ebx,ebx
    mov bx,[objptr]
    shl ebx,1
    add ebx,[vcache4b]
    mov edi,0
    mov esi,0
    mov dh,25
.loopd
    mov dl,32
.loopc
    mov ch,8
.loopb
    mov cl,8
.loopa
    mov al,[ebx]
    mov [es:edi],al
    inc edi
    inc ebx
    dec cl
    jnz .loopa
    add edi,248
    dec ch
    jnz .loopb
    add esi,8
    mov edi,esi
    dec dl
    jnz .loopc
    add esi,288*8-32*16
    mov edi,esi
    dec dh
    jnz .loopd
    pop es
    ret

NEWSYM allcache                                                         ;00052E6A
    push eax
    push ebx
    push ecx
    mov esi,[vram]
    mov edi,[vcache4b]
    mov cx,4096
.nextcache
    ; convert from [esi] to [edi]
    ; use ah = color 0, bl = color 1, bh = color 2, cl = color 3
    ; ch = color 4, dl = color 5, dh = color 6, .a = color 7
    push edi
    push esi
    push ecx

    mov byte[.rowleft],8
.donext
    xor ah,ah
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx
    mov byte[.a],0
    mov al,[esi]                ; bitplane 0
    cmp al,0
    je .skipconva
    test al,01h
    jz .skipa0
    or ah,01h
.skipa0
    test al,02h
    jz .skipa1
    or bl,01h
.skipa1
    test al,04h
    jz .skipa2
    or bh,01h
.skipa2
    test al,08h
    jz .skipa3
    or cl,01h
.skipa3
    test al,10h
    jz .skipa4
    or ch,01h
.skipa4
    test al,20h
    jz .skipa5
    or dl,01h
.skipa5
    test al,40h
    jz .skipa6
    or dh,01h
.skipa6
    test al,80h
    jz .skipa7
    or byte[.a],01h
.skipa7
.skipconva

    mov al,[esi+1]                ; bitplane 1
    cmp al,0
    je .skipconvb
    test al,01h
    jz .skipb0
    or ah,02h
.skipb0
    test al,02h
    jz .skipb1
    or bl,02h
.skipb1
    test al,04h
    jz .skipb2
    or bh,02h
.skipb2
    test al,08h
    jz .skipb3
    or cl,02h
.skipb3
    test al,10h
    jz .skipb4
    or ch,02h
.skipb4
    test al,20h
    jz .skipb5
    or dl,02h
.skipb5
    test al,40h
    jz .skipb6
    or dh,02h
.skipb6
    test al,80h
    jz .skipb7
    or byte[.a],02h
.skipb7
.skipconvb

    mov al,[esi+16]                ; bitplane 2
    cmp al,0
    je .skipconvc
    test al,01h
    jz .skipc0
    or ah,04h
.skipc0
    test al,02h
    jz .skipc1
    or bl,04h
.skipc1
    test al,04h
    jz .skipc2
    or bh,04h
.skipc2
    test al,08h
    jz .skipc3
    or cl,04h
.skipc3
    test al,10h
    jz .skipc4
    or ch,04h
.skipc4
    test al,20h
    jz .skipc5
    or dl,04h
.skipc5
    test al,40h
    jz .skipc6
    or dh,04h
.skipc6
    test al,80h
    jz .skipc7
    or byte[.a],04h
.skipc7
.skipconvc

    mov al,[esi+17]                ; bitplane 3
    cmp al,0
    je .skipconvd
    test al,01h
    jz .skipd0
    or ah,08h
.skipd0
    test al,02h
    jz .skipd1
    or bl,08h
.skipd1
    test al,04h
    jz .skipd2
    or bh,08h
.skipd2
    test al,08h
    jz .skipd3
    or cl,08h
.skipd3
    test al,10h
    jz .skipd4
    or ch,08h
.skipd4
    test al,20h
    jz .skipd5
    or dl,08h
.skipd5
    test al,40h
    jz .skipd6
    or dh,08h
.skipd6
    test al,80h
    jz .skipd7
    or byte[.a],08h
.skipd7
.skipconvd

    ; move all bytes into [edi]
    mov [edi+7],ah
    mov [edi+6],bl
    mov [edi+5],bh
    mov [edi+4],cl
    mov [edi+3],ch
    mov [edi+2],dl
    mov [edi+1],dh
    mov al,[.a]
    mov [edi],al
    add edi,8
    add esi,2
    dec byte[.rowleft]
    jnz near .donext

    pop ecx
    pop esi
    pop edi

    add esi,32
    add edi,64
    dec cx
    jnz near .nextcache
    pop ecx
    pop ebx
    pop eax
    ret

.nbg dw 0
.a   db 0
.rowleft db 0
