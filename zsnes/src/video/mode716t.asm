;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM coladdr,curmosaicsz,curvidoffset,domosaic16b,dowindow16b,mode7A,drawmode7dcolor
EXTSYM mode7B,mode7C,mode7D,mode7X0,mode7Y0,mode7set,mode7tab,DoTransp
EXTSYM pal16b,pal16bcl,pal16bxcl,scaddtype,scrnon,transpbuf,drawmode716b
EXTSYM vesa2_clbit,vram,vrama,winon,xtravbuf,winptrref,scaddset
EXTSYM fulladdtab

%include "video/mode7.mac"





;*******************************************************
; Processes & Draws Mode 7
;*******************************************************

%macro Mode716tProcess 4
    test byte[mode7set],80h
    jne near %%norep2

    ; get tile data offset into edi
%%nexttile
    mov bx,[.mode7ypos+1]
    mov ax,[.mode7xpos+1]
    mov cl,bl
    mov dl,al
    shr bx,3
    and cl,07h
    shl bx,8
    and dl,07h
    shl cl,4
    shl dl,1
    and bh,07Fh
    inc dl
    shr ax,3
    add dl,cl
    shl al,1
    xor ecx,ecx
    mov bl,al
    mov cl,[ebx+edi]
    mov eax,[.mode7xadder]
    shl ecx,7
    add [.mode7xpos],eax
    add ecx,edx
    mov eax,[.mode7yadder]
    mov dl,[ecx+edi]
    sub [.mode7ypos],eax
    %1
    add esi,%4
    add ebp,%4
    dec byte[.temp]
    jnz near %%nexttile

    xor eax,eax
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near %2
    cmp byte[winon],0
    jne near %3
    ret

;**********************************************************
; Mode 7, no repetition mode
;**********************************************************

%%norep2
    mov bx,[.mode7ypos+1]
    mov ax,[.mode7xpos+1]
%%nextvalb2
    cmp byte[.mode7ypos+2],3
    ja near %%offscr3
    cmp byte[.mode7xpos+2],3
    ja near %%offscr3
%%offscr2
    mov cl,bl
    mov dl,al
    shr bx,3
    and cl,07h
    shl bx,8
    and dl,07h
    shl cl,4
    shl dl,1
    and bh,07Fh
    inc dl
    shr ax,3
    add dl,cl
    shl al,1
    xor ecx,ecx
    mov bl,al
    mov cl,[ebx+edi]
    mov eax,[.mode7xadder]
    shl ecx,7
    add [.mode7xpos],eax
    add ecx,edx
    mov eax,[.mode7yadder]
    mov dl,[ecx+edi]
    sub [.mode7ypos],eax
    %1
    add esi,%4
    dec byte[.temp]
    jnz near %%norep2
    jmp %%finishmode7

%%offscr3
    mov eax,[.mode7xadder]
    add [.mode7xpos],eax
    mov eax,[.mode7yadder]
    sub [.mode7ypos],eax
    add esi,%4
    dec byte[.temp]
    jnz near %%norep2

%%finishmode7
    xor eax,eax
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near %2
    cmp byte[winon],0
    jne near %3
    ret
%endmacro

%macro mode7halfadd 0
    test dl,0FFh
    jz %%nodraw
    mov cx,[ebp]
    cmp cx,0
    jz %%noadd
    mov bx,[pal16bcl+edx*2]
    and ecx,[vesa2_clbit]
    add ebx,ecx
    shr ebx,1
    mov [esi],bx
    jmp %%nodraw
%%noadd
    mov bx,[pal16b+edx*2]
    mov [esi],bx
%%nodraw
%endmacro

%macro mode7fulladd 0
    test dl,0FFh
    jz %%nodraw
    mov cx,[ebp]
    mov bx,[pal16bcl+edx*2]
    and ecx,[vesa2_clbit]
    add ebx,ecx
    shr ebx,1
    mov bx,[fulladdtab+ebx*2]
    mov [esi],bx
%%nodraw
%endmacro

%macro mode7fullsub 0
    test dl,0FFh
    jz %%nodraw
    mov cx,[ebp]
    mov bx,[pal16bxcl+edx*2]
    and ecx,[vesa2_clbit]
    add ebx,ecx
    shr ebx,1
    mov bx,[fulladdtab+ebx*2]
    xor ebx,0FFFFh
    mov [esi],bx
%%nodraw
%endmacro

%macro mode7mainsub 0
    test dl,0FFh
    jz %%nodraw
    mov cx,[pal16b+edx*2]
    mov [ebp],cx
    mov [esi],cx
%%nodraw
%endmacro
    
%macro mode716theader 0
    Mode7Calculate

    ; esi = pointer to video buffer
    mov esi,[curvidoffset]       ; esi = [vidbuffer] + curypos * 288 + 16
    mov edi,[vram]

    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,64
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    jmp .skipclear
.nomosaic
    cmp byte[winon],0
    je .skipclear
    mov esi,xtravbuf+32
    mov ecx,64
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
.skipclear
    mov ebp,transpbuf+32
    mov byte[.temp],0

    ; esi = pointer to video buffer
    ; edi = pointer to vram
    ; [.mode7xadder] = dword value to add to x value (decimal between 7 & 8bit)
    ; [.mode7yadder] = dword value to add to y value (decimal between 7 & 8bit)
    ; [.mode7xpos]   = dword value of x position, decimal between 7 & 8bit
    ; [.mode7xpos+1] = word value of x position
    ; [.mode7ypos]   = dword value of y position, decimal between 7 & 8bit
    ; [.mode7ypos+1] = word value of y position
    xor ebx,ebx
    xor edx,edx
    xor ecx,ecx
%endmacro
    
%macro mode716tfooter 0
.temp        db 0       ; for byte move left
.mode7xpos   dd 0       ; x position
             db 0       ; keep this blank!
.mode7ypos   dd 0       ; y position
             db 0       ; keep this blank!
.mode7xadder dd 0       ; number to add for x
             db 0       ; keep this blank!
.mode7yadder dd 0       ; number to add for y
             db 0       ; keep this blank!
.cxloc       dw 0       ; cx location
.cyloc       dw 0       ; cy location
%endmacro

%macro mode716tmacro 0
    test byte[scaddtype],80h
    jne near .fullsub
    test byte[scaddtype],40h
    je near .fulladd

    ;; All this TBD
    Mode716tProcess mode7halfadd, domosaic16b, dowindow16b, 2
.fulladd
    Mode716tProcess mode7fulladd, domosaic16b, dowindow16b, 2
.fullsub
    Mode716tProcess mode7fullsub, domosaic16b, dowindow16b, 2
%endmacro

%macro mode716tbmacro 0
    ;; All this TBD
    Mode716tProcess mode7mainsub, domosaic16b, dowindow16b, 2
%endmacro

;*******************************************************
; Processes & Draws Mode 7
;*******************************************************
NEWSYM drawmode716t                                                     ;0005B7FC
    mode716theader
    mode716tmacro
    mode716tfooter

;**********************************************************
; Mode 7, main & sub mode
;**********************************************************

NEWSYM drawmode716tb                                                    ;0005BEDE
    mode716theader
    mode716tbmacro
    mode716tfooter

