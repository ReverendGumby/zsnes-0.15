;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM curmosaicsz,curvidoffset,domosaic16b,dowindow16b,winptrref,scaddset
EXTSYM mode7A,mode7B,mode7C,mode7D,mode7X0,mode7Y0,mode7set
EXTSYM pal16b,vram,vrama,winon,mode7tab,xtravbuf,drawmode7dcolor

%include "video/mode7.mac"

;*******************************************************
; Processes & Draws Mode 7
;*******************************************************

NEWSYM drawmode716b                                                 ;0005B509

    Mode7Calculate

    ; esi = pointer to video buffer
    mov esi,[curvidoffset]       ; esi = [vidbuffer] + curypos * 288 + 16
    mov edi,[vram]

    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,64
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    jmp .skipclear
.nomosaic
    cmp byte[winon],0
    je .skipclear
    mov esi,xtravbuf+32
    mov ecx,64
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
.skipclear
    mov byte[.temp],0

    ; esi = pointer to video buffer
    ; edi = pointer to vram
    ; [.mode7xadder] = dword value to add to x value (decimal between 7 & 8bit)
    ; [.mode7yadder] = dword value to add to y value (decimal between 7 & 8bit)
    ; [.mode7xpos]   = dword value of x position, decimal between 7 & 8bit
    ; [.mode7xpos+1] = word value of x position
    ; [.mode7ypos]   = dword value of y position, decimal between 7 & 8bit
    ; [.mode7ypos+1] = word value of y position
    xor ebx,ebx
    xor edx,edx
    xor ecx,ecx

    Mode7ProcessB Mode7Normal, domosaic16b, dowindow16b, 2

.temp        db 0       ; for byte move left
.mode7xpos   dd 0       ; x position
             db 0       ; keep this blank!
.mode7ypos   dd 0       ; y position
             db 0       ; keep this blank!
.mode7xadder dd 0       ; number to add for x
             db 0       ; keep this blank!
.mode7yadder dd 0       ; number to add for y
             db 0       ; keep this blank!
.cxloc       dw 0       ; cx location
.cyloc       dw 0       ; cy location
