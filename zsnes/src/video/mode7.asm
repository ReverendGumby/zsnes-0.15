;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM mode7tab,nglogicval,winlogicaval
EXTSYM curmosaicsz,curvidoffset,domosaic,dowindow,mode7A,mode7B
EXTSYM mode7C,mode7D,mode7X0,mode7Y0,mode7set,vram,vrama,winon,xtravbuf
EXTSYM winbg1enval, BuildWindow, domosaicng
EXTSYM pesimpng
EXTSYM mode7hr
EXTSYM BGMA, mode7ab, mode7cd, BG1SYl, BG1SXl, mosenng, mosszng

%include "video/mode7.mac"

;*******************************************************
; Processes & Draws Mode 7
;*******************************************************

NEWSYM drawmode7                                                        ;0005B165

    Mode7Calculate

    ; esi = pointer to video buffer
    mov esi,[curvidoffset]       ; esi = [vidbuffer] + curypos * 288 + 16
    mov edi,[vram]

    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+16
    mov ecx,64
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+16
    jmp .skipclear
.nomosaic
    cmp byte[winon],0
    je .skipclear
    mov esi,xtravbuf+16
    mov ecx,64
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+16
.skipclear
    mov byte[.temp],0

    ; esi = pointer to video buffer
    ; edi = pointer to vram
    ; [.mode7xadder] = dword value to add to x value (decimal between 7 & 8bit)
    ; [.mode7yadder] = dword value to add to y value (decimal between 7 & 8bit)
    ; [.mode7xpos]   = dword value of x position, decimal between 7 & 8bit
    ; [.mode7xpos+1] = word value of x position
    ; [.mode7ypos]   = dword value of y position, decimal between 7 & 8bit
    ; [.mode7ypos+1] = word value of y position
    xor ebx,ebx
    xor edx,edx
    xor ecx,ecx

    Mode7Process Mode7Normal, domosaic, dowindow, 1

.temp        db 0       ; for byte move left                                ;0005B41E
.mode7xpos   dd 0       ; x position                                        ;0005B41F
             db 0       ; keep this blank!
.mode7ypos   dd 0       ; y position                                        ;0005B424
             db 0       ; keep this blank!
.mode7xadder dd 0       ; number to add for x                               ;0005B429
             db 0       ; keep this blank!
.mode7yadder dd 0       ; number to add for y                               ;0005B42E
             db 0       ; keep this blank!
.cxloc       dw 0       ; cx location                                       ;0005B433
.cyloc       dw 0       ; cy location                                       ;0005B435

    ;; Seems to be dead code...

    xor ebx,ebx
    xor ecx,ecx
    xor edi,edi
    mov bx,ax
    and ax,07h
    shr bx,3
    shl bx,8
    and bx,7FFFh
    add edi,ebx
    mov cx,dx
    and cx,07h
    shr dx,3
    shl dx,1
    and dx,0FFh
    shl al,4
    add edi,edx
    mov dx,ax
    mov esi,[curvidoffset]
    sub esi,ecx
    cmp byte[curmosaicsz],1
    jz .dead1
    mov esi,xtravbuf+16
    mov ecx,40h
.dead2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .dead2
    mov esi,xtravbuf+16
    sub esi,eax
    jmp .dead3
.dead1
    cmp byte[winon],0
    jz .dead3
    mov esi,xtravbuf+16
    mov ecx,40h
.dead4
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .dead4
    mov esi,xtravbuf+16
    sub esi,eax
.dead3
    mov ebx,edi
    mov edi,[vram]
    mov byte[.temp2],33
.dead6
    xor ah,ah
    mov al,[ebx+edi]
    shl ax,7
    inc ax
    add ax,dx
    mov ch,8
.dead5
    mov cl,[eax+edi]
    mov [esi],cl
    inc esi
    add eax,2
    dec ch
    jnz .dead5
    add bl,2
    dec byte[.temp2]
    jnz .dead6
    ret

.temp2       db 0
