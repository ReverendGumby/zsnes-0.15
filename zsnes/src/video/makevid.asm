;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM disableeffects,winl1,winl2,winbgdata,winr1,winr2,winspdata
EXTSYM winlogica
EXTSYM winenabm,winobjen
EXTSYM winlogicb 
EXTSYM scrndis,scrnon
EXTSYM bgmode,bgtilesz,winbg1en,winenabs
EXTSYM bg1objptr,bg1ptr,bg1ptrb,bg1ptrc,bg1ptrd,bg1scrolx,bg1scroly
EXTSYM cachebg1,curbgofs1,curcolbg1,vcache2b,vcache4b,vcache8b
EXTSYM vcache2ba,vcache4ba,vcache8ba
EXTSYM vidbuffer
EXTSYM bg3highst,cbitmode,colormodedef,colormodeofs,drawline16b
EXTSYM forceblnk,newengine8b,scaddset,spritetable
EXTSYM sprleftpr,vidbright,ForceNewGfxOff
EXTSYM curypos,drawmode7,mode7set,mosaicon,mosaicsz,sprleftpr1,sprleftpr2
EXTSYM sprleftpr3
EXTSYM sprclprio,sprpriodata,sprsingle
EXTSYM cachetile2b,cachetile4b,cachetile8b,vram,CacheCheckSkip
EXTSYM cachetile2b16x16,cachetile4b16x16,cachetile8b16x16
EXTSYM osm2dis,xtravbuf
EXTSYM bg3ptr,bg3scrolx,bg3scroly,cachesingle,colormoded2
EXTSYM tempoffset,vidmemch2,vidmemch4,vidmemch8
EXTSYM ofsmcptr,ofsmady,ofsmadx,yposng,yposngom,flipyposng,flipyposngom
EXTSYM ofsmtptr,ofsmmptr,ofsmcyps,bgtxadd,bg1ptrx,bg1ptry
EXTSYM curblank,ccud

%include "video/vidmacro.mac"





;drawspritesprio

;    mov cl,[bshifter]
;*******************************************************
; DrawLine                        Draws the current line
;*******************************************************
; use curypos+bg1scroly for y location and bg1scrolx for x location
; use bg1ptr(b,c,d) for the pointer to the tile number contents
; use bg1objptr for the pointer to the object tile contents

%macro decideonmode 0
    cmp bl,2
    je .yes4bit
    cmp bl,1
    je .yes2bit
    mov byte[bshifter],6
    mov edx,[vcache8b]
    jmp .skipbits
.yes4bit
    mov byte[bshifter],2
    mov edx,[vcache4b]
    shl eax,1
    jmp .skipbits
.yes2bit
    mov byte[bshifter],0
    shl eax,2
    mov edx,[vcache2b]
.skipbits
%endmacro

%macro procmode7 3
    xor eax,eax
    xor edx,edx
    mov ax,[curypos]
    test byte[mode7set],02h
    jz %%noflip
    mov ax,261
    sub ax,[curypos]
%%noflip
    mov byte[curmosaicsz],1
    test byte[mosaicon],%3
    jz %%nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je %%nomos
    inc bl
    mov [curmosaicsz],bl
    xor bh,bh
    div bx
    xor edx,edx
    mul bx
%%nomos
    add ax,%1
    mov dx,%2
    call drawmode7
%endmacro

; window logic data
NEWSYM windowdata, times 16 db 0                                        ;00052FF4
NEWSYM numwin, db 0                                                     ;00053004
;NEWSYM multiwin, db 0
;NEWSYM multiclip, db 0
;NEWSYM multitype, db 0

;    jmp .finishwin
NEWSYM procspritessub                                                   ;00053005
    test byte[scrndis],10h
    jnz .nosprites
    test byte[scrnon+1],10h
    jz .nosprites
    test byte[scrnon],10h
    jnz .nosprites
    xor ebx,ebx
    mov bl,[curypos]
    add ebx,[cursprloc]
    mov cl,[ebx]
    add dword[cursprloc],256
    cmp cl,0
    jz .nosprites
    call drawsprites
.nosprites
    ret

NEWSYM procspritesmain                                                  ;00053045
    test byte[scrndis],10h
    jnz .nosprites
    test byte[scrnon],10h
    jz .nosprites
    xor ebx,ebx
    mov bl,[curypos]
    add ebx,[cursprloc]
    mov cl,[ebx]
    add dword[cursprloc],256
    cmp cl,0
    jz .nosprites
    call drawsprites
.nosprites
    ret

NEWSYM curbgnum, db 0                                                   ;0005307C

NEWSYM drawbackgrndsub                                                  ;0005307D
    mov esi,[colormodeofs]
    mov bl,[ebp+esi+0]
    cmp bl,0
    je near .noback
    mov al,[curbgnum]
    test byte[scrnon+1],al
    jz near .noback
    test byte[scrnon],al
    jnz near .noback
    test byte[alreadydrawn],al
    jnz near .noback
    test byte[scrndis],al
    jnz near .noback
    mov byte[winon],0
    test byte[winenabs],al
    jz near .nobackwin
    procwindow [winbg1en+ebp]
.nobackwin
    mov bl,[curbgnum]
    mov byte[curmosaicsz],1
    test byte[mosaicon],bl
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
.nomos
    mov esi,[bg1vbufloc+ebp*4]
    mov edi,[bg1tdatloc+ebp*4]
    mov edx,[bg1tdabloc+ebp*4]
    mov ebx,[bg1cachloc+ebp*4]
    mov eax,[bg1xposloc+ebp*4]
    mov cl,[curbgnum]
    test byte[bgtilesz],cl
    jnz .16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x8
    cmp byte[drawn],33
    jne .notalldrawn
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawn
    jmp .noback
.16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x16
    cmp byte[drawn],33
    jne .notalldrawnb
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnb
.noback
    ret

NEWSYM drawbackgrndmain
    mov esi,[colormodeofs]
    mov bl,[ebp+esi+0]
    cmp bl,0
    je near .noback
    mov al,[curbgnum]
    test byte[scrnon],al
    jz near .noback
    test byte[alreadydrawn],al
    jnz near .noback
    test byte[scrndis],al
    jnz near .noback
    mov byte[winon],0
    test byte[winenabm],al
    jz near .nobackwin
    procwindow [winbg1en+ebp]
.nobackwin
    mov bl,[curbgnum]
    mov byte[curmosaicsz],1
    test byte[mosaicon],bl
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
.nomos
    mov esi,[bg1vbufloc+ebp*4]
    mov edi,[bg1tdatloc+ebp*4]
    mov edx,[bg1tdabloc+ebp*4]
    mov ebx,[bg1cachloc+ebp*4]
    mov eax,[bg1xposloc+ebp*4]
    mov cl,[curbgnum]
    test byte[bgtilesz],cl
    jnz .16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x8
    cmp byte[drawn],33
    jne .notalldrawn
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawn
    jmp .noback
.16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x16
    cmp byte[drawn],33
    jne .notalldrawnb
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnb
.noback
    ret

NEWSYM procbackgrnd                                                     ;00053401
    mov esi,[colormodeofs]
    mov bl,[ebp+esi+0]
    cmp bl,0
    je near .noback
    mov al,[curbgnum]
    mov ah,al
    test byte[scrndis],al
    jnz near .noback
    test [scrnon],ax
    jz near .noback
    push ebp
    shl ebp,6
    mov edi,cachebg1
    add edi,ebp
    pop ebp
    cmp bl,[curcolbg1+ebp]
    je .skipclearcache
    mov [curcolbg1+ebp],bl
    mov ax,[bg1ptr+ebp*2]
    mov [curbgofs1+ebp*2],ax
    call fillwithnothing
.skipclearcache
    xor eax,eax
    mov [curcolor],bl
    mov ax,[bg1objptr+ebp*2]
    decideonmode
    add edx,eax
    xor eax,eax
    mov [tempcach],edx
    xor edx,edx
    mov ax,[bg1objptr+ebp*2]
    mov [curtileptr],ax
    mov ax,[bg1ptr+ebp*2]
    mov [bgptr],ax
    cmp ax,[curbgofs1+ebp*2]
    je .skipclearcacheb
    mov [curbgofs1+ebp*2],ax
    call fillwithnothing
.skipclearcacheb
    mov ax,[bg1ptrb+ebp*2]
    mov [bgptrb],ax
    mov ax,[bg1ptrc+ebp*2]
    mov [bgptrc],ax
    mov ax,[bg1ptrd+ebp*2]
    mov [bgptrd],ax
    mov bl,[curbgnum]
    mov ax,[curypos]

    mov byte[curmosaicsz],1
    test byte[mosaicon],bl
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
    xor bh,bh
    div bx
    xor edx,edx
    mul bx
.nomos

    add ax,[bg1scroly+ebp*2]
    mov dx,[bg1scrolx+ebp*2]
    mov cl,[curbgnum]
    test byte[bgtilesz],cl
    jnz .16x16
    call proc8x8
    mov [bg1vbufloc+ebp*4],esi
    mov [bg1tdatloc+ebp*4],edi
    mov [bg1tdabloc+ebp*4],edx
    mov [bg1cachloc+ebp*4],ebx
    mov [bg1yaddval+ebp*4],ecx
    mov [bg1xposloc+ebp*4],eax
    ret
.16x16
    call proc16x16
    mov [bg1vbufloc+ebp*4],esi
    mov [bg1tdatloc+ebp*4],edi
    mov [bg1tdabloc+ebp*4],edx
    mov [bg1cachloc+ebp*4],ebx
    mov [bg1yaddval+ebp*4],ecx
    mov [bg1xposloc+ebp*4],eax
.noback
    ret

NEWSYM nextprimode, db 0
NEWSYM cursprloc,   dd 0
NEWSYM curcolor,    db 0                                                ;000535D7
NEWSYM curtileptr,  dw 0
; esi = pointer to video buffer
; edi = pointer to tile data
; ebx = cached memory
; al = current x position
NEWSYM bg1vbufloc,  dd 0
NEWSYM bg2vbufloc,  dd 0
NEWSYM bg3vbufloc,  dd 0
NEWSYM bg4vbufloc,  dd 0
NEWSYM bg1tdatloc,  dd 0
NEWSYM bg2tdatloc,  dd 0
NEWSYM bg3tdatloc,  dd 0
NEWSYM bg4tdatloc,  dd 0
NEWSYM bg1tdabloc,  dd 0
NEWSYM bg2tdabloc,  dd 0
NEWSYM bg3tdabloc,  dd 0
NEWSYM bg4tdabloc,  dd 0
NEWSYM bg1cachloc,  dd 0
NEWSYM bg2cachloc,  dd 0
NEWSYM bg3cachloc,  dd 0
NEWSYM bg4cachloc,  dd 0
NEWSYM bg1yaddval,  dd 0
NEWSYM bg2yaddval,  dd 0
NEWSYM bg3yaddval,  dd 0
NEWSYM bg4yaddval,  dd 0
NEWSYM bg1xposloc,  dd 0
NEWSYM bg2xposloc,  dd 0
NEWSYM bg3xposloc,  dd 0
NEWSYM bg4xposloc,  dd 0
NEWSYM alreadydrawn, db 0

NEWSYM fillwithnothing                                                  ;0005363B
    push edi
    xor eax,eax
    mov ecx,16
.loop
    mov [edi],eax
    add edi,4
    dec ecx
    jnz .loop
    pop edi
    ret

NEWSYM bg3draw, db 0
NEWSYM maxbr,   db 0                                                    ;00053651
NEWSYM blanker                                                          ;00053652
    ; calculate current video offset
    push ebx
    push esi
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,8
    shl ebx,5
    add esi,ebx
    add esi,16
    add esi,[vidbuffer]
    mov bl,64
.next
    mov dword[esi],0
    add esi,4
    dec bl
    jnz .next
    pop esi
    pop ebx
    ret

NEWSYM drawline                                                         ;00053688
    cmp byte[cbitmode],1
    je near drawline16b
    cmp byte[curblank],0
    jnz nodrawline
    mov al,[vidbright]
    cmp al,[maxbr]
    jbe .nochange
    mov [maxbr],al
.nochange
    cmp byte[forceblnk],0
    jne blanker
    mov byte[alreadydrawn],0
;    cmp byte[curypos],70
;    jne .no
;    push ebx
;    mov bl,[winlogica]
;    mov [bg1sx],bl
;    pop ebx
;.no
    push ebx
    xor ebx,ebx
    mov bl,[bgmode]
    shl bl,2
    add ebx,colormodedef
    mov [colormodeofs],ebx
    pop ebx

    cmp byte[bgmode],7
    je near processmode7

    push esi
    push edi
    push ebx
    push edx
    push ebp
    ; calculate current video offset
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,8
    shl ebx,5
    add esi,ebx
    add esi,16
    add esi,[vidbuffer]
    mov [curvidoffset],esi
    ; clear registers
    xor eax,eax
    xor ecx,ecx
    xor ebx,ebx
    ; get current sprite table
    mov bl,[curypos]
    shl ebx,9
    add ebx,spritetable
    mov [currentobjptr],ebx
    mov dword[cursprloc],sprleftpr
; process backgrounds
; do background 2
    mov byte[curbgnum],02h
    mov ebp,01h
    call procbackgrnd
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call procbackgrnd
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call procbackgrnd
; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call procbackgrnd

    test byte[scaddset],02h
    jz near .nosubsc
; draw backgrounds
    mov byte[curbgpr],0h
; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndsub
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndsub
;    mov ebp,0
;    call procspritessub
    mov byte[curbgpr],20h
;; do background 4
;    mov byte[curbgnum],08h
;    mov ebp,03h
;    call drawbackgrndsub
; do background 3
    cmp byte[bg3highst],1
    je .bg3nothigh
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndsub
.bg3nothigh
;    mov ebp,1
;    call procspritessub
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndsub
; do background 2
    mov byte[curbgpr],0h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndsub
    call procspritessub
    call procspritessub
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndsub
;    mov ebp,2
    call procspritessub
; do background 2
    mov byte[curbgpr],20h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndsub
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndsub
;    mov ebp,3
    call procspritessub
    cmp byte[bg3highst],1
    jne .bg3high
; do background 3
    mov byte[curbgpr],20h
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndsub
.bg3high

.nosubsc
;    mov al,[winenabm]
;    mov [cwinenabm],al
    mov byte[curbgpr],0h

; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndmain
;    mov ebp,0
;    call procspritesmain
    mov byte[curbgpr],20h
; do background 3
    cmp byte[bg3highst],1
    je .bg3nothighb
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain
.bg3nothighb
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndmain
;    mov ebp,1
;    call procspritesmain
; do background 2
    mov byte[curbgpr],0h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndmain
    call procspritesmain
    call procspritesmain
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndmain
;    mov ebp,2
    call procspritesmain
; do background 2
    mov byte[curbgpr],20h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndmain
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndmain
;    mov ebp,3
    call procspritesmain
    cmp byte[bg3highst],1
    jne .bg3highb
; do background 3
    mov byte[curbgpr],20h
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain
.bg3highb
    pop ebp
    pop edx
    pop ebx
    pop edi
    pop esi
    xor eax,eax
    xor ecx,ecx
NEWSYM nodrawline
    ret

NEWSYM tempbuffer, times 33 dd 0                                        ;00053958
NEWSYM currentobjptr, dd 0                                              ;000539DC
NEWSYM curmosaicsz,   db 0                                              ;000539E0


NEWSYM processmode7                                                     ;000539E1
    push esi
    push edi
    push ebx
    push edx
    push ebp
    ; get current sprite table
    xor ebx,ebx
    mov bl,[curypos]
    shl ebx,9
    add ebx,spritetable
    mov [currentobjptr],ebx
    ; calculate current video offset
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,8
    shl ebx,5
    add esi,ebx
    add esi,16
    add esi,[vidbuffer]
    mov [curvidoffset],esi
    ; clear registers
    xor eax,eax
    xor ecx,ecx

    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites1
    test word[scrnon],1010h
    jz near .nosprites1
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr+ebx]
    cmp cl,0
    je .nosprites1
    call drawsprites
.nosprites1

    ; display mode7
    test byte[scrndis],01h
    jnz near .noback1
    ; do background 1
    test word[scrnon],0101h
    jz near .noback1
    mov byte[winon],0
    test word[winenabm],0001h
    jz near .nobackwin1
    test word[winenabm],0100h
    jnz near .nobackwin1
    procwindow [winbg1en]
.nobackwin1
    procmode7 [bg1scroly],[bg1scrolx],1
.noback1

    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites2
    test word[scrnon],1010h
    jz near .nosprites2
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr1+ebx]
    cmp cl,0
    je .nosprites2
    call drawsprites
.nosprites2

    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites3
    test word[scrnon],1010h
    jz near .nosprites3
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr2+ebx]
    cmp cl,0
    je .nosprites3
    call drawsprites
.nosprites3
    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites4
    test word[scrnon],1010h
    jz near .nosprites4
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr3+ebx]
    cmp cl,0
    je .nosprites4
    call drawsprites
.nosprites4
    pop ebp
    pop edx
    pop ebx
    pop edi
    pop esi
    ret

;*******************************************************
; Processes & Draws 4-bit sprites
;*******************************************************

NEWSYM drawsprites                                                      ;00053C68
    mov esi,[currentobjptr]
    mov edi,[curvidoffset]
    mov edx,esi
    xor ebx,ebx
.loopobj
    test byte[esi+7],20h
    jnz near .drawspriteflipx
    mov bx,[esi]
    mov ch,[esi+6]
    mov esi,[esi+2]
    mov eax,[esi]
    test al,0Fh
    jz .skipa
    add al,ch
    mov [ebx+edi-8],al
.skipa
    test ah,0Fh
    jz .skipb
    add ah,ch
    mov [ebx+edi-7],ah
.skipb
    shr eax,16
    test al,0Fh
    jz .skipc
    add al,ch
    mov [ebx+edi-6],al
.skipc
    test ah,0Fh
    jz .skipd
    add ah,ch
    mov [ebx+edi-5],ah
.skipd
    mov eax,[esi+4]
    test al,0Fh
    jz .skipe
    add al,ch
    mov [ebx+edi-4],al
.skipe
    test ah,0Fh
    jz .skipf
    add ah,ch
    mov [ebx+edi-3],ah
.skipf
    shr eax,16
    test al,0Fh
    jz .skipg
    add al,ch
    mov [ebx+edi-2],al
.skipg
    test ah,0Fh
    jz .skiph
    add ah,ch
    mov [ebx+edi-1],ah
.skiph
    add edx,8
    mov esi,edx
    dec cl
    jnz .loopobj
    mov [currentobjptr],esi
    ret

.drawspriteflipx
    mov bx,[esi]
    mov ch,[esi+6]
    mov esi,[esi+2]
    mov eax,[esi]
    test al,0Fh
    jz .skipa2
    add al,ch
    mov [ebx+edi-1],al
.skipa2
    test ah,0Fh
    jz .skipb2
    add ah,ch
    mov [ebx+edi-2],ah
.skipb2
    shr eax,16
    test al,0Fh
    jz .skipc2
    add al,ch
    mov [ebx+edi-3],al
.skipc2
    test ah,0Fh
    jz .skipd2
    add ah,ch
    mov [ebx+edi-4],ah
.skipd2
    mov eax,[esi+4]
    test al,0Fh
    jz .skipe2
    add al,ch
    mov [ebx+edi-5],al
.skipe2
    test ah,0Fh
    jz .skipf2
    add ah,ch
    mov [ebx+edi-6],ah
.skipf2
    shr eax,16
    test al,0Fh
    jz .skipg2
    add al,ch
    mov [ebx+edi-7],al
.skipg2
    test ah,0Fh
    jz .skiph2
    add ah,ch
    mov [ebx+edi-8],ah
.skiph2
    add edx,8
    mov esi,edx
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

;*******************************************************
; Processes & Draws 8x8 tiles in 2, 4, & 8 bit mode
;*******************************************************
NEWSYM proc8x8                                                          ;00053D7C
    ; ax = # of rows down
    xor ebx,ebx
    mov ebx,eax
    shr eax,3
    and ebx,07h
    and eax,63
    cmp byte[eax+edi],0
    jne .nocachereq
.docache
    cmp byte[ccud],0
    jne .nocachereq
    mov byte[eax+edi],1
    cmp byte[curcolor],2
    jne .no4b
    ; cache 4-bit
    call cachetile4b
    jmp .nocachereq
.no4b
    cmp byte[curcolor],1
    je .2b
    ; cache 8-bit
    call cachetile8b
    jmp .nocachereq
.2b
    ; cache 2-bit
    call cachetile2b
.nocachereq
    test dx,0100h
    jz .tilexa
    test al,20h
    jz .tileya
    ; bgptrd/bgptrc
    mov ecx,[bgptrd]
    mov [bgptrx1],ecx
    mov ecx,[bgptrc]
    mov [bgptrx2],ecx
    jmp .skiptile
.tileya
    ; bgptrb/bgptra
    mov ecx,[bgptrb]
    mov [bgptrx1],ecx
    mov ecx,[bgptr]
    mov [bgptrx2],ecx
    jmp .skiptile
.tilexa
    test al,20h
    jz .tileya2
    ; bgptrc/bgptrd
    mov ecx,[bgptrc]
    mov [bgptrx1],ecx
    mov ecx,[bgptrd]
    mov [bgptrx2],ecx
    jmp .skiptile
.tileya2
    ; bgptra/bgptrb
    mov ecx,[bgptr]
    mov [bgptrx1],ecx
    mov ecx,[bgptrb]
    mov [bgptrx2],ecx
.skiptile
    ; set up edi & yadder to point to tile data
    shl ebx,3
    mov [yadder],ebx
    and al,1Fh
    mov edi,[vram]
    mov ebx,eax
    shl ebx,6
    mov eax,[bgptrx1]
    add edi,ebx
    mov [temptile],edi
    add edi,eax
    ; dx = # of columns right
    ; cx = bgxlim
    mov eax,edx
    shr edx,3
    mov bl,[curypos]
    and edx,1Fh
    mov [temp],dl
    and eax,07h
    shl dl,1
    xor ebx,ebx
    add edi,edx

    mov esi,eax
    mov ebx,[tempcach]
    xor eax,eax
    mov edx,[temptile]
    mov ax,[bgptrx2]
    add edx,eax
    mov al,[temp]
    mov ecx,[yadder]
    mov ah,[bshifter]
    ; fill up tempbuffer with pointer #s that point to cached video mem
    ; to calculate pointer, get first byte
    ; esi = pointer to video buffer
    ; edi = pointer to tile data
    ; ebx = cached memory
    ; ecx = y adder
    ; edx = secondary tile pointer
    ; al = current x position
    ret

NEWSYM drawn, db 0                                                      ;00053EBB
NEWSYM curbgpr, db 0    ; 00h = low priority, 20h = high priority

%macro drawpixel8b8x8 3
    test %1,0FFh
    jz %2
    add %1,dh
    mov [esi+%3],%1
%2
%endmacro

%macro drawpixel8b8x8win 3
    or %1,%1
    jz %2
    test byte[ebp+%3],0FFh
    jnz %2
    add %1,dh
    mov [esi+%3],%1
%2
%endmacro

NEWSYM draw8x8                                                          ;00053EBD
    mov [temp],al
    mov [bshifter],ah
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
.retfromoffset
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+16
    mov ecx,64
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+16
    sub esi,eax
    jmp .domosaic
.nomosaic
    cmp byte[winon],0
    je .domosaic

    mov esi,xtravbuf+16
    mov ecx,64
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+16
    sub esi,eax

.domosaic
    mov [temptile],edx
    mov ch,33
    mov byte[drawn],0
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    add edi,2
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    and eax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl eax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    ; Start fast loop
    cmp eax,0
    je .skiploop1
    drawpixel8b8x8 al, .loopd1, 0
    drawpixel8b8x8 ah, .loopd2, 1
    shr eax,16
    drawpixel8b8x8 al, .loopd3, 2
    drawpixel8b8x8 ah, .loopd4, 3
.skiploop1
    mov eax,[ebx+4]
    cmp eax,0
    je .skiploop2
    drawpixel8b8x8 al, .loopd5, 4
    drawpixel8b8x8 ah, .loopd6, 5
    shr eax,16
    drawpixel8b8x8 al, .loopd7, 6
    drawpixel8b8x8 ah, .loopd8, 7
.skiploop2
.hprior
    add esi,8
    inc dl
    cmp dl,20h
    jne .loopc2
    mov edi,[temptile]
.loopc2
    dec ch
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic
    cmp byte[winon],0
    jne near dowindow
.nodraw
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    ; Start fast loop
    cmp eax,0
    je .skiploop1b
    drawpixel8b8x8 al, .loopd1b, 7
    drawpixel8b8x8 ah, .loopd2b, 6
    shr eax,16
    drawpixel8b8x8 al, .loopd3b, 5
    drawpixel8b8x8 ah, .loopd4b, 4
.skiploop1b
    mov eax,[ebx+4]
    cmp eax,0
    je .skiploop2b
    drawpixel8b8x8 al, .loopd5b, 3
    drawpixel8b8x8 ah, .loopd6b, 2
    shr eax,16
    drawpixel8b8x8 al, .loopd7b, 1
    drawpixel8b8x8 ah, .loopd8b, 0
.skiploop2b
    add esi,8
    inc dl
    cmp dl,20h
    jne .loopc
    mov edi,[temptile]
.loopc
    dec ch
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw2
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic
    cmp byte[winon],0
    jne near dowindow
 .nodraw2
    ret

NEWSYM domosaic                                                         ;00054106
    mov esi,xtravbuf+16
    mov edi,[curvidoffset]
;    mov cl,dh
    mov dl,dh
;    sub dl,[extraleft+ecx]
    mov ecx,256
    mov al,[esi]
    cmp byte[winon],0
    je .winoff
    mov edi,xtravbuf+16
.loopm
    mov [edi],al
    inc esi
    inc edi
    dec ecx
    jz near .doneloop
    dec dl
    jnz .loopm
    mov al,[esi]
    mov dl,dh
    jmp .loopm
.winoff
    test al,0FFh
    jz .loopp
.loopn
    mov [edi],al
    inc esi
    inc edi
    dec ecx
    jz .doneloop
    dec dl
    jnz .loopn
.loopo
    mov al,[esi]
    mov dl,dh
    test al,0FFh
    jnz .loopn
.loopp
    inc esi
    inc edi
    dec ecx
    jz .doneloop
    dec dl
    jnz .loopp
    mov al,[esi]
    mov dl,dh
    test al,0FFh
    jnz .loopn
    jmp .loopp
.doneloop
    cmp byte[winon],0
    jnz near dowindow
    ret

NEWSYM dowindow                                                         ;0005417B
    mov ebx,windowdata
    mov esi,xtravbuf+16
    mov edi,[curvidoffset]
    xor edx,edx
    xor ch,ch
.getnext
    mov cl,[ebx]
    cmp dl,cl
    je .procnext
.dorest
    sub cl,dl
    cmp ch,0
    ja .nodraw
.loopa
    mov eax,[edx+esi]
    test al,0FFh
    jz .nocopy
    mov [edi+edx],al
.nocopy
    inc dl
    dec cl
    jz .procnext
    test ah,0FFh
    jz .nocopyb
    mov [edi+edx],ah
.nocopyb
    inc dl
    dec cl
    jz .procnext
    shr eax,16
    test al,0FFh
    jz .nocopyc
    mov [edi+edx],al
.nocopyc
    inc dl
    dec cl
    jz .procnext
    test ah,0FFh
    jz .nocopyd
    mov [edi+edx],ah
.nocopyd
    inc dl
    dec cl
    jnz .loopa
.procnext
    add ch,[ebx+1]
    add ebx,2
    test byte[numwin],0FFh
    jz .finishwin
    dec byte[numwin]
    jnz .getnext
    xor cl,cl
    jmp .dorest
.nodraw
    add dl,cl
    jmp .procnext
.finishwin
    xor eax,eax
    ret

xtravbuf   times 576 db 0                                               ;00054203
; bytes to add to match y coordinates
NEWSYM yadder,     dd 0                                                 ;00054443
NEWSYM yrevadder,  dd 0
NEWSYM tempcach,   dd 0          ; points to cached memory
NEWSYM temptile,   dd 0          ; points to the secondary video pointer ;0005444F
NEWSYM bgptr,      dd 0
NEWSYM bgptrb,     dd 0
NEWSYM bgptrc,     dd 0                                                 ;0005445B
NEWSYM bgptrd,     dd 0
NEWSYM bgptrx1,    dd 0                                                 ;00054463
NEWSYM bgptrx2,    dd 0
NEWSYM curvidoffset, dd 0
NEWSYM winon,      db 0                                                 ;0005446F

;*******************************************************
; Processes & Draws 16x16 tiles in 2, 4, & 8 bit mode
;*******************************************************

NEWSYM proc16x16                                                        ;00054470
    ; ax = # of rows down
    xor ebx,ebx
    mov ebx,eax
    and ebx,07h
    mov byte[a16x16yinc],0
    test eax,08h
    jz .noincb
    mov byte[a16x16yinc],1
.noincb
    shr eax,4
    and eax,63
    cmp byte[eax+edi],0
    jne .nocachereq
    mov byte[eax+edi],1
    cmp byte[curcolor],2
    jne .no4b
    ; cache 4-bit
    call cachetile4b16x16
    jmp .nocachereq
.no4b
    cmp byte[curcolor],1
    je .2b
    ; cache 8-bit
    call cachetile8b16x16
    jmp .nocachereq
.2b
    ; cache 2-bit
    call cachetile2b16x16
.nocachereq
    test edx,0200h
    jz .tilexa
    test eax,20h
    jz .tileya
    ; bgptrd/bgptrc
    mov ecx,[bgptrd]
    mov [bgptrx1],ecx
    mov ecx,[bgptrc]
    mov [bgptrx2],ecx
    jmp .skiptile
.tileya
    ; bgptrb/bgptra
    mov ecx,[bgptrb]
    mov [bgptrx1],ecx
    mov ecx,[bgptr]
    mov [bgptrx2],ecx
    jmp .skiptile
.tilexa
    test ax,20h
    jz .tileya2
    ; bgptrc/bgptrd
    mov ecx,[bgptrc]
    mov [bgptrx1],ecx
    mov ecx,[bgptrd]
    mov [bgptrx2],ecx
    jmp .skiptile
.tileya2
    ; bgptra/bgptrb
    mov ecx,[bgptr]
    mov [bgptrx1],ecx
    mov ecx,[bgptrb]
    mov [bgptrx2],ecx
.skiptile
    and eax,1Fh
    shl ebx,3
    mov [yadder],ebx
    ; set up edi to point to tile data
    mov edi,[vram]
    mov ebx,eax
    shl ebx,6
    mov ax,[bgptrx1]
    add edi,ebx
    mov [temptile],edi
    add edi,eax
    ; dx = # of columns right
    ; cx = bgxlim
    mov eax,edx
    mov byte[a16x16xinc],0
    test edx,08h
    jz .noincd
    mov byte[a16x16xinc],1
.noincd
    shr edx,4
    and edx,1Fh
    mov [temp],dl
    and eax,07h
    shl dl,1
    xor ebx,ebx
    add edi,edx

    mov esi,eax
    mov ebx,[tempcach]
    xor eax,eax
    mov edx,[temptile]
    mov ax,[bgptrx2]
    add edx,eax
    mov ecx,[yadder]
    mov eax,[temp]
    ; fill up tempbuffer with pointer #s that point to cached video mem
    ; to calculate pointer, get first byte
    ; esi = pointer to video buffer
    ; edi = pointer to tile data
    ; ebx = cached memory
    ; ecx = y adder
    ; edx = secondary tile pointer
    ; al = current x position
    ret

NEWSYM draw16x16                                                        ;000545CF
    mov byte[drawn],0
    mov [temp],eax
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    xor ebx,ebx
    mov bl,[curypos]
    mov [temptile],edx
    ; set up y adders
    test byte[a16x16yinc],01h
    jz .noincrc
    mov word[.yadd],16
    mov word[.yflipadd],0
    jmp .yesincrc
.noincrc
    mov word[.yadd],0
    mov word[.yflipadd],16
.yesincrc
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+16
    mov ecx,64
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+16
    sub esi,eax
    jmp .domosaic
.nomosaic
    cmp byte[winon],0
    je .domosaic

    mov esi,xtravbuf+16
    mov ecx,64
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+16
    sub esi,eax

.domosaic
    mov ch,33
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    xor byte[a16x16xinc],1

    test dh,40h
    jnz .noxflip
    test byte[a16x16xinc],01h
    jnz .noincr2
    inc ax
    add edi,2
.noincr2
    jmp .yesxflip
.noxflip
    test byte[a16x16xinc],01h
    jnz .noincr
    add edi,2
    jmp .yesincr
.noincr
    inc ax
.yesincr
.yesxflip
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]

    test dh,80h
    jnz .noyflip
    add ax,word[.yadd]
    jmp .yesyflip
.noyflip
    add ax,word[.yflipadd]
.yesyflip

    and ax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl ax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    ; Start fast loop
    cmp eax,0
    je .skiploop1
    drawpixel8b8x8 al, .loopd1, 0
    drawpixel8b8x8 ah, .loopd2, 1
    shr eax,16
    drawpixel8b8x8 al, .loopd3, 2
    drawpixel8b8x8 ah, .loopd4, 3
.skiploop1
    mov eax,[ebx+4]
    cmp eax,0
    je .skiploop2
    drawpixel8b8x8 al, .loopd5, 4
    drawpixel8b8x8 ah, .loopd6, 5
    shr eax,16
    drawpixel8b8x8 al, .loopd7, 6
    drawpixel8b8x8 ah, .loopd8, 7
.skiploop2
.hprior
    add esi,8
    test byte[a16x16xinc],01h
    jnz .noincrb2
    inc dl
.noincrb2
    cmp dl,20h
    jne .loopc2
    xor dl,dl
    mov edi,[temptile]
.loopc2
    dec ch
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic
    cmp byte[winon],0
    jne near dowindow
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    ; Start fast loop
    cmp eax,0
    je .skiploop1b
    drawpixel8b8x8 al, .loopd1b, 7
    drawpixel8b8x8 ah, .loopd2b, 6
    shr eax,16
    drawpixel8b8x8 al, .loopd3b, 5
    drawpixel8b8x8 ah, .loopd4b, 4
.skiploop1b
    mov eax,[ebx+4]
    cmp eax,0
    je .skiploop2b
    drawpixel8b8x8 al, .loopd5b, 3
    drawpixel8b8x8 ah, .loopd6b, 2
    shr eax,16
    drawpixel8b8x8 al, .loopd7b, 1
    drawpixel8b8x8 ah, .loopd8b, 0
.skiploop2b
    add esi,8
    test byte[a16x16xinc],01h
    jnz .noincrb
    inc dl
.noincrb
    cmp dl,20h
    jne .loopc
    xor dl,dl
    mov edi,[temptile]
.loopc
    dec ch
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic
    cmp byte[winon],0
    jne near dowindow
    ret

.yadd      dw 0                                                     ;0005489A
.yflipadd  dw 0                                                     ;0005489C

NEWSYM temp,       db 0
NEWSYM bshifter,   db 0                                                 ;0005489F
NEWSYM a16x16xinc, db 0
NEWSYM a16x16yinc, db 0                                                 ;000548A1


