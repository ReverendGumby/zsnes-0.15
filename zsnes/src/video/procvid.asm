;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM BGMA,DSPMem,INTEnab,V8Mode,antienab,cacheud,cbitmode
EXTSYM ccud,cfield,cgfxmod,cgram,coladdb,coladdg,coladdr,cpalval,curblank
EXTSYM curfps,cvidmode,delay,draw16bnng,extlatch,fnamest,fulladdtab,En2xSaI
EXTSYM gammalevel,hirestiledat,ignor512,latchx,latchy,maxbr,ForceNewGfxOff
EXTSYM newengen,nextframe,objptr,pressed,res512switch,res640
EXTSYM resolutn,romispal,sbpmofs,sbselec,scaddtype,scadtng,scanlines
EXTSYM scbcong,selcA000,snesmouse,t1cc,vcache4b,vesa2_bpos,spritetable
EXTSYM vesa2_clbit,vesa2_gpos,vesa2_rpos,vesa2red10,vesa2selec,vidbuffer
EXTSYM vidbufferm,vram,vsyncon,vbufdptr,KeyStateSelct,forceblnk,soundon
EXTSYM Open_File,Read_File,Close_File,Create_File,Write_File,Get_File_Date
EXTSYM Triplebufen,makepal,changepal,saveselectpal,displayfpspal,superscopepal
EXTSYM DrawScreen
EXTSYM Get_MouseData,Get_MousePositionDisplacement
EXTSYM GUIEnableTransp
EXTSYM GUIFontData
EXTSYM StopSound,StartSound
EXTSYM PrevPicture,File_Seek,File_Seek_End,nggposng
EXTSYM Palette0
EXTSYM GetTimeInSeconds
EXTSYM scaddset,scrnon,spcRam,nmiprevline,bgmode,ofsmcptr
EXTSYM interlval,bg3ptr,bg3scroly,bg3scrolx,C4Ram,SprValAdd,SA1IRQEn,SA1IRQV
EXTSYM winbg1en,winlogica,wincolen,winlogicb,dsp1ptr,dsp1array,bg3objptr
EXTSYM cnetptrhead,cnetptrtail,JoyBOrig,pl2neten,Voice6Ptr,HIRQLoc,SA1DoIRQ
EXTSYM mode7A,mode7B,mode7C,mode7D,mode7set,winbg3en,winl1,winr1,SA1DMAInfo
EXTSYM winl2,winr2,VIRQLoc,SA1Enable,mode7X0,mode7Y0,SA1Temp
EXTSYM SA1IRQTemp,SA1IRQEnable,SA1DMADest,SA1DMAChar,SA1DMASource,SA1DMACount
EXTSYM objptrn,Voice0State,TempValueSnd,nglogicval,bgtilesz,C4values
EXTSYM curexecstate,TempVidInfo,LatestBank,C4ObjSelec
EXTSYM BGMS1,scadsng,winenabm,winenabs,vidbright
EXTSYM genfulladdtab,genfulladdtabng
EXTSYM KeyQuickChat,CNetType,WritetochatBuffer,NetAddChar,TimerEnable,ShowTimer
EXTSYM ClearScreenSkip,debugdisble,cmovietimeint
EXTSYM ChatNick
EXTSYM StringLength
EXTSYM chatstrLt
EXTSYM copymodeq,copymodex,copyvesa2320x240x8b,copyvesa2320x240x16b
EXTSYM JoyX2


;*******************************************************
; ShowVideo                   Processes & displays video
;*******************************************************

NEWSYM showvideo                                                        ;00051854
    push esi
    push edi
    push edx
    push ebx
    inc byte[ccud]
    mov bl,byte[ccud]
    cmp byte[cacheud],bl
    je .noinc
    mov byte[ccud],0
.noinc
    cmp byte[cbitmode],1
    je .notpal
    cmp byte[curblank],0
    jnz .notpal
    call changepal
.notpal
    call copyvid
    test byte[pressed+61],1
    jz .nosavesel
    call saveselect
.nosavesel
    pop ebx
    pop edx
    pop edi
    pop esi
    ret

;*******************************************************
; Output Hex                 Outputs the hex in al @ esi
;*******************************************************

NEWSYM outputhex                                                        ;000518A2
    push edi
    push esi
    push eax
    push ebx
    push ecx
    push esi
    mov edi,FontData
    xor ebx,ebx
    mov bl,al
    shr bl,4
    shl ebx,3
    add edi,ebx
    add edi,8
    mov cl,8
.loopa
    mov ah,[edi]
    mov ch,8
.loopb
    test ah,80h
    jz .nowrite
    mov byte[esi],128
.nowrite
    shl ah,1
    inc esi
    dec ch
    jnz .loopb
    add esi,280
    inc edi
    dec cl
    jnz .loopa
    pop esi
    add esi,8
    mov edi,FontData
    xor ebx,ebx
    mov bl,al
    and bl,0Fh
    shl ebx,3
    add edi,ebx
    add edi,8
    mov cl,8
.loopa2
    mov ah,[edi]
    mov ch,8
.loopb2
    test ah,80h
    jz .nowrite2
    mov byte[esi],128
.nowrite2
    shl ah,1
    inc esi
    dec ch
    jnz .loopb2
    add esi,280
    inc edi
    dec cl
    jnz .loopa2
    pop ecx
    pop ebx
    pop eax
    pop esi
    pop edi
    ret

NEWSYM ASCII2Font                                                       ;00051923
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,25h,28h,00h
         db 01h,02h,03h,04h,05h,06h,07h,08h,09h,0Ah,00h,00h,00h,00h,00h,00h
         db 00h,0Bh,0Ch,0Dh,0Eh,0Fh,10h,11h,12h,13h,14h,15h,16h,17h,18h,19h
         db 1Ah,1Bh,1Ch,1Dh,1Eh,1Fh,20h,21h,22h,23h,24h,00h,00h,00h,00h,26h
         db 00h,0Bh,0Ch,0Dh,0Eh,0Fh,10h,11h,12h,13h,14h,15h,16h,17h,18h,19h
         db 1Ah,1Bh,1Ch,1Dh,1Eh,1Fh,20h,21h,22h,23h,24h,00h,00h,00h,26h,00h
         db 0Dh,1Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Dh,0Fh,0Fh,0Fh,13h,13h,13h,0Bh,0Bh
         db 0Fh,0Bh,0Bh,19h,19h,19h,1Fh,1Fh,23h,19h,1Fh,0Dh,10h,23h,1Ah,10h
         db 0Bh,13h,19h,1Fh,18h,18h,0Bh,19h,00h,00h,00h,00h,00h,00h,00h,00h
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
         db 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h

NEWSYM FontData                                                         ;00051A23
         db 0,0,0,0,0,0,0,0
         db 01111100b,11000110b,11001110b,11010110b     ; 0, 1
         db 11100110b,11000110b,01111100b,00000000b
         db 00011000b,00111000b,01111000b,00011000b     ; 1, 2
         db 00011000b,00011000b,01111110b,00000000b
         db 01111100b,11000110b,00001100b,00011000b     ; 2, 3
         db 00110000b,01100110b,11111110b,00000000b
         db 01111100b,11000110b,00000110b,00111100b     ; 3, 4
         db 00000110b,11000110b,01111100b,00000000b
         db 00111100b,01101100b,11001100b,11111110b     ; 4, 5
         db 00001100b,00001100b,00001100b,00000000b
         db 11111110b,11000000b,11000000b,11111100b     ; 5, 6
         db 00000110b,11000110b,01111100b,00000000b
         db 00111100b,01100000b,11000000b,11111100b     ; 6, 7
         db 11000110b,11000110b,01111100b,00000000b
         db 11111110b,11000110b,00000110b,00001100b     ; 7, 8
         db 00011000b,00011000b,00011000b,00000000b
         db 01111100b,11000110b,11000110b,01111100b     ; 8, 9
         db 11000110b,11000110b,01111100b,00000000b
         db 01111100b,11000110b,11000110b,01111110b     ; 9, A
         db 00000110b,11000110b,01111100b,00000000b
         db 00111000b,01101100b,11000110b,11111110b     ; A, B
         db 11000110b,11000110b,11000110b,00000000b
         db 11111100b,11000110b,11000110b,11111100b     ; B, C
         db 11000110b,11000110b,11111100b,00000000b
         db 01111100b,11000110b,11000000b,11000000b     ; C, D
         db 11000000b,11000110b,01111100b,00000000b
         db 11111100b,11000110b,11000110b,11000110b     ; D, E
         db 11000110b,11000110b,11111100b,00000000b
         db 11111110b,11000000b,11000000b,11111000b     ; E, F
         db 11000000b,11000000b,11111110b,00000000b
         db 11111110b,11000000b,11000000b,11111000b     ; F, 10
         db 11000000b,11000000b,11000000b,00000000b
         db 01111100b,11000110b,11000000b,11000000b     ; G, 11
         db 11001110b,10000110b,01111100b,00000000b
         db 11000110b,11000110b,11000110b,11111110b     ; H, 12
         db 11000110b,11000110b,11000110b,00000000b     
         db 00111100b,00011000b,00011000b,00011000b     ; I, 13
         db 00011000b,00011000b,00111100b,00000000b     
         db 00011110b,00001100b,00001100b,00001100b     ; J, 14
         db 00001100b,11001100b,00111100b,00000000b     
         db 11001100b,11011000b,11110000b,11100000b     ; K, 15
         db 11110000b,11011000b,11001100b,00000000b
         db 11000000b,11000000b,11000000b,11000000b     ; L, 16
         db 11000000b,11000000b,11111110b,00000000b
         db 11000110b,11101110b,11111110b,11010110b     ; M, 17
         db 11000110b,11000110b,11000110b,00000000b
         db 11000110b,11100110b,11110110b,11011110b     ; N, 18
         db 11001110b,11000110b,11000110b,00000000b
         db 01111100b,11000110b,11000110b,11000110b     ; O, 19
         db 11000110b,11000110b,01111100b,00000000b
         db 11111100b,11000110b,11000110b,11111100b     ; P, 1A
         db 11000000b,11000000b,11000000b,00000000b
         db 01111100b,11000110b,11000110b,11000110b     ; Q, 1B
         db 11010110b,11001110b,01111110b,00000000b
         db 11111100b,11000110b,11000110b,11111100b     ; R, 1C
         db 11001100b,11000110b,11000110b,00000000b
         db 01111100b,11000110b,11000000b,01111100b     ; S, 1D
         db 00000110b,11000110b,01111100b,00000000b
         db 01111110b,00011000b,00011000b,00011000b     ; T, 1E
         db 00011000b,00011000b,00011000b,00000000b
         db 11000110b,11000110b,11000110b,11000110b     ; U, 1F
         db 11000110b,11000110b,01111100b,00000000b
         db 11000110b,11000110b,11000110b,11000110b     ; V, 20
         db 01101100b,00111000b,00010000b,00000000b
         db 11000110b,11000110b,11000110b,11010110b     ; W, 21
         db 11010110b,11111110b,11101100b,00000000b
         db 11000110b,01101100b,00111000b,00010000b     ; X, 22
         db 00111000b,01101100b,11000110b,00000000b
         db 11001100b,11001100b,01111000b,00110000b     ; Y, 23
         db 00110000b,00110000b,00110000b,00000000b
         db 11111100b,10001100b,00011000b,00110000b     ; Z, 24
         db 01100000b,01100010b,11111110b,00000000b
         db 00000000b,00000000b,00000000b,11111110b     ; -, 25
         db 00000000b,00000000b,00000000b,00000000b
         db 00000000b,00000000b,00000000b,00000000b     ; _, 26
         db 00000000b,00000000b,11111110b,00000000b
         db 01110000b,11011100b,00000110b,00000000b     ; ~, 27
         db 00000000b,00000000b,00000000b,00000000b
         db 00000000b,00000000b,00000000b,00000000b     ; ., 28
         db 00000000b,00110000b,00110000b,00000000b

;*******************************************************
; Output Char                   Outputs char in al @ esi
;*******************************************************

NEWSYM outputchar                                                       ;00051B6B
    push edi
    push esi
    push eax
    mov edi,FontData
    xor ebx,ebx
    mov bl,al
    shl ebx,3
    add edi,ebx
    mov cl,8
.loopa
    mov ah,[edi]
    mov ch,8
.loopb
    test ah,80h
    jz .nowrite
    mov byte[esi],80h
.nowrite
    shl ah,1
    inc esi
    dec ch
    jnz .loopb
    add esi,280
    inc edi
    dec cl
    jnz .loopa
    pop eax
    pop esi
    pop edi
    ret

NEWSYM outputchar16b                                                    ;00051BA0
    push edi
    push esi
    push eax
    mov edi,FontData
    xor ebx,ebx
    mov bl,al
    shl ebx,3
    add edi,ebx
    mov cl,8
.loopa
    mov ah,[edi]
    mov ch,8
.loopb
    test ah,80h
    jz .nowrite
    mov word[esi],0FFFFh
.nowrite
    shl ah,1
    add esi,2
    dec ch
    jnz .loopb
    add esi,560
    inc edi
    dec cl
    jnz .loopa
    pop eax
    pop esi
    pop edi
    ret

;*******************************************************
; Output Graphic String   Outputs String from edi to esi
;*******************************************************

NEWSYM OutputGraphicString                                              ;00051BDC
    xor eax,eax
.nextstr
    mov al,[edi]
    cmp al,0
    je .nomore
    mov al,[ASCII2Font+eax]
    call outputchar
    add esi,8
    inc edi
    jmp .nextstr
.nomore
    ret

NEWSYM OutputGraphicString16b                                           ;00051BFC
    xor eax,eax
.nextstr
    mov al,[edi]
    cmp al,0
    je .nomore
    mov al,[ASCII2Font+eax]
    call outputchar16b
    add esi,16
    inc edi
    jmp .nextstr
.nomore
    ret

;*******************************************************
; Save Select      Allows user to select save state slot
;*******************************************************
; For Save State

NEWSYM csounddisable, db 0                                      ;00051C1C
NEWSYM statefileloc,  dd 0                                      ;00051C1D

NEWSYM drawhline                                                ;00051C21
.loop
    mov byte[esi],al
    inc esi
    loop .loop
    ret

NEWSYM drawhline16b                                             ;00051C27
.loop
    mov [esi],ax
    add esi,2
    loop .loop
    ret

NEWSYM drawvline                                                ;00051C33
.loop
    mov byte[esi],al
    add esi,288
    loop .loop
    ret

NEWSYM drawvline16b                                             ;00051C3E
.loop
    mov [esi],ax
    add esi,288*2
    loop .loop
    ret

NEWSYM drawbox                                                  ;00051C4A
    ; draws a box according to position bl and color dl
    xor eax,eax
    mov al,11
    mul bl
    mov esi,75+93*288
    add esi,[vidbuffer]
    add esi,eax
    mov al,dl
    push esi
    mov ecx,12
    call drawhline
    pop esi
    push esi
    mov ecx,12
    call drawvline
    pop esi
    push esi
    add esi,11
    mov ecx,12
    call drawvline
    pop esi
    add esi,11*288
    mov ecx,12
    call drawhline
    ret

NEWSYM drawbox16b                                                       ;00051C9A
    ; draws a box according to position bl and color dx
    xor eax,eax
    mov al,11
    mul bl
    mov esi,75*2+93*288*2
    add esi,[vidbuffer]
    add esi,eax
    add esi,eax
    mov ax,dx
    push esi
    mov ecx,12
    call drawhline16b
    pop esi
    push esi
    mov ecx,12
    call drawvline16b
    pop esi
    push esi
    add esi,11*2
    mov ecx,12
    call drawvline16b
    pop esi
    add esi,11*288*2
    mov ecx,12
    call drawhline16b
    ret

%macro drawfillboxhelp 2
    mov bl,%1
    mov byte[fnamest+eax],%2
    call drawfillboxsc
%endmacro

%macro drawfillboxhelp16b 2
    mov bl,%1
    mov byte[fnamest+eax],%2
    call drawfillboxsc16b
%endmacro


NEWSYM saveselect                                                       ;00051CED
    cmp dword[MessageOn],0
    je .nochangem
    mov dword[MessageOn],1
.nochangem
    call copyvid
    mov byte[csounddisable],1
    push es
    mov es,[sbselec]
    mov edi,[sbpmofs]
    mov ecx,320
.loopa
    mov dword[es:edi],0
    add edi,4
    dec ecx
    jnz .loopa
    pop es
    cmp byte[cbitmode],1
    je near .16b
    ; set palette of colors 128,144, and 160 to white, blue, and red
    mov al,128
    mov dx,03C8h
    out dx,al
    inc dx
    mov al,63
    out dx,al
    out dx,al
    out dx,al
    mov al,144
    mov dx,03C8h
    out dx,al
    inc dx
    xor al,al
    out dx,al
    out dx,al
    mov al,50
    out dx,al
    mov al,160
    mov dx,03C8h
    out dx,al
    inc dx
    mov al,45
    out dx,al
    xor al,al
    out dx,al
    out dx,al
    ; draw a small blue box with a white border
    mov esi,70+70*288
    add esi,[vidbuffer]
    mov ecx,150
    mov al,70
.loop
    mov byte[esi],144
    inc esi
    loop .loop
    add esi,288-150
    dec al
    mov ecx,150
    jnz .loop

    mov esi,75+73*288
    add esi,[vidbuffer]
    mov edi,.stringa
    call OutputGraphicString
    mov esi,75+83*288
    add esi,[vidbuffer]
    mov edi,.stringb
    call OutputGraphicString
    mov esi,75+108*288
    add esi,[vidbuffer]
    mov edi,.stringc
    call OutputGraphicString
    mov esi,75+118*288
    add esi,[vidbuffer]
    mov edi,.stringd
    call OutputGraphicString
    mov esi,75+128*288
    add esi,[vidbuffer]
    mov edi,.stringe
    call OutputGraphicString
    mov al,128
    mov esi,70+70*288
    add esi,[vidbuffer]
    mov ecx,150
    call drawhline
    mov esi,70+70*288
    add esi,[vidbuffer]
    mov ecx,70
    call drawvline
    mov esi,70+139*288
    add esi,[vidbuffer]
    mov ecx,150
    call drawhline
    mov esi,219+70*288
    add esi,[vidbuffer]
    mov ecx,70
    call drawvline
    mov esi,75+93*288
    add esi,[vidbuffer]
    mov ecx,111
    call drawhline
    mov esi,75+104*288
    add esi,[vidbuffer]
    mov ecx,111
    call drawhline
    mov esi,75+94*288
    add esi,[vidbuffer]
    mov bl,11
.nextvline
    mov ecx,10
    push esi
    push ebx
    call drawvline
    pop ebx
    pop esi
    add esi,11
    dec bl
    jnz .nextvline
    mov esi,78+96*288
    add esi,[vidbuffer]
    mov al,1
    call outputchar
    mov bl,9
.nextnumchar
    add esi,11
    inc al
    push ebx
    call outputchar
    pop ebx
    dec bl
    jnz .nextnumchar
    mov byte[curblank],0h
    mov bl,0
    mov ebx,[statefileloc]
    mov al,byte[fnamest+ebx]
    cmp al,'T'
    jne .noT
    mov bl,0
    jmp .nexter
.noT
    mov bl,al
    sub bl,48
.nexter
    mov dl,160
    call drawbox
    call copyvid
    ; wait until esc/enter is pressed
.noesc
    mov dl,128
    call drawbox
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc
    test byte[pressed+28],1
    jnz near .enter
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc
    test byte[pressed+28],1
    jnz near .enter
    push ebx
    call copyvid
    pop ebx
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc
    test byte[pressed+28],1
    jnz near .enter
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc
    test byte[pressed+28],1
    jnz near .enter
    mov dl,160
    call drawbox
    push ebx
    call copyvid
    pop ebx
    jmp .noesc
.enter
    mov byte[pressed+28],0
    cmp bl,0
    jne .nozero
    mov al,'T'
    jmp .save
.nozero
    add bl,48
    mov al,bl
.save
    mov ebx,[statefileloc]
    mov byte[fnamest+ebx],al
.esc
    mov byte[pressed+1],0
    mov word[t1cc],0
    mov byte[csounddisable],0

    call makepal
    ret

.allred dw 0
.allgrn dw 0
.allgrnb dw 0

; Start 16-bit stuff here
.16b
    ; draw shadow behind box
    mov esi,80*2+80*288*2
    add esi,[vidbuffer]
    mov ecx,150
    mov al,70
    mov ah,5
.loop16b2
    mov dx,[esi]
    and dx,[vesa2_clbit]
    shr dx,1
    mov [esi],dx
    add esi,2
    loop .loop16b2
    add esi,288*2-150*2
    dec al
    mov ecx,150
    jnz .loop16b2

    mov ax,01Fh
    mov cl,[vesa2_rpos]
    shl ax,cl
    mov [.allred],ax
    mov ax,012h
    mov cl,[vesa2_bpos]
    shl ax,cl
    mov dx,ax
    mov ax,01h
    mov cl,[vesa2_gpos]
    shl ax,cl
    mov bx,ax
    mov ax,01h
    mov cl,[vesa2_rpos]
    shl ax,cl
    or bx,ax

    ; draw a small blue box with a white border
    mov esi,70*2+70*288*2
    add esi,[vidbuffer]
    mov ecx,150
    mov al,70
    mov ah,5
.loop16b
    mov [esi],dx
    add esi,2
    loop .loop16b
    add esi,288*2-150*2
    dec ah
    jnz .nocolinc16b
    add dx,bx
    mov ah,5
.nocolinc16b
    dec al
    mov ecx,150
    jnz .loop16b

    mov esi,75*2+73*288*2
    add esi,[vidbuffer]
    mov edi,.stringa
    call OutputGraphicString16b
    mov esi,75*2+83*288*2
    add esi,[vidbuffer]
    mov edi,.stringb
    call OutputGraphicString16b
    mov esi,75*2+108*288*2
    add esi,[vidbuffer]
    mov edi,.stringc
    call OutputGraphicString16b
    mov esi,75*2+118*288*2
    add esi,[vidbuffer]
    mov edi,.stringd
    call OutputGraphicString16b
    mov esi,75*2+128*288*2
    add esi,[vidbuffer]
    mov edi,.stringe
    call OutputGraphicString16b
    mov ax,0FFFFh
    mov esi,70*2+70*288*2
    add esi,[vidbuffer]
    mov ecx,150
    call drawhline16b
    mov esi,70*2+70*288*2
    add esi,[vidbuffer]
    mov ecx,70
    call drawvline16b
    mov esi,70*2+139*288*2
    add esi,[vidbuffer]
    mov ecx,150
    call drawhline16b
    mov esi,219*2+70*288*2
    add esi,[vidbuffer]
    mov ecx,70
    call drawvline16b
    mov esi,75*2+93*288*2
    add esi,[vidbuffer]
    mov ecx,111
    call drawhline16b
    mov esi,75*2+104*288*2
    add esi,[vidbuffer]
    mov ecx,111
    call drawhline16b
    mov esi,75*2+94*288*2
    add esi,[vidbuffer]
    mov bl,11
.nextvline16b
    mov ecx,10
    push esi
    push ebx
    call drawvline16b
    pop ebx
    pop esi
    add esi,22
    dec bl
    jnz .nextvline16b
    mov esi,78*2+96*288*2
    add esi,[vidbuffer]
    mov al,1
    call outputchar16b
    mov bl,9
.nextnumchar16b
    add esi,22
    inc al
    push ebx
    call outputchar16b
    pop ebx
    dec bl
    jnz .nextnumchar16b

    mov byte[curblank],0h
    mov bl,0
    mov ebx,[statefileloc]
    mov al,byte[fnamest+ebx]
    cmp al,'T'
    jne .noT16b
    mov bl,0
    jmp .nexter16b
.noT16b
    mov bl,al
    sub bl,48
.nexter16b
    mov dx,[.allred]
    call drawbox16b
    call copyvid
    ; wait until esc/enter is pressed
.noesc16b
    mov dx,0FFFFh
    call drawbox16b
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc16b
    test byte[pressed+28],1
    jnz near .enter16b
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc16b
    test byte[pressed+28],1
    jnz near .enter16b
    push ebx
    call copyvid
    pop ebx
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc16b
    test byte[pressed+28],1
    jnz near .enter16b
    mov ecx,2500
    call delay
    call testpressed8b
    test byte[pressed+1],1
    jnz near .esc16b
    test byte[pressed+28],1
    jnz near .enter16b
    mov dx,[.allred]
    call drawbox16b
    push ebx
    call copyvid
    pop ebx
    jmp .noesc16b
.enter16b
    mov byte[pressed+28],0
    cmp bl,0
    jne .nozero16b
    mov al,'T'
    jmp .save16b
.nozero16b
    add bl,48
    mov al,bl
.save16b
    mov ebx,[statefileloc]
    mov byte[fnamest+ebx],al
.esc16b
    mov byte[pressed+1],0
    mov word[t1cc],0
    mov byte[csounddisable],0
    ret

.stringa db 'PLEASE SELECT',0                                           ;00052330
.stringb db 'SAVE STATE SLOT',0
.stringc db 'USE CURSOR KEYS',0
.stringd db 'TO MOVE AND',0
.stringe db 'ENTER TO SELECT',0

NEWSYM testpressed8b                                                    ;0005237A
    test byte[pressed+2],1
    jz .no1
    mov bl,1
.no1
    test byte[pressed+3],1
    jz .no2
    mov bl,2
.no2
    test byte[pressed+4],1
    jz .no3
    mov bl,3
.no3
    test byte[pressed+5],1
    jz .no4
    mov bl,4
.no4
    test byte[pressed+6],1
    jz .no5
    mov bl,5
.no5
    test byte[pressed+7],1
    jz .no6
    mov bl,6
.no6
    test byte[pressed+8],1
    jz .no7
    mov bl,7
.no7
    test byte[pressed+9],1
    jz .no8
    mov bl,8
.no8
    test byte[pressed+10],1
    jz .no9
    mov bl,9
.no9
    test byte[pressed+11],1
    jz .no0
    mov bl,0
.no0
    test byte[pressed+75],1
    jz .noleft
    cmp bl,0
    je .noleft
    dec bl
    mov byte[pressed+75],2
.noleft
    test byte[pressed+77],1
    jz .noright
    cmp bl,9
    je .noright
    inc bl
    mov byte[pressed+77],2
.noright
    ret

;*******************************************************
; MakePal                     Changes the entire palette
;*******************************************************
; set the brightness with [maxbr]
NEWSYM makepal                                                          ;00052417
    mov ax,[cgram]
    mov [tempco0],ax
    test byte[scaddtype],00100000b
    jz near .noaddition
    test byte[scaddtype],10000000b
    jnz near .noaddition
    mov cx,[cgram]
    mov ax,cx
    and ax,001Fh
    add al,[coladdr]
    cmp al,01Fh
    jb .noadd
    mov al,01Fh
.noadd
    mov bx,ax
    mov ax,cx
    shr ax,5
    and ax,001Fh
    add al,[coladdg]
    cmp al,01Fh
    jb .noaddb
    mov al,01Fh
.noaddb
    shl ax,5
    add bx,ax
    mov ax,cx
    shr ax,10
    and ax,001Fh
    add al,[coladdb]
    cmp al,01Fh
    jb .noaddc
    mov al,01Fh
.noaddc
    shl ax,10
    add bx,ax
    mov [cgram],bx
.noaddition
NEWSYM makepalb
    mov edi,cgram
    mov ebx,prevpal
    xor ah,ah
.loopa
    mov cx,[edi]
    push eax
    push ebx
    mov [ebx],cx
    mov al,ah
    mov dx,03C8h
    out dx,al
    mov ax,cx
    and al,01Fh
    mov bh,[maxbr]
    mov bl,bh
    mul bl
    mov bl,15
    div bl
    shl al,1
    mov dx,03C9h
    out dx,al
    mov ax,cx
    shr ax,5
    and al,01Fh
    mov bl,bh
    mul bl
    mov bl,15
    div bl
    shl al,1
    mov dx,03C9h
    out dx,al
    mov ax,cx
    shr ax,10
    and al,01Fh
    mov bl,bh
    mul bl
    mov bl,15
    div bl
    shl al,1
    mov dx,03C9h
    out dx,al
    pop ebx
    pop eax
    add edi,2
    add ebx,2
    inc ah
    jnz .loopa
    mov al,[maxbr]
    mov [prevbright],al
    mov ax,[tempco0]
    mov [cgram],ax
    cmp byte[MessageOn],0
    je .nochange128
    mov dx,03C8h
    mov al,128
    out dx,al
    mov al,63
    inc dx
    out dx,al
    out dx,al
    out dx,al
.nochange128
    ret
NEWSYM tempco0, dw 0

;*******************************************************
; ChangePal                          Sets up the palette
;*******************************************************

NEWSYM changepal                                                        ;00052544
    mov ax,[cgram]
    mov [tempco0],ax
    test byte[scaddtype],00100000b
    jz near .noaddition
    test byte[scaddtype],10000000b
    jnz near .noaddition
    mov cx,[cgram]
    mov ax,cx
    and ax,001Fh
    add al,[coladdr]
    cmp al,01Fh
    jb .noadd
    mov al,01Fh
.noadd
    mov bx,ax
    mov ax,cx
    shr ax,5
    and ax,001Fh
    add al,[coladdg]
    cmp al,01Fh
    jb .noaddb
    mov al,01Fh
.noaddb
    shl ax,5
    add bx,ax
    mov ax,cx
    shr ax,10
    and ax,001Fh
    add al,[coladdb]
    cmp al,01Fh
    jb .noaddc
    mov al,01Fh
.noaddc
    shl ax,10
    add bx,ax
    mov [cgram],bx
.noaddition
    ; check if brightness differs
    mov al,[maxbr]
    cmp al,[prevbright]
    jne near makepalb
    ; check for duplicate palette (Compare prevpal with cgram)
    mov ebx,prevpal
    mov edi,cgram
    xor ah,ah
.loopa
    mov cx,[edi]
    cmp cx,[ebx]
    je .nochange
    push eax
    push ebx
    mov [ebx],cx
    mov al,ah
    mov dx,03C8h
    out dx,al
    mov ax,cx
    and al,01Fh
    mov bh,[maxbr]
    mov bl,bh
    mul bl
    mov bl,15
    div bl
    shl al,1
    mov dx,03C9h
    out dx,al
    mov ax,cx
    shr ax,5
    and al,01Fh
    mov bl,bh
    mul bl
    mov bl,15
    div bl
    shl al,1
    mov dx,03C9h
    out dx,al
    mov ax,cx
    shr ax,10
    and al,01Fh
    mov bl,bh
    mul bl
    mov bl,15
    div bl
    shl al,1
    mov dx,03C9h
    out dx,al
    pop ebx
    pop eax
.nochange
    add edi,2
    add ebx,2
    inc ah
    jnz .loopa
    mov ax,[tempco0]
    mov [cgram],ax
    cmp byte[MessageOn],0
    je .nochange128
    mov dx,03C8h
    mov al,128
    out dx,al
    mov al,63
    inc dx
    out dx,al
    out dx,al
    out dx,al
.nochange128
    ret

NEWSYM prevpal, times 256 dw 0          ; previous palette buffer
NEWSYM prevbright, db 0                 ; previous brightness

;*******************************************************
; CopyVid                       Copies buffer into video
;*******************************************************

NEWSYM showfps                                                          ;0005287C
    inc byte[curfps]
    cmp byte[nextframe],100
    jb .less
    mov al,[curfps]
    mov [lastfps],al
    mov al,[curfps2]
    mov [lastfps2],al

    mov byte[curfps],0
    mov byte[curfps2],0
    mov byte[nextframe],0

.less
    mov al,[lastfps]
    mov esi,200*288+32
    add esi,[vidbuffer]
    call outputhex

    mov al,[lastfps2]
    mov esi,200*288+56
    add esi,[vidbuffer]
    call outputhex

    mov eax,[JoyX2]
    shr eax,2
    mov esi,200*288+80
    add esi,[vidbuffer]
    call outputhex
    ret

    mov al,[scrnon+1]
    mov esi,200*288+104
    add esi,[vidbuffer]
    call outputhex

    mov al,[scaddset]
    mov esi,200*288+128
    add esi,[vidbuffer]
    call outputhex

    mov al,[scaddtype]
    mov esi,200*288+152
    add esi,[vidbuffer]
    call outputhex
    ret

NEWSYM SoundPlayed0, db 0
NEWSYM SoundPlayed1, db 0
NEWSYM SoundPlayed2, db 0
NEWSYM SoundPlayed3, db 0
NEWSYM SoundPlayed4, db 0
NEWSYM SoundPlayed5, db 0
NEWSYM SoundPlayed6, db 0
NEWSYM SoundPlayed7, db 0

NEWSYM ShowSound                                                        ;0005293F
    add esi,[vidbuffer]
.next
    mov [esi],ebx
    mov [esi+4],ebx
    mov [esi+8],bx
    sub esi,288
    dec al
    jnz .next
    ret

NEWSYM sounddisplay                                                     ;00052959
    push esi
    push ebx
    push eax
    mov ebx,80808080h
    cmp byte[SoundPlayed0],0
    je .nosnd0
    mov al,[SoundPlayed0]
    mov esi,223*288+16
    call ShowSound
    sub byte[SoundPlayed0],2
.nosnd0
    cmp byte[SoundPlayed1],0
    je .nosnd1
    mov al,[SoundPlayed1]
    mov esi,223*288+28
    call ShowSound
    sub byte[SoundPlayed1],2
.nosnd1
    cmp byte[SoundPlayed2],0
    je .nosnd2
    mov al,[SoundPlayed2]
    mov esi,223*288+40
    call ShowSound
    sub byte[SoundPlayed2],2
.nosnd2
    cmp byte[SoundPlayed3],0
    je .nosnd3
    mov al,[SoundPlayed3]
    mov esi,223*288+52
    call ShowSound
    sub byte[SoundPlayed3],2
.nosnd3
    cmp byte[SoundPlayed4],0
    je .nosnd4
    mov al,[SoundPlayed4]
    mov esi,223*288+64
    call ShowSound
    sub byte[SoundPlayed4],2
.nosnd4
    cmp byte[SoundPlayed5],0
    je .nosnd5
    mov al,[SoundPlayed5]
    mov esi,223*288+76
    call ShowSound
    sub byte[SoundPlayed5],2
.nosnd5
    cmp byte[SoundPlayed6],0
    je .nosnd6
    mov al,[SoundPlayed6]
    mov esi,223*288+88
    call ShowSound
    sub byte[SoundPlayed6],2
.nosnd6
    cmp byte[SoundPlayed7],0
    je .nosnd7
    mov al,[SoundPlayed7]
    mov esi,223*288+100
    call ShowSound
    sub byte[SoundPlayed7],2
.nosnd7
    pop eax
    pop esi
    pop ebx
    ret

NEWSYM copyvid                                                          ;00052A5D
    cmp dword[MessageOn],0
    je near .nomsg
    cmp byte[cbitmode],1
    je .nomsg
    mov edi,[Msgptr]
    mov esi,200*288+32
    add esi,[vidbuffer]
    call OutputGraphicString
    dec dword[MessageOn]
    jnz .nomsg
    call makepal
    jmp .nomsg
.nomsg
    cmp byte[cvidmode],1
    je near copymodeq
    cmp byte[cvidmode],2
    je near copyvesa2320x240x8b
    cmp byte[cvidmode],3
    je near copyvesa2320x240x16b
    cmp byte[cvidmode],0
    je near copymodex
    cmp byte[curblank],40h
    je .startcopy
    inc byte[curfps2]
;    call sounddisplay
;    call hextestoutput
.startcopy
    jmp copymodeq

NEWSYM lastfps,   db 0                  ; stores the last fps encountered   ;00052AE6
NEWSYM lastfps2,  db 0                  ; stores the last fps encountered   ;00052AE7
NEWSYM curfps2,   db 0                  ; current video refresh fps         ;00052AE8
NEWSYM Msgptr,    dd 0                  ; Pointer to message
NEWSYM MessageOn, dd 0                  ; Message On Countdown              ;00052AED
NEWSYM MsgCount,  dd 120                ; How long message will stay (PAL = 100)
