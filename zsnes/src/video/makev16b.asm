;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM cursprloc,curypos,scrndis,scrnon,winon,drawmode716extbg,drawmode716extbg2,SA1Enable
EXTSYM alreadydrawn,bg1cachloc,bg1tdabloc,bg1tdatloc,bg1vbufloc,bg1xposloc
EXTSYM bg1yaddval,bgmode,bgtilesz,curbgnum,drawn
EXTSYM winbg1en,winenabs
EXTSYM winl1,winl2,winr1,winr2
EXTSYM mosaicon,winenabm
EXTSYM vidbuffer
EXTSYM bg3highst,colormodedef,colormodeofs,curbgpr,curblank
EXTSYM currentobjptr,curvidoffset,cwinenabm,drawline16t,forceblnk
EXTSYM maxbr,newengen,newengine16b,preparesprpr
EXTSYM procbackgrnd,scaddset,scaddtype,spritetable,sprleftpr,ForceNewGfxOff
EXTSYM bg1scrolx,bg1scroly,drawmode716b,mode7set,mosaicsz,sprleftpr1
EXTSYM sprleftpr2,sprleftpr3,interlval
EXTSYM extbgdone
EXTSYM FPUZero,coladdb,coladdg,coladdr,vesa2_bpos
EXTSYM V8Mode,doveg,pal16bcl,pal16bxcl,prevbright,prevpal,vesa2_clbit
EXTSYM vesa2_gpos,vesa2_rpos,vesa2_usbit,vidbright
EXTSYM vesa2_gposng,vesa2_rposng,vesa2_bposng
EXTSYM cgmod,cgram,gammalevel16b
EXTSYM winspdata
EXTSYM csprbit,csprprlft,sprclprio,sprsingle,sprpriodata
EXTSYM bgofwptr,bgsubby,bshifter,curmosaicsz,osm2dis,temp
EXTSYM tempcach,temptile,winptrref,vcache2b,vcache4b,vcache8b
EXTSYM xtravbuf,yadder,yrevadder
EXTSYM vcache2ba,vcache4ba,vcache8ba
EXTSYM hirestiledat,res512switch
EXTSYM numwin,windowdata
EXTSYM bg1objptr,bg1ptr,bg1ptrc,bg3ptr,bg3scrolx,bg3scroly,cachesingle
EXTSYM colormoded2,offsetenab,offsetmclr,offsetmcol,offsetmodeptr
EXTSYM offsetmptr,offsetmshl,offsetmtst,offsetptra,offsetptrb,posyscroll
EXTSYM prevoffsetdat,prevtempcache,tempoffset,vidmemch2,vidmemch4
EXTSYM vidmemch8,vram
EXTSYM ofsmcptr,ofsmady,ofsmadx,yposng,yposngom,flipyposng,flipyposngom
EXTSYM ofsmtptr,ofsmmptr,ofsmcyps,bgtxadd,bg1ptrx,bg1ptry
EXTSYM a16x16xinc,a16x16yinc

%include "video/vidmacro.mac"





;drawspritesprio

%macro procmode716bextbg 3
    xor eax,eax
    xor edx,edx
    mov ax,[curypos]
    test byte[mode7set],02h
    jz %%noflip
    mov ax,261
    sub ax,[curypos]
    dec ax
%%noflip
    mov byte[curmosaicsz],1
    test byte[mosaicon],%3
    jz %%nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je %%nomos
    inc bl
    mov [curmosaicsz],bl
    xor bh,bh
    div bx
    xor edx,edx
    mul bx
%%nomos
    add ax,%1
    mov dx,%2
    call drawmode716extbg
%endmacro

%macro procmode716bextbg2 3
    xor eax,eax
    xor edx,edx
    mov ax,[curypos]
    mov byte[curmosaicsz],1
    test byte[mosaicon],%3
    jz %%nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je %%nomos
    inc bl
    mov [curmosaicsz],bl
    xor bh,bh
    div bx
    xor edx,edx
    mul bx
%%nomos
    call drawmode716extbg2
%endmacro

NEWSYM procspritessub16b                                                ;000548A2
    test byte[scrndis],10h
    jnz .nosprites
    test byte[scrnon+1],10h
    jz .nosprites
    test byte[scrnon],10h
    jnz .nosprites
    xor ebx,ebx
    mov bl,[curypos]
    add ebx,[cursprloc]
    mov cl,[ebx]
    add dword[cursprloc],256
.sprprio
    cmp cl,0
    je .nosprites
    call drawsprites16b
.nosprites
    ret

NEWSYM procspritesmain16b                                               ;000548E2
    test byte[scrndis],10h
    jnz .nosprites
    test byte[scrnon],10h
    jz .nosprites
    xor ebx,ebx
    mov bl,[curypos]
    add ebx,[cursprloc]
    mov cl,[ebx]
    add dword[cursprloc],256
.sprprio
    cmp cl,0
    je .nosprites
    call drawsprites16b
.nosprites
    ret

NEWSYM drawbackgrndsub16b                                               ;00054919
    mov esi,[colormodeofs]
    mov bl,[ebp+esi+0]
    cmp bl,0
    je near .noback
    mov al,[curbgnum]
    test byte[scrnon+1],al
    jz near .noback
    test byte[scrnon],al
    jnz near .noback
    test byte[alreadydrawn],al
    jnz near .noback
    test byte[scrndis],al
    jnz near .noback
    mov byte[winon],0
    test byte[winenabs],al
    jz near .nobackwin
    procwindow [winbg1en+ebp]
.nobackwin
    mov bl,[curbgnum]
    mov byte[curmosaicsz],1
    test byte[mosaicon],bl
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
.nomos
    mov esi,[bg1vbufloc+ebp*4]
    mov edi,[bg1tdatloc+ebp*4]
    mov edx,[bg1tdabloc+ebp*4]
    mov ebx,[bg1cachloc+ebp*4]
    mov eax,[bg1xposloc+ebp*4]
    mov cl,[curbgnum]
    test byte[bgtilesz],cl
    jnz .16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x816b
    cmp byte[drawn],33
    jne .notalldrawn
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawn
    jmp .noback
.16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x1616b
    cmp byte[drawn],33
    jne .notalldrawnb
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnb
.noback
    ret

NEWSYM drawbackgrndmain16b                                              ;00054AE1
    mov esi,[colormodeofs]
    mov bl,[ebp+esi+0]
    cmp bl,0
    je near .noback
    mov al,[curbgnum]
    test byte[scrnon],al
    jz near .noback
    test byte[alreadydrawn],al
    jnz near .noback
    test byte[scrndis],al
    jnz near .noback
    mov byte[winon],0
    test byte[winenabm],al
    jz near .nobackwin
    procwindow [winbg1en+ebp]
.nobackwin
    mov bl,[curbgnum]
    mov byte[curmosaicsz],1
    test byte[mosaicon],bl
    jz .nomos
    mov bl,[mosaicsz]
    cmp bl,0
    je .nomos
    inc bl
    mov [curmosaicsz],bl
.nomos
    mov esi,[bg1vbufloc+ebp*4]
    mov edi,[bg1tdatloc+ebp*4]
    mov edx,[bg1tdabloc+ebp*4]
    mov ebx,[bg1cachloc+ebp*4]
    mov eax,[bg1xposloc+ebp*4]
    mov cl,[curbgnum]
    test byte[bgtilesz],cl
    jnz .16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw8x816b
    cmp byte[drawn],33
    jne .notalldrawn
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawn
    jmp .noback
.16x16
    mov ecx,[bg1yaddval+ebp*4]
    call draw16x1616b
    cmp byte[drawn],33
    jne .notalldrawnb
    mov al,[curbgnum]
    or [alreadydrawn],al
.notalldrawnb
.noback
    ret
NEWSYM blanker16b                                                       ;00054C9D
    ; calculate current video offset
    push ebx
    push esi
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,9
    shl ebx,6
    add esi,ebx
    add esi,32
    add esi,[vidbuffer]
    mov bl,128
.next
    mov dword[esi],0
    add esi,4
    dec bl
    jnz .next
    pop esi
    pop ebx
    ret

NEWSYM drawline16b                                                      ;00054CD3
    cmp byte[curblank],0
    jne near nodrawline16b
    mov al,[vidbright]
    cmp al,[maxbr]
    jbe .nochange
    mov [maxbr],al
.nochange
    cmp byte[forceblnk],0
    jne blanker16b
    mov byte[alreadydrawn],0
    push ebx
    xor ebx,ebx
    mov bl,[bgmode]
    shl bl,2
    add ebx,colormodedef
    mov [colormodeofs],ebx
    pop ebx

    test byte[scaddset],02h
    jnz near drawline16t
    cmp dword[coladdr],0
    je .nocoladd
    test byte[scaddtype],3Fh
    jnz near drawline16t
.nocoladd
    cmp byte[bgmode],7
    je near processmode716b
    push esi
    push edi
    push ebx
    push edx
    push ebp
    ; calculate current video offset
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,9
    shl ebx,6
    add esi,ebx
    add esi,32
    add esi,[vidbuffer]
    mov [curvidoffset],esi
    ; set palette
    call setpalette16b
    ; clear back area w/ back color
    call clearback16b
    ; clear registers
    xor eax,eax
    xor ecx,ecx
    ; get current sprite table
    xor ebx,ebx
    mov bl,[curypos]
    shl ebx,9
    add ebx,spritetable
    mov [currentobjptr],ebx
    mov dword[cursprloc],sprleftpr
; process backgrounds
; do background 2
    mov byte[curbgnum],02h
    mov ebp,01h
    call procbackgrnd
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call procbackgrnd
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call procbackgrnd
; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call procbackgrnd

    test byte[scaddset],02h
    jz near .nope
.nope
    mov byte[curbgpr],0h
; do background 3
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain16b
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndmain16b
    mov byte[curbgpr],20h
; do background 3
    cmp byte[bg3highst],1
    je .bg3nothighb
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain16b
.bg3nothighb
; do background 4
    mov byte[curbgnum],08h
    mov ebp,03h
    call drawbackgrndmain16b
; do background 2
    mov byte[curbgpr],0h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndmain16b
    call procspritesmain16b
    call procspritesmain16b
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndmain16b
    call procspritesmain16b
; do background 2
    mov byte[curbgpr],20h
    mov byte[curbgnum],02h
    mov ebp,01h
    call drawbackgrndmain16b
; do background 1
    mov byte[curbgnum],01h
    mov ebp,00h
    call drawbackgrndmain16b
    call procspritesmain16b
    cmp byte[bg3highst],1
    jne .bg3highb
; do background 3
    mov byte[curbgpr],20h
    mov byte[curbgnum],04h
    mov ebp,02h
    call drawbackgrndmain16b
.bg3highb
    pop ebp
    pop edx
    pop ebx
    pop edi
    pop esi
    xor eax,eax
    xor ecx,ecx
NEWSYM nodrawline16b
    ret

NEWSYM processmode716b                                                  ;00054EE4
    push esi
    push edi
    push ebx
    push edx
    ; get current sprite table
    xor ebx,ebx
    mov bl,[curypos]
    shl ebx,9
    add ebx,spritetable
    mov [currentobjptr],ebx
    ; calculate current video offset
    xor ebx,ebx
    mov bx,[curypos]
    mov esi,ebx
    shl esi,9
    shl ebx,6
    add esi,ebx
    add esi,32
    add esi,[vidbuffer]
    mov [curvidoffset],esi
    ; set palette
    call setpalette16b
    ; clear back area w/ back color
    call clearback16b
    ; clear registers
    xor eax,eax
    xor ecx,ecx

    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites1
    test word[scrnon],1010h
    jz near .nosprites1
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr+ebx]
.nosprprio2
    cmp cl,0
    je .nosprites1
    call drawsprites16b
.nosprites1

    ; display mode7
    test byte[scrndis],01h
    jnz near .noback1
    ; do background 1
    test word[scrnon],0101h
    jz near .noback1
    mov byte[winon],0
    test word[winenabm],0001h
    jz near .nobackwin1
    test word[winenabm],0100h
    jnz near .nobackwin1
    procwindow [winbg1en]
.nobackwin1
    procmode716b [bg1scroly],[bg1scrolx],1
.noback1

    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites2
    test word[scrnon],1010h
    jz near .nosprites2
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr1+ebx]
.nosprprio3
    cmp cl,0
    je .nosprites2
    call drawsprites16b
.nosprites2

    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites3
    test word[scrnon],1010h
    jz near .nosprites3
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr2+ebx]
.nosprprio4
    cmp cl,0
    je .nosprites3
    call drawsprites16b
.nosprites3
    ; do objects
    test byte[scrndis],10h
    jnz near .nosprites4
    test word[scrnon],1010h
    jz near .nosprites4
    xor ebx,ebx
    mov bl,[curypos]
    mov cl,[sprleftpr3+ebx]
.nosprprio5
    cmp cl,0
    je .nosprites4
    call drawsprites16b
.nosprites4
    pop edx
    pop ebx
    pop edi
    pop esi
    ret

;*******************************************************
; Clear Backarea, 16-bit mode
;*******************************************************
NEWSYM clearback16b                                                     ;00055175
    test byte[scaddtype],00100000b
    jz near .noaddition
    test byte[scaddtype],10000000b
    jnz near .noaddition
    mov dx,[cgram]
    mov ax,dx
    and ax,001Fh
    add al,[coladdr]
    cmp al,01Fh
    jb .noadd
    mov al,01Fh
.noadd
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_rpos]
    shl ax,cl
    mov bx,ax
    mov ax,dx
    shr ax,5
    and ax,001Fh
    add al,[coladdg]
    cmp al,01Fh
    jb .noaddb
    mov al,01Fh
.noaddb
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_gpos]
    shl ax,cl
    add bx,ax
    mov ax,dx
    shr ax,10
    and ax,001Fh
    add al,[coladdb]
    cmp al,01Fh
    jb .noaddc
    mov al,01Fh
.noaddc
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_bpos]
    shl ax,cl
    add bx,ax
    mov ax,bx
    shl eax,16
    mov ax,bx
    mov esi,[curvidoffset]
    mov ecx,128
.loopa
    mov [esi],eax
    add esi,4
    dec ecx
    jnz .loopa
    xor eax,eax
    ret
.noaddition
    mov esi,[curvidoffset]
    mov ax,[pal16b]
    shl eax,16
    mov ax,[pal16b]
    mov ecx,128
.loopb
    mov [esi],eax
    add esi,4
    dec ecx
    jnz .loopb
    xor eax,eax
    ret

;*******************************************************
; Set palette 16bit
;*******************************************************
NEWSYM setpalall                                                        ;0005526F
    xor esi,esi
    mov byte[colleft16b],0
.loopa
    mov dx,[cgram+esi]
    mov [prevpal+esi],dx
    mov ax,dx
    and al,01Fh
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_rpos]
    mov bx,[vesa2_usbit]
    shl ax,cl
    add bx,ax
    mov ax,dx
    shr ax,5
    and al,01Fh
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_gpos]
    shl ax,cl
    add bx,ax
    mov ax,dx
    shr ax,10
    and al,01Fh
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_bpos]
    shl ax,cl
    add bx,ax
    mov ax,bx
    mov [pal16b+esi],bx
    and bx,[vesa2_clbit]
    mov [pal16bcl+esi],bx
    xor ax,0FFFFh
    and ax,[vesa2_clbit]
    mov [pal16bxcl+esi],ax
    add esi,2
    inc byte[colleft16b]
    jnz near .loopa
    mov al,[vidbright]
    mov [prevbright],al
    ret

NEWSYM colleft16b, db 0                                                 ;00055339

NEWSYM setpalette16b                                                    ;0005533A
    mov al,[vidbright]
    cmp al,[prevbright]
    jne near setpalall
    cmp byte[cgmod],0
    je near .skipall
    mov byte[cgmod],0
    xor esi,esi
    mov byte[colleft16b],0
.loopa
    mov dx,[cgram+esi]
    cmp [prevpal+esi],dx
    je near .skipa
    mov [prevpal+esi],dx
    mov ax,dx
    and al,01Fh
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_rpos]
    mov bx,[vesa2_usbit]
    shl ax,cl
    add bx,ax
    mov ax,dx
    shr ax,5
    and al,01Fh
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_gpos]
    shl ax,cl
    add bx,ax
    mov ax,dx
    shr ax,10
    and al,01Fh
    mov cl,[vidbright]
    mul cl
    mov cl,15
    div cl
    xor ah,ah
    mov cl,[vesa2_bpos]
    shl ax,cl
    add bx,ax
    mov [pal16b+esi],bx
    mov ax,bx
    and bx,[vesa2_clbit]
    mov [pal16bcl+esi],bx
    shr bx,1
    and bx,[vesa2_clbit]
    mov [pal16bclha+esi],bx
    xor ax,0FFFFh
    and ax,[vesa2_clbit]
    mov [pal16bxcl+esi],ax
.skipa
    add esi,2
    inc byte[colleft16b]
    jnz near .loopa
.skipall
    ret

NEWSYM pal16b,   times 256 dw 0                                         ;0005543D
NEWSYM pal16bcl, times 256 dw 0         ; w/ cl bit                     ;0005563D
NEWSYM pal16bclha, times 256 dw 0         ; w/ cl bit
NEWSYM pal16bxcl, times 256 dw 0FFFFh         ; xored w/ cl bit

;*******************************************************
; Processes & Draws 4-bit sprites
;*******************************************************

NEWSYM drawsprites16b                                                   ;00055C3D
    mov esi,[currentobjptr]
    mov edi,[curvidoffset]
    xor ebx,ebx
    xor eax,eax
.loopobj
    test byte[esi+7],20h
    jnz near .drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    mov esi,[esi+2]
    shl bx,1
    mov al,[esi]
    test al,0Fh
    jz .skipa
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-16],dx
.skipa
    mov al,[esi+1]
    test al,0Fh
    jz .skipb
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-14],dx
.skipb
    mov al,[esi+2]
    test al,0Fh
    jz .skipc
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-12],dx 
.skipc
    mov al,[esi+3]
    test al,0Fh
    jz .skipd
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-10],dx
.skipd
    mov al,[esi+4]
    test al,0Fh
    jz .skipe
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-8],dx
.skipe
    mov al,[esi+5]
    test al,0Fh
    jz .skipf
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-6],dx
.skipf
    mov al,[esi+6]
    test al,0Fh
    jz .skipg
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-4],dx
.skipg
    mov al,[esi+7]
    test al,0Fh
    jz .skiph
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-2],dx
.skiph
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

.drawspriteflipx
    push esi
    mov bx,[esi]
    mov ch,[esi+6]
    mov esi,[esi+2]
    shl bx,1
    mov al,[esi+7]
    test al,0Fh
    jz .skipa2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-16],dx
.skipa2
    mov al,[esi+6]
    test al,0Fh
    jz .skipb2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-14],dx
.skipb2
    mov al,[esi+5]
    test al,0Fh
    jz .skipc2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-12],dx
.skipc2
    mov al,[esi+4]
    test al,0Fh
    jz .skipd2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-10],dx
.skipd2
    mov al,[esi+3]
    test al,0Fh
    jz .skipe2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-8],dx
.skipe2
    mov al,[esi+2]
    test al,0Fh
    jz .skipf2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-6],dx
.skipf2
    mov al,[esi+1]
    test al,0Fh
    jz .skipg2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-4],dx
.skipg2
    mov al,[esi]
    test al,0Fh
    jz .skiph2
    add al,ch
    mov dx,[pal16b+eax*2]
    mov [ebx+edi-2],dx
.skiph2
    pop esi
    add esi,8
    dec cl
    jnz near .loopobj
    mov [currentobjptr],esi
    ret

NEWSYM draw8x816b                                                       ;00055DFB
    mov [temp],al
    mov [bshifter],ah
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .domosaic
.nomosaic
    cmp byte[winon],0
    je .domosaic

    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax

; tile value : bit 15 = flipy, bit 14 = flipx, bit 13 = priority value
;              bit 10-12 = palette, 0-9=tile#
.domosaic
    mov [temptile],edx
    mov byte[tileleft16b],33
    mov byte[drawn],0
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    add edi,2
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]
    and eax,03FFh                ; filter out tile #
    mov ebx,[tempcach]
    shl eax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop

    ; Begin Normal Loop
    mov cl,[bshifter]
    and dh,1Ch
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    ; Start loop
    cmp dword[ebx],0
    je .loopd4
    Draw8x816bmacro 0
    Draw8x816bmacro 1
    Draw8x816bmacro 2
    Draw8x816bmacro 3
.loopd4
    cmp dword[ebx+4],0
    je .loopd8
    Draw8x816bmacro 4
    Draw8x816bmacro 5
    Draw8x816bmacro 6
    Draw8x816bmacro 7
.loopd8
.hprior
    add esi,16
    inc dl
    cmp dl,20h
    jne .loopc2
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    cmp dword[ebx+4],0
    je .loopd4b
    Draw8x816bflipmacro 0
    Draw8x816bflipmacro 1
    Draw8x816bflipmacro 2
    Draw8x816bflipmacro 3
.loopd4b
    cmp dword[ebx],0
    je .loopd8b
    Draw8x816bflipmacro 4
    Draw8x816bflipmacro 5
    Draw8x816bflipmacro 6
    Draw8x816bflipmacro 7
.loopd8b
    add esi,16
    inc dl
    cmp dl,20h
    jne .loopc
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    cmp byte[drawn],0
    je .nodraw2
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
.nodraw2
    ret

NEWSYM tileleft16b, db 0

NEWSYM domosaic16b                                                      ;00056104
    mov esi,xtravbuf+32
    mov edi,[curvidoffset]
    mov dl,dh
    mov cl,0
    mov ax,[esi]
    cmp byte[winon],0
    je .nowin
    mov edi,xtravbuf+32
.loopm
    mov [edi],ax
    add esi,2
    add edi,2
    dec cl
    jz near .doneloop
    dec dl
    jnz .loopm
    mov ax,[esi]
    mov dl,dh
    jmp .loopm
.nowin
    test ax,0FFFFh
    jz .loopn
.loopo
    mov [edi],ax
    add esi,2
    add edi,2
    dec cl
    jz .doneloop
    dec dl
    jnz .loopo
    mov ax,[esi]
    mov dl,dh
    test ax,0FFFFh
    jnz .loopo
.loopn
    add esi,2
    add edi,2
    dec cl
    jz .doneloop
    dec dl
    jnz .loopn
    mov ax,[esi]
    mov dl,dh
    test ax,0FFFFh
    jnz .loopo
    jmp .loopn
.doneloop
    cmp byte[winon],0
    jne near dowindow16b
    ret

NEWSYM dowindow16b                                                      ;000561A3
    mov ebx,windowdata
    mov esi,xtravbuf+32
    mov edi,[curvidoffset]
    xor edx,edx
    xor ch,ch
.getnext
    mov cl,[ebx]
    cmp dl,cl
    je .procnext
.dorest
    sub cl,dl
    cmp ch,0
    ja .nodraw
.loopa
    mov ax,[esi+edx*2]
    test ax,0FFFFh
    jz .nocopy
    mov [edi+edx*2],ax
.nocopy
    inc dl
    dec cl
    jnz .loopa
.procnext
    add ch,[ebx+1]
    add ebx,2
    test byte[numwin],0FFh
    jz .finishwin
    dec byte[numwin]
    jnz .getnext
    xor cl,cl
    jmp .dorest
.nodraw
    add dl,cl
    jmp .procnext
.finishwin
    xor eax,eax
    ret

NEWSYM draw16x1616b                                                     ;00056203
    mov byte[drawn],0
    mov [temp],eax
    mov eax,esi
    mov [yadder],ecx
    mov [tempcach],ebx
    mov ebx,56
    sub ebx,ecx
    mov [yrevadder],ebx
    xor ebx,ebx
    mov bl,[curypos]
    mov [temptile],edx
    ; set up y adders
    test byte[a16x16yinc],01h
    jz .noincrc
    mov word[.yadd],16
    mov word[.yflipadd],0
    jmp .yesincrc
.noincrc
    mov word[.yadd],0
    mov word[.yflipadd],16
.yesincrc
    ; esi = pointer to video buffer
    mov esi,[curvidoffset]
    sub esi,eax           ; esi = [vidbuffer] + curypos * 288 + 16 - HOfs
    sub esi,eax
    cmp byte[curmosaicsz],1
    je .nomosaic
    mov esi,xtravbuf+32
    mov ecx,128
.clearnext
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax
    jmp .domosaic
.nomosaic
    cmp byte[winon],0
    je .domosaic

    mov esi,xtravbuf+32
    mov ecx,128
.clearnext2
    mov dword[esi],0
    add esi,4
    dec ecx
    jnz .clearnext2
    mov esi,xtravbuf+32
    sub esi,eax
    sub esi,eax

.domosaic
    mov byte[tileleft16b],33
    mov dl,[temp]
.loopa
    mov ax,[edi]
    mov dh,ah
    xor byte[a16x16xinc],1

    test dh,40h
    jnz .noxflip
    test byte[a16x16xinc],01h
    jnz .noincr2
    inc ax
    add edi,2
.noincr2
    jmp .yesxflip
.noxflip
    test byte[a16x16xinc],01h
    jnz .noincr
    add edi,2
    jmp .yesincr
.noincr
    inc ax
.yesincr
.yesxflip
    xor dh,[curbgpr]
    test dh,20h
    jnz near .hprior
    inc byte[drawn]

    test dh,80h
    jnz .noyflip
    add ax,word[.yadd]
    jmp .yesyflip
.noyflip
    add ax,word[.yflipadd]
.yesyflip

    and ax,03FFh                ; filter out tile #
.postflip
    mov ebx,[tempcach]
    shl ax,6
    add ebx,eax
    test dh,80h
    jz .normadd
    add ebx,[yrevadder]
    jmp .skipadd
.normadd
    add ebx,[yadder]
.skipadd
    test dh,40h
    jnz near .rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax

    ; Start loop
    Draw16x1616b 0, 0
    Draw16x1616b 1, 2
    Draw16x1616b 2, 4
    Draw16x1616b 3, 6
    Draw16x1616b 4, 8
    Draw16x1616b 5, 10
    Draw16x1616b 6, 12
    Draw16x1616b 7, 14
.hprior
    add esi,16
    test byte[a16x16xinc],01h
    jnz .noincrb2
    inc dl
.noincrb2
    cmp dl,20h
    jne .loopc2
    xor dl,dl
    mov edi,[temptile]
.loopc2
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

    ; reversed loop
.rloop
    mov cl,[bshifter]
    and dh,1Ch
    mov eax,[ebx]
    shl dh,cl                    ; process palette # (bits 10-12)
    xor eax,eax
    Draw16x1616b 7, 0
    Draw16x1616b 6, 2
    Draw16x1616b 5, 4
    Draw16x1616b 4, 6
    Draw16x1616b 3, 8
    Draw16x1616b 2, 10
    Draw16x1616b 1, 12
    Draw16x1616b 0, 14
.skiploop2b
    add esi,16
    test byte[a16x16xinc],01h
    jnz .noincrb
    inc dl
.noincrb
    cmp dl,20h
    jne .loopc
    xor dl,dl
    mov edi,[temptile]
.loopc
    dec byte[tileleft16b]
    jnz near .loopa
    mov dh,byte[curmosaicsz]
    cmp dh,1
    jne near domosaic16b
    cmp byte[winon],0
    jne near dowindow16b
    ret

.yadd      dw 0
.yflipadd  dw 0
