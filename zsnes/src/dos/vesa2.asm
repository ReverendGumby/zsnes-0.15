;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM printnum
EXTSYM DosExit,ZSNESBase,Change_Dir,PrintStr,newengen
EXTSYM HalfTransB,HalfTransC
EXTSYM InitDrive,gotoroot,InitDir,fulladdtab
;        EXTSYM printhex
;        EXTSYM printhex
EXTSYM UnusedBit,HalfTrans,UnusedBitXor,ngrposng,nggposng,ngbposng
;        EXTSYM printhex
EXTSYM Init_2xSaIMMX,Init_2xSaI

NEWSYM vesa2_x,         dw 320          ; Desired screen width          ;000E4375
NEWSYM vesa2_y,         dw 240          ; Height
NEWSYM vesa2_bits,      db 8            ; Bits per pixel                ;000E4379
NEWSYM vesa2selec,      dw 0            ; VESA2 Selector Location       ;000E437A
NEWSYM vesa2_rpos,      db 0            ; Red bit position              ;000E437C
NEWSYM vesa2_gpos,      db 0            ; Green bit position
NEWSYM vesa2_bpos,      db 0            ; Blue bit position
NEWSYM vesa2_usbit,     dd 0            ; Unused bit in proper bit location ;000E437F
NEWSYM vesa2_clbit,     dd 0            ; clear all bit 0's if AND is used  ;000E4383
NEWSYM vesa2_rtrcl,     dw 0            ; red transparency clear     (bit+4)        ;000E4387
NEWSYM vesa2_rtrcla,    dw 0            ; red transparency (AND) clear (not(bit+4)) ;000E4389
NEWSYM vesa2_rfull,     dw 0            ; red max (or bit*1Fh)          ;000E438B
NEWSYM vesa2_gtrcl,     dw 0            ; red transparency clear     (bit+4)
NEWSYM vesa2_gtrcla,    dw 0            ; red transparency (AND) clear (not(bit+4))
NEWSYM vesa2_gfull,     dw 0            ; red max (or bit*1Fh)
NEWSYM vesa2_btrcl,     dw 0            ; red transparency clear     (bit+4)
NEWSYM vesa2_btrcla,    dw 0            ; red transparency (AND) clear (not(bit+4))
NEWSYM vesa2_bfull,     dw 0            ; red max (or bit*1Fh)

NEWSYM fulladdtab
dcolortab times 65536 dw 0                                              ;000E4399

NEWSYM VESA2EXITTODOS                                                   ;00104399
        mov ax,0003h
        int 10h
        push edx
        mov edx,.exitfromvesa2
        mov ah,09h
        int 21h
        pop edx
        mov ah,09h
        int 21h
        mov edx,.return
        mov ah,09h
        int 21h
        mov ah,4Ch
        int 21h

.exitfromvesa2 db 'Unable to Initialize VESA2 ','$'
.return db 10,13,'$'

;*******************************************************
;	Set up Vesa 2
;*******************************************************

NEWSYM InitVesa2                                                        ;001043DA
	   ;-------------------------------------------------;
	    ; First - allocate some bytes in DOS memory for ;
	    ; communication with VBE			    ;
	   ;-------------------------------------------------;

	mov eax,0100h
	mov ebx,512/16			; 512 bytes
	int 31h				; Function 31h,100h - Allocate
					; DOS memory (512 bytes)
	jnc .gotmem
	    mov edx,.nomemmessage
            jmp VESA2EXITTODOS
	    .nomemmessage
            db ': Unable to locate DOS memory.','$'

	.gotmem
	mov fs,edx			; FS now points to the DOS
					; buffer


	   ;--------------------------------------------------;
	    ; Now, get information about the video card into ;
	    ; a data structure				     ;
	   ;--------------------------------------------------;

	mov edi,RMREGS
        mov dword[fs:0],'VBE2'          ; Request VBE 2.0 info
	mov dword[RMREGS.eax],4f00h
	mov word[RMREGS.es],ax		; Real mode segment of DOS 
					; buffer
	mov dword[RMREGS.edi],0

	push es
        push ds
	pop es
	mov eax,300h
	mov ebx,10h
	xor ecx,ecx
	int 31h				; Simulate real mode interrupt
	pop es

	jnc .int1ok
	    mov edx,.noint1message
            jmp VESA2EXITTODOS
	    .noint1message
            db ': Simulated real mode interrupt failed.','$'

	.int1ok			; Real mode int successful!!!
	mov eax,[RMREGS.eax]
	cmp al,4fh		; Check vbe interrupt went OK
	jz .vbedetected
	    mov edx,.novbemessage
            jmp VESA2EXITTODOS
	    .novbemessage
            db ': VBE not detected!!','$'

	.vbedetected
	cmp dword[fs:0000],'VESA'
	jz .vesadetected	; Check for presence of vesa
	    mov edx,.novesamessage
            jmp VESA2EXITTODOS
	    .novesamessage
            db ': VESA not detected!','$'

	.vesadetected
	cmp word[fs:0004],200h
	jae .vesa2detected	; Check we've got VESA 2.0 or greater
	    mov edx,.novesa2message
            jmp VESA2EXITTODOS
	    .novesa2message
            db ': VESA 2.0 or greater required!','$'


		;-----------------------------------------------------;
		 ; OK - vesa 2.0 or greater has been detected. Copy  ;
		 ; mode information into VESAmodelist		     ;
		;-----------------------------------------------------;

	.vesa2detected
	mov ax,[fs:12h]			; Get no. of 64k blocks
	mov [noblocks],ax
	mov ax, 2
	mov bx,[fs:10h]
	int 31h

	jnc .wegottheselector
	    mov edx, .oopsnoselector
            jmp VESA2EXITTODOS
	    .oopsnoselector
                db 'Failed to allocate vesa display selector!','$'

	.wegottheselector

	mov gs,eax
	xor eax,eax
	mov ebp,VESAmodelist
	mov ecx,512
	mov ax,[fs:0eh]

	.loopcopymodes
	    mov bx,[gs:eax]
	    mov [ebp],bx
	    cmp bx,0ffffh
	    jz .copiedmodes
	    add ebp,2
	    add eax,2
	    dec ecx
	    jz .outofmodelistspace
	    jmp .loopcopymodes

		.outofmodelistspace
		mov edx,.outofmodelistspacemessage
                jmp VESA2EXITTODOS
		.outofmodelistspacemessage
                db 'Out of VESA2 mode list space!','$'

		   ;----------------------------------------------;
		    ; OK - Scan the mode list to find a matching ;
		    ; mode for vesa2_x, vesa2_y and vesa2_depth	 ;
		   ;----------------------------------------------;

    .copiedmodes

	mov ebp,VESAmodelist
	xor ecx,ecx

    .loopcheckmodes
	mov cx, [ebp]
	cmp cx, 0ffffh
	jnz .notendoflist

	    mov edx,.endoflist
            jmp VESA2EXITTODOS

            .endoflist db 'This VESA 2 mode does not work on your video card or driver.','$'

    .notendoflist

        mov edi, RMREGS
        mov dword[RMREGS.eax],4f01h
        mov dword[RMREGS.ebx],0
        mov dword[RMREGS.ecx],ecx
        mov dword[RMREGS.edi],0

	push es
        push ds
	pop es
	mov eax,300h
	mov ebx,10h
	xor ecx,ecx
	int 31h				; Simulate real mode interrupt
	pop es
	jnc .modecheckok
	    mov edx,.modecheckfail
            jmp VESA2EXITTODOS
	    .modecheckfail
            db 'Real mode interrupt failure while checking vesa mode','$'

	.modecheckok
	add ebp,2

        test word[fs:0000h],1b
            jz near .loopcheckmodes     ; If mode is not available
        mov al,[fs:0002h]
        and al,06h
        cmp al,06h
        jnz near .loopcheckmodes
;
;        xor eax,eax
;        mov ax,[fs:12h]
;        call printnum
;        mov ah,02h
;        mov dl,'x'
;        int 21h
;        mov ax,[fs:14h]
;        call printnum
;        mov ah,02h
;        mov dl,'x'
;        int 21h
;        xor ah,ah
;        mov al,[fs:19h]
;        call printnum
;        mov ah,02h
;        mov dl,13
;        int 21h
;        mov dl,10
;        int 21h

	mov eax,[vesa2_x]
	cmp [fs:12h],ax			; Check that the height matches
	    jnz near .loopcheckmodes
	mov eax,[vesa2_y]
	cmp [fs:14h],ax			; Check that the width matches
	    jnz near .loopcheckmodes
	mov al,[vesa2_bits]
	cmp [fs:19h],al			; Check bits/pixel for match
	    jnz near .loopcheckmodes

;        mov ax,3
;        int 10h
;        xor eax,eax
;        mov ax,[fs:0h]
;        call printnum
;        jmp DosExit

	sub ebp,2
	mov ax,[ebp]
	mov [vesamode],ax		; Store vesa 2 mode number

;        call printhex
;        jmp DosExit

        mov ax,[fs:10h]
	mov [bytesperscanline],ax	; Store bytes per scan line
        mov word[vesa2_clbit],0

        ; Process Red Stuff
        mov al,[fs:20h]         ; bit sizes = [fs:19h,21h,23h]
        mov cl,al
        mov bx,1
        shl bx,cl
        cmp byte[fs:19h],6
        jne .no6bit
        mov [vesa2_usbit],bx
        inc al
.no6bit
        or [vesa2_clbit],bx
        mov [vesa2_rpos],al
        dec al
        mov cl,al
        mov bx,001Fh
        shl bx,cl
        mov word[vesa2_rfull],bx
        add al,5
        mov bx,1
        mov cl,al
        shl bx,cl
        mov word[vesa2_rtrcl],bx
        xor bx,0FFFFh
        mov word[vesa2_rtrcla],bx

;        mov ax,03h
;        int 10h
;        mov ax,[vesa2_rfull]
;        call printhex
;        jmp DosExit

        ; Process Green Stuff
        mov al,[fs:22h]
        mov cl,al
        mov bx,0001h
        cmp cl,0FFh
        je .shrg
        shl bx,cl
        jmp .shlg
.shrg
        shr bx,1
.shlg
        cmp byte[fs:21h],6
        jne .no6bitb
        mov [vesa2_usbit],bx
        inc al
.no6bitb
        or [vesa2_clbit],bx
        mov [vesa2_gpos],al
        dec al
        mov cl,al
        mov bx,001Fh
        cmp cl,0FFh
        je .shrg2
        shl bx,cl
        jmp .shlg2
.shrg2
        shr bx,1
.shlg2
        mov word[vesa2_gfull],bx
        add al,5
        mov bx,1
        mov cl,al
        shl bx,cl
        mov word[vesa2_gtrcl],bx
        xor bx,0FFFFh
        mov word[vesa2_gtrcla],bx

        ; Process Blue Stuff
        mov al,[fs:24h]
        mov cl,al
        mov bx,1
        shl bx,cl
        cmp byte[fs:23h],6
        jne .no6bitc
        mov [vesa2_usbit],bx
        inc al
.no6bitc
        or [vesa2_clbit],bx
        mov [vesa2_bpos],al
        dec al
        mov cl,al
        mov bx,001Fh
        cmp cl,0FFh
        je .shrb
        shl bx,cl
        jmp .shlb
.shrb
        shr bx,1
.shlb
        mov word[vesa2_bfull],bx
        add al,5
        mov bx,1
        mov cl,al
        shl bx,cl
        mov word[vesa2_btrcl],bx
        xor bx,0FFFFh
        mov word[vesa2_btrcla],bx

        xor word[vesa2_clbit],0FFFFh

        xor ecx,ecx
.loopers
        mov ax,cx
        test [vesa2_rtrcl],cx
        jz .nor
        and ax,[vesa2_rtrcla]
        or ax,[vesa2_rfull]
.nor
        test [vesa2_gtrcl],cx
        jz .nog
        and ax,[vesa2_gtrcla]
        or ax,[vesa2_gfull]
.nog
        test [vesa2_btrcl],cx
        jz .nob
        and ax,[vesa2_btrcla]
        or ax,[vesa2_bfull]
.nob
        shl ax,1
        mov [dcolortab+ecx*2],ax
        dec cx
        jnz .loopers

	test word[fs:0h],10000000b	; Check if linear available
	jnz .linearavailable
            mov edx,.nomsg
            jmp VESA2EXITTODOS               ; None available

		;---------------------------------------------;
		 ; OK - now set the vesa 2 mode based on the ;
		 ; information gleaned...		     ;
		;---------------------------------------------;

	.linearavailable
	or word[vesamode],4000h		; Convert mode to its LFB
					; equivalent
	mov ebx,[fs:28h]		; Read in physical base ptr

	mov cx,bx
	shr ebx,16
	mov si,[noblocks]
	xor edi,edi			; Since noblocks = number of
					; 64k blocks, these lines leave
					; si:di holding byte size
	mov eax,800h
	int 31h
	jnc .mappedphysicalarea
            mov edx,.nomsg
            jmp VESA2EXITTODOS               ; Failure!!!

	.mappedphysicalarea
	shl ebx,16
	mov bx,cx
	mov [LFBpointer],ebx

	xor eax,eax
	xor ebx,ebx
	xor ecx,ecx
	xor edx,edx
	mov ax,4f02h
	mov bx,[vesamode]
	int 10h				; Set the vesa mode
	cmp ax,004fh
	jz .modesetok
            mov edx,.nomsg
            jmp VESA2EXITTODOS               ; Failure!!!

	.modesetok

	xor eax,eax
	mov ecx,1
	int 31h				; Allocate a descriptor

	mov bx,ax			; Move our selector into bx

	mov ecx,[LFBpointer]
	mov dx,cx
	shr ecx,16
	mov eax,7
	int 31h				; Set our selector to LFB
	jnc .selectornowset
                mov edx,.nomsg
                jmp VESA2EXITTODOS           ; Failure!!!

	.selectornowset

	xor ecx,ecx
	mov cx,[noblocks]
	shl ecx,6			; Multiply by 64
	shl ecx,10			; And again by 1024
	sub ecx,1			; Necessary!!!
	mov dx,cx
	shr ecx,16			; CX:DX size of screen
	mov eax,8
	int 31h				; Set size of selector
	jnc .ok
                mov edx,.nomsg
                jmp VESA2EXITTODOS           ; Failure!!!

	.ok
	lar ecx,ebx
	shr ecx,8
	and cl,60h
	or cl,93h
	and ch,0c0h			; Keep granularity bit
	mov ax,9
	int 31h				; Set selector access rights
	jnc .accessrightsset
                mov edx,.nomsg
                jmp VESA2EXITTODOS

	.accessrightsset
        mov [vesa2selec],bx

        ret

        .nomsg db '$'

NEWSYM LFBpointer                                                       ;001049B5
	dd 0
NEWSYM noblocks                                                         ;001049B9
	dw 0
NEWSYM bytesperscanline                                                 ;001049BB
	dw 0
NEWSYM vesamode                                                         ;001049BD
	dw 0
;----------------------------------------------------------------------
NEWSYM VESAmodelist                                                     ;001049BF
	times 512 dw 0
;----------------------------------------------------------------------
NEWSYM RMREGS                                                           ;00104DBF
.edi	dd 0
.esi	dd 0
.ebp	dd 0
.esp	dd 0
.ebx	dd 0
.edx	dd 0
.ecx	dd 0
.eax	dd 0

.flags	dw 0
.es	dw 0
.ds	dw 0 
.fs	dw 0
.gs	dw 0
.ip	dw 0
.cs	dw 0
.sp	dw 0
.ss	dw 0
.spare	times 20 dd 0
;----------------------------------------------------------------------
