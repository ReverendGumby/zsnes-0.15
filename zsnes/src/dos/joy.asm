;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM JoyAC,JoyBC,JoyLC,JoyRC,JoySelec,JoyStart,JoyXC,JoyYC,WhichSW,_SW1
EXTSYM _SW2,_readSideWinder,delay,numjoy,pl1Ak,pl1Bk,pl1Lk,pl1Rk,pl1Xk
EXTSYM pl1Yk,pl1contrl,pl1downk,pl1leftk,pl1rightk,pl1selk,pl1startk
EXTSYM pl1upk,pl2Ak,pl2Bk,pl2Lk,pl2Rk,pl2Xk,pl2Yk,pl2contrl,pl2downk
EXTSYM pl2leftk,pl2rightk,pl2selk,pl2startk,pl2upk,_SWCount,_SW3,_SW4
EXTSYM pl3Ak,pl3Bk,pl3Lk,pl3Rk,pl3Xk
EXTSYM pl3Yk,pl3contrl,pl3downk,pl3leftk,pl3rightk,pl3selk,pl3startk
EXTSYM pl3upk,pl4Ak,pl4Bk,pl4Lk,pl4Rk,pl4Xk,pl4Yk,pl4contrl,pl4downk
EXTSYM pl4leftk,pl4rightk,pl4selk,pl4startk,pl4upk,pressed,read_gpp
EXTSYM pl5contrl
EXTSYM CalibXmin, CalibYmin, CalibXmax, CalibYmax


NEWSYM joy4218, db 0                                                    ;00104E41
NEWSYM joy4219, db 0
NEWSYM joy4218j, db 0
NEWSYM joy4219j, db 0
NEWSYM joy421A, db 0
NEWSYM joy421B, db 0
NEWSYM joy421Aj, db 0
NEWSYM joy421Bj, db 0

NEWSYM JoyCenterX, dd 0                                                 ;00104E49
NEWSYM JoyCenterY, dd 0
NEWSYM JoyMaxX,    dd 0
NEWSYM JoyMaxY,    dd 0
NEWSYM JoyMinX,    dd 0                                                 ;00104E59
NEWSYM JoyMinY,    dd 0
NEWSYM JoyExists,  db 0                                                 ;00104E61
NEWSYM JoyX,       dd 0                                                 ;00104E62
NEWSYM JoyY,       dd 0
NEWSYM JoyCenterX2, dd 0
NEWSYM JoyCenterY2, dd 0
NEWSYM JoyMaxX2,    dd 0
NEWSYM JoyMaxY2,    dd 0
NEWSYM JoyMinX2,    dd 0
NEWSYM JoyMinY2,    dd 0
NEWSYM JoyExists2,  db 0
NEWSYM JoyX2,       dd 0                                                ;00104E83
NEWSYM JoyY2,       dd 0
NEWSYM JoyAltrn,   db 0
NEWSYM JoyAltrn2,  db 2                                                 ;00104E8C

;NEWSYM pl3selk,   dd 0   ; 3SELECT = SHIFT
;NEWSYM pl3startk, dd 0   ; 3START = ENTER
;NEWSYM pl3upk,    dd 0   ; 3UP = up 
;NEWSYM pl3downk,  dd 0   ; 3DOWN = down 
;NEWSYM pl3leftk,  dd 0   ; 3LEFT = left 
;NEWSYM pl3rightk, dd 0   ; 3RIGHT = right 
;NEWSYM pl3Yk,     dd 0   ; 3X = INS
;NEWSYM pl3Xk,     dd 0   ; 3A = HOME
;NEWSYM pl3Lk,     dd 0   ; 3L = PAGE UP
;NEWSYM pl3Bk,     dd 0   ; 3Y = DELETE
;NEWSYM pl3Ak,     dd 0   ; 3B = END
;NEWSYM pl3Rk,     dd 0   ; 3R = PAGE DOWN


NEWSYM GetCoords                                                        ;00104E8D
  mov dword[JoyX],0
  mov dword[JoyY],0
  cli
  mov al,0
  mov dx,0201h
  out dx,al
  mov ecx,00FFFFh
.loopa
  in al,dx
  test al,01H
  jz .YAxis
  test al,02H
  jz .XAxis
  inc dword[JoyX]
  inc dword[JoyY]
  loop .loopa
  mov byte [JoyExists], 0
  mov dword[JoyX],0
  mov dword[JoyY],0
  jmp .End
.YAxis
  in al,dx
  test al,02h
  jz .YAxisOk
  nop
  nop
  inc dword[JoyY]
  nop
  loop .YAxis
  mov byte [JoyExists], 0
  mov dword[JoyX],0
  mov dword[JoyY],0
.YAxisOk
  jmp .End
.XAxis
  in al,dx
  test al,01h
  jz .XAxisOk
  nop
  nop
  inc dword[JoyX]
  nop
  loop .XAxis
  mov byte [JoyExists], 0
  mov dword[JoyX],0
  mov dword[JoyY],0
.XAxisOk
.End
  sti
  ret

NEWSYM GetCoords2                                                       ;00104F42
  mov dword[JoyX2],0
  mov dword[JoyY2],0
  cli
  mov al,0
  mov dx,0201h
  out dx,al
  mov ecx,0FFFFh
.loopa
  in al,dx
  test al,04H
  jz .YAxis
  test al,08H
  jz .XAxis
  inc dword[JoyX2]
  inc dword[JoyY2]
  loop .loopa
  mov byte [JoyExists2], 0
  mov dword[JoyX2],0
  mov dword[JoyY2],0
  jmp .End
.YAxis
  in al,dx
  test al,08h
  jz .YAxisOk
  nop
  nop
  inc dword[JoyY2]
  nop
  loop .YAxis
  mov byte [JoyExists2], 0
  mov dword[JoyX2],0
  mov dword[JoyY2],0
.YAxisOk
  jmp .End
.XAxis
  in al,dx
  test al,04h
  jz .XAxisOk
  nop
  nop
  inc dword[JoyX2]
  nop
  loop .XAxis
  mov byte [JoyExists2], 0
  mov dword[JoyX2],0
  mov dword[JoyY2],0
.XAxisOk
.End
  sti
  ret

NEWSYM UpdateDevices                                                    ;00104FF7
    mov byte[numjoy],0

    cmp byte[pl1contrl],2
    je .notkbd1
    cmp byte[pl1contrl],3
    je .notkbd1
    jmp .yeskbd1
.notkbd1
    inc byte[numjoy]
.yeskbd1

    cmp byte[pl2contrl],2
    je .notkbd2
    cmp byte[pl2contrl],3
    je .notkbd2
    jmp .yeskbd2
.notkbd2
    inc byte[numjoy]
.yeskbd2

    push eax
    push ecx
    push edx

    call GetCoords
    mov ecx,[JoyX]
    mov [JoyCenterX],ecx
    mov ecx,[JoyY]
    mov [JoyCenterY],ecx
    mov eax,[JoyCenterX]
    cmp eax,0
    jnz .havejoy1
    mov eax,[JoyCenterY]
    cmp eax,0
    jnz .havejoy1
    mov byte[JoyExists],0
    jmp .j1done
.havejoy1
    mov byte[JoyExists],1
    mov dword[JoyMinX],0
    mov dword[JoyMinY],0
    mov ecx,[JoyX]
    shl ecx,1
    mov [JoyMaxX],ecx
    mov ecx,[JoyY]
    shl ecx,1
    mov [JoyMaxY],ecx
.j1done

    mov ecx,1000
    call delay

    call GetCoords2
    mov ecx,[JoyX2]
    mov [JoyCenterX2],ecx
    mov ecx,[JoyY2]
    mov [JoyCenterY2],ecx
    mov eax,[JoyCenterX2]
    cmp eax,0
    jnz .havejoy2
    mov eax,[JoyCenterY2]
    cmp eax,0
    jnz .havejoy2
    mov byte[JoyExists2],0
    jmp .j2done
.havejoy2
    mov byte[JoyExists2],1
    mov dword[JoyMinX2],0
    mov dword[JoyMinY2],0
    mov ecx,[JoyX2]
    shl ecx,1
    mov [JoyMaxX2],ecx
    mov ecx,[JoyY2]
    shl ecx,1
    mov [JoyMaxY2],ecx
.j2done

    pop edx
    pop ecx
    pop eax
    ret

NEWSYM JoyRead                                                          ;00105139
   inc byte[JoyAltrn]

   cmp byte[JoyAltrn],4
   jae .readit
   ret
.readit
   mov byte[JoyAltrn],0

   push eax
   mov al,[JoyExists]
   cmp al,1
   je .havej1
   mov byte[joy4218],0
   mov byte[joy4219],0
   mov byte[joy4218j],0
   mov byte[joy4219j],0
   pop eax
   ret

.havej1
   push ebx
   push ecx
   push edx
   xor ebx,ebx
   call GetCoords
   mov ecx,[JoyX]
   mov edx,[JoyCenterX]
   sub edx,[JoyMinX]
   shr edx,1
   add edx,[JoyMinX]
   cmp ecx,edx
   ja .nxp
   or bl,02h
.nxp
   mov edx,[JoyMaxX]
   sub edx,[JoyCenterX]
   shr edx,1
   add edx,[JoyCenterX]
   cmp ecx,edx
   jb .nxn
   or bl,01h
.nxn
   mov ecx,[JoyY]
   mov edx,[JoyCenterY]
   sub edx,[JoyMinY]
   shr edx,1
   add edx,[JoyMinY]
   cmp ecx,edx
   ja .nyp
   or bl,08h
.nyp
   mov edx,[JoyMaxY]
   sub edx,[JoyCenterY]
   shr edx,1
   add edx,[JoyCenterY]
   cmp ecx,edx
   jb .nyn
   or bl,04h
.nyn
   mov [joy4218j],bh
   mov [joy4219j],bl

   mov dx,0201h
   xor eax,eax
   mov al,0
   out dx,al
   in al,dx
   not al
   test al,10h
   jz .not4
   or bl,40h
   or byte[joy4219j],40h
.not4
   test al,40h
   jz .not6
   or bl,80h
.not6
   test al,80h
   jz .not7
   or bh,80h
.not7
   test al,20h
   jz .not5
   or bh,40h
   or byte[joy4219j],80h
.not5   
   mov [joy4218],bh
   mov [joy4219],bl

   pop edx
   pop ecx
   pop ebx
   pop eax
   ret

NEWSYM JoyReadB                                                         ;0010524D
   inc byte[JoyAltrn]

   cmp byte[JoyAltrn],4
   jae .readit
   ret
.readit

   push eax
   mov al,[JoyExists]
   cmp al,1
   je .havej1
   mov byte[joy421A],0
   mov byte[joy421B],0
   mov byte[joy421Aj],0
   mov byte[joy421Bj],0
   pop eax
   ret

.havej1
   push ebx
   push ecx
   push edx
   xor ebx,ebx
   call GetCoords
   mov ecx,[JoyX]
   mov edx,[JoyCenterX]
   sub edx,[JoyMinX]
   shr edx,1
   add edx,[JoyMinX]
   cmp ecx,edx
   ja .nxp
   or bl,02h
.nxp
   mov edx,[JoyMaxX]
   sub edx,[JoyCenterX]
   shr edx,1
   add edx,[JoyCenterX]
   cmp ecx,edx
   jb .nxn
   or bl,01h
.nxn
   mov ecx,[JoyY]
   mov edx,[JoyCenterY]
   sub edx,[JoyMinY]
   shr edx,1
   add edx,[JoyMinY]
   cmp ecx,edx
   ja .nyp
   or bl,08h
.nyp
   mov edx,[JoyMaxY]
   sub edx,[JoyCenterY]
   shr edx,1
   add edx,[JoyCenterY]
   cmp ecx,edx
   jb .nyn
   or bl,04h
.nyn
   mov [joy421Aj],bh
   mov [joy421Bj],bl

   mov dx,0201h
   xor eax,eax
   mov al,0
   out dx,al
   in al,dx
   not al
   test al,10h
   jz .not4
   or bl,40h
   or byte[joy421Aj],40h
.not4
   test al,40h
   jz .not6
   or bl,80h
.not6
   test al,80h
   jz .not7
   or bh,80h
.not7
   test al,20h
   jz .not5
   or bh,40h
   or byte[joy421Bj],80h
.not5   
   mov [joy421A],bh
   mov [joy421B],bl

   pop edx
   pop ecx
   pop ebx
   pop eax
   ret

NEWSYM JoyReadControl                                                   ;001053A
   inc byte[JoyAltrn2]

   cmp byte[JoyAltrn2],4
   jae .readit
   ret
.readit

   push eax
   mov al,[JoyExists2]
   cmp al,1
   je .havej1
   mov byte[joy421Aj],0
   mov byte[joy421Bj],0
   pop eax
   ret

.havej1
   push ebx
   push ecx
   push edx
   xor ebx,ebx
   call GetCoords2
   mov ecx,[JoyX2]
   mov edx,[JoyCenterX2]
   sub edx,[JoyMinX2]
   shr edx,1
   add edx,[JoyMinX2]
   cmp ecx,edx
   ja .nxp
   or bl,02h
.nxp
   mov edx,[JoyMaxX2]
   sub edx,[JoyCenterX2]
   shr edx,1
   add edx,[JoyCenterX2]
   cmp ecx,edx
   jb .nxn
   or bl,01h
.nxn
   mov ecx,[JoyY2]
   mov edx,[JoyCenterY2]
   sub edx,[JoyMinY2]
   shr edx,1
   add edx,[JoyMinY2]
   cmp ecx,edx
   ja .nyp
   or bl,08h
.nyp
   mov edx,[JoyMaxY2]
   sub edx,[JoyCenterY2]
   shr edx,1
   add edx,[JoyCenterY2]
   cmp ecx,edx
   jb .nyn
   or bl,04h
.nyn
   mov [joy421Aj],bh
   mov [joy421Bj],bl

   mov dx,0201h
   xor eax,eax
   mov al,0
   out dx,al
   in al,dx
   not al
   test al,40h
   jz .not6
   or byte[joy421Aj],40h
.not6
   test al,80h
   jz .not7
   or byte[joy421Bj],80h
.not7

   pop edx
   pop ecx
   pop ebx
   pop eax
   ret
