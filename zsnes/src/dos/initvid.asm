;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

%include "macros.mac"

EXTSYM InitVesa2,cbitmode,cvidmode,makepal
EXTSYM selcA000,vesa2_bits
EXTSYM vesa2_x,vesa2_y

;*******************************************************
; InitVideo
;*******************************************************
NEWSYM initvideo                                                        ;0005165D
    mov byte[cbitmode],0
    cmp byte[cvidmode],0
    je near .initmodex
    cmp byte[cvidmode],1
    je near .initmodeq
    cmp byte[cvidmode],2
    je near .initvesa2320x240x8
    cmp byte[cvidmode],3
    je near .initvesa2320x240x16
    ret

;*******************************************************
; InitModeX               Sets up 320x240 unchained mode
;*******************************************************

.initmodex
    mov ax,0013h
    int 10h

    mov dx,03C4h
    mov ax,0604h
    out dx,ax
    mov dx,03D4h
    mov ax,0E317h 
    out dx,ax
    mov ax,0014h 
    out dx,ax
    mov dx,03C4h
    mov ax,0F02h
    out dx,ax

    mov dx,03C2h
    mov al,0E3h
    out dx,al
    mov dx,03D4h
    mov ax,2C11h
    out dx,ax
    mov ax,0D06h
    out dx,ax
    mov ax,3E07h
    out dx,ax
    mov ax,0EA10h
    out dx,ax
    mov ax,0AC11h
    out dx,ax
    mov ax,0DF12h
    out dx,ax
    mov ax,0E715h
    out dx,ax
    mov ax,0616h
    out dx,ax

    mov dx,03C6h
    mov al,0FFh
    out dx,al
    mov dx,03C4h
    ; select all 4 planes and clear
    mov ax,0F02h
    out dx,ax
    push es
    mov ax,[selcA000]
    mov es,eax
    xor edi,edi
    mov ecx,65536/4
    xor eax,eax
    rep stosd
    pop es
    call makepal
    ret

;*******************************************************
; InitModeQ                 Sets up 256x256 chained mode
;*******************************************************

.initmodeq
    mov ax,0013h
    int 10h
    mov dx,03D4h
    mov al,11h
    out dx,al

    inc dx
    in al,dx
    and al,7Fh
    mov ah,al
    dec dx
    mov al,11h
    out dx,al
    inc dx
    mov al,ah
    out dx,al

    mov dx,03C2h
    mov al,0E3h
    out dx,al
    mov dx,03D4h
    mov ax,5F00h
    out dx,ax
    mov ax,3F01h
    out dx,ax
    mov ax,4002h
    out dx,ax
    mov ax,8203h
    out dx,ax
    mov ax,4A04h
    out dx,ax
    mov ax,9A05h
    out dx,ax
    mov ax,2306h
    out dx,ax
    mov ax,0B207h
    out dx,ax
    mov ax,0008h
    out dx,ax
    mov ax,6109h
    out dx,ax
    mov ax,0A10h
    out dx,ax
    mov ax,0AC11h
    out dx,ax
    mov ax,0FF12h
    out dx,ax
    mov ax,2013h
    out dx,ax
    mov ax,4014h
    out dx,ax
    mov ax,0715h
    out dx,ax
    mov ax,1A16h
    out dx,ax
    mov ax,0A317h
    out dx,ax
    mov dx,03C4h
    mov ax,0101h
    out dx,ax
    mov ax,0E04h
    out dx,ax
    mov dx,03CEh
    mov ax,4005h
    out dx,ax
    mov ax,0506h
    out dx,ax

    mov dx,03DAh
    in al,dx
    mov dx,03C0h
    mov al,30h
    out dx,al
    mov al,41h
    out dx,al

    mov dx,03DAh
    in al,dx
    mov dx,03C0h
    mov al,33h
    out dx,al
    mov al,0h
    out dx,al

    mov dx,03C6h
    mov al,0FFh
    out dx,al
    call makepal
    ret

;*******************************************************
; InitVESA2 320x240x8           Set up Linear 320x240x8b
;*******************************************************

.initvesa2320x240x8
    mov word[vesa2_x],320
    mov word[vesa2_y],240
    mov byte[vesa2_bits],8
    call InitVesa2
    call makepal
    ret

;*******************************************************
; InitVESA2 320x240x16         Set up Linear 320x240x16b
;*******************************************************

.initvesa2320x240x16
    mov byte[cbitmode],1
    mov word[vesa2_x],320
    mov word[vesa2_y],240
    mov byte[vesa2_bits],16
    call InitVesa2
    ret
